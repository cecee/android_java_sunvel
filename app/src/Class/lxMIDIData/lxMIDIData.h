/******************************************************************************/
/*                                                                            */
/*?MIDIData.h - MIDIData긶긞??긲?귽깑                  (C)2002-2013 궘궦  */
/*                                                                            */
/******************************************************************************/
/* This library is free software; you can redistribute it and/or */
/* modify it under the terms of the GNU Lesser General Public */
/* License as published by the Free Software Foundation; either */
/* version 2.1 of the License, or (at your option) any later version. */

/* This library is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU */
/* Lesser General Public License for more details. */

/* You should have received a copy of the GNU Lesser General Public */
/* License along with this library; if not, write to the Free Software */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

#ifndef _LXMIDIDATA_H_
#define _LXMIDIDATA_H_

#include <wchar.h>
//#include "SDL.h"


/* C++궔귞귖럊뾭됀?궴궥귡 */
#ifdef __cplusplus
extern "C" {
#endif

//typedef int BOOL;
//typedef unsigned long DWORD;

/* __stdcall궻믦? */
/*
#ifndef
#if defined(WINDOWS) || defined(_WINDOWS) || defined(__WINDOWS) || \
	defined(WIN32) || defined(_WIN32) || defined(__WIN32) || \
	defined(WIN64) || defined(_WIN64) || defined(__WIN64)
#define
#else
#define
#endif
#endif
*/

/* MIDIEvent?몾뫬 */
/* 몂뺴뛀?긳깑깏깛긏깏긚긣?몾 */
/* 긩?긤룈룜궼먥뫮렄뜌궳뙂믦궠귢귏궥 */
typedef struct tagMIDIEvent {
	long m_lTempIndex;                  /* 궞궻귽긹깛긣궻덇렄밒궶귽깛긢긞긏긚(0궔귞럑귏귡) */
	long m_lTime;                       /* 먥뫮렄뜌[Tick]뼌궼SMPTE긖긳긲깒??뭁댧 */
	long m_lKind;                       /* 귽긹깛긣궻롰쀞(0x00?0xFF) */
	long m_lLen;                        /* 귽긹깛긣궻긢??뮮궠[긫귽긣] */
	unsigned char* m_pData;             /* 귽긹깛긣궻긢??긫긞긲?귉궻?귽깛? */
	unsigned long m_lData;              /* 귽긹깛긣궻긢??긫긞긲?(MIDI?긿깛긨깑귽긹깛긣궻궴궖럊궎) */
	struct tagMIDIEvent* m_pNextEvent;  /* 렅궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pPrevEvent;  /* 멟궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pNextSameKindEvent; /* 렅궻벏궣롰쀞궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pPrevSameKindEvent; /* 멟궻벏궣롰쀞궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pNextCombinedEvent; /* 렅궻뙅뜃귽긹깛긣뺎렃뾭?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pPrevCombinedEvent; /* 멟궻뙅뜃귽긹깛긣뺎렃뾭?귽깛?(궶궚귢궽NULL) */
	void* m_pParent;                    /* 릂(MIDITrack긆긳긙긃긏긣)귉궻?귽깛?(궶궚귢궽NULL) */
	long m_lUser1;                      /* 깇?긗?뾭렔뾕쀌덃1(뼟럊뾭) */
	long m_lUser2;                      /* 깇?긗?뾭렔뾕쀌덃2(뼟럊뾭) */
	long m_lUser3;                      /* 깇?긗?뾭렔뾕쀌덃3(뼟럊뾭) */
	long m_lUserFlag;                   /* 깇?긗?뾭렔뾕쀌덃4(뼟럊뾭) */
} MIDIEvent;

/* MIDITrack?몾뫬 */
/* 몂뺴뛀깏깛긏깏긚긣?몾 */
typedef struct tagMIDITrack {
	long m_lTempIndex;                  /* 궞궻긣깋긞긏궻덇렄밒궶귽깛긢긞긏긚(0궔귞럑귏귡) */
	long m_lNumEvent;                   /* 긣깋긞긏볙궻귽긹깛긣릶 */
	struct tagMIDIEvent* m_pFirstEvent; /* 띍룊궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDIEvent* m_pLastEvent;  /* 띍뚣궻귽긹깛긣귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDITrack* m_pPrevTrack;  /* 멟궻긣깋긞긏귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDITrack* m_pNextTrack;  /* 렅궻긣깋긞긏귉궻?귽깛?(궶궚귢궽NULL) */
	void*  m_pParent;                   /* 릂(MIDIData긆긳긙긃긏긣)귉궻?귽깛? */
	long m_lInputOn;                    /* 볺쀍(0=OFF, 1=On) */
	long m_lInputPort;                  /* 볺쀍??긣(-1=n/a, 0?15=??긣붥뜂) */
	long m_lInputChannel;               /* 볺쀍?긿깛긨깑(-1=n/1, 0?15=?긿깛긨깑붥뜂) */
	long m_lOutputOn;                   /* 뢯쀍(0=OFF, 1=On) */
	long m_lOutputPort;                 /* 뢯쀍??긣(-1=n/a, 0?15=??긣붥뜂) */
	long m_lOutputChannel;              /* 뢯쀍?긿깛긨깑(-1=n/1, 0?15=?긿깛긨깑붥뜂) */
	long m_lTimePlus;                   /* ?귽?+ */
	long m_lKeyPlus;                    /* 긌?+ */
	long m_lVelocityPlus;               /* 긹깓긘긡귻+ */
	long m_lViewMode;                   /* ?렑긾?긤(0=믅륂갂1=긤깋?) */
	long m_lForeColor;                  /* 멟똧륡 */
	long m_lBackColor;                  /* 봶똧륡 */
	long m_lReserved1;                  /* ?뽵쀌덃1(럊뾭뗕?) */
	long m_lReserved2;                  /* ?뽵쀌덃2(럊뾭뗕?) */
	long m_lReserved3;                  /* ?뽵쀌덃3(럊뾭뗕?) */
	long m_lReserved4;                  /* ?뽵쀌덃4(럊뾭뗕?) */
	long m_lUser1;                      /* 깇?긗?뾭렔뾕쀌덃1(뼟럊뾭) */
	long m_lUser2;                      /* 깇?긗?뾭렔뾕쀌덃2(뼟럊뾭) */
	long m_lUser3;                      /* 깇?긗?뾭렔뾕쀌덃3(뼟럊뾭) */
	long m_lUserFlag;                   /* 깇?긗?뾭렔뾕쀌덃4(뼟럊뾭) */
} MIDITrack;

/* MIDIData?몾뫬 */
/* 몂뺴뛀깏깛긏깏긚긣?몾 */
typedef struct tagMIDIData {
	unsigned long m_lFormat;            /* SMF긲긅??긞긣(0/1) */
	unsigned long m_lNumTrack;          /* 긣깋긞긏릶(0?걞) */
	unsigned long m_lTimeBase;          /* ?귽?긹?긚(쀡갌120) */
	struct tagMIDITrack* m_pFirstTrack; /* 띍룊궻긣깋긞긏귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDITrack* m_pLastTrack;  /* 띍뚣궻긣깋긞긏귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDITrack* m_pNextSeq;    /* 렅궻긘?긑깛긚귉궻?귽깛?(궶궚귢궽NULL) */
	struct tagMIDITrack* m_pPrevSeq;    /* 멟궻긘?긑깛긚귉궻?귽깛?(궶궚귢궽NULL) */
	void*  m_pParent;                   /* 릂(륂궸NULL갃룶뿀?깛긐깏긚긣귩긖??긣) */
	long m_lReserved1;                  /* ?뽵쀌덃1(럊뾭뗕?) */
	long m_lReserved2;                  /* ?뽵쀌덃2(럊뾭뗕?) */
	long m_lReserved3;                  /* ?뽵쀌덃3(럊뾭뗕?) */
	long m_lReserved4;                  /* ?뽵쀌덃4(럊뾭뗕?) */
	long m_lUser1;                      /* 깇?긗?뾭렔뾕쀌덃1(뼟럊뾭) */
	long m_lUser2;                      /* 깇?긗?뾭렔뾕쀌덃2(뼟럊뾭) */
	long m_lUser3;                      /* 깇?긗?뾭렔뾕쀌덃3(뼟럊뾭) */
	long m_lUserFlag;                   /* 깇?긗?뾭렔뾕쀌덃4(뼟럊뾭) */
} MIDIData;

/*MIDIClock구조체*/
typedef struct tagMIDIClock{
	long m_lTimeMode;/*시간 모드 */
	long m_lResolution;/*분해능[매틱/4분 음표]또는[매틱/1프레임]*/
	long m_lTempo;/*속도[μ초/4분 음표]*/
	long m_lDummyTempo;/*더미 속도[μ초/4분 음표](슬래이브 모드시의 임시 보관용)*/
	long m_lSpeed;/*스피드(10000이 표준, 0=정지, 20000이 두배의 속도)*/
	long m_lMIDIInSyncMode;/*MIDI입력 동기 모드(0=마스터, 1=MIDI타이밍 클락, 2=SMPTE/MTC)*/
	long m_lPeriod;/*콜백 함수 호출 간격(밀리초]*/
	long m_lMillisec;/*시간[밀리 초]*/
	long m_lMillisecMod;/*시간 오차 보정용*/
	long m_lOldMillisec;/* 지난 시각[밀리 초]보유용*/
	long m_lDummyMillisec;/*더미 시간[밀리 초](슬래이브 모드시의 임시 보관용)*/
	long m_lDummyMillisecMod;/*더미 시간 오차 보정용(슬래이브 모드시의 임시 보관용)*/
	long m_lOldDummyMillisec;/* 지난 더미 시간[밀리 초](슬래이브 모드시의 임시 보관용)*/
	long m_lTickCount;/*매틱 카운트[Tick]*/
	long m_lTickCountMod;/*매틱 카운트 오차 보정용*/
	long m_lOldTickCount;/*지난번 매틱 카운트[Tick]보유용*/
	long m_lTimerID;//타이머 ID
//	SDL_TimerID m_lTimerID;//SDL Timer 타이머 ID
	unsigned char m_bySMPTE[8];/*MIDI타임 코드 쿼터 프레임 보유용*/
	volatile long m_lRunning;/*1때 동작 중 0때 정지 중*/
	volatile long m_lLocked;/*1때 멤버 변수의 조작 금지, 0때 허가*/
}MIDIClock;


/* 궩궻뫜궻?긏깓 */
#define MIDIEVENT_MAXLEN       65536

/* 긲긅??긞긣궸듫궥귡?긏깓 */
#define MIDIDATA_FORMAT0       0x00 /* 긲긅??긞긣0 */
#define MIDIDATA_FORMAT1       0x01 /* 긲긅??긞긣1 */
#define MIDIDATA_FORMAT2       0x02 /* 긲긅??긞긣2 */

/* 긡깛?궸듫궥귡?긏깓 */
/* 뭾댰갌긡깛?궻뭁댧궼궥귊궲[?귽긏깓뷳/tick]궴궥귡갃 */
#define MIDIEVENT_MAXTEMPO     (60000000)
#define MIDIEVENT_MINTEMPO     (1)
#define MIDIEVENT_DEFTEMPO     (60000000/120)

/* 긣깋긞긏릶궸듫궥귡?긏깓 */
#define MIDIDATA_MAXMIDITRACKNUM 65535

/* ?귽?긾?긤궸듫궥귡?긏깓 */
#define MIDIDATA_TPQNBASE      0  /* TPQN긹?긚 */
#define MIDIDATA_SMPTE24BASE   24 /* 24긲깒??/뷳 */
#define MIDIDATA_SMPTE25BASE   25 /* 25긲깒??/뷳 */
#define MIDIDATA_SMPTE29BASE   29 /* 29.97긲깒??/뷳 */
#define MIDIDATA_SMPTE30BASE   30 /* 30긲깒??/뷳 */

/* ?귽?깒?깏깄?긘깈깛(빁됶?)궸듫궥귡?긏깓 */
/* TPQN긹?긚궻뤾뜃갌4빁돶븘궇궫귟궻빁됶? */
/* 걳뭾댰갌븕믅TPQN궻빁됶?궼갂24,48,72,96,120,144,168,192,216,240,360,384,480,960궳궇귡 */
#define MIDIDATA_MINTPQNRESOLUTION   1   /* TPQN깒?깏깄?긘깈깛띍룷뭠=1 */
#define MIDIDATA_MAXTPQNRESOLUTION   32767 /* TPQN깒?깏깄?긘깈깛띍묈뭠=32767 */
#define MIDIDATA_DEFTPQNRESOLUTION   120 /* TPQN깒?깏깄?긘깈깛뷭?뭠=120 */

/* SMPTE긹?긚궻뤾뜃갌1긲깒??궇궫귟궻빁됶? */
/* 걳뭾댰갌븕믅SMPTE궻빁됶?궼갂10,40,80궶궵궕묆?밒궳궇귡 */
#define MIDIDATA_MINSMPTERESOLUTION  1   /* SMPTE깒?깏깄?긘깈깛띍룷뭠=1 */
#define MIDIDATA_MAXSMPTERESOLUTION  255 /* SMPTE깒?깏깄?긘깈깛띍묈뭠=255 */
#define MIDIDATA_DEFSMPTERESOLUTION  10  /* SMPTE깒?깏깄?긘깈깛뷭?뭠=10 */

/* 띍묈??긣릶 */
#define MIDIDATA_MAXNUMPORT         256

/* SMPTE긆긲긜긞긣궸듫궥귡?긏깓 */
#define MIDIEVENT_SMPTE24           0x00 /* 24긲깒???뷳 */
#define MIDIEVENT_SMPTE25           0x01 /* 25긲깒???뷳 */
#define MIDIEVENT_SMPTE30D          0x02 /* 30긲깒???뷳(긤깓긞긵) */
#define MIDIEVENT_SMPTE30N          0x03 /* 30긲깒???뷳(긩깛긤깓긞긵) */

/* 뮧맜궸듫궥귡?긏깓 */
#define MIDIEVENT_MAJOR             0x00 /* 뮮뮧 */
#define MIDIEVENT_MINOR             0x01 /* 뭒뮧 */

/* MIDIEVENT_KIND?긏깓 (긓긽깛긣궻긇긞긓볙궼긢??븫궻뮮궠귩렑궥) */
#define MIDIEVENT_SEQUENCENUMBER    0x00 /* 긘?긑깛긚긥깛긫?(2긫귽긣) */
#define MIDIEVENT_TEXTEVENT         0x01 /* 긡긌긚긣(됀빾뮮빒럻쀱) */
#define MIDIEVENT_COPYRIGHTNOTICE   0x02 /* 뮊띿뙛(됀빾뮮빒럻쀱) */
#define MIDIEVENT_TRACKNAME         0x03 /* 긣깋긞긏뼹갋긘?긑깛긖뼹(됀빾뮮빒럻쀱) */
#define MIDIEVENT_INSTRUMENTNAME    0x04 /* 귽깛긚긣귾깑긽깛긣(됀빾뮮빒럻쀱) */
#define MIDIEVENT_LYRIC             0x05 /* 됊럩(됀빾뮮빒럻쀱) */
#define MIDIEVENT_MARKER            0x06 /* ??긇?(됀빾뮮빒럻쀱) */
#define MIDIEVENT_CUEPOINT          0x07 /* 긌깄??귽깛긣(됀빾뮮빒럻쀱) */
#define MIDIEVENT_PROGRAMNAME       0x08 /* 긵깓긐깋?뼹(됀빾뮮빒럻쀱) */
#define MIDIEVENT_DEVICENAME        0x09 /* 긢긫귽긚뼹(됀빾뮮빒럻쀱) */
#define MIDIEVENT_CHANNELPREFIX     0x20 /* ?긿깛긨깑긵깒긲귻긞긏긚(1긫귽긣) */
#define MIDIEVENT_PORTPREFIX        0x21 /* ??긣긵깒긲귻긞긏긚(1긫귽긣) */
#define MIDIEVENT_ENDOFTRACK        0x2F /* 긄깛긤긆긳긣깋긞긏(0긫귽긣) */
#define MIDIEVENT_TEMPO             0x51 /* 긡깛?(3긫귽긣) */
#define MIDIEVENT_SMPTEOFFSET       0x54 /* SMPTE긆긲긜긞긣(5긫귽긣) */
#define MIDIEVENT_TIMESIGNATURE     0x58 /* 뵋럔딯뜂(4긫귽긣) */
#define MIDIEVENT_KEYSIGNATURE      0x59 /* 뮧맜딯뜂(2긫귽긣) */
#define MIDIEVENT_SEQUENCERSPECIFIC 0x7F /* 긘?긑깛긖?벲렔궻귽긹깛긣(됀빾뮮긫귽긥깏) */
#define MIDIEVENT_NOTEOFF           0x80 /* 긩?긣긆긲(3긫귽긣) */
#define MIDIEVENT_NOTEON            0x90 /* 긩?긣긆깛(3긫귽긣) */
#define MIDIEVENT_KEYAFTERTOUCH     0xA0 /* 긌?귺긲??(3긫귽긣) */
#define MIDIEVENT_CONTROLCHANGE     0xB0 /* 긓깛긣깓?깋?(3긫귽긣) */
#define MIDIEVENT_PROGRAMCHANGE     0xC0 /* 긵깓긐깋??긃깛긙(2긫귽긣) */
#define MIDIEVENT_CHANNELAFTERTOUCH 0xD0 /* ?긿깛긨깑귺긲??(2긫귽긣) */
#define MIDIEVENT_PITCHBEND         0xE0 /* 긯긞?긹깛긤(3긫귽긣) */
#define MIDIEVENT_SYSEXSTART        0xF0 /* 긘긚긡?긄긏긚긏깑?긘깞(됀빾뮮긫귽긥깏) */
#define MIDIEVENT_SYSEXCONTINUE     0xF7 /* 긘긚긡?긄긏긚긏깑?긘깞궻뫏궖(됀빾뮮긫귽긥깏) */

/* MIDIEVENT_KIND?긏깓 (댥돷궻4궰궼MIDIData_SetKind궻덙릶궸럊귦귢귡) */
#define MIDIEVENT_NOTEONNOTEOFF     0x180 /* 긩?긣(0x9n+0x8n) */
#define MIDIEVENT_NOTEONNOTEON0     0x190 /* 긩?긣(0x9n+0x9n(vel==0)) */
#define MIDIEVENT_PATCHCHANGE       0x1C0 /* 긬긞??긃깛긙(CC#32+CC#0+긵깓긐깋??긃깛긙) */
#define MIDIEVENT_RPNCHANGE         0x1A0 /* RPN?긃깛긙(CC#101+CC#100+CC#6) */
#define MIDIEVENT_NRPNCHANGE        0x1B0 /* NRPN?긃깛긙(CC#99+CC#98+CC#6) */

/* MIDIEVENT_DUMP?긏깓 */
#define MIDIEVENT_DUMPALL           0x0000FFFF
#define MIDIEVENT_DUMPTIME          0x00000001
#define MIDIEVENT_DUMPKIND          0x00000010
#define MIDIEVENT_DUMPLEN           0x00000020
#define MIDIEVENT_DUMPDATA          0x00000040
#define MIDIEVENT_DUMPUSER1         0x00000100
#define MIDIEVENT_DUMPUSER2         0x00000200
#define MIDIEVENT_DUMPUSERFLAG      0x00000400

/* forEachTrack?긏깓 */
#define forEachTrack(pMIDIData, pTrack) \
for((pTrack)=(pMIDIData)->m_pFirstTrack;(pTrack);(pTrack)=(pTrack)->m_pNextTrack)

/* forEachTrackInverse?긏깓 */
#define forEachTrackInverse(pMIDIData, pTrack) \
for((pTrack)=(pMIDIData)->m_pLastTrack;(pTrack);(pTrack)=(pTrack)->m_pPrevTrack)


/* forEachEvent?긏깓 */
#define forEachEvent(pTrack,pEvent) \
for((pEvent)=(pTrack)->m_pFirstEvent;(pEvent);(pEvent)=(pEvent)->m_pNextEvent)

/* forEachEventInverse?긏깓 */
#define forEachEventInverse(pTrack,pEvent) \
for((pEvent)=(pTrack)->m_pLastEvent;(pEvent);(pEvent)=(pEvent)->m_pPrevEvent)

/******************************************************************************/
/*                                                                            */
/*?MIDIDataLib긏깋긚듫릶                                                     */
/*                                                                            */
/******************************************************************************/

/* 깓긑?깑궻먠믦 */
char*  MIDIDataLib_SetLocaleA (int nCategory, const char *pszLocale);
wchar_t*  MIDIDataLib_SetLocaleW (int nCategory, const wchar_t* pszLocale);
#ifdef UNICODE_X
#define MIDIDataLib_SetLocale MIDIDataLib_SetLocaleW
#else
#define MIDIDataLib_SetLocale MIDIDataLib_SetLocaleA
#endif

/******************************************************************************/
/*                                                                            */
/*?MIDIEvent긏깋긚듫릶                                                       */
/*                                                                            */
/******************************************************************************/

/* 뭾갌__stdcall궼Windows먭뾭궳궥갃Linux궻뤾뜃궼__stdcall귩뤑궢궲궘궬궠궋 */

/* 뙅뜃귽긹깛긣궻띍룊궻귽긹깛긣귩뺅궥갃 */
/* 뙅뜃귽긹깛긣궳궶궋뤾뜃갂pEvent렔릆귩뺅궥갃*/
MIDIEvent*  MIDIEvent_GetFirstCombinedEvent (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궻띍뚣궻귽긹깛긣귩뺅궥갃 */
/* 뙅뜃귽긹깛긣궳궶궋뤾뜃갂pEvent렔릆귩뺅궥갃*/
MIDIEvent*  MIDIEvent_GetLastCombinedEvent (MIDIEvent* pEvent);

/* 뭁뫬귽긹깛긣귩뙅뜃궥귡 */
long  MIDIEvent_Combine (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣귩먛귟뿣궥 */
long  MIDIEvent_Chop (MIDIEvent* pEvent);

/* MIDI귽긹깛긣궻랁룣(뙅뜃궢궲궋귡뤾뜃궳귖뭁덇궻MIDI귽긹깛긣귩랁룣) */
long  MIDIEvent_DeleteSingle (MIDIEvent* pMIDIEvent);

/* MIDI귽긹깛긣궻랁룣(뙅뜃궢궲궋귡뤾뜃갂뙅뜃궢궲궋귡MIDI귽긹깛긣귖랁룣) */
long  MIDIEvent_Delete (MIDIEvent* pMIDIEvent);

/* MIDI귽긹깛긣(봀댰)귩맯맟궢갂MIDI귽긹깛긣귉궻?귽깛?귩뺅궥(렪봲렄NULL갂댥돷벏뾩) */
MIDIEvent*  MIDIEvent_Create
(long lTime, long lKind, unsigned char* pData, long lLen);

/* 럚믦귽긹깛긣궴벏궣MIDI귽긹깛긣귩맯맟궢갂MIDI귽긹깛긣귉궻?귽깛?귩뺅궥(렪봲렄NULL갂댥돷벏뾩) */
MIDIEvent*  MIDIEvent_CreateClone (MIDIEvent* pMIDIEvent);

/* 긘?긑깛긚붥뜂귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateSequenceNumber
(long lTime, long lNum);

/* 긡긌긚긣귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateTextEventA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateTextEventW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateTextEvent MIDIEvent_CreateTextEventW
#else
#define MIDIEvent_CreateTextEvent MIDIEvent_CreateTextEventA
#endif

/* 뮊띿뙛귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateCopyrightNoticeA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateCopyrightNoticeW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateCopyrightNotice MIDIEvent_CreateCopyrightNoticeW
#else
#define MIDIEvent_CreateCopyrightNotice MIDIEvent_CreateCopyrightNoticeA
#endif

/* 긣깋긞긏뼹귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateTrackNameA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateTrackNameW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateTrackName MIDIEvent_CreateTrackNameW
#else
#define MIDIEvent_CreateTrackName MIDIEvent_CreateTrackNameA
#endif

/* 귽깛긚긣귾깑긽깛긣뼹귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateInstrumentNameA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateInstrumentNameW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateInstrumentName MIDIEvent_CreateInstrumentNameW
#else
#define MIDIEvent_CreateInstrumentName MIDIEvent_CreateInstrumentNameA
#endif

/* 됊럩귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateLyricA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateLyricW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateLyric MIDIEvent_CreateLyricW
#else
#define MIDIEvent_CreateLyric MIDIEvent_CreateLyricA
#endif

/* ??긇?귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateMarkerA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateMarkerW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateMarker MIDIEvent_CreateMarkerW
#else
#define MIDIEvent_CreateMarker MIDIEvent_CreateMarkerA
#endif

/* 긌깄??귽깛긣귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateCuePointA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateCuePointW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateCuePoint MIDIEvent_CreateCuePointW
#else
#define MIDIEvent_CreateCuePoint MIDIEvent_CreateCuePointA
#endif

/* 긵깓긐깋?뼹귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateProgramNameA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateProgramNameW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateProgramName MIDIEvent_CreateProgramNameW
#else
#define MIDIEvent_CreateProgramName MIDIEvent_CreateProgramNameA
#endif

/* 긢긫귽긚뼹귽긹깛긣맯맟 */
MIDIEvent*  MIDIEvent_CreateDeviceNameA
(long lTime, const char* pszText);
MIDIEvent*  MIDIEvent_CreateDeviceNameW
(long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_CreateDeviceName MIDIEvent_CreateDeviceNameW
#else
#define MIDIEvent_CreateDeviceName MIDIEvent_CreateDeviceNameA
#endif

/* ?긿깛긨깑긵깒긲귻긞긏긚귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateChannelPrefix (long lTime, long lCh);

/* ??긣긵깒긲귻긞긏긚귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreatePortPrefix (long lTime, long lNum);

/* 긄깛긤긆긳긣깋긞긏귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateEndofTrack (long lTime);

/* 긡깛?귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateTempo (long lTime, long lTempo);

/* SMPTE긆긲긜긞긣귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateSMPTEOffset
(long lTime, long lMode, long lHour, long lMin, long lSec, long lFrame, long lSubFrame);

/* 뵋럔딯뜂귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateTimeSignature
(long lTime, long lnn, long ldd, long lcc, long lbb);

/* 뮧맜딯뜂귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateKeySignature
(long lTime, long lsf, long lmi);

/* 긘?긑깛긖?벲렔궻귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateSequencerSpecific
(long lTime, char* pData, long lLen);

/* 긩?긣긆긲귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateNoteOff
(long lTime, long lCh, long lKey, long lVel);

/* 긩?긣긆깛귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateNoteOn
(long lTime, long lCh, long lKey, long lVel);

/* 긩?긣귽긹깛긣궻맯맟(MIDIEvent_CreateNoteOnNoteOn0궴벏궣) */
/* (긩?긣긆깛갋긩?긣긆깛(0x9n(vel==0))궻2귽긹깛긣귩맯맟궢갂*/
/* 긩?긣긆깛귽긹깛긣귉궻?귽깛?귩뺅궥갃) */
MIDIEvent*  MIDIEvent_CreateNote
(long lTime, long lCh, long lKey, long lVel, long lDur);

/* 긩?긣귽긹깛긣궻맯맟(0x8n뿣뙫?) */
/* (긩?긣긆깛(0x9n)갋긩?긣긆긲(0x8n)궻2귽긹깛긣귩맯맟궢갂*/
/* NoteOn귉궻?귽깛?귩뺅궥) */
MIDIEvent*  MIDIEvent_CreateNoteOnNoteOff
(long lTime, long lCh, long lKey, long lVel1, long lVel2, long lDur);

/* 긩?긣귽긹깛긣궻맯맟(0x9n뿣뙫?) */
/* (긩?긣긆깛(0x9n)갋긩?긣긆깛(0x9n(vel==0))궻2귽긹깛긣귩맯맟궢갂*/
/* NoteOn귉궻?귽깛?귩뺅궥) */
MIDIEvent*  MIDIEvent_CreateNoteOnNoteOn0
(long lTime, long lCh, long lKey, long lVel, long lDur);

/* 긌?귺긲???긞?귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateKeyAftertouch
(long lTime, long lCh, long lKey, long lVal);

/* 긓깛긣깓?깋?귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateControlChange
(long lTime, long lCh, long lNum, long lVal);

/* RPN귽긹깛긣궻맯맟 */
/* (CC#101+CC#100+CC#6궻3귽긹깛긣귩맯맟궢갂CC#101귉궻?귽깛?귩뺅궥) */
MIDIEvent*  MIDIEvent_CreateRPNChange
(long lTime, long lCh, long lCC101, long lCC100, long lVal);

/* NRPN귽긹깛긣궻맯맟 */
/* (CC#99+CC#98+CC#6궻3귽긹깛긣귩맯맟궢갂CC#99귉궻?귽깛?귩뺅궥) */
MIDIEvent*  MIDIEvent_CreateNRPNChange
(long lTime, long lCh, long lCC99, long lCC98, long lVal);

/* 긵깓긐깋??긃깛긙귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateProgramChange
(long lTime, long lCh, long lNum);

/* 긫깛긏갋긬긞?귽긹깛긣궻맯맟 */
/* (CC#0+CC#32+PC궻3귽긹깛긣귩맯맟궢갂CC#0귉궻?귽깛?귩뺅궥) */
MIDIEvent*  MIDIEvent_CreatePatchChange
(long lTime, long lCh, long lCC0, long lCC32, long lNum);

/* ?긿깛긨깑귺긲???긞?귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateChannelAftertouch
(long lTime, long lCh, long lVal);

/* 긯긞?긹깛긤귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreatePitchBend
(long lTime, long lCh, long lVal);

/* 긘긚긡?긄긏긚긏깑?긘깞귽긹깛긣궻맯맟 */
MIDIEvent*  MIDIEvent_CreateSysExEvent
(long lTime, unsigned char* pBuf, long lLen);




/* 긽?귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsMetaEvent (MIDIEvent* pMIDIEvent);

/* 긘?긑깛긚붥뜂궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsSequenceNumber (MIDIEvent* pMIDIEvent);

/* 긡긌긚긣귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsTextEvent (MIDIEvent* pMIDIEvent);

/* 뮊띿뙛귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsCopyrightNotice (MIDIEvent* pMIDIEvent);

/* 긣깋긞긏뼹귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsTrackName (MIDIEvent* pMIDIEvent);

/* 귽깛긚긣귾깑긽깛긣뼹귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsInstrumentName (MIDIEvent* pMIDIEvent);

/* 됊럩귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsLyric (MIDIEvent* pMIDIEvent);

/* ??긇?귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsMarker (MIDIEvent* pMIDIEvent);

/* 긌깄??귽깛긣귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsCuePoint (MIDIEvent* pMIDIEvent);

/* 긵깓긐깋?뼹귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsProgramName (MIDIEvent* pEvent);

/* 긢긫귽긚뼹귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsDeviceName (MIDIEvent* pEvent);

/* ?긿깛긨깑긵깒긲귻긞긏긚귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsChannelPrefix (MIDIEvent* pEvent);

/* ??긣긵깒긲귻긞긏긚귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsPortPrefix (MIDIEvent* pEvent);

/* 긄깛긤긆긳긣깋긞긏귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsEndofTrack (MIDIEvent* pMIDIEvent);

/* 긡깛?귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsTempo (MIDIEvent* pMIDIEvent);

/* SMPTE긆긲긜긞긣귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsSMPTEOffset (MIDIEvent* pMIDIEvent);

/* 뵋럔딯뜂귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsTimeSignature (MIDIEvent* pMIDIEvent);

/* 뮧맜딯뜂귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsKeySignature (MIDIEvent* pMIDIEvent);

/* 긘?긑깛긖벲렔궻귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsSequencerSpecific (MIDIEvent* pMIDIEvent);


/* MIDI귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsMIDIEvent (MIDIEvent* pMIDIEvent);

/* 긩?긣긆깛귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
/* (긩?긣긆깛귽긹깛긣궳긹깓긘긡귻0궻귖궻궼긩?긣긆긲귽긹깛긣궴귒궶궥갃댥돷벏뾩) */
long  MIDIEvent_IsNoteOn (MIDIEvent* pMIDIEvent);

/* 긩?긣긆긲귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsNoteOff (MIDIEvent* pMIDIEvent);

/* 긩?긣귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsNote (MIDIEvent* pMIDIEvent);

/* NOTEONOTEOFF귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
/* 궞귢궼긩?긣긆깛(0x9n)궴긩?긣긆긲(0x8n)궕뙅뜃귽긹깛긣궢궫귽긹깛긣궳궶궚귢궽궶귞궶궋갃 */
long  MIDIEvent_IsNoteOnNoteOff (MIDIEvent* pEvent);

/* NOTEONNOTEON0귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
/* 궞귢궼긩?긣긆깛(0x9n)궴긩?긣긆긲(0x9n,vel==0)궕뙅뜃귽긹깛긣궢궫귽긹깛긣궳궶궚귢궽궶귞궶궋갃 */
long  MIDIEvent_IsNoteOnNoteOn0 (MIDIEvent* pEvent);

/* 긌?귺긲???긞?귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsKeyAftertouch (MIDIEvent* pEvent);

/* 긓깛긣깓?깑?긃깛긙귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsControlChange (MIDIEvent* pEvent);

/* RPN?긃깛긙귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsRPNChange (MIDIEvent* pEvent);

/* NRPN?긃깛긙귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsNRPNChange (MIDIEvent* pEvent);

/* 긵깓긐깋??긃깛긙귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsProgramChange (MIDIEvent* pEvent);

/* 긬긞??긃깛긙귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsPatchChange (MIDIEvent* pEvent);

/* ?긿깛긨깑귺긲???긞?귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsChannelAftertouch (MIDIEvent* pEvent);

/* 긯긞?긹깛긤귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsPitchBend (MIDIEvent* pEvent);

/* 긘긚긡?긄긏긚긏깑?긘깞귽긹깛긣궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDIEvent_IsSysExEvent (MIDIEvent* pMIDIEvent);

/* 븖뾙귽긹깛긣궳궇귡궔궵궎궔뮧귊귡 */
long  MIDIEvent_IsFloating (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궳궇귡궔궵궎궔뮧귊귡 */
long  MIDIEvent_IsCombined (MIDIEvent* pEvent);


/* 귽긹깛긣궻롰쀞귩롦벦 */
long  MIDIEvent_GetKind (MIDIEvent* pEvent);

/* 귽긹깛긣궻롰쀞귩먠믦 */
long  MIDIEvent_SetKind (MIDIEvent* pEvent, long lKind);

/* 귽긹깛긣궻뮮궠롦벦 */
long  MIDIEvent_GetLen (MIDIEvent* pEvent);

/* 귽긹깛긣궻긢??븫귩롦벦 */
long  MIDIEvent_GetData (MIDIEvent* pEvent, unsigned char* pBuf, long lLen);

/* 귽긹깛긣궻긢??븫귩먠믦(궞궻듫릶궼묈빾딅뙬궳궥갃맢뜃맜궻?긃긌궼궢귏궧귪) */
long  MIDIEvent_SetData (MIDIEvent* pEvent, unsigned char* pBuf, long lLen);

/* 귽긹깛긣궻긡긌긚긣귩롦벦(긡긌긚긣갋뮊띿뙛갋긣깋긞긏뼹갋귽깛긚긣귾깑긽깛긣뼹갋 */
/* 됊럩갋??긇?갋긌깄??귽깛긣갋긵깓긐깋?뼹갋긢긫귽긚뼹궻귒) */
char*  MIDIEvent_GetTextA (MIDIEvent* pEvent, char* pBuf, long lLen);
wchar_t*  MIDIEvent_GetTextW (MIDIEvent* pEvent, wchar_t* pBuf, long lLen);
#ifdef UNICODE_X
#define MIDIEvent_GetText MIDIEvent_GetTextW
#else
#define MIDIEvent_GetText MIDIEvent_GetTextA
#endif

/* 귽긹깛긣궻긡긌긚긣귩먠믦(긡긌긚긣갋뮊띿뙛갋긣깋긞긏뼹갋귽깛긚긣귾깑긽깛긣뼹갋 */
/* 됊럩갋??긇?갋긌깄??귽깛긣갋긵깓긐깋?뼹갋긢긫귽긚뼹궻귒) */
long  MIDIEvent_SetTextA (MIDIEvent* pEvent, const char* pszText);
long  MIDIEvent_SetTextW (MIDIEvent* pEvent, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDIEvent_SetText MIDIEvent_SetTextW
#else
#define MIDIEvent_SetText MIDIEvent_SetTextA
#endif

/* SMPTE긆긲긜긞긣궻롦벦(SMPTE긆긲긜긞긣귽긹깛긣궻귒) */
long  MIDIEvent_GetSMPTEOffset
(MIDIEvent* pEvent, long* pMode, long* pHour, long* pMin, long* pSec, long* pFrame, long* pSubFrame);

/* SMPTE긆긲긜긞긣궻먠믦(SMPTE긆긲긜긞긣귽긹깛긣궻귒) */
long  MIDIEvent_SetSMPTEOffset
(MIDIEvent* pEvent, long lMode, long lHour, long lMin, long lSec, long lFrame, long lSubFrame);

/* 긡깛?롦벦(긡깛?귽긹깛긣궻귒) */
long  MIDIEvent_GetTempo (MIDIEvent* pEvent);

/* 긡깛?먠믦(긡깛?귽긹깛긣궻귒) */
long  MIDIEvent_SetTempo (MIDIEvent* pEvent, long lTempo);

/* 뵋럔딯뜂롦벦(뵋럔딯뜂귽긹깛긣궻귒) */
long  MIDIEvent_GetTimeSignature
(MIDIEvent* pEvent, long* lnn, long* ldd, long* lcc, long* bb);

/* 뵋럔딯뜂궻먠믦(뵋럔딯뜂귽긹깛긣궻귒) */
long  MIDIEvent_SetTimeSignature
(MIDIEvent* pEvent, long lnn, long ldd, long lcc, long lbb);

/* 뮧맜딯뜂궻롦벦(뮧맜딯뜂귽긹깛긣궻귒) */
long  MIDIEvent_GetKeySignature (MIDIEvent* pEvent, long* psf, long* pmi);

/* 뮧맜딯뜂궻먠믦(뮧맜딯뜂귽긹깛긣궻귒) */
long  MIDIEvent_SetKeySignature (MIDIEvent* pEvent, long lsf, long lmi);

/* 귽긹깛긣궻긽긞긜?긙롦벦(MIDI?긿깛긨깑귽긹깛긣땩귂긘긚긡?긄긏긚긏깑?긘깞궻귒) */
long  MIDIEvent_GetMIDIMessage (MIDIEvent* pEvent, char* pMessage, long lLen);

/* 귽긹깛긣궻긽긞긜?긙먠믦(MIDI?긿깛긨깑귽긹깛긣땩귂긘긚긡?긄긏긚긏깑?긘깞궻귒) */
long  MIDIEvent_SetMIDIMessage (MIDIEvent* pEvent, char* pMessage, long lLen);

/* 귽긹깛긣궻?긿깛긨깑롦벦(MIDI?긿깛긨깑귽긹깛긣궻귒) */
long  MIDIEvent_GetChannel (MIDIEvent* pEvent);

/* 귽긹깛긣궻?긿깛긨깑먠믦(MIDI?긿깛긨깑귽긹깛긣궻귒) */
long  MIDIEvent_SetChannel (MIDIEvent* pEvent, long lCh);

/* 귽긹깛긣궻렄뜌롦벦 */
long  MIDIEvent_GetTime (MIDIEvent* pEvent);

/* 귽긹깛긣궻렄뜌먠믦 */
long  MIDIEvent_SetTimeSingle (MIDIEvent* pEvent, long lTime);

/* 귽긹깛긣궻렄뜌먠믦 */
long  MIDIEvent_SetTime (MIDIEvent* pEvent, long lTime);

/* 귽긹깛긣궻긌?롦벦(긩?긣긆긲갋긩?긣긆깛갋?긿깛긨깑귺긲??궻귒) */
long  MIDIEvent_GetKey (MIDIEvent* pEvent);

/* 귽긹깛긣궻긌?먠믦(긩?긣긆긲갋긩?긣긆깛갋?긿깛긨깑귺긲??궻귒) */
long  MIDIEvent_SetKey (MIDIEvent* pEvent, long lKey);

/* 귽긹깛긣궻긹깓긘긡귻롦벦(긩?긣긆긲갋긩?긣긆깛궻귒) */
long  MIDIEvent_GetVelocity (MIDIEvent* pEvent);

/* 귽긹깛긣궻긹깓긘긡귻먠믦(긩?긣긆긲갋긩?긣긆깛궻귒) */
long  MIDIEvent_SetVelocity (MIDIEvent* pEvent, long cVel);

/* 뙅뜃귽긹깛긣궻돶뮮궠롦벦(긩?긣궻귒) */
long  MIDIEvent_GetDuration (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궻돶뮮궠먠믦(긩?긣궻귒) */
long  MIDIEvent_SetDuration (MIDIEvent* pEvent, long lDuration);

/* 뙅뜃귽긹깛긣궻긫깛긏롦벦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_GetBank (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궻긫깛긏뤵댧(MSB)롦벦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_GetBankMSB (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궻긫깛긏돷댧(LSB)롦벦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_GetBankLSB (MIDIEvent* pEvent);

/* 뙅뜃귽긹깛긣궻긫깛긏먠믦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_SetBank (MIDIEvent* pEvent, long lBank);

/* 뙅뜃귽긹깛긣궻긫깛긏뤵댧(MSB)먠믦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_SetBankMSB (MIDIEvent* pEvent, long lBankMSB);

/* 뙅뜃귽긹깛긣궻긫깛긏돷댧(LSB)먠믦(RPN?긃깛긙갋NRPN?긃깛긙갋긬긞??긃깛긙궻귒) */
long  MIDIEvent_SetBankLSB (MIDIEvent* pEvent, long lBankLSB);

/* 귽긹깛긣궻붥뜂롦벦(긓깛긣깓?깑?긃깛긙갋긵깓긐깋??긃깛긙궻귒) */
long  MIDIEvent_GetNumber (MIDIEvent* pEvent);

/* 귽긹깛긣궻붥뜂먠믦(긓깛긣깓?깑?긃깛긙갋긵깓긐깋??긃깛긙궻귒) */
long  MIDIEvent_SetNumber (MIDIEvent* pEvent, long cNum);

/* 귽긹깛긣궻뭠롦벦(긌?귺긲??갋긓깛긣깓?깋?갋?긿깛긨깑귺긲??갋긯긞?긹깛긤) */
long  MIDIEvent_GetValue (MIDIEvent* pEvent);

/* 귽긹깛긣궻뭠먠믦(긌?귺긲??갋긓깛긣깓?깋?갋?긿깛긨깑귺긲??갋긯긞?긹깛긤) */
long  MIDIEvent_SetValue (MIDIEvent* pEvent, long nVal);

/* 렅궻귽긹깛긣귉궻?귽깛?귩롦벦(궶궚귢궽NULL) */
MIDIEvent*  MIDIEvent_GetNextEvent (MIDIEvent* pMIDIEvent);

/* 멟궻귽긹깛긣귉궻?귽깛?귩롦벦(궶궚귢궽NULL) */
MIDIEvent*  MIDIEvent_GetPrevEvent (MIDIEvent* pMIDIEvent);

/* 렅궻벏롰궻귽긹깛긣귉궻?귽깛?귩롦벦(궶궚귢궽NULL) */
MIDIEvent*  MIDIEvent_GetNextSameKindEvent (MIDIEvent* pMIDIEvent);

/* 멟궻벏롰궻귽긹깛긣귉궻?귽깛?귩롦벦(궶궚귢궽NULL) */
MIDIEvent*  MIDIEvent_GetPrevSameKindEvent (MIDIEvent* pMIDIEvent);

/* 릂긣깋긞긏귉궻?귽깛?귩롦벦(궶궚귢궽NULL) */
MIDITrack*  MIDIEvent_GetParent (MIDIEvent* pMIDIEvent);

/* 귽긹깛긣궻볙뾢귩빒럻쀱?뙸궸빾듂 */
char*  MIDIEvent_ToStringExA (MIDIEvent* pEvent, char* pBuf, long lLen, long lFlags);
wchar_t*  MIDIEvent_ToStringExW (MIDIEvent* pEvent, wchar_t* pBuf, long lLen, long lFlags);
#ifdef UNICODE_X
#define MIDIEvent_ToStringEx MIDIEvent_ToStringExW
#else
#define MIDIEvent_ToStringEx MIDIEvent_ToStringExA
#endif

/* 귽긹깛궻볙뾢긣귩빒럻쀱?뙸궸빾듂 */
char*  MIDIEvent_ToStringA (MIDIEvent* pEvent, char* pBuf, long lLen);
wchar_t*  MIDIEvent_ToStringW (MIDIEvent* pEvent, wchar_t* pBuf, long lLen);
#ifdef UNICODE_X
#define MIDIEvent_ToString MIDIEvent_ToStringW
#else
#define MIDIEvent_ToString MIDIEvent_ToStringA
#endif


/******************************************************************************/
/*                                                                            */
/*?MIDITrack긏깋긚듫릶                                                       */
/*                                                                            */
/******************************************************************************/

long  MIDITrack_GetNumEvent (MIDITrack* pMIDITrack);
MIDIEvent*  MIDITrack_GetFirstEvent (MIDITrack* pMIDITrack);
MIDIEvent*  MIDITrack_GetLastEvent (MIDITrack* pMIDITrack);
MIDIEvent*  MIDITrack_GetFirstKindEvent (MIDITrack* pTrack, long lKind);
MIDIEvent*  MIDITrack_GetLastKindEvent (MIDITrack* pTrack, long lKind);
MIDITrack*  MIDITrack_GetNextTrack (MIDITrack* pTrack);
MIDITrack*  MIDITrack_GetPrevTrack (MIDITrack* pTrack);
MIDIData*  MIDITrack_GetParent (MIDITrack* pMIDITrack);
long  MIDITrack_CountEvent (MIDITrack* pMIDITrack);
long  MIDITrack_GetBeginTime (MIDITrack* pMIDITrack);
long  MIDITrack_GetEndTime (MIDITrack* pMIDITrack);

char*  MIDITrack_GetNameA (MIDITrack* pMIDITrack, char* pBuf, long lLen);
wchar_t*  MIDITrack_GetNameW (MIDITrack* pMIDITrack, wchar_t* pBuf, long lLen);
#ifdef UNICODE_X
#define MIDITrack_GetName MIDITrack_GetNameW
#else
#define MIDITrack_GetName MIDITrack_GetNameA
#endif

long  MIDITrack_GetInputOn (MIDITrack* pTrack);
long  MIDITrack_GetInputPort (MIDITrack* pTrack);
long  MIDITrack_GetInputChannel (MIDITrack* pTrack);
long  MIDITrack_GetOutputOn (MIDITrack* pTrack);
long  MIDITrack_GetOutputPort (MIDITrack* pTrack);
long  MIDITrack_GetOutputChannel (MIDITrack* pTrack);
long  MIDITrack_GetTimePlus (MIDITrack* pTrack);
long  MIDITrack_GetKeyPlus (MIDITrack* pTrack);
long  MIDITrack_GetVelocityPlus (MIDITrack* pTrack);
long  MIDITrack_GetViewMode (MIDITrack* pTrack);
long  MIDITrack_GetForeColor (MIDITrack* pTrack);
long  MIDITrack_GetBackColor (MIDITrack* pTrack);


long  MIDITrack_SetNameA (MIDITrack* pMIDITrack, const char* pszText);
long  MIDITrack_SetNameW (MIDITrack* pMIDITrack, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_SetName MIDITrack_SetNameW
#else
#define MIDITrack_SetName MIDITrack_SetNameA
#endif

long  MIDITrack_SetInputOn (MIDITrack* pTrack, long lInputOn);
long  MIDITrack_SetInputPort (MIDITrack* pTrack, long lInputPort);
long  MIDITrack_SetInputChannel (MIDITrack* pTrack, long lInputChannel);
long  MIDITrack_SetOutputOn (MIDITrack* pTrack, long lOutputOn);

long  MIDITrack_SetOutputPort (MIDITrack* pTrack, long lOutputPort);
long  MIDITrack_SetOutputChannel (MIDITrack* pTrack, long lOutputChannel);
long  MIDITrack_SetTimePlus (MIDITrack* pTrack, long lTimePlus);
long  MIDITrack_SetKeyPlus (MIDITrack* pTrack, long lKeyPlus);
long  MIDITrack_SetVelocityPlus (MIDITrack* pTrack, long lVelocityPlus);
long  MIDITrack_SetViewMode (MIDITrack* pTrack, long lMode);
long  MIDITrack_SetForeColor (MIDITrack* pTrack, long lForeColor);
long  MIDITrack_SetBackColor (MIDITrack* pTrack, long lBackColor);
long  MIDITrack_GetXFVersion (MIDITrack* pMIDITrack);
void  MIDITrack_Delete (MIDITrack* pMIDITrack);
MIDITrack*  MIDITrack_Create ();

MIDITrack*  MIDITrack_CreateClone (MIDITrack* pTrack);
long  MIDITrack_InsertSingleEventAfter
(MIDITrack* pMIDITrack, MIDIEvent* pEvent, MIDIEvent* pTarget);
long  MIDITrack_InsertSingleEventBefore
(MIDITrack* pMIDITrack, MIDIEvent* pEvent, MIDIEvent* pTarget);
long  MIDITrack_InsertEventAfter
(MIDITrack* pMIDITrack, MIDIEvent* pEvent, MIDIEvent* pTarget);
long  MIDITrack_InsertEventBefore
(MIDITrack* pMIDITrack, MIDIEvent* pEvent, MIDIEvent* pTarget);

long  MIDITrack_InsertEvent (MIDITrack* pMIDITrack, MIDIEvent* pEvent);

long  MIDITrack_InsertSequenceNumber
(MIDITrack* pMIDITrack, long lTime, long lNum);

long  MIDITrack_InsertTextEventA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertTextEventW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertTextEvent MIDITrack_InsertTextEventW
#else
#define MIDITrack_InsertTextEvent MIDITrack_InsertTextEventA
#endif

long  MIDITrack_InsertCopyrightNoticeA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertCopyrightNoticeW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertCopyrightNotice MIDITrack_InsertCopyrightNoticeW
#else
#define MIDITrack_InsertCopyrightNotice MIDITrack_InsertCopyrightNoticeA
#endif

long  MIDITrack_InsertTrackNameA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertTrackNameW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertTrackName MIDITrack_InsertTrackNameW
#else
#define MIDITrack_InsertTrackName MIDITrack_InsertTrackNameA
#endif

long  MIDITrack_InsertInstrumentNameA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertInstrumentNameW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertInstrumentName MIDITrack_InsertInstrumentNameW
#else
#define MIDITrack_InsertInstrumentName MIDITrack_InsertInstrumentNameA
#endif

long  MIDITrack_InsertLyricA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertLyricW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X_X
#define MIDITrack_InsertLyric MIDITrack_InsertLyricW
#else
#define MIDITrack_InsertLyric MIDITrack_InsertLyricA
#endif

long  MIDITrack_InsertMarkerA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertMarkerW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X_X
#define MIDITrack_InsertMarker MIDITrack_InsertMarkerW
#else
#define MIDITrack_InsertMarker MIDITrack_InsertMarkerA
#endif

long  MIDITrack_InsertCuePointA
(MIDITrack* pMIDITrack, long lTime, const char* pszText);
long  MIDITrack_InsertCuePointW
(MIDITrack* pMIDITrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertCuePoint MIDITrack_InsertCuePointW
#else
#define MIDITrack_InsertCuePoint MIDITrack_InsertCuePointA
#endif

long  MIDITrack_InsertProgramNameA
(MIDITrack* pTrack, long lTime, const char* pszText);
long  MIDITrack_InsertProgramNameW
(MIDITrack* pTrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertProgramName MIDITrack_InsertProgramNameW
#else
#define MIDITrack_InsertProgramName MIDITrack_InsertProgramNameA
#endif

long  MIDITrack_InsertDeviceNameA
(MIDITrack* pTrack, long lTime, const char* pszText);
long  MIDITrack_InsertDeviceNameW
(MIDITrack* pTrack, long lTime, const wchar_t* pszText);
#ifdef UNICODE_X
#define MIDITrack_InsertDeviceName MIDITrack_InsertDeviceNameW
#else
#define MIDITrack_InsertDeviceName MIDITrack_InsertDeviceNameA
#endif

long  MIDITrack_InsertChannelPrefix (MIDITrack* pTrack, long lTime, long lCh);
long  MIDITrack_InsertPortPrefix (MIDITrack* pTrack, long lTime, long lPort);
long  MIDITrack_InsertEndofTrack
(MIDITrack* pMIDITrack, long lTime);
long  MIDITrack_InsertTempo
(MIDITrack* pMIDITrack, long lTime, long lTempo);
long MIDITrack_InsertSMPTEOffset
(MIDITrack* pTrack, long lTime, long lMode,
 long lHour, long lMin, long lSec, long lFrame, long lSubFrame);
long  MIDITrack_InsertTimeSignature
(MIDITrack* pMIDITrack, long lTime, long lnn, long ldd, long lcc, long lbb);
long  MIDITrack_InsertKeySignature
(MIDITrack* pMIDITrack, long lTime, long lsf, long lmi);
long  MIDITrack_InsertSequencerSpecific
(MIDITrack* pMIDITrack, long lTime, char* pBuf, long lLen);
long  MIDITrack_InsertNoteOff
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lKey, long lVel);
long  MIDITrack_InsertNoteOn
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lKey, long lVel);
long  MIDITrack_InsertNote
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lKey, long lVel, long lDur);
long  MIDITrack_InsertKeyAftertouch
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lKey, long lVal);
long  MIDITrack_InsertControlChange
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lNum, long lVal);
long  MIDITrack_InsertRPNChange
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lCC101, long lCC100, long lVal);
long  MIDITrack_InsertNRPNChange
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lCC99, long lCC98, long lVal);
long  MIDITrack_InsertProgramChange
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lNum);

/* 긣깋긞긏궸긬긞??긃깛긙귽긹깛긣귩맯맟궢궲?볺 */
long  MIDITrack_InsertPatchChange
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lCC0, long lCC32, long lNum);

/* 긣깋긞긏궸?긿깛긨깑귺긲??귽긹깛긣귩맯맟궢궲?볺 */
long  MIDITrack_InsertChannelAftertouch
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lVal);

/* 긣깋긞긏궸긯긞?긹깛긤귽긹깛긣귩맯맟궢궲?볺 */
long  MIDITrack_InsertPitchBend
	(MIDITrack* pMIDITrack, long lTime, long lCh, long lVal);

/* 긣깋긞긏궸긘긚긡?긄긏긚긏깑?긘깞귽긹깛긣귩맯맟궢궲?볺 */
long  MIDITrack_InsertSysExEvent
	(MIDITrack* pMIDITrack, long lTime, unsigned char* pBuf, long lLen);

/* 긣깋긞긏궔귞귽긹깛긣귩1궰롦귟룣궘(귽긹깛긣긆긳긙긃긏긣궼랁룣궢귏궧귪) */
long  MIDITrack_RemoveSingleEvent (MIDITrack* pTrack, MIDIEvent* pEvent);

/* 긣깋긞긏궔귞귽긹깛긣귩롦귟룣궘(귽긹깛긣긆긳긙긃긏긣궼랁룣궢귏궧귪) */
long  MIDITrack_RemoveEvent (MIDITrack* pMIDITrack, MIDIEvent* pEvent);


/* MIDI긣깋긞긏궕븖뾙긣깋긞긏궳궇귡궔궵궎궔귩뮧귊귡 */
long  MIDITrack_IsFloating (MIDITrack* pMIDITrack);


/* MIDI긣깋긞긏궕긜긞긣귺긞긵긣깋긞긏궴궢궲맫궢궋궞궴귩둴봃궥귡 */
long  MIDITrack_CheckSetupTrack (MIDITrack* pMIDITrack);

/* MIDI긣깋긞긏궕긩깛긜긞긣귺긞긵긣깋긞긏궴궢궲맫궢궋궞궴귩둴봃궥귡 */
long  MIDITrack_CheckNonSetupTrack (MIDITrack* pMIDITrack);

/* ?귽?긓?긤귩?깏뷳렄뜌궸빾듂(럚믦긣깋긞긏볙궻긡깛?귽긹깛긣귩딈궸똶럁) */
long  MIDITrack_TimeToMillisec (MIDITrack* pMIDITrack, long lTime);

/* ?깏뷳렄뜌귩?귽?긓?긤궸빾듂(럚믦긣깋긞긏볙궻긡깛?귽긹깛긣귩딈궸똶럁) */
long  MIDITrack_MillisecToTime (MIDITrack* pMIDITrack, long lMillisec);

/* ?귽?긓?긤귩룷먢갌뵋갌긡귻긞긏궸빁됶(럚믦긣깋긞긏볙궻뵋럔딯뜂귩딈궸똶럁) */
long  MIDITrack_BreakTimeEx
	(MIDITrack* pMIDITrack, long lTime, long* pMeasure, long* pBeat, long* pTick,
	long* pnn, long* pdd, long* pcc, long* pbb);

/* ?귽?긓?긤귩룷먢갌뵋갌긡귻긞긏궸빁됶(럚믦긣깋긞긏볙궻뵋럔딯뜂귩딈궸똶럁) */
long  MIDITrack_BreakTime
	(MIDITrack* pMIDITrack, long lTime, long* pMeasure, long* pBeat, long* pTick);

/* 룷먢갌뵋갌긡귻긞긏궔귞?귽?긓?긤귩맯맟(럚믦긣깋긞긏볙궻뵋럔딯뜂귩딈궸똶럁) */
long  MIDITrack_MakeTimeEx
	(MIDITrack* pMIDITrack, long lMeasure, long lBeat, long lTick, long* pTime,
	long* pnn, long* pdd, long* pcc, long* pbb);

/* 룷먢갌뵋갌긡귻긞긏궔귞?귽?긓?긤귩맯맟(럚믦긣깋긞긏볙궻뵋럔딯뜂귩딈궸똶럁) */
long  MIDITrack_MakeTime
	(MIDITrack* pMIDITrack, long lMeasure, long lBeat, long lTick, long* pTime);

/* 럚믦댧뭫궸궓궚귡긡깛?귩롦벦 */
long  MIDITrack_FindTempo
(MIDITrack* pMIDITrack, long lTime, long* pTempo);

/* 럚믦댧뭫궸궓궚귡뵋럔딯뜂귩롦벦 */
long  MIDITrack_FindTimeSignature
(MIDITrack* pMIDITrack, long lTime, long* pnn, long* pdd, long* pcc, long* pbb);

/* 럚믦댧뭫궸궓궚귡뮧맜딯뜂귩롦벦 */
long  MIDITrack_FindKeySignature
(MIDITrack* pMIDITrack, long lTime, long* psf, long* pmi);




/******************************************************************************/
/*                                                                            */
/*?MIDIData긏깋긚듫릶                                                        */
/*                                                                            */
/******************************************************************************/

/* MIDI긢??궻럚믦긣깋긞긏궻뮳멟궸긣깋긞긏귩?볺 */
long _TimeProc(MIDIClock* pMIDIClock, long mperiod);

long  MIDIData_InsertTrackBefore
(MIDIData* pMIDIData, MIDITrack* pMIDITrack, MIDITrack* pTarget);
long  MIDIData_InsertTrackAfter
(MIDIData* pMIDIData, MIDITrack* pMIDITrack, MIDITrack* pTarget);
long  MIDIData_AddTrack (MIDIData* pMIDIData, MIDITrack* pMIDITrack);
long  MIDIData_DuplicateTrack (MIDIData* pMIDIData, MIDITrack* pTrack);
long  MIDIData_RemoveTrack (MIDIData* pMIDIData, MIDITrack* pMIDITrack);
void  MIDIData_Delete (MIDIData* pMIDIData);
MIDIData*  MIDIData_Create (long lFormat, long lNumTrack, long lTimeMode, long lResolution);
long  MIDIData_GetFormat (MIDIData* pMIDIData);
long  MIDIData_SetFormat (MIDIData* pMIDIData, long lFormat);
long  MIDIData_GetTimeBase (MIDIData* pMIDIData, long* pMode, long* pResolution);
long  MIDIData_GetTimeMode (MIDIData* pMIDIData);
long  MIDIData_GetTimeResolution (MIDIData* pMIDIData);
long  MIDIData_SetTimeBase (MIDIData* pMIDIData, long lMode, long lResolution);
long  MIDIData_GetNumTrack (MIDIData* pMIDIData);
long  MIDIData_CountTrack (MIDIData* pMIDIData);
long  MIDIData_GetXFVersion (MIDIData* pMIDIData);
MIDITrack*  MIDIData_GetFirstTrack (MIDIData* pMIDIData);
MIDITrack*  MIDIData_GetLastTrack (MIDIData* pMIDIData);
MIDITrack*  MIDIData_GetTrack (MIDIData* pMIDIData, long lTrackIndex);
long  MIDIData_GetBeginTime (MIDIData* pMIDIData);
long  MIDIData_GetEndTime (MIDIData* pMIDIData);
char*  MIDIData_GetTitleA (MIDIData* pMIDIData, char* pData, long lLen);
wchar_t*  MIDIData_GetTitleW (MIDIData* pMIDIData, wchar_t* pData, long lLen);
#ifdef UNICODE_X
#define MIDIData_GetTitle MIDIData_GetTitleW
#else
#define MIDIData_GetTitle MIDIData_GetTitleA
#endif

/* MIDI긢??궻?귽긣깑귩듗댲먠믦 */
long  MIDIData_SetTitleA (MIDIData* pMIDIData, const char* pszData);
long  MIDIData_SetTitleW (MIDIData* pMIDIData, const wchar_t* pszData);
#ifdef UNICODE_X
#define MIDIData_SetTitle MIDIData_SetTitleW
#else
#define MIDIData_SetTitle MIDIData_SetTitleA
#endif

/* MIDI긢??궻긖긳?귽긣깑귩듗댲롦벦 */
char*  MIDIData_GetSubTitleA (MIDIData* pMIDIData, char* pData, long lLen);
wchar_t*  MIDIData_GetSubTitleW (MIDIData* pMIDIData, wchar_t* pData, long lLen);
#ifdef UNICODE_X
#define MIDIData_GetSubTitle MIDIData_GetSubTitleW
#else
#define MIDIData_GetSubTitle MIDIData_GetSubTitleA
#endif

/* MIDI긢??궻긖긳?귽긣깑귩듗댲먠믦 */
long  MIDIData_SetSubTitleA (MIDIData* pMIDIData, const char* pszData);
long  MIDIData_SetSubTitleW (MIDIData* pMIDIData, const wchar_t* pszData);
#ifdef UNICODE_X
#define MIDIData_SetSubTitle MIDIData_SetSubTitleW
#else
#define MIDIData_SetSubTitle MIDIData_SetSubTitleA
#endif

/* MIDI긢??궻뮊띿뙛귩듗댲롦벦 */
char*  MIDIData_GetCopyrightA (MIDIData* pMIDIData, char* pData, long lLen);
wchar_t*  MIDIData_GetCopyrightW (MIDIData* pMIDIData, wchar_t* pData, long lLen);
#ifdef UNICODE_X
#define MIDIData_GetCopyright MIDIData_GetCopyrightW
#else
#define MIDIData_GetCopyright MIDIData_GetCopyrightA
#endif

/* MIDI긢??궻뮊띿뙛귩듗댲먠믦 */
long  MIDIData_SetCopyrightA (MIDIData* pMIDIData, const char* pszData);
long  MIDIData_SetCopyrightW (MIDIData* pMIDIData, const wchar_t* pszData);
#ifdef UNICODE_X
#define MIDIData_SetCopyright MIDIData_SetCopyrightW
#else
#define MIDIData_SetCopyright MIDIData_SetCopyrightA
#endif

/* MIDI긢??궻긓긽깛긣귩듗댲롦벦 */
char*  MIDIData_GetCommentA (MIDIData* pMIDIData, char* pData, long lLen);
wchar_t*  MIDIData_GetCommentW (MIDIData* pMIDIData, wchar_t* pData, long lLen);
#ifdef UNICODE_X
#define MIDIData_GetComment MIDIData_GetCommentW
#else
#define MIDIData_GetComment MIDIData_GetCommentA
#endif

/* MIDI긢??궻긓긽깛긣귩듗댲먠믦 */
long  MIDIData_SetCommentA (MIDIData* pMIDIData, const char* pszData);
long  MIDIData_SetCommentW (MIDIData* pMIDIData, const wchar_t* pszData);
#ifdef UNICODE_X
#define MIDIData_SetComment MIDIData_SetCommentW
#else
#define MIDIData_SetComment MIDIData_SetCommentA
#endif

long  MIDIData_TimeToMillisec (MIDIData* pMIDIData, long lTime);
long  MIDIData_MillisecToTime (MIDIData* pMIDIData, long lMillisec);
long  MIDIData_BreakTime
	(MIDIData* pMIDIData, long lTime, long* pMeasure, long* pBeat, long* pTick);
long  MIDIData_BreakTimeEx
	(MIDIData* pMIDIData, long lTime, long* pMeasure, long* pBeat, long* pTick,
	long* pnn, long* pdd, long* pcc, long* pbb);
long  MIDIData_MakeTime
	(MIDIData* pMIDIData, long lMeasure, long lBeat, long lTick, long* pTime);
long  MIDIData_MakeTimeEx
	(MIDIData* pMIDIData, long lMeasure, long lBeat, long lTick, long* pTime,
	long* pnn, long* pdd, long* pcc, long* pbb);
long  MIDIData_FindTempo
	(MIDIData* pMIDIData, long lTime, long* pTempo);
long  MIDIData_FindTimeSignature
	(MIDIData* pMIDIData, long lTime, long* pnn, long* pdd, long* pcc, long* pbb);
long  MIDIData_FindKeySignature
	(MIDIData* pMIDIData, long lTime, long* psf, long* pmi);

/* 궞궻MIDI긢??궸빶궻MIDI긢??귩??긙궥귡(20080715봯?) */
/*long  MIDIData_Merge (MIDIData* pMIDIData, MIDIData* pMergeData, */
/*	long lTime, long lFlags, long* pInsertedEventCount, long* pDeletedEventCount);*/

/* 뺎뫔갋벶귒뜛귒뾭듫릶 */

/* MIDIData귩긚?깛??긤MIDI긲?귽깑(SMF)궔귞벶귒뜛귒갂*/
/* 륷궢궋MIDI긢??귉궻?귽깛?귩뺅궥(렪봲렄NULL) */
MIDIData*  MIDIData_LoadFromSMFA (const char* pszFileName);
MIDIData*  MIDIData_LoadFromSMFW (const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_LoadFromSMF MIDIData_LoadFromSMFW
#else
#define MIDIData_LoadFromSMF MIDIData_LoadFromSMFA
#endif

/* MIDI긢??귩긚?깛??긤MIDI긲?귽깑(SMF)궴궢궲뺎뫔 */
long  MIDIData_SaveAsSMFA (MIDIData* pMIDIData, const char* pszFileName);
long  MIDIData_SaveAsSMFW (MIDIData* pMIDIData, const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_SaveAsSMF MIDIData_SaveAsSMFW
#else
#define MIDIData_SaveAsSMF MIDIData_SaveAsSMFA
#endif

/* MIDIData귩긡긌긚긣긲?귽깑궔귞벶귒뜛귒갂 */
/* 륷궢궋MIDI긢??귉궻?귽깛?귩뺅궥(렪봲렄NULL) */
MIDIData*  MIDIData_LoadFromTextA (const char* pszFileName);
MIDIData*  MIDIData_LoadFromTextW (const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_LoadFromText MIDIData_LoadFromTextW
#else
#define MIDIData_LoadFromText MIDIData_LoadFromTextA
#endif

/* MIDIData귩긡긌긚긣긲?귽깑궴궢궲뺎뫔 */
long  MIDIData_SaveAsTextA (MIDIData* pMIDIData, const char* pszFileName);
long  MIDIData_SaveAsTextW (MIDIData* pMIDIData, const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_SaveAsText MIDIData_SaveAsTextW
#else
#define MIDIData_SaveAsText MIDIData_SaveAsTextA
#endif

/* MIDIData귩긫귽긥깏긲?귽깑궔귞벶귒뜛귒갂*/
/* 륷궢궋MIDI긢??귉궻?귽깛?귩뺅궥(렪봲렄NULL) */
MIDIData*  MIDIData_LoadFromBinaryA (const char* pszFileName);
MIDIData*  MIDIData_LoadFromBinaryW (const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_LoadFromBinary MIDIData_LoadFromBinaryW
#else
#define MIDIData_LoadFromBinary MIDIData_LoadFromBinaryA
#endif

/* MIDIData귩긫귽긥깏긲?귽깑궸뺎뫔 */
long  MIDIData_SaveAsBinaryA (MIDIData* pMIDIData, const char* pszFileName);
long  MIDIData_SaveAsBinaryW (MIDIData* pMIDIData, const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_SaveAsBinary MIDIData_SaveAsBinaryW
#else
#define MIDIData_SaveAsBinary MIDIData_SaveAsBinaryA
#endif

/* MIDIData귩Cherrry긲?귽깑(*.chy)궔귞벶귒뜛귒갂 */
/* 륷궢궋MIDI긢??귉궻?귽깛?귩뺅궥(렪봲렄NULL) */
MIDIData*  MIDIData_LoadFromCherryA (const char* pszFileName);
MIDIData*  MIDIData_LoadFromCherryW (const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_LoadFromCherry MIDIData_LoadFromCherryW
#else
#define MIDIData_LoadFromCherry MIDIData_LoadFromCherryA
#endif

/* MIDI긢??귩Cherry긲?귽깑(*.chy)궸뺎뫔 */
long  MIDIData_SaveAsCherryA (MIDIData* pMIDIData, const char* pszFileName);
long  MIDIData_SaveAsCherryW (MIDIData* pMIDIData, const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_SaveAsCherry MIDIData_SaveAsCherryW
#else
#define MIDIData_SaveAsCherry MIDIData_SaveAsCherryA
#endif

/* MIDI긢??귩MIDICSV긲?귽깑(*.csv)궔귞벶귒뜛귒 */
/* 륷궢궋MIDI긢??귉궻?귽깛?귩뺅궥(렪봲렄NULL) */
MIDIData*  MIDIData_LoadFromMIDICSVA (const char* pszFileName);
MIDIData*  MIDIData_LoadFromMIDICSVW (const wchar_t* pszFileName);
#ifdef UNICODE_X
#define MIDIData_LoadFromMIDICSV MIDIData_LoadFromMIDICSVW
#else
#define MIDIData_LoadFromMIDICSV MIDIData_LoadFromMIDICSVA
#endif

/* MIDI긢??귩MIDICSV긲?귽깑(*.csv)궴궢궲뺎뫔 */
long  MIDIData_SaveAsMIDICSVA (MIDIData* pMIDIData, const char* pszFileName);
long  MIDIData_SaveAsMIDICSVW (MIDIData* pMIDIData, const wchar_t* pszFileName);

FILE *lx_wfopen(
   const wchar_t *filename,
   const wchar_t *mode
);

#ifdef UNICODE_X
#define MIDIData_SaveAsMIDICSV MIDIData_SaveAsMIDICSVW
#else
#define MIDIData_SaveAsMIDICSV MIDIData_SaveAsMIDICSVA
#endif

//**************************************************************

#define MIDICLOCK_TEMPO(U) (60000000/(U))
#define MIDICLOCK_MAXTEMPO 60000000
#define MIDICLOCK_MINTEMPO 1

/* ?귽?긾?긤(댥돷궻뭠궻귒뿕뾭됀?) */
#define MIDICLOCK_TPQNBASE         0 /* TPQN긹?긚 */
#define MIDICLOCK_SMPTE24BASE     24 /* SMPTE24긹?긚(24긲깒???뷳) */
#define MIDICLOCK_SMPTE25BASE     25 /* SMPTE25긹?긚(25긲깒???뷳) */
#define MIDICLOCK_SMPTE29BASE     29 /* SMPTE29긹?긚(29.97긲깒???뷳) */
#define MIDICLOCK_SMPTE30BASE     30 /* SMPTE30긹?긚(30긲깒???뷳) */

/* ?귽?긾?긤(ver0.6댥멟궴궻뚚듂궻궫귕궸럄궢궲궇귡) */
#define MIDICLOCK_MASTERTPQNBASE         0 /* TPQN긹?긚 */
#define MIDICLOCK_MASTERSMPTE24BASE     24 /* SMPTE24긹?긚(24긲깒???뷳) */
#define MIDICLOCK_MASTERSMPTE25BASE     25 /* SMPTE25긹?긚(25긲깒???뷳) */
#define MIDICLOCK_MASTERSMPTE29BASE     29 /* SMPTE29긹?긚(29.97긲깒???뷳) */
#define MIDICLOCK_MASTERSMPTE30BASE     30 /* SMPTE30긹?긚(30긲깒???뷳) */

/* MIDI볺쀍벏딖긾?긤(댥돷궻뭠궻귒뿕뾭됀?) */
#define MIDICLOCK_MASTER                 0 /* ?긚?? */
#define MIDICLOCK_SLAVEMIDITIMINGCLOCK   1 /* 긚깒?긳긾?긤(MIDI?귽?깛긐긏깓긞긏믁?) */
#define MIDICLOCK_SLAVESMPTEMTC          2 /* 긚깒?긳긾?긤(SMPTE/MTC믁?) */

/* 긚긯?긤[?0.01걪] */
#define MIDICLOCK_SPEEDNORMAL        10000 /* 긚긯?긤=100걪 */
#define MIDICLOCK_SPEEDSLOW           5000 /* 긚긯?긤=50걪 */
#define MIDICLOCK_SPEEDFAST          20000 /* 긚긯?긤=200걪 */
#define MIDICLOCK_MINSPEED               0 /* 긚긯?긤=먄? */
#define MIDICLOCK_MAXSPEED          100000 /* 긚긯?긤=띍묈 */


//static int timer_handler (int signum)	;
//static int timer_handler (int signum, MIDIClock* pMIDIClock);
//void MIDIClock_timer_start(void);

void  MIDIClock_Delete (MIDIClock* pMIDIClock);
MIDIClock*  MIDIClock_Create (long lTimeMode, long lResolution, long lTempo);
long  MIDIClock_GetTimeBase (MIDIClock* pMIDIClock, long* pTimeMode, long* pResolution);
long  MIDIClock_SetTimeBase (MIDIClock* pMIDIClock, long lTimeMode, long lResolution);
long  MIDIClock_GetTempo (MIDIClock* pMIDIClock);
long  MIDIClock_SetTempo (MIDIClock* pMIDIClock, long lTempo);
long  MIDIClock_GetSpeed (MIDIClock* pMIDIClock);
long  MIDIClock_SetSpeed (MIDIClock* pMIDIClock, long lSpeed);
long  MIDIClock_GetMIDIInSyncMode (MIDIClock* pMIDIClock);
long  MIDIClock_SetMIDIInSyncMode (MIDIClock* pMIDIClock, long lMIDIInSyncMode);
long  MIDIClock_Start (MIDIClock* pMIDIClock);
long  MIDIClock_Stop (MIDIClock* pMIDIClock);
long  MIDIClock_Reset (MIDIClock* pMIDIClock);
long  MIDIClock_IsRunning (MIDIClock* pMIDIClock);
long  MIDIClock_GetMillisec (MIDIClock* pMIDIClock);
long  MIDIClock_SetMillisec (MIDIClock* pMIDIClock, long lMillisec);
long  MIDIClock_GetTickCount (MIDIClock* pMIDIClock);
long  MIDIClock_SetTickCount (MIDIClock* pMIDIClock, long lTickCount);
long  MIDIClock_PutMIDITimingClock (MIDIClock* pMIDIClock);
long  MIDIClock_PutSysExSMPTEMTC (MIDIClock* pMIDIClock, unsigned char cHour,
unsigned char cMinute, unsigned char cSecond, unsigned char cFrame);
long  MIDIClock_PutSMPTEMTC (MIDIClock* pMIDIClock, unsigned char cSMPTEMTC);
long  MIDIClock_PutMIDIMessage (MIDIClock* pMIDIClock, unsigned char* pMessage, long lLen);

//*********************************************************************************************

void MIDIIO_OpenSerial(void);
void  MIDIIO_Put_n(long port, unsigned char* data, long length);
void MIDIIO_All_ContolOff(void);
void MIDIIO_All_NoteOff(void);
void MIDIIO_SetNRPN(unsigned char  hi_add, unsigned char low_add, unsigned char data );
void MIDIIO_SetNRPN2(unsigned char  hi_add, unsigned char low_add, unsigned char hi_data, unsigned char low_data);
void MIDIIO_GS_reset(void);

#ifdef __cplusplus
}
#endif


#endif
