#include "front.h"
#include "extern.h"

front::front()
{
    vFRONTMessage.clear();
    vFRONTKey.clear();
    vFRONTKeyBuffer.clear();
    FRONT_getVersion();
}

front::~front(){}

void front::FRONT_sigfrom_front(void)
{
	int i,ix;
	char data[128];
	int vdata;
    int res,sZ;
    long frontKey;

	//TRACE("#@#DBG_ timer_sigfrom_front\n");
	memset(data,0,128);
	res = read(TTY_MIDI_Port1, data, 128);
	if(res<=0) return;
#ifdef DBG_FRONT
	TRACE("#@#DBG_ Front res[%d]\n", res);
#endif
	for(i=0;i<res;i++)
	{
		vdata=data[i];

#ifdef DBG_FRONT
//	TRACE("#@#DBG_ Front vdata[%x]\n", vdata);
#endif
 		switch(vdata)
 		{
            case 0xFB:
             kind=0;
             vFRONTMessage.clear();
             vFRONTMessage.push_back(vdata&0xff);
             break;
            case 0xFC:
              kind=1;
              vFRONTKey.clear();
              vFRONTKey.push_back(vdata&0xff);
              break;
            case 0xFA:
              if(kind==0){
                 vFRONTMessage.push_back(vdata&0xff);
                 memset(frontVersion,0,sizeof(frontVersion));
                 for(ix=0;ix < 16;ix++){
                    frontVersion[ix]= vFRONTMessage[ix+1];
                  }
#ifdef DBG_FRONT
                 //for(ix=0;ix < 16;ix++){
                 //   TRACE("#@#DBG_ Front DATA[%d][%x]\n", ix, frontVersion[ix]);
                 // }
#endif
                 vFRONTMessage.clear();

               }
               if(kind==1){
                   vFRONTKey.push_back(vdata&0xff);
                    frontKey= (vFRONTKey[1]<<24) + (vFRONTKey[2]<<16) + (vFRONTKey[3]<<8) + vFRONTKey[4];
                    vFRONTKeyBuffer.push_back(frontKey);


 #ifdef DBG_FRONT
                     TRACE("#@#DBG_ Front frontKey[%x][%x][%x][%x][%x][%x][%x]-[%08x]\n", vFRONTKey[0], vFRONTKey[1], vFRONTKey[2], vFRONTKey[3], vFRONTKey[4], vFRONTKey[5], vFRONTKey[6], frontKey);
  #endif
                 vFRONTKey.clear();

                 }
             break;
           default:
            if(kind==0) vFRONTMessage.push_back(vdata&0xff);
            else if(kind==1) vFRONTKey.push_back(vdata&0xff);
            break;
        }

	}

}

void front::FRONT_getVersion(void)
{
 Uint8 data[3]={0xb0,0x32,0x2F};
 MIDIIO_Put_n(15, data,3);
}

void front::startMidiFled( )
{
 Uint8 data[3]={0xb0,0x32,0x2A};
 MIDIIO_Put_n(15, data,3);
}

void front::stopMidiFled( )
{
 Uint8 data[3]={0xb0,0x32,0x2B};
 MIDIIO_Put_n(15, data,3);
}

