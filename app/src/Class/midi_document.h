/////////////////////////////////////
//   midi_document.h
/////////////////////////////////////
#ifndef _MIDI_DOCUMENT_H_
#define _MIDI_DOCUMENT_H_

#define MAX_LINE 256  //ó���� �ִ� �ټ�
#define MUSICSCORE_START_MEASURE 1

typedef unsigned short Uint16;
typedef unsigned char  Uint8;
typedef unsigned int   Uint32;

#define uint32_t Uint32
#include "lxMIDIData/lxMIDIData.h"
#include <string.h>
#include <ctype.h>
#include <vector>
#include <string>
using namespace std;

typedef struct _MDK_TAIL//
{
	long offset;
	long size;
	long idx;
}MDK_TAIL;

typedef struct _LYRIC_NOTE
{
	long ltimeA;
	long ltimeB;
	long ldura;
	long lposA;
	long lposB;
	long ldelta;//단위시간당 이동거리
	string str;
	string ruby;
} LYRIC_NOTE;

typedef struct _UNI_STRING
{
	int sex;
	int attr;
	Uint16 wline[256];
}UNI_STRING;


typedef struct {
	int curLine;
	int curLtime;
} CUR_PLAY_STATUS;

typedef struct _SongInfoW
{
    char u8_title[128];
	long sex;
	long mKey;
	long wKey;
	long dataKey;
	long sf,mi, lnn, ldd, lcc, lbb;
} SongInfoW;


typedef struct //
{
    long ltime;
    long port;
    MIDIEvent* PlayEvent;
}EVENT_LIST;

typedef struct _TRACK_INFORMATION
{
    MIDITrack *pTRACK_Sys;
    MIDITrack *pTRACK_Nrpn;
    MIDITrack *pTRACK_Melody;
    MIDITrack *pTRACK_DrumP1C10;
    MIDITrack *pTRACK_DrumP1C11;
    MIDITrack *pTRACK_DrumP2C10;
    MIDITrack *pTRACK_DrumP2C11;
    MIDITrack *pTRACK_Bass1;
    MIDITrack *pTRACK_Bass2;
    MIDITrack *pTRACK_Chord1;
    MIDITrack *pTRACK_Chord2;
    MIDITrack *pTRACK_Lyric;
    MIDITrack *pTRACK_Function;
    MIDITrack *pTRACK_GK;
    MIDITrack *pTRACK_BR;
    MIDITrack *pTRACK_PS;
    MIDITrack *pTRACK_INST;
    MIDITrack *pTRACK_CHRS;
    MIDITrack *pTRACK_SDFX;

    MIDITrack *pTRACK_EXDR1;
    MIDITrack *pTRACK_EXDR2;
    MIDITrack *pTRACK_EXBASS;
    MIDITrack *pTRACK_EXSYS;
    MIDITrack *pTRACK_EXBREF;

    string nameTRACK_Melody;
}TRACK_INFORMATION;

typedef struct // ���� �̵� ��Ȳ
{
	int IsPlay;
	int chorusCnt;
	int rhythmKind;
	char path[256];//미디파일 패스
	char lMFILE_Kind;
	char lSong_Kind;//0:MIDI, 1:MP3, 2:AV
	long A_B[2];
	long lTickCount;	//현재 Tick
	long SongEnd_ltime;
	long SongEnd_ms;
	long lTimeResolution;
	long TitleOffTime;
	int  TotalEventCnt;
	int TotalLyricPointCnt;
	int  lNoteCnt;					//Lyric-Note Total Count
    int  lxLyricCnt;

    int  ScoreNoteCnt;					//Lyric-Note Total Count
	int  lCurrentLine;
	int  lCurSpeed;//사용
    int  lCurKeyPlus;//사용
    int  lCurMelody;
    int  lCurSex;
    int  lCurPage;//add 18.02.21

	int  lMainMelodyChannel;
	int  lCurMp3_ms;

	int  totalLyricLine;
	int  LyricCode;  //0:BIG5 1:UTF8 2:UNICODE 8:Shift_Jis 9:KS5601

	int  lPageUnit;
	//int  lCurPage;//16
	int  lLastPage;
	int  lCurScoreLine;
	int  lScreenMode;
	int  lCurOnPlay; //0:Pause 1:Play
    int  lCursorLen;

	long LyricDisplay_ltime;  //����׸��� �ð�(Ÿ��Ʋ ����� �ð�)
	long LastScoreNoteOn;		//�Ǻ� ������ Note �����ϴ�  �ð�
	long LastScoreNoteOff;
	long LastMeasure;

	int curDw;
	int curText;
	int lmesureCnt;
	int lmesureDrawCnt;
	long hasMelody_track;
	long SF, MI, BEAT;
	int ChorusPart;
	int RefZifferNo;
	int KeySignature;
}PLAYMIDI_STATUS;

typedef struct
{
    vector<LYRIC_NOTE> vLyNote;
	int Blk_Cnt;
	int Line_StartTime;
	int Line_EndTime;//display 시간
	int updn;  //0:up  1:dn -1: No draw
	int LineSex;
	int LineAttr;
	int Jull;
    int PosA;
    int PosB;
    long Text_length;
}LINE_INFORMATION;

typedef struct _KEY_VALUE
{
    int Sex;//0:woman 1:man 2:합창
    int DataKey;
    int MaleKey;
    int FemaleKey;
    int ChorusKey;
    int OrgSinerKey;
}KEY_VALUE;

typedef struct
{
    int enStaff;
    int cntTrack;
    int cntMeasurePerLine;
    int cntLinePerPage;
    int cntMeasurePerPage;
    int cursorlengh;
    int enableZiffer;
    int enableObli;
}STAFF_MODE;


typedef struct _MIDI_TITLE
{
    long ltime;
    string title;
}MIDI_TITLE;

typedef struct _JULL_POS
{
    long ltimeA;
    long ltimeB;
}JULL_POS;

typedef struct _JULL_INFO
{
    long jullTimeA;
    long jumpTime;
    long jullTimeB;
    long countDown;
    long bakJa;
}JULL_INFO;


typedef	struct
{
	char score_load_ok;
	char MusicScoreDrawON;
	int ScoreDrawEnable;//Draw Cursor Flag
	//int TitleOff_Send;
	int Jni_Key_Flag;
	int Jni_Melody_Flag;
	int Jni_LoadFlag;
	int jump_Flag;
	int jump_Send;
	int staffReady;
	int scoreReady;
	int count_down;
	int jullStop;
	int rhythm_seq;
	int chorus;
}FLAG;

class midi_doc
{

public:
    PLAYMIDI_STATUS PLY_STATUS;
    MIDIClock* pMIDIClock;
	MIDIData *pMIDIData;
	MIDITrack *ScoreTrack;
   // crypt *pCrypt;

    FLAG lflag;//각종플레그
	KEY_VALUE KeyInfo;
    TRACK_INFORMATION TRACK_Info;

    STAFF_MODE staff_mode[4];
    STAFF_MODE *pSaffMode;

    vector<MIDI_TITLE> vSongTitle;
	vector<LYRIC_NOTE> vLyricNote;
	vector<string> vLyricStr;
	vector<UNI_STRING> vUniStr;
	vector<EVENT_LIST> vPlayMidiEvent;
	vector<EVENT_LIST> vPlayRhythmEvent;

    vector<LINE_INFORMATION>vLi;
    vector<string> vStrScore;
    vector<string> v_staff;
    vector<JULL_POS>vJullPos;

    vector<JULL_INFO>vJullInfo;
    vector<long> vCounDown;
	vector<long> vLricTick;
    vector<long>vMadiKeyInfo;


    vector <string> vPCM_Chorus;

	SongInfoW songInfo;
    MDK_TAIL mdk_table;

	int EventListCnt;
	long Oldltime;

	//char  staff_char[2048][64];
	//char score_char[512][64];
	//int staff_cnt;
	//int score_cnt;

    int  lCursorLen;
    long DrawingFag;
    int ScoreCursorEnable;//Draw Cursor Flag
    int curPageLastLtime;
    int curPageBeginLtime;

//hymn......
	long JuelPoint[32];
	int  JuelLine[32];
	int  CurjuelEvent;
	Uint16 lDw[256];
//=======================
//	static void *t_function(void *data);
//	static void timer_handler (int signum)	;
	//=================

	vector<string> split(string data, string token);
	string ReplaceAll(string &str, const std::string& from, const std::string& to);
	void init_satffMode();
	void set_satffMode(int type);
	//void vPCM_Chorus_clear(void);
	int MIDIDOC_Create(char *filename);
	int MIDIDOC_mkSplitFile(char *filename, int kind);
	int MIDIDOC_GetKindFile(char *filename);
	void DK_GetSongKey(void);
	void DK_MK_FrontLedMarker();
    void DK_LoadJull(void);
    void DK_LoadCountDown(void);
	void DK_LoadChorus(MIDITrack* pMIDITrack, char* file);

	int linecntW(Uint16 *s);
	//Uint16 *b2w_CU(char *s,int Code);
	Uint16* b2w_CU(const char *s,  int Code);
	int make_utf(unsigned char *q, Uint16 k);
	int uni2utf(Uint16 *unicode, unsigned char *utf8);
	Uint16 *UNICODE_Null(Uint16 *s, int Sz);
	int     UNICODE_strLengh(Uint16 *s);
	void wStringcpy(Uint16* C, Uint16 *A, Uint16 *B);
	Uint16* wStringTrim(Uint16* s);
//	int Update_Hymn(char *filename);
//	int Update_Inbar(char *filename);
	int make_Duration(long *Tbl_Lyric, int line_cnt);


    int Put_LineInfo(vector<LYRIC_NOTE> vNoteTick);
	int Put_CharPos(int line);
    vector<LYRIC_NOTE> MK_LyricBlock(vector<UNI_STRING> vmDispUniStr, vector<LYRIC_NOTE> vNoteTick);
    vector<LINE_INFORMATION> MK_LineInfor(vector<LYRIC_NOTE> vLyricNote);

	int GetDisplayLine(long ltime);
	int GetEraseLine(long ltime);
	int GetNextLine(long ltime);
//	GetNextLine
	static int compareX(const void* a, const void* b);
	void remove_event(MIDITrack* pMIDITrack);
	void put_ctrl_event(MIDIData* ctrlMIDIData);
	void put_drum_event(vector<MIDIEvent*> vEvent);
	void put_bass_event(vector<MIDIEvent*> vEvent);
	int  put_rhyevent(char *pattern_file, char *ctrl_file );
	int  Make_Rhythm_pattern();
	void Make_EventList();
	//void Make_Rhythm_EventList();

	int JamakGetEreseBlkId( int _curr_Line, int _curr_ltime);
	//int mkTimeTableNoteON(MIDIData* pMIDIData);
    int mkTimeTableNoteON(MIDITrack *pMIDITrack);

    bool Decrypt(char *source, char* destination, long length);

	midi_doc();
	~midi_doc();

};
#endif