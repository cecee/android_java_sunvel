//////////////////////
#ifndef __FRONT_H__
#define __FRONT_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
using namespace std;

class front
{
public:
    char frontVersion[32];
    vector<char> vFRONTMessage;
    vector<char> vFRONTKey;
    vector<long> vFRONTKeyBuffer;

    int kind;//0: version 1:Keydata

public:
    void FRONT_getVersion(void);
    void FRONT_sigfrom_front(void);
    void startMidiFled(void);
    void stopMidiFled(void);
	front();
	~front();
};

#endif
