#ifndef __MIDI_MAIN_H__
#define __MIDI_MAIN_H__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <vector>
#include "lxMIDIData/lxMIDIData.h"
#include "midi_document.h"

//#define MAX_SIZE 128// max size of queue
#define TRUE 1
#define FALSE 0
typedef unsigned char   Uint8;
/*
// queue struct
typedef struct {
  MIDIEvent* queue[MAX_SIZE];
    int front;
    int rear;
    int count;
  }Queue;
  */
class midi_main
{
    public:

	int loaded_timer;
    int mic_flag;

	int midi_LoadMusic(char *infile);
	int midi_UnLoadMusic(void);
	int midi_PlayMusic(char *infile);
	void midi_MelodyAdj(int value );
	void MessageOut(EVENT_LIST listEvent);
	void MessageOut_Null(EVENT_LIST listEvent);

	void MessageInsert(void);
	void sysExKeyAdj(int key );
	void rhythm_seq(void);

    void writerMidiNFC( Uint8 data, int length);

    void FRONT_getVersion(void);
    void FRONT_sigfrom_front(void);

	midi_main();
	~midi_main();

};
#endif