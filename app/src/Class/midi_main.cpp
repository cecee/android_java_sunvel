﻿//ndk-build
//ant debug
//install -r bin\SDLActivity-debug.apk

#include "midi_main.h"
#include "midi_timer.h"
#include "extern.h"
#include <jni.h>

midi_main::midi_main(){
    mic_flag=0;
 //   vFRONTSerial.clear();
 //   vFRONTMessage.clear();
  //  FRONT_getVersion();//front firm ware version 일어둠
}

midi_main::~midi_main(){
// 	TRACE("#@#=======Kill thread pthread_join(&p_thread, NULL, midi_PlayThread, this)===\n");
/*
	if(pDoc!=NULL){
        pDoc->PLY_STATUS.IsPlay=0; //stop playing
	    if(pDoc->pMIDIClock!=NULL){
            MIDIClock_Stop(pDoc->pMIDIClock);
            MIDIClock_Reset(pDoc->pMIDIClock);
            MIDIClock_Delete(pDoc->pMIDIClock);
            pDoc->pMIDIClock=NULL;
        }
   }
 */
  //  MIDIIO_GS_reset();
}

int midi_main::midi_UnLoadMusic(void)
{
   TRACE("#@#=======midi_UnLoadMusic !!!!!==============\n");
    pFRONT->stopMidiFled();
    MIDIClock_Stop(pDoc->pMIDIClock);
    MIDIClock_Reset(pDoc->pMIDIClock);
    MIDIClock_Delete(pDoc->pMIDIClock); pDoc->pMIDIClock=NULL;
    MIDIData_Delete(pDoc->pMIDIData);   pDoc->pMIDIData=NULL;
    MIDIIO_All_NoteOff();
    MIDIIO_GS_reset();

    //pMidi_timer->TimerSet(false);
   return 1;
}


int midi_main::midi_PlayMusic(char *infile)
{
   	int rtn;
   	long lTimeMode,lTimeResolution;
	pDoc->PLY_STATUS.IsPlay=0;
	CurrentScoreKey=0;
	memcpy(pDoc->PLY_STATUS.path, infile, 256);
	rtn = pDoc->MIDIDOC_Create(infile);

   TRACE("#@#>>>>>>midi_PlayMusic MIDIDOC_Create[%s] rtn[%d]\n", infile, rtn);
    if(rtn==0) return 0;
 	MIDIData_GetTimeBase (pDoc->pMIDIData, &lTimeMode, &lTimeResolution);
    pDoc->PLY_STATUS.lTimeResolution = lTimeResolution;
    if(pDoc->pMIDIClock!=NULL)
    {
        MIDIClock_Stop(pDoc->pMIDIClock);
        MIDIClock_Reset(pDoc->pMIDIClock);
        MIDIClock_Delete(pDoc->pMIDIClock);
        pDoc->pMIDIClock=NULL;
    }
    pDoc->pMIDIClock = MIDIClock_Create (MIDICLOCK_TPQNBASE, lTimeResolution, 60000000/100);//mp3 song기준
    MIDIClock_Stop(pDoc->pMIDIClock);
    MIDIClock_Reset(pDoc->pMIDIClock);
    pMidi_timer->TimerSet(true);
    MIDIClock_SetSpeed(pDoc->pMIDIClock, 10000);
    pDoc->EventListCnt=0;
    pDoc->PLY_STATUS.IsPlay=1;
    MIDIIO_GS_reset();
    pFRONT->startMidiFled();
//------------------------------------------
   return 1;
}


void midi_main::MessageOut(EVENT_LIST listEvent )
{
    unsigned char byMessage[64];
    long mLineNo, mVelocity;
    long midi_key;
    char pData[32]={0,};
    char sNum[2];

    long lLen,lport;
    MIDIEvent *pMIDIEvent;
    pMIDIEvent=listEvent.PlayEvent;
    lport=listEvent.port;
	long kind=MIDIEvent_GetKind(pMIDIEvent);

//	TRACE("#@# MIDIEvent_GetKind=====[%X] \n", kind);
    if (MIDIEvent_IsTempo ( pMIDIEvent)) {
        long lTempo = MIDIEvent_GetTempo (pMIDIEvent);
        //TRACE("#@# Tempo MIDIEvent_IsTempo=====lTempo[%ld] \n", lTempo);
        MIDIClock_SetTempo (pDoc->pMIDIClock, lTempo);
     }
    else if (MIDIEvent_IsMarker( pMIDIEvent) && lport==15)
    {
         memset(pData,0,32);
         MIDIEvent_GetText(pMIDIEvent,pData,32);

         //TRACE("#@#chorus ======MIDIEvent_IsMarker==== pData[%s] \n", pData);
         if(pData[0]=='c'){
            sNum[0]=pData[1]; sNum[1]=pData[2];
           // int id= pData[2]-'0';
           int id=atoi(sNum);
            pDoc->lflag.chorus=id;
     #ifdef DBG_MDK
         TRACE("#@#chorus MIDIEvent_IsMarker== idx[%x]\n",id);
     #endif
             //MIDI_Pcm(pDoc->vPCM_Chorus[id]);
            //TRACE("#@#DK======MIDIEvent_IsMarker==== pDoc->vPCM_Chorus[%d][%x][%d] \n",id, pDoc->vPCM_Chorus[id].pcm_buffer,pDoc->vPCM_Chorus[id].length );
         }
    }
     else if (MIDIEvent_IsSysExEvent (pMIDIEvent))
     {
          lLen = MIDIEvent_GetLen (pMIDIEvent);
          MIDIEvent_GetData (pMIDIEvent, byMessage, 64);
          if(byMessage[6]==0x00 && byMessage[7]==0x7f && byMessage[8]==0x00 && byMessage[9]==0x41 ){;}//reset는 하지 않음
          else MIDIIO_Put_n(lport,byMessage,lLen);
        //  MIDIIO_Put_n(lport,byMessage,lLen);
	 }

     else if (MIDIEvent_IsMIDIEvent (pMIDIEvent))
     {
          lLen = MIDIEvent_GetLen (pMIDIEvent);
          long ch= MIDIEvent_GetChannel (pMIDIEvent);
          MIDIEvent_GetData (pMIDIEvent, byMessage, 64);
         if(lport==0 || lport==1) MIDIIO_Put_n(lport, byMessage,lLen);
         if(lport==15 && ch==0) {
             if(byMessage[0]==0xb0)  {
                MIDIIO_Put_n(lport, byMessage,lLen);//for FLED
            }
         }
        if( (ch==(pDoc->PLY_STATUS.lMainMelodyChannel)&0xff) && lport==0){
               if(MIDIEvent_IsNoteOn(pMIDIEvent))
              {
                long velocity = MIDIEvent_GetVelocity(pMIDIEvent);
                long key = MIDIEvent_GetKey(pMIDIEvent);
                mLineNo = key + pDoc->PLY_STATUS.lCurKeyPlus;
                midi_key= CurrentScoreKey&0x0000FFFF;
                mLineNo=(mLineNo<<24)&0xff000000;
                mVelocity=(velocity<<16)&0x00ff0000;
                midi_key= midi_key + mLineNo+ mVelocity;
                CurrentScoreKey=midi_key;
               }
              else if(MIDIEvent_IsNoteOff(pMIDIEvent))
              {
                    CurrentScoreKey = CurrentScoreKey&0x0000FFFF;
              }
          }
     }
}


void midi_main::MessageOut_Null(EVENT_LIST listEvent )
{
    unsigned char byMessage[64];
    long lLen,lport;
    MIDIEvent *pMIDIEvent;
    pMIDIEvent=listEvent.PlayEvent;
    lport=listEvent.port;
    lLen = MIDIEvent_GetLen (pMIDIEvent);
    MIDIEvent_GetData (pMIDIEvent, byMessage, 64);
    if(lport<2 && byMessage[0]>=0xA0)
    {
        if(byMessage[6]==0x00 && byMessage[7]==0x7f && byMessage[8]==0x00 && byMessage[9]==0x41 ){;}//reset는 하지 않음
        else MIDIIO_Put_n(lport, byMessage,lLen);
    }
}

void midi_main::MessageInsert(void)
{
	  if(pDoc->lflag.Jni_Key_Flag==1)
	  {
	  		sysExKeyAdj (pDoc->PLY_STATUS.lCurKeyPlus);
	  		pDoc->lflag.Jni_Key_Flag=0;
	  }
	if(pDoc->lflag.Jni_Melody_Flag==1)
	{
		TRACE("#@# Melody ADJUST==============ch[%d] [%d]\n", pDoc->PLY_STATUS.lMainMelodyChannel, pDoc->PLY_STATUS.lCurMelody);
		midi_MelodyAdj(pDoc->PLY_STATUS.lCurMelody);
		pDoc->lflag.Jni_Melody_Flag=0;
	}

	if(pDoc->lflag.Jni_LoadFlag ==1)
	{
		sysExKeyAdj (pDoc->PLY_STATUS.lCurKeyPlus);//key
		midi_MelodyAdj(pDoc->PLY_STATUS.lCurMelody);//mainMelodyVolume
		MIDIIO_SetNRPN(0x37, 0x07, MIDI_Master_Volume);// 이유는 모르겠지만 볼륨이 안먹은 경우가 있어서..... 나중에 꼭 찾아 봐야할것...
		pDoc->lflag.Jni_LoadFlag=0;
	}
}

void midi_main::sysExKeyAdj(int key )
{
 	Uint8 sysExKey[11]={0xf0, 0x41, 0x10, 0x42, 0x12, 0x40, 0x00, 0x05, 0x40, 0x63, 0xf7};

	sysExKey[8] = 0x40+key;
	TRACE("#@#####sysExKeyAdj [%d] sysExKey[8]=[%x]\n", key, sysExKey[8] );
	MIDIIO_Put_n(0, sysExKey,11);//Port1 Key adj
	MIDIIO_Put_n(1, sysExKey,11);//Port2 Key adj
}

void midi_main::midi_MelodyAdj(int value )
{

    Uint8 melodyVolume[3]={0,};
    if(value>=127) value=127;
    else if(value<0) value=0;
    melodyVolume[0]= 0xB0 + (Uint8)pDoc->PLY_STATUS.lMainMelodyChannel;//ch
    melodyVolume[1]= 0x07 ;//ctl 07
    melodyVolume[2]= (Uint8)value ;//
    TRACE("#@#vv midi_MelodyAdj ch[%d] value[%d]\n",(Uint8)pDoc->PLY_STATUS.lMainMelodyChannel, (Uint8)value);
    MIDIIO_Put_n(0, melodyVolume,3);
}



void midi_main::rhythm_seq()
{
  //  pDoc->lflag.jump_Flag=0;
  #ifdef DBG_RHY
   //  TRACE("#@#DBG_RHY rhythm_seq--------- pDoc->lflag.rhythm_seq[%d]\n",pDoc->lflag.rhythm_seq);
  #endif
    if(pDoc->lflag.rhythm_seq==0) return;

    switch(pDoc->lflag.rhythm_seq)
    {
       case 1:
           // SetMIDI_Volume(0);
            pDoc->lflag.rhythm_seq=2;
            pDoc->Make_EventList();
            pDoc->lflag.jump_Flag=3;
            break;
        case 2:
             pDoc->lflag.rhythm_seq=3;
            break;
        case 3:
             pDoc->lflag.rhythm_seq=0;
             //SetMIDI_Volume(0x7f);
            break;
        default:
            break;

    }
}

//////////////////////////////////////////