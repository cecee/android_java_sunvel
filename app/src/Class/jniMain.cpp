#include "com_cecee_imove_JniFunction.h"
#include <jni.h>
#include "extern.h"

#include <string.h>
#include <stdio.h>
#include <android/log.h>
#include <pthread.h>

#include <string>
#include <string.h>
#include <stdio.h>

#if 0
//#include <zip.h>
//#include "alsaplayer.h"
#include <memory>
#include <fstream>
#include <iostream>
#endif

using namespace std;
//std::shared_ptr<Ksubox::AlsaPlayer> aplayer;
//--------------------------------------------
long  timer_sigfrom_mic(void);
void  SetMIDI_Volume(int vol);

int mic_flag=0;
int pcmStart_flag=1;
float chorusVolume;

long CurrentScoreKey;
vector<string>v_staff;
vector<char> vMicSig;

//--------------------------------------------

#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

 static pthread_mutex_t thread_mutex;
 pthread_t p_thread;
 pthread_attr_t attr;

 pthread_t Score_thread;
 pthread_attr_t Score_attr;
 char draw_page;

char query_buf[1024];

midi_main *pMIDI_MAIN;
midi_doc *pDoc;
music_score *pMusic_score;
draw_score *pDraw_score;
midi_timer *pMidi_timer;
coco *pCoco;
front *pFRONT;

#if 0
char* codeTable[4][12] =
{{ (char*)"C", (char*)"Db", (char*)"D", (char*)"Eb", (char*)"E", (char*)"F", (char*)"Gb", (char*)"G", (char*)"Ab", (char*)"A", (char*)"Bb", (char*)"B"},
 { (char*)"C",(char*)"C#",(char*)"D",(char*)"D#",(char*)"E",(char*)"F",(char*)"F#",(char*)"G",(char*)"G#",(char*)"A", (char*)"A#", (char*)"B" },
 { (char*)"Cm",(char*)"Dbm",(char*)"Dm",(char*)"Ebm",(char*)"Em",(char*)"Fm",(char*)"Gbm",(char*)"Gm",(char*)"Abm",(char*)"Am", (char*)"Bbm", (char*)"Bm"},
 { (char*)"Cm",(char*)"C#m",(char*)"Dm",(char*)"D#m",(char*)"Em",(char*)"Fm",(char*)"F#m",(char*)"Gm",(char*)"G#m",(char*)"Am", (char*)"A#m", (char*)"Bm" }
};
#endif

int end_flag;

JavaVM *j_vm;
JNIEnv *j_env;
JNIEnv *_env;

jobject j_obj;
static jclass jNativesCls;
jclass j_cls;
jmethodID android_call;
jmethodID mjmethod;
//int seial_fd;

int JNI_OnLoad(JavaVM* vm, void* reserved) {
	j_vm = vm;
	j_vm->GetEnv((void**) &j_env, JNI_VERSION_1_6);
	j_cls = j_env->FindClass( "com/cecee/imove/JniFunction");
	android_call = j_env->GetMethodID(j_cls, "FromJNIThread", "(I)V");
    j_obj = j_env->NewGlobalRef(j_env->NewObject(j_cls, android_call));
	return JNI_VERSION_1_6;
}

void JNI_OnUnload(JavaVM *vm, void *reserved) {
  // j_env->DeleteGlobalRef(j_obj);
}


int  QuerySendToJava(int kind)
{
//    j_vm->AttachCurrentThread(&j_env, NULL);
	//jNativesCls = _env->FindClass("com/cecee/imove/JniFunction");
	//mjmethod = _env->GetStaticMethodID (jNativesCls, "QueryFromC", "(I)V" ); // <- 특히 이놈 설정 주의!!
	//_env->CallStaticVoidMethod( jNativesCls, mjmethod, kind);
	j_env->CallVoidMethod(j_obj, android_call, kind);///For Current Tick
//	j_vm->DetachCurrentThread();
	return 1;
}

void  SetMIDI_Volume(int vol)
{
//MIDI Synth Master volume 0 (mute) to 7Fh (max) 3707h

 if(vol>127)vol=127;
 if(vol<0)vol=0;
 MIDIIO_SetNRPN(0x37, 0x07, vol);
}

long  timer_sigfrom_mic(void)
{
	int i;
	int res=-1;
	int key,velocity,detune,tmp,lineNum;
	char data[128];
	int vdata;
	long mic_key;
    long ScoreKey;

	memset(data,0,128);
	res = read(TTY_MIDI_Port0, data, 128);
	if(res<=0) return 0;
	//TRACE("#@# midi_in res[%d]\n", res);
	for(i=0;i<res;i++){
		TRACE("#@# sig_mic_in[%d/%d]-[0x%02x]\n", i, res ,data[i]);
		 vMicSig.push_back(data[i]);
	}
	for(;;)
	{
		if(vMicSig.empty()) break;
		vdata=vMicSig.front();
        vMicSig.erase(vMicSig.begin());
 		switch(vdata)
 		{
            case 0x90:  mic_flag=1;  break;
            case 0xb0:  mic_flag=2;  break;
            case 0xe0:  mic_flag=3;  break;
            case 0x80:  mic_flag=4;  break;
           default:
            switch(mic_flag)
            {
                case 1: //note on
			        ScoreKey= CurrentScoreKey & 0xFFFF00FF;
			      	lineNum = (pDoc!=NULL) ? pDraw_score->KeytoLineNo(vdata, pDoc->PLY_STATUS.KeySignature): vdata;
                    lineNum=(lineNum<<8)&0xff00;
                    CurrentScoreKey= ScoreKey + lineNum;
                 //   TRACE("#@# sig_mic_in note_on---vdata[%x] lineNum[%04x] CurrentScoreKey[%08x]\n",vdata, lineNum, CurrentScoreKey);
			        mic_flag=0;
                    break;
                case 2:
                    if(vdata==0x07) mic_flag=0x10;
                    break;
                case 3:
                    detune=vdata;
                    mic_flag=0;
                    break;
                case 4:  //note off
                    ScoreKey= CurrentScoreKey&0xFFFF00FF;
                    CurrentScoreKey=ScoreKey;
                    mic_flag=0;
                    break;
                case 0x10:
                   // velocity=data;
                    TRACE("#@# mic_velocity[0x%02x]\n",vdata);
                    ScoreKey= CurrentScoreKey&0xFFFFFF00;
                    mic_key=ScoreKey+(vdata&0xff);
                    CurrentScoreKey=mic_key;
                    mic_flag=0;
                    break;
            }
            break;
        }
	}
	//TRACE("#@# sig_mic_in CurrentScoreKey[%08x]\n",CurrentScoreKey);
	return CurrentScoreKey;
}

int checkUtfString(const char* bytes)
{
    const char* origBytes = bytes;

    if (bytes == NULL) {
        return 0;
    }

    while (*bytes != '\0') {
        unsigned char utf8 = *(bytes++);
        // Switch on the high four bits.
        switch (utf8 >> 4) {
            case 0x00:
            case 0x01:
            case 0x02:
            case 0x03:
            case 0x04:
            case 0x05:
            case 0x06:
            case 0x07: {
                // Bit pattern 0xxx. No need for any extra bytes.
                break;
            }
            case 0x08:
            case 0x09:
            case 0x0a:
            case 0x0b:
            case 0x0f: {
                /*
                 * Bit pattern 10xx or 1111, which are illegal start bytes.
                 * Note: 1111 is valid for normal UTF-8, but not the
                 * modified UTF-8 used here.
                 */
                TRACE("#@# JNI WARNING: illegal start byte 0x%x\n", utf8);
                goto fail;
            }
            case 0x0e: {
                // Bit pattern 1110, so there are two additional bytes.
                utf8 = *(bytes++);
                if ((utf8 & 0xc0) != 0x80) {
                    TRACE("#@#JNI WARNING: illegal continuation byte 0x%x\n", utf8);
                    goto fail;
                }
                // Fall through to take care of the final byte.
            }
            case 0x0c:
            case 0x0d: {
                // Bit pattern 110x, so there is one additional byte.
                utf8 = *(bytes++);
                if ((utf8 & 0xc0) != 0x80) {
                    TRACE("#@#JNI WARNING: illegal continuation byte 0x%x\n", utf8);
                    goto fail;
                }
                break;
            }
        }
    }
    return 1;
fail:
    TRACE("#@#Error--string: '%s'\n", origBytes);
	return 0;
}

#if 1
 void *threadLoop(void *data)
{
    j_vm->AttachCurrentThread(&j_env, NULL);
    while(end_flag)
    {
       pthread_mutex_lock(&thread_mutex);
       pthread_mutex_unlock(&thread_mutex);
//   TRACE("#@#000Thread Looping.......j_env[%x]\n", j_env);
       if(pDoc!=NULL){
         if(pDoc->lflag.chorus > 0){
         QuerySendToJava( pDoc->lflag.chorus + 0xc0 );
#ifdef DBG_MDK
            TRACE("#@#Chorus_Event== idx[%x]\n",pDoc->lflag.chorus);
#endif
         pDoc->lflag.chorus = -1;
         }
         else if(pcmStart_flag==0 || pcmStart_flag==1 ){
             QuerySendToJava( 0xfc );
             pcmStart_flag=-1;
         }
        }
    	usleep(20);
    }
   j_vm->DetachCurrentThread();
//    LOGE("Thread Loop Exiting");
     pthread_exit(NULL);
}

int start_thread(){
    int ret;
    if(p_thread < 1)
    {

		//	if(pthread_mutex_init(&thread_mutex, NULL) != 0) LOGE( "Error initing mutex" );
	 		if( pthread_create(&p_thread, NULL, threadLoop, NULL) == 0)
            {
                TRACE( "Started thread#: %ld\n", p_thread);
              //  if( pthread_attr_destroy(&attr) != 0 ) return -1;
                if(pthread_detach(p_thread)!=0) LOGE( "Error detaching thread" );
            }
            else      LOGE( "Error starting thread" );

    pthread_join(p_thread, (void**)&ret);
    }
	return 1;
}

JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_JNIThread  (JNIEnv *env, jobject obj, jint flg)
{
	//end_flag==0 stop condition;
	//flg==1 run
	if(end_flag) {
		if(flg) return;
		else end_flag=0;
	}
	else
	{
		if(flg) {
			start_thread();
			end_flag=1;
		}
		else end_flag=0;
	}

}

JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_startThread  (JNIEnv *env, jobject obj)
{
//	TRACE("#@# Java_com_cecee_imove_JniFunction_startThread env[%x]\n", env);
	start_thread();
}
#endif


JNIEXPORT jstring JNICALL Java_com_cecee_imove_JniFunction_MessageFromC  (JNIEnv *env, jobject obj, jstring msg)
{
//	char buf[256];
	const char *str = env->GetStringUTFChars( msg, 0);
//   TRACE("JNIEXPORT==>%s", str);
//	TRACE("#@# JniFunction_MessageFromC[%s]\n",query_buf);
   	env->ReleaseStringUTFChars( msg, str);
	return env-> NewStringUTF(query_buf);
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIOpenSerial (JNIEnv *env, jobject obj, jint arg)
{
	_env=env;//꽁수로 call_java_test함수의 env못만들어서.....
	MIDIIO_OpenSerial();
	MIDIIO_GS_reset();
	if(pMidi_timer!=NULL) delete pMidi_timer;
	pMidi_timer = new midi_timer(); //start midi timer
	pCoco=new coco();
	pFRONT=new front();
   return 1;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDICloseDevice(JNIEnv *env, jobject obj)
{
	end_flag=0;
	pMidi_timer->TimerSet(false);
	if(pMusic_score != NULL) delete pMusic_score;
	if(pDraw_score != NULL) delete pDraw_score;
	if(pMIDI_MAIN != NULL)   delete pMIDI_MAIN;
	pMusic_score=NULL;
	pDraw_score=NULL;
	pMIDI_MAIN=NULL;
	return 1;
}
 //   public native int MIDIOpen(String path, int LyricCode, int key, int speed, int MelodyVol);
JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIOpen  (JNIEnv *env,  jobject obj,  jstring path, jint LyricCode, jint key, jint speed, jint MelodyVol)
{
    int rtn;
    char mPath[256];
    const char *cString = env->GetStringUTFChars(path, 0);
///////////////////++++++++
    pMIDI_MAIN = new midi_main();
    pDoc = new midi_doc();
///////////////////-------------
    sprintf(mPath,"%s",cString);
    env->ReleaseStringUTFChars(path, cString);
    TRACE("#@#==========Java_com_cecee_imove_JniFunction_MIDIOpen=====path[%s]\n",mPath);
    //------------
    vMicSig.clear();
    pDoc->PLY_STATUS.lSong_Kind=0;//MIDI
	pDoc->PLY_STATUS.lCurKeyPlus = key;
	pDoc->PLY_STATUS.lCurSpeed = speed;
	pDoc->PLY_STATUS.LyricCode = LyricCode;
	pDoc->PLY_STATUS.lCurMelody = MelodyVol;
	CurrentScoreKey=0;
	SetMIDI_Volume(0x7f);
   	rtn = pMIDI_MAIN->midi_PlayMusic(mPath);
   	if(rtn==0){
   	    pMidi_timer->TimerSet(false);
   	    if(pMIDI_MAIN) delete pMIDI_MAIN;
   	    pMIDI_MAIN=NULL;
   	    if(pDoc) delete pDoc;
   	    pDoc=NULL;
   	}
    return rtn;
  }

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineSex  (JNIEnv *env, jobject obj)
{
       int sz=pDoc->PLY_STATUS.totalLyricLine;
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vLi[i].LineSex;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIUnload (JNIEnv *env, jobject obj)
{
    vMicSig.clear(); vector<char>().swap(vMicSig);
    pDoc->PLY_STATUS.IsPlay = 0;
    MIDIClock_Stop(pDoc->pMIDIClock);
    pMIDI_MAIN->midi_UnLoadMusic();
    TRACE("#@####C++ MIDI unloading...\n");
    if(pMusic_score != NULL) delete pMusic_score;  pMusic_score=NULL;
    if(pDraw_score != NULL) delete pDraw_score; pDraw_score=NULL;
    if(pMIDI_MAIN != NULL)   delete pMIDI_MAIN; pMIDI_MAIN=NULL;
    if(pDoc != NULL) delete pDoc; pDoc=NULL;
    return 1;
}


 JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_TimeToPosition(JNIEnv *env, jobject obj, jint line,  jint ltime)
 {

 	if(pDoc==NULL || line<0 || ltime<0) return -1;
 	int i,ix;
 	int earse_point=-1;
 	int BlkId=-1;
  	long timeA = pDoc->vLi[line].Line_StartTime;
 	long timeB = pDoc->vLi[line].Line_EndTime;

 	int lbcnt= pDoc->vLi[line].Blk_Cnt;
 	if(lbcnt<1) return -1;
 	int line_length=pDoc->vLi[line].vLyNote[lbcnt-1].lposB;
 	int line_endTime=pDoc->vLi[line].Line_EndTime;

 	int bk_ltime[32]={0,};
    for(i=0;i<lbcnt;i++) bk_ltime[i] = pDoc->vLi[line].vLyNote[i].ltimeA;
    bk_ltime[i++]= pDoc->vLi[line].Line_EndTime;

    BlkId=-1;
    if( timeA <=ltime && ltime <timeB){
        for(ix=0;ix<(lbcnt+1);ix++){
            if( bk_ltime[ix] >= ltime) {  BlkId=ix-1;   break; }
        }
    }
   // TRACE("#@#@@@ line[%d(%d-%d)] lbcnt[%d/%d] ltime[%d] bk_ltime[%d][%d][%d][%d][%d][%d][%d][%d]\n",line, timeA,timeB, BlkId,lbcnt, ltime, bk_ltime[0],bk_ltime[1],bk_ltime[2],bk_ltime[3],bk_ltime[4],bk_ltime[5],bk_ltime[6],bk_ltime[7]);
   if(BlkId<0) return -1;

    if(BlkId == 0) {
		earse_point = ((ltime - pDoc->vLi[line].vLyNote[0].ltimeA) * pDoc->vLi[line].vLyNote[BlkId].ldelta) / 1000;
  	}
	else if(BlkId<0){
		if(line_endTime<=ltime) earse_point=line_length+10;
		else  earse_point=-1;
	}
	else
	{
      earse_point = ((ltime - pDoc->vLi[line].vLyNote[BlkId].ltimeA) * pDoc->vLi[line].vLyNote[BlkId].ldelta) / 1000;
      earse_point = earse_point + pDoc->vLi[line].vLyNote[BlkId-1].lposB;
	}
  	return earse_point;
 }

 JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCurrentTick  (JNIEnv *env, jobject obj)
 {
		 if(pDoc==NULL) return 0;
		 return pDoc->PLY_STATUS.lTickCount;
 }

 JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCurrentTime  (JNIEnv *env, jobject obj)
 {
		 if(pDoc==NULL) return 0;
		 return MIDIData_TimeToMillisec(pDoc->pMIDIData, pDoc->PLY_STATUS.lTickCount);
 }

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCurrentPage(JNIEnv *env, jobject obj)
{
    if(pDraw_score == NULL) return -1;
    long curr_time=pDoc->PLY_STATUS.lTickCount;
   // if(pDoc->PLY_STATUS.hasMelody_track==0)   return -1;
    return pDraw_score->lCurPage = pDraw_score->GetCurrentPage(curr_time);

}


 JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCursorPos(JNIEnv *env, jobject obj)
 {
   // if(pDoc->PLY_STATUS.hasMelody_track==0)   return -1;
    if(pDraw_score == NULL) return -1;
    return pDraw_score->Get_CursorPos(pDoc->PLY_STATUS.lTickCount);
 }

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_DrawScoreGetPosX(JNIEnv *env, jobject obj, jint tick)
 {
    if(pDoc==NULL || pDraw_score==NULL || pMusic_score==NULL) return -1;
    //if(pDoc->PLY_STATUS.hasMelody_track==0) return -1;
	return pDraw_score->TimeToScorelineX(tick);
 }

 void *threadMusicScoreDraw_View(void *data)
{
//   TRACE("#@# threadLoop mJavaVM[%x] android_call[%d]\n", mJavaVM, android_call);
   	j_vm->AttachCurrentThread(&j_env, NULL);
    //pthread_mutex_lock(&Score_thread_mutex);
	pDraw_score->MusicScoreDraw_View(draw_page, pDraw_score->ObliEnable);//0:악보만  1:악보+오브리
	//pthread_mutex_unlock(&thread_mutex);
	//pthread_mutex_unlock(&Score_thread_mutex);
   	j_vm->DetachCurrentThread();
     pthread_exit(NULL);
     pDoc->lflag.staffReady=2;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_DrawScorePage (JNIEnv *env, jobject obj, jint page)
{
	int ret;
    if(pDraw_score==NULL || pDoc == NULL || pDoc->PLY_STATUS.hasMelody_track==0) return 0;
	pDoc->v_staff.clear();
    pDoc->vStrScore.clear();
    pDoc->lflag.staffReady=0;
	if(pDoc->lflag.staffReady >0) return 0;//악보만들고 있거나 만들어져 있으면 리턴
    if(page<0) page= 0;

	pDoc->lflag.ScoreDrawEnable = page;
	pDraw_score->ScoreCursorEnable=0;
    draw_page=page;
    pDraw_score->MusicScoreDraw_View(draw_page, 0);//0:악보만  1:악보+오브리
#if 0
    if(Score_thread < 1)
    {
		if( pthread_create(&Score_thread, NULL, threadMusicScoreDraw_View, NULL) == 0)
		{
			if(pthread_detach(Score_thread)!=0) {
#ifdef DBG_JNI
                TRACE( "#@#Error detaching thread\n" );
#endif
            }
		}
		else {
#ifdef DBG_JNI
            LOGE( "Error starting thread" );
#endif
        }
		pthread_join(Score_thread, (void**)&ret);
    }
    Score_thread=NULL;
#endif
    pDoc->lflag.staffReady=2;
	return 1;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_DrawScoreIsReady (JNIEnv *env, jobject obj)
{
    if(pDoc==NULL) return NULL;
    return pDoc->lflag.staffReady;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_DrawScoreClearReady(JNIEnv *env, jobject obj)
{
    if(pDoc==NULL) return NULL;
    pDoc->lflag.staffReady=0;
    return 1;
}




JNIEXPORT jobjectArray JNICALL Java_com_cecee_imove_JniFunction_GetStaffStringArray (JNIEnv *env, jobject obj)
{
    if(pDraw_score==NULL || pDoc == NULL) return NULL;
    int i,rtn;
    int sz=pDoc->v_staff.size();
    if(sz==0) return NULL;
    char *buf[sz];
    char str_buf[128];
    jobjectArray joa= (jobjectArray)env->NewObjectArray(sz, env->FindClass("java/lang/String"),env->NewStringUTF(""));
 	for(i=0;i<sz;i++){
        memset(str_buf,0,sizeof(str_buf));
        sprintf(str_buf,"%s",pDoc->v_staff[i].c_str());
#ifdef DBG_STAFF
    TRACE("#@#DBG_STAFF GetStaffStringArray[%d/%d][%s]\n",i,sz, str_buf);
#endif
        if(checkUtfString(str_buf)==0) continue;
        buf[i]=&str_buf[0];
        jstring j_file = env -> NewStringUTF(buf[i]);
        env->SetObjectArrayElement(joa,i,j_file);
        env->DeleteLocalRef(j_file);
  	}
    //memset(&pDoc->staff_char,0,sizeof(pDoc->staff_char));
   // pDoc->v_staff.clear();
	return joa;
}

JNIEXPORT jobjectArray JNICALL Java_com_cecee_imove_JniFunction_GetScoreStringArray (JNIEnv *env, jobject obj)
{
    if(pDraw_score==NULL || pDoc == NULL) return NULL;
    int i,sz;
    string str;
    sz=pDoc->vStrScore.size();
    char *buf[sz];
    char str_buf[128];
    jobjectArray joa= (jobjectArray)env->NewObjectArray(sz, env->FindClass("java/lang/String"),env->NewStringUTF(""));
    for(i=0;i<sz;i++){
        memset(str_buf,0,sizeof(str_buf));
        strcpy(str_buf,pDoc->vStrScore[i].c_str());
#ifdef DBG_SCORE
    TRACE("#@#DBG_SCORE GetScoreStringArray[%d/%d][%s]\n",i,sz, str_buf);
#endif
        if(checkUtfString(str_buf)==0) continue;
        buf[i]=&str_buf[0];
        jstring j_file = env -> NewStringUTF(buf[i]);
        env->SetObjectArrayElement(joa,i,j_file);
        env->DeleteLocalRef(j_file);
    }
    pDoc->vStrScore.clear();
    return joa;
}


JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLyricLine  (JNIEnv *env, jobject obj, jint flg)
 {
	if(pDoc==NULL) return NULL;
	int line=-3;
	long curr_time = pDoc->PLY_STATUS.lTickCount;
	switch(flg){
		case 0:
			line = pDoc->GetDisplayLine(curr_time);
			break;
		case 1:
			line = pDoc->GetEraseLine(curr_time);
			break;
		case 2:
			line = pDoc->GetNextLine(curr_time);
			break;
		case 3:
		    //if(pDoc->PLY_STATUS.hasMelody_track==0)   return -1;
			if(pDraw_score!=NULL)	{
				line=pDraw_score->lCurPage = pDraw_score->GetCurrentPage(curr_time);
			}else line=-1;
		 break;
		 case 4:
		    //if(pDoc->PLY_STATUS.hasMelody_track==0)   return -1;
		 	if(pDraw_score!=NULL)	return pDraw_score->Get_CursorPos(curr_time);
			else return -1;
		 case 40:
		    //if(pDoc->PLY_STATUS.hasMelody_track==0)   return -1;
		 	if(pDraw_score!=NULL)	return pDraw_score->TimeToScorelineX(curr_time);
			else return -1;
		case 5:
			line = pDoc->lflag.staffReady;
			break;
		case 6:
			line=pDoc->lflag.staffReady=0;
			break;
        case 7:
            //if(line==1) pDoc->lflag.scoreReady=0; //읽어갔기 때문에 현재 처리중 이라고 "0"
            line = pDoc->lflag.scoreReady;
            break;
        case 8:
            line=pDoc->lflag.scoreReady=0;
             break;
		default: break;

	}
	return line;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetSpeed  (JNIEnv *env, jobject, jint speed)
  {
  	if(pDoc==NULL) return NULL;
  	long curTempo;
  	int Xspeed;
  	if(pDoc->pMIDIClock == NULL) return 0;
   	pDoc->PLY_STATUS.lCurSpeed = speed;
  	Xspeed = 10000 + (100 * speed * 5);//5%씩 증가
  	MIDIClock_SetSpeed(pDoc->pMIDIClock, Xspeed);
   	long mTempo = 60000000L/ MIDIClock_GetTempo(pDoc->pMIDIClock);
  	return mTempo;
  }

 JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetJumpPostion  (JNIEnv *env,  jobject obj, jint pos)
  {
  	if(pDoc==NULL) return NULL;

  	long jmpPos_ltime;
  	long jmpPos_ltimeMs;
  	long pTempo;
  	int curr_Line=pDoc->PLY_STATUS.lCurrentLine;
  	int last_line = pDoc->PLY_STATUS.totalLyricLine-1;
  	int cur_ltime=pDoc->PLY_STATUS.lTickCount;
	int offset= 30;//pDoc->PLY_STATUS.lTimeResolution;

    TRACE("#@####C++ MIDISetJumpPostion[%d]=====curr_Line[%d]  last_line[%d] \n", pos, curr_Line,  last_line);
  	switch(pos)
  	{
  		case -1://이전줄
   			pDoc->lflag.jump_Flag=1;
   			if(cur_ltime <= pDoc->PLY_STATUS.LyricDisplay_ltime) jmpPos_ltime=  pDoc->PLY_STATUS.LyricDisplay_ltime - offset;//처음줄 지워지기전이면 첫줄로
  			else jmpPos_ltime = (pDoc->vLi[curr_Line-1].updn==-1)? pDoc->vLi[curr_Line-2].Line_StartTime - offset : pDoc->vLi[curr_Line-1].Line_StartTime - offset;
   			break;
 		case 0://무조건 첫음으로 Repeat기능
  			pDoc->lflag.jump_Flag=2; jmpPos_ltime= 0;	break;
  		case 1://다음줄
            pDoc->lflag.jump_Flag=1;
            if(curr_Line>=last_line) //현재 마지막줄
            {
                pDoc->lflag.jump_Flag=0;
                return -1;
            }
            else
            {
                if(cur_ltime <= pDoc->PLY_STATUS.LyricDisplay_ltime) jmpPos_ltime=  pDoc->PLY_STATUS.LyricDisplay_ltime - offset; //처음줄 지워지기전이면 첫줄로
                else jmpPos_ltime = (pDoc->vLi[curr_Line+1].updn==-1) ? jmpPos_ltime = pDoc->vLi[curr_Line+2].Line_StartTime - offset : pDoc->vLi[curr_Line+1].Line_StartTime - offset;
            }

  		    break;
  		 case 2: //A
  		 		pDoc->lflag.jump_Flag=0;
  		 		pDoc->PLY_STATUS.A_B[0] = cur_ltime;
  		 	return -1;
  		 case 3: //B
   				pDoc->lflag.jump_Flag=0;
  		 		pDoc->PLY_STATUS.A_B[1] = cur_ltime;
  		 		jmpPos_ltime = pDoc->PLY_STATUS.A_B[0];
  		 	break;
  		 case 4: //Clear A-B
  		 	pDoc->lflag.jump_Flag=0;
  		 	pDoc->PLY_STATUS.A_B[0] = pDoc->PLY_STATUS.A_B[1]=-1;
  		 	return -1;
  		case 5://무조건 첫줄
 			pDoc->lflag.jump_Flag=1;
 			jmpPos_ltime=  pDoc->PLY_STATUS.LyricDisplay_ltime;
  			break;

  		default: break;
  	}

  	TRACE("#@####C++ MIDISetJumpPostion[%d]=====curr_Line[%d]  last_line[%d] jmpPos_ltime[%ld]\n", pos, curr_Line,  last_line, jmpPos_ltime);

    //ppp 180701
 	if(jmpPos_ltime!=0) MIDIData_FindTempo(pDoc->pMIDIData,jmpPos_ltime,&pTempo);
 	else
 	{
 	    MIDIIO_GS_reset();
 	    pDoc->PLY_STATUS.lTickCount=0;
 	    MIDIClock_SetTickCount(pDoc->pMIDIClock, 0);
 	    return 0;
 	//    MIDIData_FindTempo(pDoc->pMIDIData,60,&pTempo);//60 tick 안에 tempo값 가져옴
 	}
 	MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
   	MIDIClock_SetTickCount(pDoc->pMIDIClock, jmpPos_ltime);
  	return   MIDIData_TimeToMillisec(pDoc->pMIDIData, jmpPos_ltime);
 }

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetNRPN  (JNIEnv *env, jobject obj, jint add, jint data)
{

	Uint8 nprn[4];
	nprn[0]= (Uint8)(add>>8)&0xff;
	nprn[1]= (Uint8)add&0xff;
	if(data>0x7F){
        nprn[2]= (Uint8) (data>>8)&0xff;//hi
        nprn[3]= (Uint8) data&0xff;//low
        MIDIIO_SetNRPN2(nprn[0], nprn[1], nprn[2], nprn[3]);
	}
	else{
        nprn[2]= (Uint8) data;
        MIDIIO_SetNRPN(nprn[0], nprn[1], nprn[2]);
	}

	return 1;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetSysKey (JNIEnv *env, jobject obj, jint key)
{

    if(pDoc==NULL) return NULL;
    pDoc->lflag.Jni_Key_Flag=1; //MIDI가 플레이 중일때 프래그 체크해서 셋팅
    pDoc->PLY_STATUS.lCurKeyPlus= key;
	return key;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetSfMi (JNIEnv *env, jobject obj)
{
    int sfmi;
    if(pDoc==NULL) return NULL;
    sfmi=(pDoc->PLY_STATUS.SF<<8)+pDoc->PLY_STATUS.SF;
    return sfmi;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetKey (JNIEnv *env, jobject obj)
{
    if(pDoc==NULL) return NULL;
    long  key_info;
    key_info= (pDoc->KeyInfo.Sex<<24)|(pDoc->KeyInfo.DataKey<<16)| (pDoc->KeyInfo.MaleKey<<8)|pDoc->KeyInfo.FemaleKey;
   TRACE("#@####C++ JniFunction_MIDIGetKey--key[%08lX] \n", key_info);

    return key_info;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_songLyricCode(JNIEnv *env, jobject obj)
{
    if(pDoc==NULL) return NULL;
    return pDoc->PLY_STATUS.LyricCode;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetMelodyChVolume (JNIEnv *env, jobject obj, jint value)
{
	 if(pDoc==NULL) return NULL;
	 pDoc->PLY_STATUS.lCurMelody = value;
	 pDoc->lflag.Jni_Melody_Flag=1; //
//	 TRACE("#@####C++ MIDISetMelodyChVolume  ch[%d]---Vol[%d] \n", pDoc->PLY_STATUS.lMainMelodyChannel, value);
	return value;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetLineTextTime  (JNIEnv *env, jobject obj, jintArray arr, jint line, jint textLength)
{
         if(pDoc==NULL) return NULL;
         jint *intArray = env->GetIntArrayElements( arr, NULL);
         int i, j;
         int posB[64]={0,};
         int tBcnt=pDoc->vLi[line].Blk_Cnt;
         if(tBcnt==0) return 0;
         if (intArray == NULL)    return 0;

         for(i=0;i<tBcnt;i++)posB[i] = intArray[i];
         pDoc->vLi[line].vLyNote[0].lposA=0;
         for(i=1;i<tBcnt;i++) pDoc->vLi[line].vLyNote[i].lposA=posB[i-1];
         for(i=0;i<tBcnt;i++) pDoc->vLi[line].vLyNote[i].lposB=posB[i];
         pDoc->vLi[line].Text_length = textLength;
 	 	//TRACE("#@####C++ Blk_Cnt[%d]-Blk_lBpoint[%d] textLength[%d]\n",tBcnt,  pDoc->vLi[line].vLyNote[tBcnt-1].lposB, textLength );
/////////////////////////////////////
//각 라인 블럭의 ldelta 계산
/////////////////////////////////////
    int dwGap, dtGap, pA, pB, tA, tB;
    for ( i = 0; i < tBcnt; i++)
    {
 		pA=pDoc->vLi[line].vLyNote[i].lposA;
		pB=pDoc->vLi[line].vLyNote[i].lposB;
		tA=pDoc->vLi[line].vLyNote[i].ltimeA;
		tB=pDoc->vLi[line].vLyNote[i].ltimeB;
		dwGap=pB-pA;  if(dwGap<1) dwGap=1;
        dtGap= tB-tA; if(dtGap<1) dtGap=1;
        pDoc->vLi[line].vLyNote[i].ldelta = (dwGap*1000) /dtGap;
     }
     pDoc->vLi[line].vLyNote[tBcnt-1].ldelta=  (pDoc->vLi[line].vLyNote[tBcnt-1].ldelta * 110)/100;// 마지막거 10% 도 지울수 있게 조정
  	env->ReleaseIntArrayElements( arr, intArray, 0);
	return 1;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCurrentScoreKey (JNIEnv *env, jobject obj)
{
   return CurrentScoreKey;
}

/*
JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISetLineTextTime  (JNIEnv *env, jobject obj, jintArray arr, jint line, jint textLength)
{
         jint *intArray = env->GetIntArrayElements( arr, NULL);
         int i,j;
         if (intArray == NULL)    return 0;
         memset(&pDoc->ProcessLine[line].Blk_lBpoint,0 , sizeof(pDoc->ProcessLine[line].Blk_lBpoint));
         int tBcnt=pDoc->ProcessLine[line].Blk_Cnt;
         pDoc->ProcessLine[line].Text_length = textLength;
   		for(i=0;i<tBcnt;i++)
   		{
   		    pDoc->ProcessLine[line].Blk_lBpoint[i]=intArray[i];
   		    TRACE("#@####C++ ProcessLine[%d/%d].Blk_lBpoint[%ld]-intArray[%d] \n",line, i, pDoc->ProcessLine[line].Blk_lBpoint[i],intArray[i]);
   		}
   		pDoc->ProcessLine[line].Blk_lBpoint[tBcnt-1]=pDoc->ProcessLine[line].Blk_lBpoint[tBcnt-1]+20;//좀더 지워질수 있도록...
 	 	TRACE("#@####C++ Blk_Cnt[%d]-Blk_lBpoint[%ld] textLength[%d]\n",tBcnt,  pDoc->ProcessLine[line].Blk_lBpoint[tBcnt-1], textLength );
 /////////////////////////////////////
//각 라인의끝글자 딜레이 타임 계산
/////////////////////////////////////
    int dwGap, dtGap, A, B;
 //   int nextT;
    for ( i = 0; i < tBcnt; i++)
    {
 		A = (i==0)? 0: A=pDoc->ProcessLine[line].Blk_lBpoint[i-1];
		B = pDoc->ProcessLine[line].Blk_lBpoint[i];
		dwGap = B-A;
        dtGap= pDoc->ProcessLine[line].Blk_lBtime[i]-pDoc->ProcessLine[line].Blk_lAtime[i];
        if(dtGap<=0) dtGap=1;
         pDoc->ProcessLine[line].Blk_ldelta[i] = (dwGap*1000) /dtGap;
     }
  	env->ReleaseIntArrayElements( arr, intArray, 0);
	return 1;
}
*/



JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineBlkCnt (JNIEnv *env, jobject obj)
{
       int sz=pDoc->PLY_STATUS.totalLyricLine;
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vLi[i].Blk_Cnt;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineUpDn(JNIEnv *env, jobject obj)
{
       int sz=pDoc->PLY_STATUS.totalLyricLine;
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vLi[i].updn;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineTimeA (JNIEnv *env, jobject obj)
{
       int sz=pDoc->PLY_STATUS.totalLyricLine;
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vLi[i].Line_StartTime;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineTimeB(JNIEnv *env, jobject obj)
{
       int sz=pDoc->PLY_STATUS.totalLyricLine;
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vLi[i].Line_EndTime;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetJullPosA(JNIEnv *env, jobject obj)
{
       if(pDoc==NULL) return NULL;
       //int sz=pDoc->vJullInfo.size();
       int sz=pDoc->vJullPos.size();
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vJullPos[i].ltimeA;
       //for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vJullInfo[i].jumpTime;//절포지션을 절시작점으로..
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCountDnPos(JNIEnv *env, jobject obj)
{
       if(pDoc==NULL) return NULL;
       int sz=pDoc->vJullInfo.size();
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vJullInfo[i].countDown;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetCountDnBakja (JNIEnv *env, jobject obj)
{
       if(pDoc==NULL) return NULL;
       int sz=pDoc->vJullInfo.size();
       int tmpArr[sz];
       jintArray retArr = env->NewIntArray(sz);
       for (int i = 0; i < sz; i++)   tmpArr[i]= (int) pDoc->vJullInfo[i].bakJa;
       env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
       return retArr;
}

JNIEXPORT jobjectArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetTitle (JNIEnv *env, jobject obj)
{
   int i,rtn;
   int sz;
   char pData[2048];
    sz=pDoc->vSongTitle.size();
    char *buf[sz];
    jobjectArray joa= (jobjectArray)env->NewObjectArray(sz,  env->FindClass("java/lang/String"),env->NewStringUTF(""));
    for(i=0;i<sz;i++){
        TRACE("#@#Title [%s]  sz[%d/%d]",pDoc->vSongTitle[i].title.c_str(), i, sz);
        memset(pData,0,sizeof(pData));
        sprintf(pData,"#T%06ld%s", pDoc->vSongTitle[i].ltime, pDoc->vSongTitle[i].title.c_str());
        if(checkUtfString(pData)==0) continue;
        buf[i]=&pData[0];
        jstring j_file = env -> NewStringUTF(buf[i]);
        env->SetObjectArrayElement(joa,i,j_file);
        env->DeleteLocalRef(j_file);
    }
	return joa;
}

JNIEXPORT jobjectArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineBaseBlk  (JNIEnv *env, jobject obj, jint line)
{
   int i,rtn;
   int sz=(int)pDoc->vLi[line].Blk_Cnt;
    char *buf[sz];
    char pData[256];
 //	TRACE("#@# GetScoreStringArray   sz[%d]",sz);
    jobjectArray joa= (jobjectArray)env->NewObjectArray(sz,  env->FindClass("java/lang/String"),env->NewStringUTF(""));
 	for(i=0;i<sz;i++){
 		strcpy(pData, pDoc->vLi[line].vLyNote[i].str.c_str());
 		if(checkUtfString(pData)==0) continue;
 		buf[i]=&pData[0];
 		jstring j_file = env -> NewStringUTF(buf[i]);
   		env->SetObjectArrayElement(joa,i,j_file);
  		env->DeleteLocalRef(j_file);
  	}
	return joa;
}

JNIEXPORT jobjectArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineRubyBlk (JNIEnv *env, jobject obj, jint line)
{
   int i,rtn;
   int sz=(int)pDoc->vLi[line].Blk_Cnt;
    char *buf[sz];
    char pData[256];
    jobjectArray joa= (jobjectArray)env->NewObjectArray(sz,  env->FindClass("java/lang/String"),env->NewStringUTF(""));
 	for(i=0;i<sz;i++){
 		strcpy(pData, pDoc->vLi[line].vLyNote[i].ruby.c_str());
 		if(checkUtfString(pData)==0) continue;
 		buf[i]=&pData[0];
 		jstring j_file = env -> NewStringUTF(buf[i]);
   		env->SetObjectArrayElement(joa,i,j_file);
  		env->DeleteLocalRef(j_file);
  	}
	return joa;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineBaseBlkTimeA (JNIEnv *env, jobject obj, jint line)
{
     if(pDoc->PLY_STATUS.totalLyricLine==0) return NULL;
     int sz=(int)pDoc->vLi[line].Blk_Cnt;
     jintArray   retArr = env->NewIntArray(sz);
     int tmpArr[sz];
     memset(&tmpArr,0,sizeof(tmpArr));
     for(int i=0;i<sz;i++) tmpArr[i]= pDoc->vLi[line].vLyNote[i].ltimeA;
     env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
     return retArr;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_MIDIGetLineBaseBlkTimeB (JNIEnv *env, jobject obj, jint line)
{
     if(pDoc->PLY_STATUS.totalLyricLine==0) return NULL;
     int sz=(int)pDoc->vLi[line].Blk_Cnt;
     jintArray   retArr = env->NewIntArray(sz);
     int tmpArr[sz];
     memset(&tmpArr,0,sizeof(tmpArr));
      for(int i=0;i<sz;i++) tmpArr[i]=pDoc->vLi[line].vLyNote[i].ltimeB;//
     env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
     return retArr;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_songEndTick  (JNIEnv *env, jobject obj)
{
     return  pDoc->PLY_STATUS.SongEnd_ltime;
}
JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_songTotalLine (JNIEnv *env, jobject obj)
{
   return  pDoc->PLY_STATUS.totalLyricLine;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_madiPrev(JNIEnv *env, jobject obj)
{
  	if(pDoc==NULL) return -1;
    long pTempo;
  	long cur_ltime=pDoc->PLY_STATUS.lTickCount;
  	long timeTo;
  	long mm,bb,tt;

  	if(cur_ltime<0 || cur_ltime > pDoc->PLY_STATUS.SongEnd_ltime || pDoc->PLY_STATUS.IsPlay== 0) return -1;
  	MIDIData_BreakTime(pDoc->pMIDIData, cur_ltime, &mm, &bb, &tt);
  	if(mm<2) return -1;
  	mm=mm-1;
  	MIDIData_MakeTime(pDoc->pMIDIData, mm, 0, 0, &timeTo);
	pDoc->lflag.jump_Flag=1;

 	if(timeTo==0) //처음이면
    {
        MIDIIO_GS_reset();
        pDoc->PLY_STATUS.lTickCount=0;
        MIDIClock_SetTickCount(pDoc->pMIDIClock, 0);
        return 0;
    }
    else MIDIData_FindTempo(pDoc->pMIDIData, timeTo, &pTempo);

    MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
   	MIDIClock_SetTickCount(pDoc->pMIDIClock, timeTo);
    return   timeTo;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_madiNext(JNIEnv *env, jobject obj)
{
  	if(pDoc==NULL) return -1;

    long pTempo;
  	long cur_ltime=pDoc->PLY_STATUS.lTickCount;
  	long last_madi= pDoc->PLY_STATUS.LastMeasure;
  	long timeTo;
  	long mm,bb,tt;
  	if(cur_ltime<0 || cur_ltime > pDoc->PLY_STATUS.SongEnd_ltime || pDoc->PLY_STATUS.IsPlay== 0) return -1;
  	MIDIData_BreakTime(pDoc->pMIDIData, cur_ltime, &mm, &bb, &tt);
  	if((last_madi-2) < mm) return -1;
  	mm=mm+1;
  	MIDIData_MakeTime(pDoc->pMIDIData, mm, 0, 0, &timeTo);
	pDoc->lflag.jump_Flag=1;
    MIDIData_FindTempo(pDoc->pMIDIData, timeTo, &pTempo);
    MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
   	MIDIClock_SetTickCount(pDoc->pMIDIClock, timeTo);
    return   timeTo;
}


JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_ganjuJump (JNIEnv *env, jobject obj)
{
    int ret=0;
    if(pDoc==NULL) return 0;
    if(pDoc->lflag.jullStop==1) return 0;

    int i,vSz;
    JULL_INFO jull_info;
    long ltimeA,ltimeB, ltimeJ, jmpPos_ltime, pTempo;
    long lCurTime = pDoc->PLY_STATUS.lTickCount;
    pDoc->lflag.jump_Flag=0;
    vSz=pDoc->vJullInfo.size();

    for(i=0;i<vSz;i++){
        jull_info=pDoc->vJullInfo[i];
        ltimeA=jull_info.jullTimeA;
        ltimeB=jull_info.jullTimeB;
        ltimeJ=jull_info.jumpTime;
        if(ltimeA<=lCurTime && lCurTime<ltimeB && lCurTime<ltimeJ)
        {
     	    jmpPos_ltime = ltimeJ;
     	    pDoc->lflag.jump_Flag=1;
            MIDIData_FindTempo(pDoc->pMIDIData, jmpPos_ltime, &pTempo);
            MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
            MIDIClock_SetTickCount(pDoc->pMIDIClock, jmpPos_ltime);
            ret=1;
            break;
        }

    }

    return ret;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_firstVerse   (JNIEnv *env, jobject obj, jint enable)
{
    if(pDoc==NULL) return -1;
    if(pDoc->vJullInfo.size()>=2) {
       pDoc->lflag.jullStop = (enable==0) ? 0:1;
#ifdef DBG_JULL
        TRACE("#@#DBG_JULL firstVerse  ltimeB[%ld]\n",pDoc->vJullInfo[1].jullTimeA);
#endif
    }
    else   pDoc->lflag.jullStop=0;

    return pDoc->lflag.jullStop;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_verseJump(JNIEnv *env, jobject obj)//절점프
{
    int ret = 0;
    if(pDoc==NULL) return -1;
    int i;
   	long jmpPos_ltime, pTempo;
   	long lCurTime=pDoc->PLY_STATUS.lTickCount;
    int	jullCnt=pDoc->vJullInfo.size();
    if(jullCnt<2) return -1;//2절이상으로 구성된 노래에만 적용됨

    pDoc->lflag.jump_Flag=0;
    for(i=0;i<jullCnt-1;i++){
        if(pDoc->vJullInfo[i].jullTimeA <=lCurTime && lCurTime<pDoc->vJullInfo[i+1].jullTimeA )
        {
            jmpPos_ltime = pDoc->vJullInfo[i+1].jumpTime;
            pDoc->lflag.jump_Flag=1;
            MIDIData_FindTempo(pDoc->pMIDIData, jmpPos_ltime, &pTempo);
            MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
            MIDIClock_SetTickCount(pDoc->pMIDIClock, jmpPos_ltime);
            ret=1;
            break;
        }

    }
    return ret;
}


JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_rhythmChange (JNIEnv *env, jobject obj, jint rhythm)
{
    if(pDoc==NULL) return -1;
#ifdef DBG_RHY
        TRACE("#@#DBG_RHY kind of rhythm[%d]\n",rhythm);
#endif
    pDoc->PLY_STATUS.rhythmKind = rhythm;
    pDoc->lflag.rhythm_seq=1;
    return rhythm;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_writerNfcBuf  (JNIEnv *env, jobject obj, jbyteArray buf)
{
         jbyte* byteArray = env->GetByteArrayElements( buf, NULL);
         if (byteArray == NULL)    return 0;

         int bufCnt = env->GetArrayLength(buf);
         int i;
         unsigned char nfc_buf[2048]={0,};
         int offset=0;

         nfc_buf[offset++]=0xF0;
         nfc_buf[offset++]=0x00;
         nfc_buf[offset++]=(bufCnt>>8)&0xff;//HI length packet
         nfc_buf[offset++]=bufCnt&0xff;     //LO length packet
         for(i=0;i<bufCnt;i++) nfc_buf[offset++] = byteArray[i]&0xff;
         nfc_buf[offset++]=0x55;
         nfc_buf[offset++]=0xF7;
#ifdef DBG_NFC
    for(i=0;i<offset;i++) {
        TRACE("#@#DBG_NFC [%d/%d] bufCnt[%d] data[%x]\n",i,offset,bufCnt,nfc_buf[i]);
    }

#endif
        MIDIIO_Put_n(15, nfc_buf,offset);
        env->ReleaseByteArrayElements( buf, byteArray, 0);
  	     return 1;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_readNfcBuf (JNIEnv *env, jobject obj)
{
    return 0;
}

JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_readFrontKeyBuffer (JNIEnv *env, jobject obj)
{
    //TRACE("#@# readFrontKeyBuffer 11111111\n");

      int sz= pFRONT->vFRONTKeyBuffer.size();
      jintArray   retArr = env->NewIntArray(sz);
     int tmpArr[sz];
     memset(&tmpArr,0,sizeof(tmpArr));
     for(int i=0;i<sz;i++) tmpArr[i]=pFRONT->vFRONTKeyBuffer[i];//
     pFRONT->vFRONTKeyBuffer.clear();
     env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
     return retArr;
}

JNIEXPORT jstring JNICALL Java_com_cecee_imove_JniFunction_getFrontVersion (JNIEnv *env, jobject obj)
{
    pFRONT->FRONT_getVersion();
    //usleep(50000);//50ms
    //char fver[32];
    //sprintf(fver,"%s",frontVersio.c_str());
    return env->NewStringUTF(pFRONT->frontVersion);
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_GetlongValue  (JNIEnv *env, jobject obj, jint idx){
    int tmp= -1;
    long curr_time;
    int kind;
    int value;

    kind=(idx & 0xff00)>>8;
    value = idx & 0xff;
    switch(idx){
          case 4://get TitleOffTime
             tmp= pDoc->PLY_STATUS.TitleOffTime;
             break;
          case 0xf0://MIDIClock_Stop
            MIDIClock_Stop(pDoc->pMIDIClock);
            tmp = 1;
            break;
         case 0xf1://MIDIClock_Start
             MIDIClock_Start(pDoc->pMIDIClock);
             tmp = 1;
             break;
          case 0xf2://MIDIClock_Reset
              MIDIClock_Reset(pDoc->pMIDIClock);
              tmp = 1;
              break;
          default:
           break;
    }
    return tmp;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDIIsPlay (JNIEnv *env, jobject obj)
{
    if(pDoc !=NULL)    return pDoc->PLY_STATUS.IsPlay;
    else return 0;
}

JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_MIDISeek (JNIEnv *env, jobject obj,  jint tick)
{
  	if(pDoc==NULL) return -1;
    long pTempo;
	pDoc->lflag.jump_Flag=1;
    MIDIData_FindTempo(pDoc->pMIDIData, tick, &pTempo);
    MIDIClock_SetTempo (pDoc->pMIDIClock, pTempo);
   	MIDIClock_SetTickCount(pDoc->pMIDIClock, tick);
    return   tick;
}
JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_pcmEnable (JNIEnv *env, jobject obj, jint enable)
{
    if(enable) pcmStart_flag=1;
    else   pcmStart_flag=0;
}

JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_chorusSetVolume (JNIEnv *env, jobject obj, jfloat volume)
{
    chorusVolume = volume;
    if(volume < 0) pcmStart_flag=0;
    else pcmStart_flag=1;
}

JNIEXPORT jfloat JNICALL Java_com_cecee_imove_JniFunction_chorusGetVolume (JNIEnv *env, jobject obj)
{
    return chorusVolume;
}


JNIEXPORT jintArray JNICALL Java_com_cecee_imove_JniFunction_cocoIsMessage (JNIEnv *env, jobject obj)
{
    if(pCoco==NULL || pCoco->isCocoMessage==0) return NULL;
    int sz=pCoco->vCocoMessage.size();
    int tmpArr[sz];
    jintArray retArr = env->NewIntArray(sz);
    for (int i = 0; i < sz; i++)   tmpArr[i]= (int)pCoco->vCocoMessage[i];
    env->SetIntArrayRegion(retArr, 0, sz, tmpArr);
    pCoco->isCocoMessage=0;
    return retArr;
}


JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_cocoRespCmd (JNIEnv *env, jobject obj, jint id, jint cmd)
{
    int cs=0;
    COCO_RESP_CMD mcccmd;
    mcccmd.stx=STX;
    mcccmd.etx=ETX;
    mcccmd.cmd=cmd&0xff;
    mcccmd.id=id&0xff;
    mcccmd.len=0;
    cs= (mcccmd.cmd+ mcccmd.id+ mcccmd.len)%128;
    mcccmd.cs=cs;
    pCoco->coco_writer_cmd(mcccmd);
}

JNIEXPORT void JNICALL Java_com_cecee_imove_JniFunction_cocoRespData
  (JNIEnv *env, jobject obj, jint id, jint cmd, jint d0, jint d1, jint d2, jint d3, jint d4)
{
    int cs=0;
    COCO_RESP_DATA mccdata;
    mccdata.stx=STX;
    mccdata.etx=ETX;
    mccdata.cmd=cmd&0xff;
    mccdata.id=id&0xff;
    mccdata.BD0=d0&0xff;
    mccdata.BD1=d1&0xff;
    mccdata.D2=d2&0xff;
    mccdata.D3=d3&0xff;
    mccdata.BD4=d4&0xff;
    mccdata.len=0x05;
    cs= (mccdata.cmd+ mccdata.id+ mccdata.len + mccdata.BD0 + mccdata.BD1 + mccdata.D2 + mccdata.D3 + mccdata.BD4)%128;
    mccdata.cs=cs;
    pCoco->coco_writer_data(mccdata);
}



// jinyong kim : build test ---< start >---
  JNIEXPORT jint JNICALL Java_com_cecee_imove_JniFunction_TESTGetBirthDay  (JNIEnv *env, jobject obj, jint flg)
  {
  	long birth_month;
 	long birth_date;

 	birth_month = 4;
 	birth_date = 5;

 	if(flg==0)
 	    return birth_month;
 	else
 		return birth_date;
  }
  JNIEXPORT jstring JNICALL Java_com_cecee_imove_JniFunction_TESTGetName (JNIEnv *env, jobject obj)
  {
     // const char *name = env->GetStringUTFChars(pDoc->songInfo.strTitle, NULL);//Java String to C Style string
      char msg[256] = "";
      jstring result;
      strcpy(msg, "KimJinYong");
     // env->ReleaseStringUTFChars(string, name);
      puts(msg);
      result = env->NewStringUTF(msg); // C style string to Java String
      return result;
  }

// jinyong kim : build test ---< end >---

