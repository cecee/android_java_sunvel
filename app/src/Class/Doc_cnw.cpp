#include "Doc_cnw.h"
#include "extern.h"
#include <vector>
#include <string>
#include <string.h>
#include <stdio.h>

using namespace std;

int Doc_cnw::Update_cnw(char *filename)
{
	int i,ii, line, noteCnt;
	int max_jmp;
	int inx,inxB;
	int Iacc,IaccB;
	long lTrackTime;
	long instPos[64][2];
	long seekoffset;
	long wavSz;
	char choFn[64];

    pDoc->pMIDIData= MIDIData_LoadFromSMFA(filename);
	pDoc->PLY_STATUS.TitleOffTime = 480*5;
	pDoc->PLY_STATUS.LyricDisplay_ltime = 480*5;

TRACE("#@###(0-1) Update_CNW pMIDIData[%x] ---------------------------------\n", pDoc->pMIDIData);
    pDoc->PLY_STATUS.lNoteCnt = cnwSplitTrack(pDoc->pMIDIData);
    pDoc->PLY_STATUS.lxLyricCnt = pDoc->PLY_STATUS.lNoteCnt;
//    TRACE("#@###(0-2)-----------------------------------cnwSplitTrack\n");

    pDoc->DK_MK_FrontLedMarker();//put Front LED track
// TRACE("#@###(1)-----------------------------------Update_CNW  ---------------------------------\n");
    pDoc->DK_LoadJull();//MIDI에  마킹된 데이타 vJullPos에 쌓기
    pDoc->DK_LoadCountDown();//jull대신에 CountDown으로 절처리 구곡 신속 동일하게 처리
    pDoc->DK_GetSongKey();
    pDoc->DK_LoadChorus(pDoc->TRACK_Info.pTRACK_CHRS, "test");

    if(pDoc->TRACK_Info.pTRACK_Lyric) cnwGetTitle(pDoc->TRACK_Info.pTRACK_Lyric);

    if(pDoc->TRACK_Info.pTRACK_Lyric &&  pDoc->PLY_STATUS.lNoteCnt >0) {
        cnwPutEventMarker();//Lyric Display time = Title off Time를 같은시간으로 처리
        pDoc->mkTimeTableNoteON(pDoc->TRACK_Info.pTRACK_Lyric);
    }
 	pDoc->Make_EventList();
//TRACE("#@#vv##(5)-----------------------------------Update_CNW  ---------------------------------\n");
	if(pDoc->PLY_STATUS.lNoteCnt) pMusic_score->LoadMusicScore(pDoc->pMIDIData);

    TRACE("#@#vv##(6)LoadMusicScore lNoteCnt[%d] lxLyricCnt[%d]\n",pDoc->PLY_STATUS.lNoteCnt, pDoc->PLY_STATUS.lxLyricCnt);

	return 1;
}

int Doc_cnw::cnwSplitTrack(MIDIData* pMIDIData)
{
	MIDITrack* pMIDITrack = NULL;
	MIDITrack* exMIDITrack_Sys = NULL;
	MIDITrack* exMIDITrack_BRef = NULL;
	MIDITrack* exMIDITrack_Bass = NULL;
	MIDITrack* exMIDITrack_Dr1 = NULL;
	MIDITrack* exMIDITrack_Dr2 = NULL;

    MIDIEvent* pMIDIEvent = NULL;
    MIDIEvent *pNextEvent=NULL;
    int i,j;
    long ch, key, MadiBeatKey, madiCnt, cnt;
    long ltime, llMx, llBx, llTx;

	char pData[128];
	long lport,lch;
	string str;
	long res;
	long kind;

	pDoc->PLY_STATUS.lMainMelodyChannel=0;

    ltime = MIDIData_GetEndTime(pMIDIData);
//////////////////////////////////////////////////////////
// sys track에  15000000  뒤에 tempo event delete
//////////////////////////////////////////////////////////
    if(ltime>15000000)
    {
        pMIDITrack=pMIDIData->m_pFirstTrack;
        pMIDIEvent=pMIDITrack->m_pFirstEvent;
        while(pMIDIEvent){
            pNextEvent=pMIDIEvent->m_pNextEvent;
            ltime=MIDIEvent_GetTime(pMIDIEvent);
            if(ltime>15000000){
               MIDIEvent_DeleteSingle(pMIDIEvent);
            }
            pMIDIEvent=pNextEvent;
        }
    }
///////////////////////////////////////////////////////////
    pDoc->PLY_STATUS.SongEnd_ltime = MIDIData_GetEndTime(pMIDIData);

	pDoc->TRACK_Info.pTRACK_Sys=pMIDIData->m_pFirstTrack;
  	forEachTrack (pMIDIData, pMIDITrack) {
		memset(pData,0,128);
		kind=0xff;
		str="";
		MIDITrack_GetName(pMIDITrack,pData,128);
		lport=MIDITrack_GetOutputPort(pMIDITrack);
		lch=MIDITrack_GetOutputChannel(pMIDITrack);
		if(pData[0]==0) continue;
		str = pData;
        //if (str.find("SYSEX") != string::npos) pDoc->TRACK_Info.pTRACK_Sys=pMIDITrack;
        if (str.find("NRPN") != string::npos) pDoc->TRACK_Info.pTRACK_Nrpn=pMIDITrack;
        else if (str.find("/") != string::npos && lch==0) {
            //"/"를 표시한트랙이있어 ch로 한번더 확인
            pDoc->TRACK_Info.nameTRACK_Melody=str;
            pDoc->TRACK_Info.pTRACK_Melody=pMIDITrack;
            pDoc->PLY_STATUS.hasMelody_track = 1;
        }
        else if (str.find("@B@") != string::npos) {
        	if(lport==0)  pDoc->TRACK_Info.pTRACK_Bass1=pMIDITrack; //sprintf(convPara.TRACKNAME_BASSp1,"%s",pData);
        	else if(lport==1) pDoc->TRACK_Info.pTRACK_Bass2=pMIDITrack;//sprintf(convPara.TRACKNAME_BASSp2,"%s",pData);
        }
        else if (str.find("@R@") != string::npos) {
				if(lport==0 && lch==9) pDoc->TRACK_Info.pTRACK_DrumP1C10=pMIDITrack;
				else if(lport==0 && lch==10) pDoc->TRACK_Info.pTRACK_DrumP1C11=pMIDITrack;
				else if(lport==1 && lch==9) pDoc->TRACK_Info.pTRACK_DrumP2C10=pMIDITrack;
				else if(lport==1 && lch==11) pDoc->TRACK_Info.pTRACK_DrumP2C11=pMIDITrack;
        }
        else if (str.find("@CD@") != string::npos) pDoc->TRACK_Info.pTRACK_Chord1=pMIDITrack;

        else if (str.find("@L@") != string::npos) {
            pDoc->PLY_STATUS.LyricCode=0;
            //if (str.find("KR") != string::npos || str.find("EN") != string::npos) pDoc->PLY_STATUS.LyricCode=0;
            if (str.find("JP") != string::npos) pDoc->PLY_STATUS.LyricCode=1;
            else if (str.find("CN") != string::npos) pDoc->PLY_STATUS.LyricCode=2;
            else if (str.find("U8") != string::npos) pDoc->PLY_STATUS.LyricCode=8;
            pDoc->TRACK_Info.pTRACK_Lyric=pMIDITrack;
        }

        else if (str.find("@F@") != string::npos) pDoc->TRACK_Info.pTRACK_Function=pMIDITrack;
        else if (str.find("@GK@") != string::npos) pDoc->TRACK_Info.pTRACK_GK=pMIDITrack;
        else if (str.find("@BR@") != string::npos) pDoc->TRACK_Info.pTRACK_BR=pMIDITrack;
        else if (str.find("@PS@") != string::npos) pDoc->TRACK_Info.pTRACK_PS=pMIDITrack;
        else if (str.find("$INST") != string::npos) pDoc->TRACK_Info.pTRACK_INST=pMIDITrack;
        else if (str.find("$CHRS") != string::npos) pDoc->TRACK_Info.pTRACK_CHRS=pMIDITrack;
        else if (str.find("$SDFX") != string::npos) pDoc->TRACK_Info.pTRACK_SDFX=pMIDITrack;
 	}//forEachTrack

    MIDITrack_SetName(pDoc->TRACK_Info.pTRACK_Sys, "SYSEX");
    exMIDITrack_Sys  = MIDITrack_CreateClone(pDoc->TRACK_Info.pTRACK_Sys);//0번 track 대응
    //exMIDITrack_BRef = MIDITrack_CreateClone(pDoc->TRACK_Info.pTRACK_Bass1);

    exMIDITrack_Bass=MIDITrack_Create();//9번 channel 대응
    exMIDITrack_Dr1=MIDITrack_Create();//10번 channel 대응
    exMIDITrack_Dr2=MIDITrack_Create();//11번 channel 대응

    MIDITrack_InsertEndofTrack(exMIDITrack_Bass,480);
    MIDITrack_InsertEndofTrack(exMIDITrack_Dr1,480);
    MIDITrack_InsertEndofTrack(exMIDITrack_Dr2,480);

    MIDITrack_SetName(exMIDITrack_Sys,"@X@sys");
    //MIDITrack_SetName(exMIDITrack_BRef,"@X@bref");
    MIDITrack_SetName(exMIDITrack_Bass,"@X@bass");
    MIDITrack_SetName(exMIDITrack_Dr1,"@X@Dr1");
    MIDITrack_SetName(exMIDITrack_Dr2,"@X@Dr2");

    MIDIData_InsertTrackAfter(pMIDIData, exMIDITrack_Sys, pMIDIData->m_pLastTrack);
    pDoc->TRACK_Info.pTRACK_EXSYS = exMIDITrack_Sys;

   // MIDIData_InsertTrackAfter(pMIDIData, exMIDITrack_BRef, pMIDIData->m_pLastTrack);
   // pDoc->TRACK_Info.pTRACK_EXBREF = exMIDITrack_BRef;
    MIDIData_InsertTrackAfter(pMIDIData, exMIDITrack_Bass, pMIDIData->m_pLastTrack);
    pDoc->TRACK_Info.pTRACK_EXBASS = exMIDITrack_Bass;

    MIDIData_InsertTrackAfter(pMIDIData, exMIDITrack_Dr1, pMIDIData->m_pLastTrack);
    pDoc->TRACK_Info.pTRACK_EXDR1 = exMIDITrack_Dr1;

    MIDIData_InsertTrackAfter(pMIDIData, exMIDITrack_Dr2, pMIDIData->m_pLastTrack);
    pDoc->TRACK_Info.pTRACK_EXDR2 = exMIDITrack_Dr2;

///make bass reference

  //pTRACK_EXBREF delete without MadiBeatKey++
    vector<long>vMadiBeatKey;
    ltime = MIDIData_GetEndTime(pMIDIData);

    MIDIData_BreakTime(pMIDIData, ltime, &llMx, &llBx, &llTx);
    madiCnt=llMx+1;

#ifdef DBG_RHY
   TRACE("#@#DBG ltime[%d] madiCnt[%04d] pTRACK_Bass2[%x] ltime[%d]----\n",ltime, madiCnt, pDoc->TRACK_Info.pTRACK_Bass2, ltime);
#endif
     if(pDoc->TRACK_Info.pTRACK_Bass2 != NULL){

       forEachEvent (pDoc->TRACK_Info.pTRACK_Bass2, pMIDIEvent)//Port2_BASS
       {
               MIDIEvent_Combine(pMIDIEvent);
               if(MIDIEvent_IsNoteOn(pMIDIEvent))
               {
                   ltime=MIDIEvent_GetTime(pMIDIEvent);
                   MIDIData_BreakTime(pMIDIData, ltime, &llMx, &llBx, &llTx);
                   key=MIDIEvent_GetKey(pMIDIEvent);
                   if((llBx==0 || llBx==2 )&& (llTx==0))
                   {
                       MadiBeatKey= (llMx<<16) + (llBx<<8) + key;
                       vMadiBeatKey.push_back(MadiBeatKey);
                   }
               }
       }
   }
#ifdef DBG_RHY
      TRACE("#@#DBG ltime[%d] madiCnt [%04d] pTRACK_Bass1[%x]------------\n",ltime, madiCnt, pDoc->TRACK_Info.pTRACK_Bass1);
#endif
   if(pDoc->TRACK_Info.pTRACK_Bass1 != NULL){
        forEachEvent (pDoc->TRACK_Info.pTRACK_Bass1, pMIDIEvent)//Port1_BASS
        {
                MIDIEvent_Combine(pMIDIEvent);
                if(MIDIEvent_IsNoteOn(pMIDIEvent))
                {
                    ltime=MIDIEvent_GetTime(pMIDIEvent);
                    MIDIData_BreakTime(pMIDIData, ltime, &llMx, &llBx, &llTx);
                    key=MIDIEvent_GetKey(pMIDIEvent);
                    if((llBx==0 || llBx==2 )&& (llTx==0))
                    {
                        MadiBeatKey= (llMx<<16) + (llBx<<8) + key;
                        vMadiBeatKey.push_back(MadiBeatKey);
                    }
                }
        }
    }
#ifdef DBG_RHY
   // for(i=0;i<vMadiBeatKey.size();i++) TRACE("#@#DBG vMadiBeatKey [%04d]------------[%08x]\n",i, vMadiBeatKey[i]);
#endif

    for(i=0;i<madiCnt;i++)  pDoc->vMadiKeyInfo.push_back(0xFEFE);//init vMadiKeyInfo empty
    for(i=0;i<vMadiBeatKey.size();i++)
     {
         long _ttt=vMadiBeatKey[i];
         long _madi=(_ttt>>16);
         long _beat=(_ttt>>8)&0xff;
         long _key=_ttt & 0xff;
         if(_beat==0) pDoc->vMadiKeyInfo[_madi]= (pDoc->vMadiKeyInfo[_madi]&0x00FF)+((_key<<8)&0xFF00);
         else if(_beat==2) pDoc->vMadiKeyInfo[_madi] = (pDoc->vMadiKeyInfo[_madi]&0xFF00) + _key;
     }
#ifdef DBG_RHY
   // for(i=0;i<madiCnt;i++) TRACE("#@#DBG vMadiKeyInfo madi[%04d/%04d]-------[%08x]\n",i,madiCnt, pDoc->vMadiKeyInfo[i]); usleep(10);
#endif
    int lxLyricCnt=0;
    if(pDoc->TRACK_Info.pTRACK_Lyric !=NULL) forEachEvent(pDoc->TRACK_Info.pTRACK_Lyric, pMIDIEvent) if (MIDIEvent_IsNoteOn(pMIDIEvent)) lxLyricCnt++;
	return lxLyricCnt;
}

void Doc_cnw::cnwPutEventMarker(void)
{
    MIDIEvent *pMIDIEvent=NULL;
    long ltime=0;
    long ltimeTitleOff;
    forEachEvent (pDoc->TRACK_Info.pTRACK_Lyric, pMIDIEvent) {
        MIDIEvent_Combine(pMIDIEvent);
        if(MIDIEvent_IsNoteOn(pMIDIEvent))
        {
          ltime = MIDIEvent_GetTime (pMIDIEvent);
          break;
        }
    }
	ltimeTitleOff=(ltime>=600)? ltime-480 : ltime;
    pDoc->PLY_STATUS.TitleOffTime=ltimeTitleOff;
	pDoc->PLY_STATUS.LyricDisplay_ltime = pDoc->PLY_STATUS.TitleOffTime;

}



int Doc_cnw::UNICODE_strLengh(Uint16 *s)
{
	int i;
	for(i=0;i<256;i++)
	{
		if(s[i]==0x00)break;
	}
	return i;
}


int Doc_cnw::cnwGetTitle(MIDITrack* pMIDITrack)
{
    Uint16 *Dw;
	char *xxx;
	char utf8[1024];
    int LyricCode=pDoc->PLY_STATUS.LyricCode;
    MIDIEvent *pMIDIEvent;
    MIDI_TITLE midi_title;
    long ltime;
    char pData[256];
    string x_str="";
    string y_str="";
#ifdef DBG_LYRIC
    TRACE("#@#vvv제목 /작사 /작곡 /가수=====>cnwGetTitle[%x] LyricCode[%d]\n",pMIDITrack, LyricCode);
#endif
    pDoc->vSongTitle.clear();
    long extime=0;
    forEachEvent (pMIDITrack, pMIDIEvent) {
        if(MIDIEvent_IsTextEvent(pMIDIEvent))
        {
            ltime = MIDIEvent_GetTime (pMIDIEvent);
             MIDIEvent_GetText (pMIDIEvent, pData, 256);
            if(ltime==extime){
                x_str=(char *)pData;
                y_str=y_str+x_str;
            }
            else{
                xxx=(char*)y_str.c_str();
                Dw = pDoc->b2w_CU(xxx, LyricCode);//각Lange_code->uni
                memset(utf8,0,sizeof(utf8));
                pDoc->uni2utf(Dw , (Uint8*)utf8);
                string xStr=(char *)utf8;
                midi_title.ltime=extime;
                midi_title.title=xStr;
                pDoc->vSongTitle.push_back(midi_title);
                xStr=""; y_str="";
            }
            extime=ltime;
        }
    }

    if(y_str.size() != 0){
         xxx=(char*)y_str.c_str();
         Dw = pDoc->b2w_CU(xxx, LyricCode);//각Lange_code->uni
         memset(utf8,0,sizeof(utf8));
         pDoc->uni2utf(Dw , (Uint8*)utf8);
         string xStr=(char *)utf8;
         // TRACE("#@#2vvv==========>Song Title[%s]\n",xStr.c_str());
         midi_title.ltime=extime;
         midi_title.title=xStr;
         pDoc->vSongTitle.push_back(midi_title);
         xStr=""; y_str="";
    }
    else{ //제목없음 표시
          midi_title.ltime=0;
          midi_title.title="#A No TITLE";
          pDoc->vSongTitle.push_back(midi_title);
    }
    return 1;
}

int Doc_cnw::cnwLoadGasa(MIDITrack* pMIDITrack)
{
    MIDIEvent *pMIDIEvent=NULL;
    long vSz;
    char pData[256];
	Uint16 *Dw;
	Uint16 Uni[128];
    int i;
	int lineCnt=3;
	char buf_in[256];
	char tmp[256];
	unsigned char utf8[512];
	string s_str;
    string xStr="";
    string yStr="";
    int LyricCode=pDoc->PLY_STATUS.LyricCode;
  ///제목 /작사 /작곡 노래++
 // cnwGetTitle(pMIDITrack);
///제목 /작사 /작곡 노래--
//-------------------------------------------------------------
    UNI_STRING wStrBuf;
    vector<string> vmline;
    vmline.clear();
    vmline=pDoc->split(yStr,"¶");
    vSz=vmline.size();
    for(i=0;i<vSz;i++){
        memset(pData,0,sizeof(pData));
        strcpy(pData, vmline[i].c_str());
        Dw = pDoc->b2w_CU(pData, LyricCode);
        memcpy(&wStrBuf.wline, Dw,256);
        pDoc->vUniStr.push_back(wStrBuf);//가사 줄만듬
 #ifdef DBG_LYRIC
        TRACE("#@#vv--line[%d/%d][%s]\n",i, vSz, vmline[i].c_str());
 #endif
    }
    vmline.clear();  vector<string> ().swap(vmline);
  	return 1;
}

Doc_cnw::Doc_cnw(){

}
Doc_cnw::~Doc_cnw(){}
