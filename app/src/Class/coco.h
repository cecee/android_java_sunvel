//////////////////////
#ifndef __COCO_H__
#define __COCO_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
using namespace std;

#define STX 0xFB
#define ETX 0xFA

typedef unsigned short      Uint16;
typedef unsigned char       Uint8;
typedef unsigned int        Uint32;


typedef struct _COCO_RESP_CMD{
    Uint8 stx;
    Uint8 cmd;
    Uint8 id;
    Uint8 len;
    Uint8 cs;
    Uint8 etx;
}COCO_RESP_CMD;

typedef struct _BD0{
	Uint8 standby: 1; //0:선곡대기중  1:연주중
	Uint8 play_status: 1; //0:연주가능상태  1:연주불가능상태
}BD0;
typedef struct _BD1{
	Uint8 mode: 1; //0:시간모드  1:코인모드
	Uint8 coin: 1; //0:남은시간코인  1:리모컨입력상태
}BD1;

typedef struct _BD4{
	Uint8 mode: 1; //0:시간모드  1:코인모드
	Uint8 coin: 1; //0:남은시간코인  1:리모컨입력상태
}BD4;

typedef struct _COCO_RESP_DATA{
    Uint8 stx;
    Uint8 cmd;
    Uint8 id;
    Uint8 len;
    Uint8 BD0;//반주기 동작상태
    Uint8 BD1;//반주기 동작모드
    Uint8 D2;
    Uint8 D3;
    Uint8 BD4;
    Uint8 cs;
    Uint8 etx;
}COCO_RESP_DATA;

class coco
{
public:
    vector<char> vCocoSerial;
    vector<char> vCocoMessage;

    COCO_RESP_CMD coco_cmd;
    int isCocoMessage=0;
public:
    void timer_sigfrom_coco(void);
    void coco_writer_cmd(COCO_RESP_CMD cmd);
    void coco_writer_data(COCO_RESP_DATA cmd);
	coco();
	~coco();
};

#endif
