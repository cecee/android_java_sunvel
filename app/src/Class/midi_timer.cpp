﻿//ndk-build
//ant debug
//install -r bin\SDLActivity-debug.apk
#define CLOCKID CLOCK_REALTIME
#define SIG SIGRTMIN

#define PERIOD 10000 //10000 //10ms
#include "midi_timer.h"
#include "extern.h"
#include <pthread.h>

void prev_jump(long lCurTime, int flg);
void next_jump(long lCurTime);

midi_timer::midi_timer()
{
	TRACE("#@#vv   ------------------TIMER Create!!!------------------------ \n");
	timerid=NULL;
	is_timer_on= FALSE;
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = timer_handler;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIG, &sa, NULL) == -1) return;
/* Block timer signal temporarily */
     sigemptyset(&mask);
     sigaddset(&mask, SIG);
     if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return;
	/* Create the timer */
	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = SIG;
	sev.sigev_value.sival_ptr = &timerid;
	if (timer_create(CLOCKID, &sev, &timerid) == -1) return;
	TimerSet(true);
	TRACE("#@#vv   ------------------TIMER Create!!!------------------------ \n");
}

midi_timer::~midi_timer(){
  	TRACE("#@#vv   ------------------TIMER KILL------------------------ \n");
	its.it_value.tv_sec = 0;///freq_nanosecs / 1000000000;
	its.it_value.tv_nsec = 0;//freq_nanosecs % 1000000000;
	its.it_interval.tv_sec = its.it_value.tv_sec;
	its.it_interval.tv_nsec = its.it_value.tv_nsec;
	if(timerid)	timer_delete(timerid);
	timerid=NULL;
    is_timer_on = FALSE;
}

void midi_timer::TimerSet(bool start)
{
   // TRACE("#@#vv starting timer flag[%d]\n", start);
    if(start == is_timer_on) return;
    is_timer_on=start;
	its.it_value.tv_sec = 0;///freq_nanosecs / 1000000000;
	its.it_value.tv_nsec = (start) ? (PERIOD*1000) : 0;//freq_nanosecs % 1000000000;
	its.it_interval.tv_sec = its.it_value.tv_sec;
	its.it_interval.tv_nsec = its.it_value.tv_nsec;
    if (timer_settime(timerid, 0, &its, NULL) == -1) return;
}



void prev_jump(long lCurTime, int flg)
{
    int i, eventKind;
    unsigned char pData[16];
    EVENT_LIST _PlayEvent;

    pDoc->Oldltime=lCurTime;

    TRACE("#@#DBG prev_jump[pDoc->lflag.rhythm_seq[%d] flg[%d]\n", pDoc->lflag.rhythm_seq, flg);

    for(i=0;i<pDoc->PLY_STATUS.TotalEventCnt ;i++)
    {
        _PlayEvent=(EVENT_LIST) pDoc->vPlayMidiEvent[i];
        if(_PlayEvent.ltime >= lCurTime)
        {
                pDoc->EventListCnt=i;
                pDoc->Oldltime=lCurTime;
                break;
        }
        else  ///tempo, control은 세팅해야된다.
        {
           if(flg==0) pMIDI_MAIN->MessageOut_Null(_PlayEvent);
           else if(flg && _PlayEvent.ltime<480 ) pMIDI_MAIN->MessageOut_Null(_PlayEvent);
        }
     }
}


void next_jump(long lCurTime)
{
    int i, eventKind;
    unsigned char pData[16];
    EVENT_LIST _PlayEvent;
    while(1)
    {
        if( pDoc->EventListCnt >=pDoc->PLY_STATUS.TotalEventCnt) //song finished
        {
            pDoc->PLY_STATUS.IsPlay=0;
            break;
        }
        _PlayEvent=(EVENT_LIST) pDoc->vPlayMidiEvent[pDoc->EventListCnt];
        if(_PlayEvent.ltime <= lCurTime)
        {
           pMIDI_MAIN->MessageOut_Null(_PlayEvent);
           pDoc->EventListCnt++;
        }
        else {
            pDoc->Oldltime=lCurTime;
            break;
        }
    }
}

void midi_timer::timer_handler(int sig, siginfo_t *si, void *uc)
{
      	int i;
      	long lCurTime;
      	long eventKind;
      	long lTempo, lvolume;

      	EVENT_LIST _PlayEvent;
        timer_sigfrom_mic();
        pCoco->timer_sigfrom_coco();
        pFRONT->FRONT_sigfrom_front();

     //  TRACE("#@#### pMIDI_MAIN[%x] pDoc[%x] pDoc->pMIDIData[%x] pDoc->pMIDIClock[%x] \n", pMIDI_MAIN, pDoc, pDoc->pMIDIData, pDoc->pMIDIClock);
        if(pMIDI_MAIN==NULL || pDoc==NULL || pDoc->pMIDIData==NULL || pDoc->pMIDIClock==NULL) return;
		if( pDoc->PLY_STATUS.IsPlay==0)  return; //곡이 끝난 상태이면
        _TimeProc(pDoc->pMIDIClock, PERIOD/1000);
 	////TRACE("#@#000Thread Looping.......tick[%ld] A[%d] B[%d]----Volume[%d]\n", pDoc->PLY_STATUS.lTickCount, pDoc->PLY_STATUS.A_B[0], pDoc->PLY_STATUS.A_B[1], Volume);
     	lCurTime = pDoc->PLY_STATUS.lTickCount  = pDoc->pMIDIClock->m_lTickCount;
		if( lCurTime > pDoc->PLY_STATUS.SongEnd_ltime)  {
			pDoc->lflag.Jni_LoadFlag = -1;
			pDoc->PLY_STATUS.IsPlay=0;//MIDI Finished
		}
		///첫번째 마디에서 기본값 셋팅
		if(pDoc->lflag.Jni_LoadFlag == -1 && lCurTime > (pDoc->PLY_STATUS.lTimeResolution *4)) pDoc->lflag.Jni_LoadFlag =1;
#if 0
		///AtoB 셋팅
		if(pDoc->PLY_STATUS.A_B[1]>0 &&  lCurTime > pDoc->PLY_STATUS.A_B[1])
		{
			MIDIClock_SetTickCount(pDoc->pMIDIClock, pDoc->PLY_STATUS.A_B[0]);
			lCurTime=pDoc->PLY_STATUS.A_B[0];
			pDoc->lflag.jump_Flag=1;
		}
#endif
		pMIDI_MAIN->rhythm_seq();


////점프조건+++
        if(pDoc->lflag.jump_Flag > 0){
             //lvolume=MIDI_Master_Volume;
            MIDIIO_All_NoteOff();
            //SetMIDI_Volume(0);
            if(pDoc->lflag.jump_Flag==2)//처음으로
            {
                MIDIClock_SetTickCount(pDoc->pMIDIClock, 0);
                lCurTime = pDoc->Oldltime=0;
                pDoc->EventListCnt=0;
            }
            else if(pDoc->lflag.jump_Flag==3)//리듬변환용점프로 480까지만 null out한다
            {
#ifdef DBG_RHY
//TRACE("#@#DBG_RHY(1) 리듬변환시 ---lCurTime [%d] pDoc->Oldltime[%d]\n",lCurTime, pDoc->Oldltime);
#endif
                prev_jump(lCurTime, 1);
                MIDIData_FindTempo(pDoc->pMIDIData,lCurTime,&lTempo);
                MIDIClock_SetTempo (pDoc->pMIDIClock, lTempo);
            }
            else
            {
               TRACE("#@###jump(1)----------lCurTime [%d] pDoc->Oldltime[%d]\n",lCurTime, pDoc->Oldltime);
                 if(lCurTime - pDoc->Oldltime <= 0)//이전으로
                {

                   prev_jump(lCurTime, 0);
                }
                else //이후로
                {
                   next_jump(lCurTime);
                }
                pDoc->Oldltime = lCurTime;
            }
           // SetMIDI_Volume(lvolume);
            //pDoc->lflag.jump_Send=1;
            pDoc->lflag.jump_Flag=0;
         }
////점프조건---

        if(pDoc->lflag.jullStop==1){ //1절끝내기 들어왔을때
            long releaseTime= 1440;//480*2 두마디 뒤에 정지
            long finishTime=pDoc->vJullInfo[1].jullTimeA + releaseTime;//480*2 두마디 뒤에 정지

            if( lCurTime >= pDoc->vJullInfo[1].jullTimeA && lCurTime < finishTime) //FadeOut시작
            {
                int passTime = lCurTime- pDoc->vJullInfo[1].jullTimeA;
                int dVdR= (127*100/releaseTime)+1;//+1를 한 이유는 조금더 빨리 완전한 Mute
                int volume= (dVdR*passTime)/100;
                if(volume >= 127) volume=127;
                SetMIDI_Volume(127-volume);
 #ifdef DBG_JULL
         TRACE("#@#DBG_JULL check lCurTime[%ld] passTime[%d] dVdR[%d] volume[%d]\n",lCurTime, passTime, dVdR, volume);
 #endif
            }
            else if( lCurTime >= finishTime)
            {
                pDoc->lflag.jullStop=0;
                pDoc->PLY_STATUS.IsPlay=0;
            }

        }

     	if(lCurTime >= pDoc->Oldltime)
    	{
     		pDoc->Oldltime=lCurTime;
     		while(1)
	  		{
				if( pDoc->EventListCnt >=pDoc->PLY_STATUS.TotalEventCnt) //song finished
				{
					pDoc->PLY_STATUS.IsPlay=0;
					break;
				}

               _PlayEvent=(EVENT_LIST) pDoc->vPlayMidiEvent[pDoc->EventListCnt];
				if(_PlayEvent.ltime <= lCurTime)
				{
					pDoc->EventListCnt++;
					pMIDI_MAIN->MessageOut(_PlayEvent);

				}
				else 	break;
			}
		}
		pMIDI_MAIN->MessageInsert();
		return ;
}


int midi_timer::midi_timerStop()
{
    MIDIClock_Stop(pDoc->pMIDIClock);
   return 1;
}

int midi_timer::midi_timerStart()
{
    MIDIClock_Start(pDoc->pMIDIClock);
   return 1;
}

int midi_timer::midi_timerReset()
{
    MIDIClock_Reset(pDoc->pMIDIClock);
    return 1;
}


