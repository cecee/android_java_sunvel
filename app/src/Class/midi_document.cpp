#include "extern.h"
#include "midi_document.h"
#include "lxMIDIData/lxMIDIData.h"
#include "lxMusicScore/music_score.h"
//#include "lxMusicScore/draw_score.h"
#include "Doc_cnw.h"

#include "B5_U5.H"
#include "K2UN.H"
#include "b52gb2be.h"
#include "JIS.H"

#if 1
#include <memory>
#include <fstream>
#include <iostream>
#include <malloc.h>
#endif

const int C1 = 4210;//박차번호
const int C2 = 8376;//장팀장차번호
const int KEY = 63915;//복정동639-15


using namespace std;
std::string midi_doc::ReplaceAll(std::string &str, const std::string& from, const std::string& to){
    size_t start_pos = 0; //string처음부터 검사
    while((start_pos = str.find(from, start_pos)) != std::string::npos)  //from을 찾을 수 없을 때까지
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // 중복검사를 피하고 from.length() > to.length()인 경우를 위해서
    }
    return str;
}

vector<string> midi_doc::split(string data, string token)
{
    vector<string> output;
    size_t pos = string::npos; // size_t to avoid improbable overflow
    do
    {
        pos = data.find(token);
        output.push_back(data.substr(0, pos));
        if (string::npos != pos)
            data = data.substr(pos + token.size());
    } while (string::npos != pos);
    return output;
}

midi_doc::midi_doc()
{
	
	pMIDIClock = NULL;
	pSaffMode= NULL;
	pMIDIData= NULL;
	ScoreTrack= NULL;
	memset(&songInfo,0,sizeof(SongInfoW));
	memset(&lflag,0,sizeof(lflag));
	lflag.Jni_LoadFlag=-1;
	lflag.ScoreDrawEnable=-1;
    lflag.chorus=-1;

    vSongTitle.clear();
	vLyricNote.clear();
	vLyricStr.clear();
	vLricTick.clear();
	vUniStr.clear();
	vPlayMidiEvent.clear();
	vPlayRhythmEvent.clear();

    vLi.clear();
    vStrScore.clear();
    v_staff.clear();
    vJullPos.clear();
    vStrScore.clear();
    vPCM_Chorus.clear();
    vMadiKeyInfo.clear();
    vJullInfo.clear();

	PLY_STATUS.hasMelody_track=0;
	PLY_STATUS.totalLyricLine =0;
	PLY_STATUS.rhythmKind=0;
	PLY_STATUS.chorusCnt=0;
	PLY_STATUS.IsPlay=0;
	PLY_STATUS.lCurOnPlay=1;
	PLY_STATUS.lCurKeyPlus=0;
	PLY_STATUS.lCurSpeed=0;//기준이 0 입니다.
	PLY_STATUS.lCurMelody=70;//기준이 90입니다.
	PLY_STATUS.A_B[0]=-1;
	PLY_STATUS.A_B[1]=-1;

	EventListCnt=0;
	Oldltime=0;

    init_satffMode();
    TRACK_Info.pTRACK_Sys=NULL;
    TRACK_Info.pTRACK_Nrpn=NULL;
    TRACK_Info.pTRACK_Melody=NULL;
    TRACK_Info.pTRACK_DrumP1C10=NULL;
    TRACK_Info.pTRACK_DrumP1C11=NULL;
    TRACK_Info.pTRACK_DrumP2C10=NULL;
    TRACK_Info.pTRACK_DrumP2C11=NULL;
    TRACK_Info.pTRACK_Bass1=NULL;
    TRACK_Info.pTRACK_Bass2=NULL;
    TRACK_Info.pTRACK_Chord1=NULL;
    TRACK_Info.pTRACK_Chord2=NULL;
    TRACK_Info.pTRACK_Lyric=NULL;
    TRACK_Info.pTRACK_Function=NULL;
    TRACK_Info.pTRACK_GK=NULL;
    TRACK_Info.pTRACK_BR=NULL;
    TRACK_Info.pTRACK_PS=NULL;
    TRACK_Info.pTRACK_INST=NULL;
    TRACK_Info.pTRACK_CHRS=NULL;
    TRACK_Info.pTRACK_SDFX=NULL;

    TRACK_Info.pTRACK_EXDR1=NULL;
    TRACK_Info.pTRACK_EXDR2=NULL;
    TRACK_Info.pTRACK_EXBASS=NULL;
    TRACK_Info.pTRACK_EXSYS=NULL;
    TRACK_Info.pTRACK_EXBREF=NULL;
}


midi_doc::~midi_doc()
{
	PLY_STATUS.IsPlay = 0;
    vPCM_Chorus.clear();
    vSongTitle.clear(); vector<MIDI_TITLE>().swap(vSongTitle);
    vLricTick.clear();vector<long>().swap(vLricTick);
    vLyricNote.clear(); vector<LYRIC_NOTE>().swap(vLyricNote);
   	vLi.clear(); vector<LINE_INFORMATION>().swap(vLi);
    vStrScore.clear(); vector<string>().swap(vStrScore);
    v_staff.clear(); vector<string>().swap(v_staff);
    vJullInfo.clear(); vector<JULL_INFO>().swap(vJullInfo);

    //if(pCrypt != NULL) { delete pCrypt; pCrypt=NULL;}
}
/*
void midi_doc::vPCM_Chorus_clear(){
	for(int i=0; i<vPCM_Chorus.size();i++){
	    if(vPCM_Chorus[i].pcm_buffer !=NULL ) free(vPCM_Chorus[i].pcm_buffer);
	}
	vPCM_Chorus.clear();vector<PCM_BUFFER>().swap(vPCM_Chorus);
}
*/
void midi_doc::init_satffMode()
{
/////////INBAR
	staff_mode[0].enStaff=1;
	staff_mode[0].cntMeasurePerLine=2;
	staff_mode[0].cntLinePerPage=2;
	staff_mode[0].cntMeasurePerPage=2;//each 2measure drawing again
	//staff_mode[0].enableZiffer=1;//inbar need chinease
	staff_mode[0].enableZiffer=0;
	staff_mode[0].enableObli=0;
	staff_mode[0].cursorlengh=SCORE_BAR_LENGTH1;
/////////MLX
	staff_mode[1].enStaff=1;
	staff_mode[1].cntMeasurePerLine=2;
	staff_mode[1].cntLinePerPage=2;
	staff_mode[1].cntMeasurePerPage=8;
	//staff_mode[1].enableZiffer=1; /inbar need chinease
	staff_mode[1].enableZiffer=0;
	staff_mode[1].enableObli=1;
	staff_mode[1].cursorlengh=SCORE_BAR_LENGTH2;
////////////
	staff_mode[2].enStaff=0;
}

void midi_doc::set_satffMode(int type)
{
    /////////type 0:DK 1:MLX 2:MIDI
    pSaffMode=&staff_mode[type];
    if(pDraw_score!=NULL){
        pDraw_score->Ziffersystem=pSaffMode->enableZiffer;
        pDraw_score->lCursorLen = pSaffMode->cursorlengh;
        pDraw_score->ObliEnable = pSaffMode->enableObli;
        pDraw_score->cntMeasurePerLine = pSaffMode->cntMeasurePerLine;
        pDraw_score->cntLinePerPage = pSaffMode->cntLinePerPage;
        pDraw_score->cntMeasurePerPage  =pSaffMode->cntMeasurePerPage;
    }
    pSaffMode= NULL;
}

int midi_doc::MIDIDOC_Create(char *filename)
{
	Doc_cnw *pDoc_cnw=NULL;
    pMusic_score=NULL;

    int kind;
    char *pData[64]={0,};
    int rtn=0;
    kind = MIDIDOC_GetKindFile(filename);
	TRACE("#@# MIDIDOC_Create=====filename[%s]-kind[%x]\n",filename, kind);
	if(kind<0) return 0;//file 없음
	if(kind>0){

	    rtn=MIDIDOC_mkSplitFile(filename, kind);
	    if(rtn==0) return 0;//file 없음
        sprintf(filename,"/sdcard/.00");
	}

	PLY_STATUS.lCursorLen=80;
    pDoc_cnw = new Doc_cnw;
    pMusic_score= new music_score;
    pDraw_score= new draw_score;
    set_satffMode(0);

    rtn=pDoc_cnw->Update_cnw(filename);

    if(rtn==0){
        if(pDraw_score) delete pDraw_score;
        if(pMusic_score) delete pMusic_score;
        pDraw_score=NULL;
        pMusic_score=NULL;
    }

#if 1
        TRACE("#@# MIDIData_SaveAsSMFA =====pDoc->pMIDIData[%x]\n",pDoc->pMIDIData);
		MIDIData_SaveAsSMFA(pDoc->pMIDIData, "/sdcard/test.mid");//For test
#endif

    PLY_STATUS.lSong_Kind=0;//MIDI Only
    delete pDoc_cnw;
	return rtn;
}

int midi_doc::MIDIDOC_mkSplitFile(char *filename, int kind)
{

    FILE *infile;
    FILE *outfile;
    int i;
    int fileLength;
    int mdx_cnt;
    long mdx_offset;

    char temp_buf[16];
    char *buf;
    char *decrypt;
    //char *decrypt_buf;

    char pData[256]={0,};
    string strFileName;
    infile=fopen(filename,"rb");
    if(infile==NULL){
#ifdef DBG_MDK
        TRACE("#@#DBG_MDK MIDIDOC_mkSplitFile===========>_File Open Error!!![%s]\n",filename);
#endif
        return 0;
    }

    fseek(infile, 0, SEEK_END);
    fileLength = ftell(infile);
    fseek( infile,  -3, SEEK_END);
    memset(temp_buf,0,16);
    fread( temp_buf, 8, 1, infile );
    mdx_cnt=temp_buf[0];
    PLY_STATUS.chorusCnt=mdx_cnt;
    mdx_offset=fileLength-((mdx_cnt*12)+3);
#ifdef DBG_MDK
    TRACE("#@#DBG_MDK mdx_cnt[%d] mdx_offset [%x]\n",mdx_cnt, mdx_offset);
#endif
    long offset;
    vPCM_Chorus.clear();
    for(i=0;i<mdx_cnt;i++){
        char decrypt_buf[12];
        fseek( infile,  mdx_offset+(i*12), SEEK_SET);
        memset(temp_buf,0,16);
        fread( temp_buf, 12, 1, infile );
  #ifdef DBG_MDK
      TRACE("#@#DBG_MDK mdx_cnt[%d/%d] temp_buf[%x][%x][%x][%x]\n",i, mdx_cnt, temp_buf[0], temp_buf[1], temp_buf[2], temp_buf[3]);
  #endif
      if(kind==0x0c)  {
        memset(decrypt_buf,0,12);
        Decrypt(temp_buf,decrypt_buf,12);
        memset(&mdk_table,0,sizeof(mdk_table));
        memcpy(&mdk_table,decrypt_buf,sizeof(mdk_table));
      }
      else{
        memset(&mdk_table,0,sizeof(mdk_table));
        memcpy(&mdk_table,temp_buf,sizeof(mdk_table));
      }
    #ifdef DBG_MDK
        TRACE("#@#DBG_MDK mdx_cnt[%d] offset[%08x] size[%x] idx[%x]\n",i, mdk_table.offset, mdk_table.size, mdk_table.idx);
    #endif
        memset(temp_buf,0,16);
        fseek(infile, mdk_table.offset, SEEK_SET);
        buf=(char *)malloc(mdk_table.size);
        fread( buf,1, mdk_table.size, infile );

        memset(pData,0,sizeof(pData));
        if(i==0){//midi file decrypt
            sprintf(pData,"/sdcard/.%02X",mdk_table.idx);
            remove(pData);
            outfile=fopen(pData,"wb");
            if(kind==0x0c){
                decrypt=(char *)malloc(mdk_table.size);
                memset(decrypt,0,mdk_table.size);
                Decrypt(buf,decrypt,mdk_table.size);
                fwrite(decrypt,1, mdk_table.size, outfile );
                free(decrypt);
            }
            else{
                fwrite(buf,1, mdk_table.size, outfile );
             }
            fclose(outfile);
        }
        else
        {
            sprintf(pData,"/sdcard/%02X",mdk_table.idx);
     #ifdef DBG_MDK
         TRACE("#@#DBG_MDK mdx_cnt[%d] file[%s] size[%x] idx[%x]\n",i, pData, mdk_table.size, mdk_table.idx);
     #endif
            outfile=fopen(pData,"wb");
            fwrite( buf,1, mdk_table.size, outfile );
            fclose(outfile);
            strFileName = (char *)pData;
            vPCM_Chorus.push_back(strFileName);
        }
        free(buf);
    }
    fclose(infile);
    return 1;
}

int midi_doc::MIDIDOC_GetKindFile(char *filename)
{

    FILE *infile;
    char temp_buf[8];
    int kind= -1;

    string strFileName=(char*)filename;
    infile=fopen(filename,"rb");
    if(infile==NULL){
         TRACE("#@#===========>MIDI_File Open Error!!![%s]\n",filename);
         return -1;
    }
    fread( temp_buf, 8, 1, infile );
    if (strFileName.find("mdk") != string::npos){
         if (temp_buf[0]=='M' && temp_buf[1]=='T' && temp_buf[2]=='h' && temp_buf[3]=='d') kind = 1;//non-crypt and package file
        else  kind = 0x0c;//crypt and package file

    }
    else if (strFileName.find("mid") != string::npos){
        if (temp_buf[0]=='M' && temp_buf[1]=='T' && temp_buf[2]=='h' && temp_buf[3]=='d') kind = 0;//pure midi file
        else  kind = -1;//non define

    }

    fclose(infile);
    return kind;
}


 int  midi_doc::compareX(const void* a, const void* b)
{
   EVENT_LIST* q0= (EVENT_LIST*)a;
   EVENT_LIST* q1= (EVENT_LIST*)b;
   if ( q0->ltime < q1->ltime)     return -1;
   else if (q0->ltime > q1->ltime) return 1;
   else  return 0;
}

void midi_doc::DK_LoadChorus(MIDITrack* pMIDITrack, char* file)
{
    int i;
    //int ChorusCnt=0;
    long length;
    long ltime,lpc;
    ifstream is;
    MIDIEvent* pMIDIEvent = NULL;
    char pData[128];
    int ChorusCnt=PLY_STATUS.chorusCnt;

    if(ChorusCnt<1) return;
#ifdef DBG_MDK
    TRACE("#@#DBG_MDK DK_LoadChorus----------------(1)\n");
#endif
    int idx=0;
    forEachEvent (pMIDITrack, pMIDIEvent) {
        if(MIDIEvent_IsProgramChange(pMIDIEvent)){
            ltime = MIDIEvent_GetTime (pMIDIEvent);
            lpc=MIDIEvent_GetNumber(pMIDIEvent);
            memset(pData,0,128);
            sprintf(pData,"c%02d",lpc);
 #ifdef DBG_MDK
     TRACE("#@#DBG_MDK DK_LoadChorus Marking[%s] ltime[%d]----------------(1)\n",pData, ltime);
 #endif
            //MIDITrack_InsertMarker(pMIDITrack, ltime-90, pData);//(1)90tick정도 빠르게
            MIDITrack_InsertMarker(pMIDITrack, ltime-80, pData);//(2)80tick정도 빠르게 노부장이 조금 빠르다해서 190317
        }
    }
}

void midi_doc::remove_event(MIDITrack* pMIDITrack)
{
    MIDIEvent* pMIDIEvent = NULL;
    MIDIEvent *pNextEvent=NULL;
    pMIDIEvent=pMIDITrack->m_pFirstEvent;
    while(pMIDIEvent){
        pNextEvent=pMIDIEvent->m_pNextEvent;
        if(MIDIEvent_IsEndofTrack(pMIDIEvent)){;}
        else if(MIDIEvent_IsTrackName(pMIDIEvent)){;}
        else MIDIEvent_DeleteSingle(pMIDIEvent);
        pMIDIEvent=pNextEvent;
    }
}

void midi_doc::put_ctrl_event(MIDIData* ctrlMIDIData)
{
    MIDITrack* pMIDITrack = NULL;
    MIDIEvent* pMIDIEvent = NULL;
    long ltime,ch,number,value;

   	forEachTrack (ctrlMIDIData, pMIDITrack) {
        forEachEvent (pMIDITrack, pMIDIEvent) {
            ltime=MIDIEvent_GetTime(pMIDIEvent);
            if(MIDIEvent_IsControlChange(pMIDIEvent)) {
                ch = MIDIEvent_GetChannel(pMIDIEvent);
                number = MIDIEvent_GetNumber(pMIDIEvent);
                value = MIDIEvent_GetValue(pMIDIEvent);
                switch(ch){
                     case 1://ch2
                         MIDITrack_InsertControlChange(pDoc->TRACK_Info.pTRACK_EXBASS, ltime, ch, number, value);
                         break;
                     case 9://ch10
                         MIDITrack_InsertControlChange(pDoc->TRACK_Info.pTRACK_EXDR1, ltime, ch, number, value);
                         break;
                     case 10://ch11
                         MIDITrack_InsertControlChange(pDoc->TRACK_Info.pTRACK_EXDR2, ltime, ch, number, value);
                         break;
                }

            }
            else if(MIDIEvent_IsProgramChange(pMIDIEvent)) {
                 ch = MIDIEvent_GetChannel(pMIDIEvent);
                 number = MIDIEvent_GetNumber(pMIDIEvent);
                 switch(ch){
                      case 1://ch2
                          MIDITrack_InsertProgramChange(pDoc->TRACK_Info.pTRACK_EXBASS, ltime, ch, number);
                          break;
                      case 9://ch10
                          //MIDITrack_InsertControlChange(pDoc->TRACK_Info.pTRACK_EXDR1, pMIDIEvent);
                          MIDITrack_InsertProgramChange(pDoc->TRACK_Info.pTRACK_EXDR1, ltime, ch, number);
                          break;
                      case 10://ch11
                         // MIDITrack_InsertControlChange(pDoc->TRACK_Info.pTRACK_EXDR2, pMIDIEvent);
                          MIDITrack_InsertProgramChange(pDoc->TRACK_Info.pTRACK_EXDR2, ltime, ch, number);
                          break;
                 }

             }
         }
   	}
}

void midi_doc::put_drum_event(vector<MIDIEvent*> vEvent )
{
    MIDITrack* pMIDITrack = NULL;
    MIDIEvent* pMIDIEvent = NULL;
    int i,j,vSz;
    long last_madi, last_block;
    long llMx,llBx,llTx;
    long m_time, ltime, ch, key, velo, dura;
    MIDIData_BreakTime(pMIDIData, pDoc->PLY_STATUS.SongEnd_ltime, &llMx, &llBx, &llTx);
    last_madi=llMx;
    last_block=last_madi/4;
    last_block=((last_madi%4)!=0) ? last_block+1: last_block;

    vSz=vEvent.size();

       for(i=0;i<last_block;i++){
          for(j=0;j<vSz;j++){
             pMIDIEvent = vEvent[j];
            // if(MIDIEvent_IsNoteOn(pMIDIEvent) || MIDIEvent_IsControlChange(pMIDIEvent) || MIDIEvent_IsProgramChange(pMIDIEvent)){
             if(MIDIEvent_IsNoteOn(pMIDIEvent))
             {
                m_time=MIDIEvent_GetTime(pMIDIEvent);
                ltime=(i*(480*4))+m_time;
                ch = MIDIEvent_GetChannel(pMIDIEvent);
                key=MIDIEvent_GetKey(pMIDIEvent);
                velo=MIDIEvent_GetVelocity(pMIDIEvent);
                dura=MIDIEvent_GetDuration(pMIDIEvent);
                switch(ch){
                      case 9://ch10
                         MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXDR1, ltime, ch, key, velo,dura);
                         break;
                     case 10://ch11
                         MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXDR2, ltime, ch, key, velo,dura);
                         break;
                }
             }

          }
      }
}

void midi_doc::put_bass_event(vector<MIDIEvent*> vEvent )
{

    typedef struct _SEC_INFO
    {
        vector<MIDIEvent*> vSectionEvent;
    }SEC_INFO;
    SEC_INFO section[8];
    long vSecSz[8];
    long RefKey;
    MIDIEvent* vMIDIEvent;
    MIDIEvent* pMIDIEvent;
    long vltime, ltime, ch, key, velo, dura;
    long llMx,llBx,llTx;
    long lMadiKeyInfo, lmadiKey1, lmadiKey2;
    int vSz = vEvent.size();
    int madiCnt = pDoc->vMadiKeyInfo.size();
    int i,j;

    for(i=0;i<8;i++) section[i].vSectionEvent.clear();
    for(i=0;i<vSz;i++){
        vMIDIEvent = vEvent[i];
        if(MIDIEvent_GetChannel(vMIDIEvent)!=1) continue;
        if(!MIDIEvent_IsNoteOn(vMIDIEvent)) continue;

        vltime = MIDIEvent_GetTime(vMIDIEvent);
        if(vltime>=0 && vltime<240) {
            section[0].vSectionEvent.push_back(vMIDIEvent);
            if(vltime==0)  RefKey = MIDIEvent_GetKey(vMIDIEvent);
#ifdef DBG_RHY
       TRACE("#@#DBG put_bass_event vltime[%d]\n",vltime);
#endif
        }
        else if(vltime>=240 && vltime<480) section[1].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=480 && vltime<720) section[2].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=720 && vltime<960) section[3].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=960 && vltime<1200) section[4].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=1200 && vltime<1440) section[5].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=1440 && vltime<1680) section[6].vSectionEvent.push_back(vMIDIEvent);
        else if(vltime>=1680 && vltime<1920) section[7].vSectionEvent.push_back(vMIDIEvent);
    }
    for(i=0;i<8;i++) vSecSz[i] = section[i].vSectionEvent.size();

#ifdef DBG_RHY
    for(i=0;i<8;i++){
       TRACE("#@#DBG put_bass_event vSecSz[%d][%d] RefKey[%d]\n",i, vSecSz[i], RefKey);
     }
#endif

    for(i=0;i<madiCnt;i++)
    {

        lMadiKeyInfo = pDoc->vMadiKeyInfo[i];
        lmadiKey1= (lMadiKeyInfo>>8)&0xff;
        lmadiKey2= lMadiKeyInfo &0xff;

        if(lmadiKey1 != 0xfe) lmadiKey1 = lmadiKey1-RefKey;
        if(lmadiKey2 != 0xfe) lmadiKey2 = lmadiKey2-RefKey;

        long lmsec=i%4;
        switch(lmsec)
        {
#if 1
            case 0: //0,4,8,12번째 마디 Beat1--process
                if(lmadiKey1 !=0xfe){
                    for(j=0;j<vSecSz[0];j++){
                         vMIDIEvent = section[0].vSectionEvent[j]; //0~240
                         vltime = MIDIEvent_GetTime(vMIDIEvent);
                         key=MIDIEvent_GetKey(vMIDIEvent);
                         //key= key + key_adj[lmadiKey1];
                         key= key + lmadiKey1;
                         velo=MIDIEvent_GetVelocity(vMIDIEvent);
                         dura=MIDIEvent_GetDuration(vMIDIEvent);
                         MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);
                    }
                }
                if(lmadiKey2 !=0xfe){
                     for(j=0;j<vSecSz[1];j++){
                         vMIDIEvent = section[1].vSectionEvent[j]; //240~480
                         vltime = MIDIEvent_GetTime(vMIDIEvent);
                         key=MIDIEvent_GetKey(vMIDIEvent);
                         //key=key+key_adj[lmadiKey2];
                         key= key + lmadiKey2;
                         velo=MIDIEvent_GetVelocity(vMIDIEvent);
                         dura=MIDIEvent_GetDuration(vMIDIEvent);
                         MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);
                     }

                }
                break;
#endif
            case 1: //1,5,9,13번째 마디 Beat1--process
                if(lmadiKey1 !=0xfe){
                    for(j=0;j<vSecSz[2];j++){
                          vMIDIEvent = section[2].vSectionEvent[j]; //480~720
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-480;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey1];
                          key= key + lmadiKey1;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);
                    }
                }
                if(lmadiKey2 !=0xfe){
                     for(j=0;j<vSecSz[3];j++){
                          vMIDIEvent = section[3].vSectionEvent[j]; //720~960
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-480;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey2];
                          key= key + lmadiKey2;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);

                     }
                }
                break;
            case 2:
                 if(lmadiKey1 !=0xfe){
                     for(j=0;j<vSecSz[4];j++){
                         vMIDIEvent = section[4].vSectionEvent[j]; //960-1200
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-960;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey1];
                          key= key + lmadiKey1;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);

                     }
                 }
                 if(lmadiKey2 !=0xfe){
                      for(j=0;j<vSecSz[5];j++){
                          vMIDIEvent = section[5].vSectionEvent[j]; //720~960
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-960;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey2];
                          key= key + lmadiKey2;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);

                      }
                 }
                break;
            case 3:
                if(lmadiKey1 !=0xfe){
                    for(j=0;j<vSecSz[6];j++){
                         vMIDIEvent = section[6].vSectionEvent[j]; //1440-1680
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-1440;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey1];
                          key= key + lmadiKey1;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);

                    }
                }
                if(lmadiKey2 !=0xfe){
                     for(j=0;j<vSecSz[7];j++){
                          vMIDIEvent = section[7].vSectionEvent[j]; //1440~1920
                          vltime = MIDIEvent_GetTime(vMIDIEvent)-1440;
                          key=MIDIEvent_GetKey(vMIDIEvent);
                          //key=key+key_adj[lmadiKey2];
                          key= key + lmadiKey2;
                          velo=MIDIEvent_GetVelocity(vMIDIEvent);
                          dura=MIDIEvent_GetDuration(vMIDIEvent);
                          MIDITrack_InsertNote(pDoc->TRACK_Info.pTRACK_EXBASS, (i*480)+vltime, ch, key, velo,dura);

                     }
                }
                break;
        }
    }

    for(i=0;i<8;i++) {
        for(j=0;j<section[i].vSectionEvent.size();j++){
            section[i].vSectionEvent[j] = NULL;
        }
        section[i].vSectionEvent.clear();
    }

}

int midi_doc::put_rhyevent(char *pattern_file, char *ctrl_file )
{
    MIDIData* rhyMIDIData = MIDIData_LoadFromSMFA(pattern_file);
    MIDIData* ctrlMIDIData = MIDIData_LoadFromSMFA(ctrl_file);
    if(rhyMIDIData==NULL || ctrlMIDIData==NULL){
        pDoc->PLY_STATUS.rhythmKind=0;//no rhythm pattern
        return 0;
    }
    MIDITrack* pMIDITrack = NULL;
    MIDIEvent* pMIDIEvent = NULL;
    MIDIEvent *pNextEvent = NULL;
    vector<MIDIEvent*>vAevent;
    long  ltempo;
    vAevent.clear();

    MIDIData_SetTimeBase(rhyMIDIData, MIDIDATA_TPQNBASE, 120);
    MIDIData_SetTimeBase(ctrlMIDIData, MIDIDATA_TPQNBASE, 120);
////(1)SYSEX Track Tempo설정
    MIDIData_FindTempo(rhyMIDIData,480,&ltempo);
    MIDITrack_InsertTempo(pDoc->TRACK_Info.pTRACK_Sys, 480,ltempo);

   	forEachTrack (rhyMIDIData, pMIDITrack) {
        forEachEvent (pMIDITrack, pMIDIEvent) {
            MIDIEvent_Combine(pMIDIEvent);
            vAevent.push_back(pMIDIEvent);
         }
   	}

    put_ctrl_event(ctrlMIDIData);
    put_bass_event(vAevent);
    put_drum_event(vAevent);

    vAevent.clear(); vector<MIDIEvent*>().swap(vAevent);

    MIDIData_Delete(rhyMIDIData);
    MIDIData_Delete(ctrlMIDIData);
    rhyMIDIData = NULL;
    ctrlMIDIData = NULL;
    return 1;
}

int midi_doc::Make_Rhythm_pattern()
{
    int i, j;

    MIDITrack* pMIDITrack = NULL;
    MIDIEvent* pMIDIEvent = NULL;
    MIDIEvent *pNextEvent=NULL;
    char pData[128];
    char cData[128];

    long ltime, ltempo;
    int rhythm= pDoc->PLY_STATUS.rhythmKind;

  //sys track tempo delete++
    pMIDITrack = pDoc->TRACK_Info.pTRACK_Sys;
    pMIDIEvent=pMIDITrack->m_pFirstEvent;
    while(pMIDIEvent)
    {
      pNextEvent=pMIDIEvent->m_pNextEvent;
      if(MIDIEvent_IsTempo(pMIDIEvent)){
         MIDIEvent_DeleteSingle(pMIDIEvent);
      }
      pMIDIEvent=pNextEvent;
    }
  //sys track tempo delete--
    if(rhythm==0) {
     //sys track tempo 복구
#ifdef DBG_RHY
      TRACE("#@#DBG_RHY ==========pDoc->TRACK_Info.pTRACK_Sys  tempo ==========복구==============\n");
#endif
        pMIDITrack = pDoc->TRACK_Info.pTRACK_EXSYS;
        pMIDIEvent=pMIDITrack->m_pFirstEvent;
        while(pMIDIEvent)
        {
            pNextEvent=pMIDIEvent->m_pNextEvent;
            if(MIDIEvent_IsTempo(pMIDIEvent)){
                ltime = MIDIEvent_GetTime(pMIDIEvent);
                ltempo = MIDIEvent_GetTempo(pMIDIEvent);
                MIDITrack_InsertTempo(pDoc->TRACK_Info.pTRACK_Sys, ltime,ltempo);//ppp tempo 240
            }
            pMIDIEvent=pNextEvent;
        }
        return 0;
   }

//(1)모든이벤트삭제++
    remove_event(pDoc->TRACK_Info.pTRACK_EXBASS);
    remove_event(pDoc->TRACK_Info.pTRACK_EXDR1);
    remove_event(pDoc->TRACK_Info.pTRACK_EXDR2);
//모든이벤트삭제--
   switch(rhythm)
   {
        case 1:
            sprintf(pData,"/sdcard/pattern/DS1/DS1-1.MID");
            sprintf(cData,"/sdcard/pattern/DS1/DS1-1C.MID");
            break;//
         case 2:
             sprintf(pData,"/sdcard/pattern/DS2/DS2-1.MID");
             sprintf(cData,"/sdcard/pattern/DS2/DS2-1C.MID");
             break;//

         case 3:
             sprintf(pData,"/sdcard/pattern/DS3/DS3-1.MID");
             sprintf(cData,"/sdcard/pattern/DS3/DS3-1C.MID");
             break;//
          case 4:
              sprintf(pData,"/sdcard/pattern/GO1/GO1-1.MID");
              sprintf(cData,"/sdcard/pattern/GO1/GO1-1C.MID");
              break;//
         case 5:
             sprintf(pData,"/sdcard/pattern/GO2/GO2-1.MID");
             sprintf(cData,"/sdcard/pattern/GO2/GO2-1C.MID");
             break;//
         case 6:
             sprintf(pData,"/sdcard/pattern/HK/HK-1.MID");
             sprintf(cData,"/sdcard/pattern/HK/HK-1C.MID");
             break;//
         case 7:
             sprintf(pData,"/sdcard/pattern/SW1/SW1-1.MID");
             sprintf(cData,"/sdcard/pattern/SW1/SW1-1C.MID");
             break;//
             break;//
         case 8:
             sprintf(pData,"/sdcard/pattern/SW2/SW2-1.MID");
             sprintf(cData,"/sdcard/pattern/SW2/SW2-1C.MID");
             break;//
         case 9:
             sprintf(pData,"/sdcard/pattern/TW/TW-1.MID");
             sprintf(cData,"/sdcard/pattern/TW/TW-1C.MID");
             break;//


   }
   put_rhyevent(pData, cData );

#ifdef DBG_RHY
      TRACE("#@#DBG_RHY make test midi file=====\n");
      MIDIData_SaveAsSMFA (pMIDIData, "/sdcard/xx.mid");
#endif
   return rhythm;

}

void midi_doc::Make_EventList()
{
    int i, isPattern, eventCnt;
    long ltime, port, lch;
    char pData[128];
    string trackName="";

    MIDITrack* pMIDITrack = NULL;
    MIDIEvent* pMIDIEvent = NULL;
 //   int rhythmKind = pDoc->PLY_STATUS.rhythmKind;
#ifdef RHY_FUNCTION
    isPattern = Make_Rhythm_pattern();
#else
    isPattern = 0;
#endif

#ifdef DBG_RHY
    TRACE("#@#DBG Make_Rhythm_EventList =================isPattern[%d]=============\n",isPattern);
#endif
///=================================
    eventCnt=0;
   	forEachTrack (pMIDIData, pMIDITrack) {
         forEachEvent (pMIDITrack, pMIDIEvent)  eventCnt++;
   	}

  	EVENT_LIST* pListEvent=(EVENT_LIST*)calloc( eventCnt , sizeof(EVENT_LIST));
    EVENT_LIST* m_pEventList ;
   	eventCnt=0;
   	forEachTrack (pMIDIData, pMIDITrack) {
        port=MIDITrack_GetOutputPort(pMIDITrack);
        lch=MIDITrack_GetOutputChannel(pMIDITrack);

        MIDITrack_GetName(pMIDITrack,pData,128);
        trackName=(char*)pData;
        if (trackName.find("NRPN") != string::npos) continue;

        if (isPattern==0)
        {
            if (trackName.find("@X@") != string::npos) continue;
        }
        else //isPattern==1 일때 Drum-Track, Base-Track 연주하지 않음
        {
               if (trackName.find("@R@") != string::npos || trackName.find("@B@") != string::npos || trackName.find("@X@sys") != string::npos) continue;
        }

        forEachEvent (pMIDITrack, pMIDIEvent) {
               ltime = MIDIEvent_GetTime (pMIDIEvent);
               m_pEventList=(EVENT_LIST*)(pListEvent+eventCnt);
               m_pEventList->ltime=ltime;
               m_pEventList->port=port;
               m_pEventList->PlayEvent=pMIDIEvent;
               eventCnt++;
         }
   	}
   	qsort((EVENT_LIST*) pListEvent,  // Beginning address of array
       			eventCnt,            // Number of elements in array
       			sizeof(EVENT_LIST),  // Size of each element
       			compareX );          // Pointer to compare function
////push_back++
    vPlayMidiEvent.clear();
	EVENT_LIST* _pEventList ;
	EVENT_LIST play_event;
	for(i=0;i<eventCnt;i++)
	{
		_pEventList=(EVENT_LIST*)(pListEvent+i);
        play_event.ltime=_pEventList->ltime;
        play_event.port=_pEventList->port;
        play_event.PlayEvent=_pEventList->PlayEvent;
        vPlayMidiEvent.push_back(play_event);
	//	TRACE("#@# [%d] pListEvent->ltime[%ld] pListEvent->Event[%lx]   port[%ld]\n", i,  _pEventList->ltime,  _pEventList->PlayEvent, _pEventList->port);
	}
////push_back--
    free(pListEvent);
 	PLY_STATUS.TotalEventCnt=eventCnt;
 	TRACE("#@### Make_Rhythm_EventList TotalEventCnt[%d]\n",PLY_STATUS.TotalEventCnt);
	return;
}


int midi_doc::mkTimeTableNoteON(MIDITrack *pMIDITrack)
{

    typedef struct _LLYRIC
    {
        long ltime;
        string str;
    } LLYRIC;

	MIDIEvent *pMIDIEvent=NULL;
	int i,ix, sz;
	int channel;
	long ltime,Dura;
	long title_time = 480*5;
	char pData[256];
	LLYRIC llyric;

    vector<LYRIC_NOTE> vNoteTick;
    vector<string> vmLyStr;
    vector<LLYRIC> vllyric;

    LYRIC_NOTE lvLN;
    string xStr,yStr, pStr;
    Uint16 *Dw;
    char strUTF8[256]={0,};
    int LyricCode = PLY_STATUS.LyricCode;

 	PLY_STATUS.SongEnd_ltime = MIDIData_GetEndTime(pDoc->pMIDIData);
 	PLY_STATUS.SongEnd_ms =MIDIData_TimeToMillisec(pDoc->pMIDIData, PLY_STATUS.SongEnd_ltime);
 	PLY_STATUS.lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);

    i=0;
    forEachEvent (pMIDITrack, pMIDIEvent) {
        if(MIDIEvent_IsNoteOn(pMIDIEvent)){
            ltime = MIDIEvent_GetTime (pMIDIEvent);
            Dura=MIDIEvent_GetDuration (pMIDIEvent);
                if(i==0)  title_time=ltime;
                lvLN.ltimeA=ltime;//Note 끝내는시간
                lvLN.ltimeB=Dura+ltime;//Note 끝내는시간
                vNoteTick.push_back(lvLN);
                i++;
        }
    }



    PLY_STATUS.lNoteCnt=0;
	if(i==0){
	    PLY_STATUS.lxLyricCnt=0;
	    return PLY_STATUS.lNoteCnt;
	}

    vllyric.clear();
    forEachEvent (pMIDITrack, pMIDIEvent) {
        if(MIDIEvent_IsLyric(pMIDIEvent))
        {
            ltime = MIDIEvent_GetTime (pMIDIEvent);
            MIDIEvent_GetText(pMIDIEvent, pData, 256);
            llyric.ltime=ltime;
            pStr= pData;
            llyric.str=pStr;
            vllyric.push_back(llyric);
        }
    }

    sz=vllyric.size();
    vmLyStr.clear();
    long exltime=0; xStr=""; yStr="";
    for(i=0;i<sz;i++){
        long nextTime= (i!=(sz-1)) ? vllyric[i+1].ltime:PLY_STATUS.SongEnd_ltime;
        xStr=vllyric[i].str;
        ltime=vllyric[i].ltime;
        yStr=yStr+xStr;
        if(nextTime !=ltime){
            memset(pData,0,256);
            strcpy(pData, yStr.c_str());
            Dw = b2w_CU(pData, LyricCode);
            memset(strUTF8,0,256);
            uni2utf(Dw, (Uint8*)strUTF8);
            xStr=(char *)strUTF8;
            ReplaceAll(xStr," ","");
            ReplaceAll(xStr,"_","");//"_"삭제
            ReplaceAll(xStr,"^"," ");
            ReplaceAll(xStr,"\r","¶");
            ReplaceAll(xStr,"\n","¶");
            ReplaceAll(xStr,"¶¶","¶");
            ///TRACE("#@#vv====[%s]\n",xStr.c_str());
            vmLyStr.push_back(xStr);
            yStr="";
        }
    }

/// [Abc][/][/¶] ===> [Abc¶][/][/¶]   바꾸고 낭중에 [Abc¶]+++
/// [Abc][/][/^] ===> [Abc^][/][/^]   바꾸고 낭중에 [Abc^]+++
    sz=vmLyStr.size();
    for(i=0;i<sz;i++){
         if (vmLyStr[i].find("/¶") != string::npos){
            for(ix=i;ix>0;ix--){
                if (vmLyStr[ix].find("/") == string::npos){
                    vmLyStr[ix]=vmLyStr[ix]+"¶";
                    break;
                }
            }
         }
         else if (vmLyStr[i].find("/ ") != string::npos){
             for(ix=i;ix>0;ix--){
                 if (vmLyStr[ix].find("/") == string::npos){
                     vmLyStr[ix]=vmLyStr[ix]+" ";
                     break;
                 }
             }
          }
    }
/// [Abc][/][/¶] ===> [Abc¶][/][/¶]   바꾸고 낭중에 [Abc¶]---


    int noteSz=vNoteTick.size();
    int lyricSz=vmLyStr.size();

    for(i=0;i<noteSz;i++){
        if(i<=lyricSz) vNoteTick[i].str=vmLyStr[i];
    }

	PLY_STATUS.lNoteCnt = vNoteTick.size();
//	PLY_STATUS.TitleOffTime = title_time;
//	PLY_STATUS.LyricDisplay_ltime = title_time;

	Put_LineInfo(vNoteTick);

	vector<string> ().swap(vmLyStr);
	vector<LYRIC_NOTE> ().swap(vNoteTick);

	TRACE("#@#vv======== mkTimeTableNoteON  noteSz[%d] lyricSz[%d]=======\n", noteSz, lyricSz);
	return PLY_STATUS.lNoteCnt ;
}

vector<LINE_INFORMATION>  midi_doc::MK_LineInfor(vector<LYRIC_NOTE> vLyricNote)
{
    LINE_INFORMATION mLi;
    LYRIC_NOTE mLN;
    vector<LINE_INFORMATION> vmli;
    vector<LYRIC_NOTE> vmLN;
    int i;
    string str,yStr;
    char pData[256];
    vmli.clear();

    int vSz=vLyricNote.size();
    for(i=0;i<vSz;i++){
        mLN=vLyricNote[i];
       str=mLN.str;
       ReplaceAll(mLN.str,"¶¶","");
       ReplaceAll(mLN.str,"¶","");
        vmLN.push_back(mLN);

        if (str.find("¶¶") != string::npos){//엔터두개있는경우
            mLi.vLyNote=vmLN;
            mLi.Blk_Cnt=vmLN.size();
            vmli.push_back(mLi);
          ////dummy line
            mLi.Blk_Cnt=0; //counter만 로하고 나머지는 똑같이 낭중에 ltimeA, ltimeB 계산 용이하도록
            vmli.push_back(mLi);//dummy line추가 빈라인
            vmLN.clear();
        }
       // else if (mLN.str.find("¶") != string::npos){
        else if (str.find("¶") != string::npos){
             mLi.vLyNote=vmLN;
             mLi.Blk_Cnt=vmLN.size();
             vmli.push_back(mLi);
             vmLN.clear();
         }
    }
    //마지막라인추가...
     mLi.vLyNote=vmLN;
     mLi.Blk_Cnt=vmLN.size();
     vmli.push_back(mLi);
     vmLN.clear();
    TRACE("#@#vv PLY_STATUS.totalLyricLine[%d] vmli[%d]\n", PLY_STATUS.totalLyricLine, vmli.size());
    return vmli;
}

int midi_doc::Put_LineInfo(vector<LYRIC_NOTE> vNoteTick)
{
	int i,ix,j;
	int line, before_erCnt, erCnt;
	int line_cnt, blk_cnt;
	int WcharCnt;
//	int lineOffset=3;
//	int Tbl_LyricOffset=1;
//	int lupdn;
	int mCnt=0;
	int mBlkCnt=0;
	LINE_INFORMATION mLineInfo;
    vector<LYRIC_NOTE> vmLyNote;
    vector<string>vLine;

    LYRIC_NOTE mLN;
    vmLyNote.clear();
    vLine.clear();
    vLi.clear();
 /*
     vector<LYRIC_NOTE> vLyNote;
 	int  Blk_Cnt;
 	int Line_StartTime;
 	int Line_EndTime;//display 시간
 	int updn;  //0:up  1:dn -1: No draw
 	int LineSex;
	int LineAttr;
     int PosA;
     int PosB;
     long Text_length;
 	Uint16 Line_DispStr[128]; //unicode
 //	int LineGanju;
// 	int WcharCnt;
 //	int index[128];
typedef struct _LYRIC_NOTE
{
	long ltimeA;
	long ltimeB;
	long ldura;
	long lposA;
	long lposB;
	long ldelta;//단위시간당 이동거리
	string str;
	string ruby;
} LYRIC_NOTE;
 */
    int vSz=vNoteTick.size();
    // vNoteTick[vSz-1].str
    ReplaceAll(vNoteTick[vSz-1].str,"¶","");
    TRACE("#@#vv(1)----마지막 가사 블럭 vSz[%d] [%s]\n",vSz, vNoteTick[vSz-1].str.c_str());

    //vNoteTick[vSz-1].str+="¶";
    //TRACE("#@#vv(2)----마지막 가사 블럭 [%s]\n",vNoteTick[vSz-1].str.c_str());
    string xStr="";
    string yStr="";
    line_cnt=0; blk_cnt=0;
    for(i=0;i<vSz;i++){
        xStr=vNoteTick[i].str;
        if (xStr.find("/") != string::npos) continue;
        ReplaceAll(vNoteTick[i].str,"¶","");
        vmLyNote.push_back(vNoteTick[i]);
        if (xStr.find("¶") != string::npos){
            blk_cnt++;
            mLineInfo.vLyNote=vmLyNote;
            mLineInfo.Blk_Cnt=blk_cnt;
            mLineInfo.Line_StartTime=0;
            mLineInfo.Line_EndTime=0;
            mLineInfo.LineSex=-1;
            mLineInfo.LineAttr=-1;
            mLineInfo.Jull=0;
            mLineInfo.PosA=-1;
            mLineInfo.PosB=-1;
            mLineInfo.Text_length=-1;
            vLi.push_back(mLineInfo);
            vmLyNote.clear();
            line_cnt++; blk_cnt=0;
        }
        else blk_cnt++;
        yStr=yStr+xStr;
    }
    mLineInfo.vLyNote=vmLyNote;
    mLineInfo.Blk_Cnt=blk_cnt;
    mLineInfo.Line_StartTime=0;
    mLineInfo.Line_EndTime=0;
    mLineInfo.LineSex=-1;
    mLineInfo.LineAttr=-1;
    mLineInfo.Jull=0;
    mLineInfo.PosA=-1;
    mLineInfo.PosB=-1;
    mLineInfo.Text_length=-1;
    vLi.push_back(mLineInfo);
    vmLyNote.clear();

    vLine=split(yStr, "¶");
    vSz=vLi.size();
    PLY_STATUS.totalLyricLine=vSz;

///(1) Line_StartTime, Line_EndTime 정리
    int n;
    int lastA;
    int lastB;
    int nextLineTimeA;
    int btw;
    for(i=0;i<vSz-1;i++) {
        vLi[i].Line_StartTime = vLi[i].vLyNote[0].ltimeA;
        vLi[i].Line_EndTime = vLi[i+1].vLyNote[0].ltimeA;
    }
    vLi[vSz-1].Line_StartTime = vLi[vSz-1].vLyNote[0].ltimeA;
    vLi[vSz-1].Line_EndTime = vLi[vSz-1].vLyNote[vLi[vSz-1].Blk_Cnt-1].ltimeB;

///(2) Line_updown/절 정리
    int ud=0;
    for(i=0;i<vSz;i++) {
        vLi[i].updn=ud;
        int endBlk=vLi[i].Blk_Cnt-1;
        string endStr =vLi[i].vLyNote[endBlk].str;
        if (endStr.find("*") != string::npos){
            if(i<(vSz-1)) vLi[i+1].Jull=1;
            ReplaceAll(vLi[i].vLyNote[endBlk].str,"*","");
            vLi[i].updn=0;
        }
        ud++;
        ud =ud%2;
    }

#if 0
    for(i=0;i<vSz;i++) {
        TRACE("#@#vv vLine[%d] [%d-%d] updn[%d] Jull[%d] [%s]\n", i, vLi[i].Line_StartTime, vLi[i].Line_EndTime, vLi[i].updn, vLi[i].Jull, vLine[i].c_str());
        for(ix=0;ix<vLi[i].Blk_Cnt;ix++){
            TRACE("#@#vv vLi.str[%d/%d][%s]\n", ix, vLi[i].Blk_Cnt, vLi[i].vLyNote[ix].str.c_str());
        }
        usleep(100);
    }
#endif

///(3) ruby 정리
    vector<string>vx;
     for(i=0;i<vSz;i++)
     {
         for(ix=0;ix<vLi[i].vLyNote.size();ix++){
             xStr=vLi[i].vLyNote[ix].str;//[아버지]<abazi>
             if (xStr.find("]<") != string::npos){
               ReplaceAll(xStr,"[","");
               ReplaceAll(xStr,">","");
               vx.clear();
               vx=split(xStr,"]<");
               if (vx[1].find("¶") != string::npos)//ruby문자에 엔터들어있는거 빼냄
               {
                 vLi[i].vLyNote[ix].str=vx[0]+"¶";
                 ReplaceAll(vx[1],"¶","");
                 vLi[i].vLyNote[ix].ruby=vx[1];
               }
               else
                {
                    vLi[i].vLyNote[ix].str=vx[0];
                    vLi[i].vLyNote[ix].ruby=vx[1];
                }
              }
              else{
              // mLN.str=vmLY1[i].str;
               vLi[i].vLyNote[ix].ruby="";
              }
        }
     }
  //   for(i=0;i<vSz;i++) TRACE("#@#vv>>>(1)loop[%d] str{%s} ruby{%s} ltimeA[%d] ltimeB[%d]\n",i, vLi[i].vLyNote.str.c_str(), vLi[i].vLyNote.ruby.c_str(), vLi[i].vLyNote.ltimeA, vLi[i].vLyNote.ltimeB);
    vector<string> ().swap(vx);
  //put ruby--

///(4) duration 정리
    for(i=0;i<vSz;i++) {
        int subSz=vLi[i].Blk_Cnt;
        for(ix=0;ix<subSz;ix++){
         int  nextA = (ix==(subSz-1)) ? vLi[i].Line_EndTime  : vLi[i].vLyNote[ix+1].ltimeA;
         long duration=nextA - vLi[i].vLyNote[ix].ltimeA;
         if(duration>480) duration = 480;
         vLi[i].vLyNote[ix].ltimeB = vLi[i].vLyNote[ix].ltimeA + duration;
        }
    }

/////
    for(i=0;i<vSz;i++){
        int A=vLi[i].Line_StartTime;
        int B=vLi[i].Line_EndTime;
        n=vLi[i].Blk_Cnt;
        lastA=vLi[i].vLyNote[n-1].ltimeA;
        lastB=vLi[i].vLyNote[n-1].ltimeB;

        if(i==vSz-1) nextLineTimeA=PLY_STATUS.SongEnd_ltime;
        else nextLineTimeA=vLi[i+1].Line_StartTime;
        btw=nextLineTimeA-lastB;
         string  xxstr=vLi[i].vLyNote[0].str;
        long mm,bb,tt;
        MIDIData_BreakTime(pMIDIData, A, &mm, &bb, &tt);
           //       TRACE("#@#vv DK_LoadCountDown [%d/%d][%d]--[%03d:%03d:%03d]\n",i,vCounDown.size(),ltick, mm+1, bb+1, tt);
        long mm1,bb1,tt1;
        MIDIData_BreakTime(pMIDIData, lastA, &mm1, &bb1, &tt1);
       // TRACE("#@#vv>>> line[%d] AB[%d-%d] last[%d-%d] btw[%d] n[%d] ==>%s [%03d:%03d:%03d] [%03d:%03d:%03d]\n",i , A, B, lastA,lastB, btw, n, xxstr.c_str(),mm+1, bb+1, tt, mm1+1, bb1+1, tt1 );
       usleep(10);
    }
	return 0;
}

void  midi_doc::DK_GetSongKey()
{
    int i;
    int  sex, midi_key, male_key, female_key, chorus_key, singer_key;
    long psf,pmi;
    vector<string> vName;
    int vSz;
    int res,kind=0xff;
	long major[12]={0,   -5,     2,   -3,     4,   -1,   6,   1,   -4,    3,  -2,   5};
	long minor[12]={-3,   4,    -1,    6,     1,   -4,   3,  -2,    5,    0,  -5,   2};
//	string jungKeyMajor[12] = {"C","Db","D","Eb","E","F","F#","G","Ab","A","Bb","B"};
//	string JungKeyMinor[12] =  { "Cm","C#m","Dm","Ebm","Em","Fm","F#m","Gm","Abm","Am","Bbm","Bm" };
	string jungKey[24] = {"C","Db","D","Eb","E","F","F#","G","Ab","A","Bb","B","Cm","C#m","Dm","Ebm","Em","Fm","F#m","Gm","Abm","Am","Bbm","Bm" };
//TRACE("#@#(1)---cnwLoadKey----\n");
    MIDIData_FindKeySignature(pMIDIData,480,&psf,&pmi);
    pDoc->PLY_STATUS.SF=psf;
    pDoc->PLY_STATUS.MI=pmi;

    vName=pDoc->split(pDoc->TRACK_Info.nameTRACK_Melody, "/");
    male_key=0; female_key=0;
//TRACE("#@#(1-1)---cnwLoadKey----vName.size()[%d]\n",vName.size());
    if(vName.size()>=6)
    {
        for(i=0;i<24;i++){
            res=jungKey[i].find(vName[4]);
            if(res>=0)	{ male_key=i;  break;}
        }
        for(i=0;i<24;i++){
            res=jungKey[i].find(vName[5]);
            if(res>=0)	{ female_key=i;  break;}
        }
        vSz=vName.size();
        for(i=0;i<vSz;i++) TRACE("#@#---vName[%d][%s]\n",i, vName[i].c_str());

         if (pDoc->TRACK_Info.nameTRACK_Melody.find("MM") != string::npos) {
             sex=1;
             midi_key=male_key;
         }
         else if (pDoc->TRACK_Info.nameTRACK_Melody.find("FF") != string::npos) {
                sex=0;
                midi_key=female_key;
         }
         else if (pDoc->TRACK_Info.nameTRACK_Melody.find("MF") != string::npos) {
                sex=1;
               midi_key=male_key;
         }
         else if (pDoc->TRACK_Info.nameTRACK_Melody.find("FM") != string::npos) {
                sex=0;
                midi_key=female_key;
         }
     }
    else//Melody Track에 정보없을때
    {
        midi_key=0;
        male_key=0;
        female_key=4;
    }
//TRACE("#@#(2)---cnwLoadKey----\n");
    pDoc->KeyInfo.Sex=sex;
    pDoc->KeyInfo.DataKey=midi_key;
    pDoc->KeyInfo.MaleKey=male_key;
    pDoc->KeyInfo.FemaleKey=female_key;
}

//port15 ch0 Lyric track에 ctrl50-0,1,2,3추가....
void midi_doc::DK_MK_FrontLedMarker()
{
    int i,j;
    long last_tick;
    long ltime;
   	long mm,bb,tt;
   	long pmm,pbeat,ptick,pnn, pdd,pcc,pbb;
   	long port;
    MIDITrack* pMIDITrack;
    MIDIEvent *pMIDIEvent=NULL;
    pMIDITrack = TRACK_Info.pTRACK_Lyric;
    last_tick= MIDIData_GetEndTime(pMIDIData);
   	MIDIData_BreakTime(pMIDIData, last_tick, &mm, &bb, &tt);

   	port=MIDITrack_GetOutputPort(pMIDITrack);

#ifdef DBG_FRONT
    TRACE("#@#DBG_ port[%d]Last_time[%ld] [%05ld:%02ld:%03ld] \n",port, last_tick, mm, bb, tt );
#endif
    for(i=0;i<mm+1;i++){
        MIDITrack_MakeTime(pMIDITrack, i,0,0,&ltime);
        MIDITrack_BreakTimeEx(pMIDITrack, ltime, &pmm, &pbeat, &ptick, &pnn, &pdd, &pcc, &pbb);

       // if(ltime>60) MIDITrack_InsertControlChange(pMIDITrack,ltime,0,50,0);//channel 0 , control 50 Front Led signal
 #ifdef DBG_FRONT
     //TRACE("#@#DBG_[%d][%ld] [%05ld:%02ld:%03ld] pnn/pdd[%ld/%ld] pcc[%d]\n",i,ltime,pmm,pbeat,ptick,pnn,pdd,pcc );
 #endif
        for(j=0;j<pnn;j++){
            MIDITrack_MakeTime(pMIDITrack, i,j,0,&ltime);
            if(ltime>60) MIDITrack_InsertControlChange(pMIDITrack,ltime-10,0,50,j);//channel 0 , control 50 Front Led signal
  #ifdef DBG_FRONT
      //TRACE("#@#DBG_[%d][%d][%ld] \n",i, j, ltime);
  #endif
        }

    }
}


void midi_doc::DK_LoadJull(void)
{
    MIDITrack* pMIDITrack;
    MIDITrack* mMIDITrack;
    MIDITrack* fMIDITrack;
    MIDIEvent *pMIDIEvent=NULL;
    MIDIEvent *jMIDIEvent=NULL;
    fMIDITrack = TRACK_Info.pTRACK_Function;
    mMIDITrack = TRACK_Info.pTRACK_Melody;
    JULL_POS JullPos;
    if(pMIDITrack==NULL) return;
    int i, posCnt;
    long jtime,ltime,jdura;
    vJullPos.clear();
    forEachEvent (fMIDITrack, pMIDIEvent) {
        MIDIEvent_Combine(pMIDIEvent);
        if(MIDIEvent_IsNoteOn(pMIDIEvent))
        {
          ltime = MIDIEvent_GetTime (pMIDIEvent);
          JullPos.ltimeA = ltime;
          JullPos.ltimeB = -1;
          vJullPos.push_back(JullPos);
         }
    }

//   if(posCnt==0) return;//jull없음

#if 0
....
///////절 ending 찾기+++
    for(i=0;i<posCnt-1;i++){
        JullPos=vJullPos[i+1];
        jtime=JullPos.ltimeA;
        forEachEvent (mMIDITrack, pMIDIEvent) {
            MIDIEvent_Combine(pMIDIEvent);
            if(MIDIEvent_IsNoteOn(pMIDIEvent))
            {
              ltime = MIDIEvent_GetTime (pMIDIEvent);
              if(ltime>=(jtime-480)) break;
              jMIDIEvent=pMIDIEvent;
             }
        }
        jtime = MIDIEvent_GetTime (jMIDIEvent);
        jdura = MIDIEvent_GetDuration (jMIDIEvent);
        vJullPos[i].ltimeB = jtime+jdura+480;
    }
    ///마지막거+++
    jMIDIEvent=NULL;
    jtime = MIDITrack_GetEndTime(mMIDITrack);//endof melody track
   // vJullPos[posCnt-1].ltimeB = jtime;

    forEachEventInverse(mMIDITrack, pMIDIEvent){
      MIDIEvent_Combine(pMIDIEvent);
      if(MIDIEvent_IsNoteOn(pMIDIEvent))
      {
        ltime = MIDIEvent_GetTime (pMIDIEvent);
        if(ltime < (jtime-480)) {
            jMIDIEvent=pMIDIEvent;
            break;
        }
       }
    }
    if(jMIDIEvent!=NULL){
        jtime = MIDIEvent_GetTime (jMIDIEvent);
        jdura = MIDIEvent_GetDuration (jMIDIEvent);
        vJullPos[posCnt-1].ltimeB = jtime+jdura+480;
    }
    else{
        vJullPos[posCnt-1].ltimeB = jtime;
    }

    ///마지막거---
#endif

///////절 ending 찾기----
#ifdef DBG_JULL
    long ltimeA, ltimeB, mm,bb,tt;
       posCnt=vJullPos.size();
    for(i=0;i<posCnt;i++){
        ltimeA=vJullPos[i].ltimeA;
        ltimeB=vJullPos[i].ltimeB;
        MIDIData_BreakTime(pMIDIData, ltimeA, &mm, &bb, &tt);
        TRACE("#@#DBG_JULL vJullPos[%d/%d] ltimeAB[%ld-%ld][%06d:%02d:%03d]]\n",i , posCnt,ltimeA, ltimeB,mm+1,bb+1,tt);
    }
#endif

}

void midi_doc::DK_LoadCountDown(void)
{
   // MIDITrack* pMIDITrack;
    MIDIEvent *pMIDIEvent=NULL;
    MIDITrack* pMIDITrack = TRACK_Info.pTRACK_Lyric;
    MIDITrack* fMIDITrack = TRACK_Info.pTRACK_Function;
    if(pMIDITrack==NULL) return;
    long ltime, exltime, exmadi;
    long pnn, pdd, pcc, pbb;
    long mm,bb,tt,bj;
    long between;
    JULL_INFO jull_info;

    //check JULL event @F@ Track에 Note check
    //


    //5마디이상 쉬는 구간 찾기
    vCounDown.clear();
    exltime=0;
    forEachEvent (pMIDITrack, pMIDIEvent)
    {
        MIDIEvent_Combine(pMIDIEvent);
        if(MIDIEvent_IsNoteOn(pMIDIEvent))
        {
              ltime = MIDIEvent_GetTime (pMIDIEvent);
              MIDIData_FindTimeSignature(pDoc->pMIDIData, ltime, &pnn, &pdd, &pcc, &pbb);
              if( pnn==3 || pnn==6 ) { between= 360*4; bj=3;}//1440 3/4, 6/8박자 계열
              else { between= 480*4; bj=4;}
              if((ltime-exltime) >= between){
                 jull_info.jullTimeA= exltime;
                 jull_info.jullTimeB= ltime;
                 jull_info.bakJa= bj;

                 if(bj==3){
                    jull_info.jumpTime = ltime-(360*2);
                    jull_info.countDown = ltime-360;
                 }
                 else {
                    jull_info.jumpTime=ltime-(480*2);
                    jull_info.countDown = ltime-480;
                 }

                 vJullInfo.push_back(jull_info);
                 vCounDown.push_back(ltime);
               }
              exltime=ltime;
         }
    }
#ifdef  DBG_JULL
   // for(int i=0;i<vCounDown.size();i++) {
   //     long  ltick= vCounDown[i];
   // 	MIDIData_BreakTime(pDoc->pMIDIData, ltick, &mm, &bb, &tt);
   //     TRACE("#@#vv DK_LoadCountDown [%d/%d][%d]--[%03d:%03d:%03d]\n",i,vCounDown.size(),ltick, mm+1, bb+1, tt);
   //  }
     for(int i=0;i< vJullInfo.size(); i++) {
        jull_info= vJullInfo[i];
        ltime =jull_info.jullTimeA;
        bj=jull_info.bakJa;
     	MIDIData_BreakTime(pDoc->pMIDIData, ltime, &mm, &bb, &tt);
        TRACE("#@#DBG_JULL (1)jullTimeA [%d/%d][%d]--[%03d:%03d:%03d]--bj[%d]\n",i,vJullInfo.size(), ltime, mm+1, bb+1, tt, bj);
        ltime=jull_info.jullTimeB;
     	MIDIData_BreakTime(pDoc->pMIDIData, ltime, &mm, &bb, &tt);
        TRACE("#@#DBG_JULL (2)jullTimeB [%d/%d][%d]--[%03d:%03d:%03d]\n",i,vJullInfo.size(), ltime, mm+1, bb+1, tt);
         ltime=jull_info.jumpTime;
      	MIDIData_BreakTime(pDoc->pMIDIData, ltime, &mm, &bb, &tt);
        TRACE("#@#DBG_JULL (3)jumpTime [%d/%d][%d]--[%03d:%03d:%03d]\n",i,vJullInfo.size(), ltime, mm+1, bb+1, tt);
      }
#endif
}

int midi_doc::linecntW(Uint16 *s)
{
	int i,rtn;
	int finished=0;
     for(rtn=i= 0; i<128; i++)
      {
        switch(s[i])
          {
            case 0x00:
            	finished=1;
            	break;
             default :
                rtn++;
            break;
          }
      if(finished) break;
      }
    return(rtn);
}
//Code 3: KS->Unicode
Uint16* midi_doc::b2w_CU(const char *s,  int Code)
{
union {
	Uint16 i;
	Uint8 c[2];
}U;
    Uint16 *ptr=0;
    Uint16 i,j;
    Uint8 xx;
    memset(lDw,0,512);//2014.12.25
    //TRACE("b2w_CU Code[%d][%x][%x][%x][%x]\n",Code, s[0],s[1],s[2],s[3]);
    if(Code == 99) Code = 1; //	cngUincode = TRUE;

	for(i=j=0; i<256; i++)//2014.12.25
	{
		xx=(unsigned char) s[i];
		if(xx<0x80 && (Code<5 || Code==8 || Code==9 || Code==88))
		{
			lDw[j++]=s[i];
			if(s[i]==0) break; // nkr 2008.12.12
		}
		else
		{
			switch(Code)
			  {
				case 10://swap
					U.c[1]=s[i++];
					U.c[0]=s[i];
					lDw[j++]=U.i;
					if(s[i+1]==0) break;
					break;
				case 2: // Big5
				case 3: // b5_gb2
					U.c[1]=s[i++];
					U.c[0]=s[i];
					if(s[i]==0) break;
		//			if(U.i<0xA440 || U.i>0xFCAE) // old
					if(U.i<0xA140 || U.i>0xFCFE) // new
					  {
					  	lDw[j]=lDw[j+1]=0;
							break; // error
					  }
///////////대만 중국어 (번체 /간체 사용
                if(Code==1) ptr = (Uint16 *)b5_gb2;
                else ptr=(Uint16 *)b5_u5; // big5
					//U.i=0xfb79;
					ptr+=(U.i-0xA140);
					U.i=*ptr &0xffff;
					lDw[j++]=U.i;
					if(s[i+1]==0) break;
					break;
				case 20: // GBii
					U.c[1]=s[i++];
					U.c[0]=s[i];
					U.i=0;
					lDw[j++]=U.i;
					if(s[i+1]==0) break;
					break;
				case 0: // Ksc5601
					U.c[1]=s[i++];
					U.c[0]=s[i];
                    //TRACE("#@#>%02x(%02x,%02x)\n",U.i,U.c[0],U.c[1]);
					//ch 20080701
					if(U.i<0xB0A1 || U.i>0xC900)
					{
						//printf("Ksc5601  U.i:0x%04x_\n", U.i);
						switch(U.i)
						{
							case 0xa4a1: lDw[j++]=0x3131; break;
							case 0xa4a4: lDw[j++]=0x3134; break;
							case 0xa4a7: lDw[j++]=0x3137; break;
							case 0xa4a9: lDw[j++]=0x3139; break;
							case 0xa4b1: lDw[j++]=0x3141; break;
							case 0xa4b2: lDw[j++]=0x3142; break;
							case 0xa4b5: lDw[j++]=0x3145; break;
							case 0xa4b7: lDw[j++]=0x3147; break;
							case 0xa4b8: lDw[j++]=0x3148; break;
							case 0xa4ba: lDw[j++]=0x314a; break;
							case 0xa4bb: lDw[j++]=0x314b; break;
							case 0xa4bc: lDw[j++]=0x314c; break;
							case 0xa4bd: lDw[j++]=0x314d; break;
							case 0xa4be: lDw[j++]=0x314e; break;
							case 0xa4a2: lDw[j++]=0x3132; break;
							case 0xa4a8: lDw[j++]=0x3138; break;
							case 0xa4b3: lDw[j++]=0x3143; break;
							case 0xa4b6: lDw[j++]=0x3146; break;
							case 0xa4b9: lDw[j++]=0x3149; break;
							default: lDw[j]=lDw[j+1]=0; break;
					  }
					}
					else
					{
						ptr=(Uint16 *)(k2un+(U.i-0xB0A1));
						U.i=*ptr;
						lDw[j++]=U.i;

					if(s[i+1]==0) break;
					}
					break;
				case 1: // Jis: NODA_MIDI
					//printf("s[i]:0x%x, xx=0x%x\n",s[i],xx );
					if(xx>0xa0 && xx<0xe0)
					{
					//	lDw[j++]=jis_kana[xx-0xa1];
					//printf("s[i]:%x_->lDw[j]:%x_\n",s[i], lDw[j-1] );
						break;
					}
					U.c[1]=s[i++];
					U.c[0]=s[i];

					if(U.i<0x8140 || U.i>0xEAA4)
					  { lDw[j]=lDw[j+1]=0;
						break; // error
					  }
//printf("b2w_CU:Code=%d,%04x\n",Code,U.i);
					ptr=(Uint16 *)(jis+(U.i-0x8140));
					U.i=*ptr;
		//printf(" lDw:%04X_\n", lDw[j-1]);
		//			U.i=0x3131; // test
					lDw[j++]=U.i;
					if(s[i+1]==0) break;
					break;
				case 15:// change char ==> WORD
					U.c[1]=s[i+1];
					U.c[0]=s[i];
			//		D[j++]=s[i+1];
			//		D[j++]=s[i];
					lDw[j++]=U.i;
					i++;
					if(s[i]==0) break;
					break; // test
				case 16:// change char ==> WORD nkr 04.20
					lDw[j++]=s[i];
					if(s[i]==0) break;
					break; // test
				case 4: // utf8 ==> uni
					if((s[i] & 0x80) == 0) lDw[j++]=s[i]; // ASCII character
					else if((s[i] & 0xc0) == 0x80)	return lDw;  // unexpected tail character
					else if((s[i] & 0xe0) == 0xc0)
					 {
						if ((s[i + 1] & 0xc0) != 0x80) return lDw; // unexpected head character
		 				U.c[1]=(s[i] & 0x1c) >> 2;
		 				U.c[0]=((s[i] & 0x03) << 6) | (s[i+1] & 0x3f);
						lDw[j++]=U.i; i++;
					 }
					else if((s[i] & 0xf0) == 0xe0)
					 {
						if ((s[i + 1] & 0xc0) != 0x80) return lDw; // unexpected head character
						if ((s[i + 2] & 0xc0) != 0x80) return lDw; // unexpected head character
		 				U.c[1]=((s[i] & 0xf) << 4) | ((s[i+1] & 0x3c) >> 2);
		 				U.c[0]=((s[i+1] & 0x03) << 6) | (s[i+2] & 0x3f);
						lDw[j++]=U.i; i+=2;
					 }
					else return lDw; // error
					break;
				default:
//					memset(lDw,0,256);
					break;
			  }
		  }
	  }
	lDw[j++]=0;
	lDw[j++]=0;
//	TRACE("b2w_CU Code[%d]  lDw[%x][%x][%x][%x]\n",Code, lDw[0],lDw[1],lDw[2],lDw[3]);
	return lDw;
}

Uint16* midi_doc::UNICODE_Null(Uint16 *s, int Sz)
{
	int i;
//	Uint16 *trim=;
	for(i=0;i<Sz;i++)
	{
		if(s[i]==0x0d || s[i]==0x0a)
		{
			s[i]=0;
			break;
		}
	}
	return s;
}

int midi_doc::UNICODE_strLengh(Uint16 *s)
{
	int i;
	for(i=0;i<256;i++)
	{
		if(s[i]==0x00)break;
	}
	return i;
}

void midi_doc::wStringcpy(Uint16* C, Uint16 *A, Uint16 *B)
{
	int i,j,k;
	Uint16 tmp[256]={0,};
	k=0;
	for(i=0;i<128;i++)
	{
	  if(A[i]==0) break;
	  else {
	  	tmp[k++]=A[i];
	  }

	}
	for(j=0;j<128;j++)
	{
		tmp[k+j]=B[j];
		TRACE("#@####B[%d][%x]\n",j, B[j]);
		if(B[j]==0 || B[j]==0x0d)  {tmp[k+j]=0; break;}
	}


//remove 0x0d, 0x0a
	for(i=0,j=0;i<256;i++)
	{
		switch(tmp[i])
		{
			case 0x0d:
				C[j++]=0x20;
				break;
			case 0x0a:
				break;
			case 0x00:
				C[j++]=tmp[i];
				return;
			default:
				C[j++]=tmp[i];
				break;
		}
	}

}

Uint16* midi_doc::wStringTrim(Uint16* s)
{
	int i;
	for (i = 0; i < 256 ; i++)
	{
		if( s[i]==0x0d) {s[i]=0; break;}
		//else tmp[i]=s[i];
	}
	return s;
}

int midi_doc::make_utf(unsigned char *q, Uint16 k)
{
	// unicode��
	// 0x0000�̸鼭 ��ȯ ����� UTF-8�� ���
	if(  k == 0x0000 ) {
		*q = (unsigned char)k;
		return 1;
	}
	// 0x0001���� 0x007F����
	else if( (k > 0x0000) && (k <= 0x007F) ) {
		*q = (unsigned char)k;
		return 1;
	}
	// 0x0081���� 0x07FF����
	else if( (k >= 0x0080) && (k <= 0x07FF) ) {
		*q = *q | 0xC0;
		*q = *q | (unsigned char)((k & 0x07C0) >> 6);

		*(q+1) = *(q+1) | 0x80;
		*(q+1) = *(q+1) | (unsigned char)(k & 0x003F);

		return 2;
	}
	// 0x0800���� 0xFFFF����
	else if( (k >= 0x0800) && (k <= 0xFFFF) ) {
		*q = *q | 0xE0;
		*q = *q | (unsigned char)((k & 0xF000) >> 12);

		*(q+1) = *(q+1) | 0x80;
		*(q+1) = *(q+1) | (unsigned char)((k & 0x0FC0) >> 6);

		*(q+2) = *(q+2) | 0x80;
		*(q+2) = *(q+2) | (unsigned char)(k & 0x003F);

		return 3;
	}
	// 0x00010000 �̻��� ����
	else {
	}

	return 0;
}

//
// int uni2utf(unsigned char *p, unsigned char *q, int, int)
//
// ù��° �μ��� UNICODE�� ��� �ִ� unsigned char �� �迭�� �ּ�
// �ι�° �μ��� UTF-8�� ��� �� unsigned char �� �迭�� �ּ�
// ����° �μ��� ù��° �μ��� ũ��(UNICODE�� ����ִ� ũ��)
//
// ���ϰ� : ��ȯ�� UTF-8�� ����
//
//
int midi_doc::uni2utf(Uint16 *unicode , unsigned char *utf8)
{
	int t, j, l;
//	unsigned char  *p, *q;
	Uint16 k;
	j=0;
	for(t = 0; ; t ++) {
		l = make_utf(utf8+j, unicode[t]);
		j += l;
		if(unicode[t]==0x00) break;
	}
	return j;
}

int midi_doc::GetDisplayLine(long ltime)
{
	int i;
	int line = -4;
	int lineCnt = PLY_STATUS.totalLyricLine;
	if(lineCnt==0) return -3;

	long lFirstltime = PLY_STATUS.LyricDisplay_ltime;

	//TRACE("#@#==============>ltime[%d]  lFirstltime[%d] SongEnd_ltime[%d]\n",ltime,   lFirstltime, PLY_STATUS.SongEnd_ltime);

	if(ltime<lFirstltime) return -2;
	//if( ltime>=lFirstltime && ltime < ProcessLine[0].Line_StartTime) return -1;
	if( ltime>=lFirstltime && ltime < vLi[0].Line_StartTime) return -1;

	for(i=0;i<lineCnt;i++)
	{
	  //	if((ltime>=ProcessLine[i].Line_StartTime) && (ltime < ProcessLine[i].Line_EndTime))
	  if((ltime>=vLi[i].Line_StartTime) && (ltime < vLi[i].Line_EndTime))
	  {
	  		if(vLi[i].updn==-1) continue;
	  		else line=i;
	  		break;
	  }
	}
	if (ltime >= PLY_STATUS.SongEnd_ltime) line = -3; //곡 끝났음....
    pDoc->PLY_STATUS.lCurrentLine = line;
	return line;
}

int midi_doc::GetEraseLine(long ltime)
{
	int i;
	int line = -4;
	int lineCnt = PLY_STATUS.totalLyricLine;
	long lFirstltime = PLY_STATUS.LyricDisplay_ltime;
	long lineEndTime;
	if(lineCnt==0) return -3;

	if(ltime<lFirstltime) return -2;
	if( ltime>=lFirstltime && ltime < vLi[0].Line_StartTime) return -1;

	for(i=0;i<lineCnt;i++)
	{
		if(ltime>=vLi[i].Line_StartTime && ltime < vLi[i].Line_EndTime)
	  	{
	  		if(vLi[i].updn==-1) continue;
	  		else 	line=i;
	  		break;
	  	}
	}
	if (ltime >= PLY_STATUS.SongEnd_ltime) line = -3; //곡 끝났음....
	return line;
}

//int midi_doc::GetDisplayLine(long ltime)
int midi_doc::GetNextLine(long ltime)
{
	int i;
	int line = -4;
	int lineCnt = PLY_STATUS.totalLyricLine;
	long lFirstltime = PLY_STATUS.LyricDisplay_ltime;
	if(ltime<lFirstltime) return -2;
	if( ltime>=lFirstltime && ltime < vLi[0].Line_StartTime) return -1;
	for(i=0;i<lineCnt;i++)
	{
	  	if((ltime>=vLi[i].Line_StartTime) && (ltime < vLi[i].Line_EndTime))
	  	{
	  		if(vLi[i].updn==-1) continue;
	  		else line=i;
	  		break;
	  	}
	}
	if (ltime >= PLY_STATUS.SongEnd_ltime) line = -3; //곡 끝났음....
	//TRACE("#@#pDOC에서===========>ltime[%d]  lFirstltime[%d] SongEnd_ltime[%d] --line[%d]\n",ltime,   lFirstltime, PLY_STATUS.SongEnd_ltime, line);
//	TRACE("#@#===ltime[%d]==line[%d]===========>\n",ltime, line);
	return line;
}

////////////////////////////////////////////////////////////////////
bool midi_doc::Decrypt(char *source, char* destination, long length)
{
 long i;
 int previousBlock;
 long key = KEY;

 if(!source || !destination || length <= 0) return FALSE;
 for(i=0; i<length; i++)
 {
  if(source[i]>=0x80){
    previousBlock = source[i]|0xffffff00;
  }
  else  previousBlock = source[i];

  destination[i] =source[i]^(key>>8);
  key = (previousBlock + key) * C1 + C2;
 }
 return TRUE;
}
