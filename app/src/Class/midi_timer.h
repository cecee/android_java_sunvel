#ifndef __MIDI_TIMER_H__
#define __MIDI_TIMER_H__
//--------------------
//For timer
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
#include <vector>
#include <string>
//-----------------------
#include "midi_document.h"
#include "lxMIDIData/lxMIDIData.h"
#include <jni.h>

using namespace std;

class midi_timer
{
	public:
	struct sigevent sev;
    struct itimerspec its;
    sigset_t mask;
    struct sigaction sa;
    timer_t timerid;
    bool is_timer_on;
    int mic_flag;
   // static long timer_sigfrom_mic(void);
    static void timer_handler(int sig, siginfo_t *si, void *uc);

    void TimerSet(bool start);
	int midi_timerStop();
	int midi_timerStart();
	int midi_timerReset();


	midi_timer();
	~midi_timer();
//...
};
#endif