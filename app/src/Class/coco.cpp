#include "coco.h"
#include "extern.h"
coco::coco(){
    vCocoSerial.clear();
    vCocoMessage.clear();
}
coco::~coco(){}

void coco::timer_sigfrom_coco(void)
{
	int i;
	char data[128];
	int vdata;
    int res;

#ifdef DBG_COCO
	//TRACE("#@#DBG_COCO timer_sigfrom_coco\n");
	//coco_writer_cmd(coco_cmd);
#endif
	memset(data,0,128);
	res = read(TTY_COCO, data, 128);
	if(res<=0) return;
#ifdef DBG_COCO
	TRACE("#@#DBG_COCO coco_in res[%d]\n", res);
#endif

	for(i=0;i<res;i++){
#ifdef DBG_COCO
		TRACE("#@#DBG_COCO from coco [%d/%d]-[0x%02x]\n", i, res ,data[i]);
#endif
		 vCocoSerial.push_back(data[i]&0xff);
	}
	for(;;)
	{
		if(vCocoSerial.empty()) break;
		vdata=vCocoSerial.front();
        vCocoSerial.erase(vCocoSerial.begin());
 		switch(vdata)
 		{
            case STX:
#ifdef DBG_COCO
	            TRACE("#@#DBG_COCO coco_in STX[%d]\n", vdata);
#endif
             isCocoMessage=0;
             vCocoMessage.clear();
             vCocoMessage.push_back(vdata&0xff);
             break;
            case ETX:
#ifdef DBG_COCO
	            TRACE("#@#DBG_COCO coco_in ETX[%d]\n", vdata);
#endif
             vCocoMessage.push_back(vdata&0xff);
             isCocoMessage=1;
             break;
           default:
            vCocoMessage.push_back(vdata&0xff);
            break;
        }
	}
}





void  coco::coco_writer_cmd(COCO_RESP_CMD cmd)
 {

 //    cmd.len=0;
     Uint8 *data;
     int length=6;//
     data=(Uint8 *)malloc(length);
     memcpy(data,&cmd,length);
 #ifdef DBG_COCO
     TRACE("#@#DBG_COCO coco_out stx[%x] cmd[%x] etx[%x]\n",cmd.stx, cmd.cmd, cmd.etx);
 	TRACE("#@#DBG_COCO coco_out [%x][%x][%x][%x][%x][%x]\n",data[0],data[1],data[2],data[3],data[4],data[5]);
 #endif
     write(TTY_COCO, data, length);
     free(data);
 }

 void  coco::coco_writer_data(COCO_RESP_DATA cmd)
 {

     Uint8 *data;
     int length=11;//
     data=(Uint8 *)malloc(length);
     memcpy(data,&cmd,length);
 #ifdef DBG_COCO
    TRACE("#@#DBG_COCO coco_out stx[%x] cmd[%x] etx[%x]\n",cmd.stx, cmd.cmd, cmd.etx);
 	TRACE("#@#DBG_COCO coco_out [%x][%x][%x][%x][%x][%x]\n",data[0],data[1],data[2],data[3],data[4],data[5]);
 #endif
     write(TTY_COCO, data, length);
     free(data);
 }