//////////////////////
#ifndef __DOC_CNW_H__
#define __DOC_CNW_H__

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lxMIDIData/lxMIDIData.h"
#include "midi_document.h"
#include <vector>
#include <string>
using namespace std;

///Note-Block구성
typedef struct _CNW_NOTE_LYRIC_BLOCK{
    string strLyric;
    Uint16 dwLyric[128];
    long textSz;
    long ltime;
    long lduration;
    long lgap;
} CNW_NOTE_LYRIC_BLOCK;

//OneLine 구성
typedef struct _CNW_VECTOR_LINE
{
    string OneLineStr;
    Uint16 OneLineUni[256];
    long lineSex;
    string line_attrbute;
    vector<CNW_NOTE_LYRIC_BLOCK> vBlock;
} CNW_VECTOR_LINE;

//전체 GASA구성
typedef struct _CNW_GASA_INFORMATION
{
    int chr_code;
    vector<CNW_VECTOR_LINE> vLine;
} CNW_GASA_INFORMATION;

typedef struct
{
	long inx;//=0xCCAA55AA; //????? ????
	long rev;//=0;
	long m_textFileSz;
	long m_midiSz;
	long mHymnSecSz;
	long gasaTableSz;
	long mScoreSz;
	char protectKeyCode[8];//=0xFFFFFFFF; //????? ????
	long midi_infoPoint;
	long textFilePoint;
	long mFilePoint;
	long gasaTablePoint;
	long HymnSectionPoint;
	long ScoreMIDIPoint;
}mxHEADER_A;

class Doc_cnw
{
public:
    CNW_GASA_INFORMATION gasainfo;
    vector<CNW_NOTE_LYRIC_BLOCK> vTotalBlock;
    vector<CNW_VECTOR_LINE> vTotalLine;
	char protect_keycode[9];
	long keyNum;
	char *pMidisong;

public:
    int Update_cnw(char *filename);
	int makeDec(unsigned char* dataEncBuf, long sz);
    int cnwSplitTrack(MIDIData* pMIDIData);
    void cnwPutEventMarker(void);
    int cnwLoadGasa(MIDITrack* pMIDITrack);
    int cnwGetTitle(MIDITrack* pMIDITrack);
    int UNICODE_strLengh(Uint16 *s)  ;


	Doc_cnw();
	~Doc_cnw();
};

#endif
