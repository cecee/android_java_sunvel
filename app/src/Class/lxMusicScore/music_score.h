//#pragma once
#ifndef __MUSIC_SCORE_H__
#define __MUSIC_SCORE_H__

//#ifdef WIN32
//#include <Windows.h>
//#include <tchar.h>
//#else
typedef unsigned char BOOL;
//#endif
//#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
//#include <iconv.h>
#include "../lxMIDIData/lxMIDIData.h"
#include "../midi_document.h"
//#include "../swc/swc_ptr_array.h"
//#include "../swc/swc_array.h"
//#include "../swc/swc.h"

#define true 1
#define false 0

extern long clefKeyTbl[12];
extern long clefKeyTbl_minor[12];

extern char* codeTable[4][12];
//extern char* eomtKeyMajor[12];
//extern char* eomtKeyMinor[12];
extern char* jungKeyMajor[12];
extern char* JungKeyMinor[12];

#define MIDIEVENT_SELECTED             0x00000004
// Track����
typedef struct
{
	int  m_lmajor;											//major or minor
	int  m_lMeasureCnt;
	long m_lTop;                        // ߾Ӯ���[pixel]
	long m_lHeight;                     // �Ԫ�[pixel]
	long m_lFlags;                      // �÷��� 1:�������ڸ� 2: �������ڸ� 3:�뺸ǥ
	long m_lMinKey;                     // Ʈ���� ��������(0-127)
	long m_lMaxKey;	                    // Ʈ���� �ְ�����(0-127)
//		WCPtrArray m_theNoteInfoArray;			    //��ǥ ������ �迭
//		WCPtrArray m_theNoteGroupInfoArray;		  //��ǥ ���� �׷��� �迭
//		WCPtrArray m_theTripletGroupInfoArray;	//3���� ��ǥ �׷� ������ �迭
} MusicalScoreTrackInfo;

// ������
typedef struct {
	long m_lTime;                       // �����ｪ���㷫�����[Tick][Subframe]
	long m_lDuration;                   // �����ｪ˪�����[Tick][Subframe]
	long m_lLeft;                       // �����ｪ�����[pixel]
	long m_lSignatureWidth;             // �����ｪ������??������?����[pixel]
	long m_lPreWidth;                   // �����ｪ��������[pixel]
	long m_lWidth;                      // �����ｪ���[pixel]
	long m_lPostWidth;                  // �����ｪ���������[pixel]
	long m_lFlags;                      // flag
	long m_lTimeSignature;              // �����ｪ������?
	long m_lKeySignature;               // �����ｪ�������?
} MusicalScoreMeasureInfo;

typedef struct tagMusicalScoreNoteInfo {
	MIDIEvent* m_pNoteOnEvent;          // ��?�ȫ��󫤫٫�ȪتΫݫ���
	MIDIEvent* m_pNoteOffEvent;         // ��?�ȫ��ի��٫�ȪتΫݫ���
	long m_lNoteOrRest;					//��ǥ: 0, ��ǥ: 1 //hdkim
	long m_lRestDura;
	long m_natural;							//0: natural �ȱ׸���. 1: natural�׸���.
	long m_lFlagNew;
	long m_lNoteOnTime;                 // ��?�ȫ�����ʾ[Tick]
	long m_lNoteOnMeasure;              // ��?�ȫ�����
	long m_lNoteOnBeat;                 // ��?�ȫ�����
	long m_lNoteOnTick;                 // ��?�ȫ���ƫ��ë�
	long m_lNoteOffTime;                // ��?�ȫ�����ʾ[Tick]
	long m_lNoteOffMeasure;             // ��?�ȫ�����
	long m_lNoteOffBeat;                // ��?�ȫ�����
	long m_lNoteOffTick;                // ��?�ȫ��իƫ��ë�
	long m_lFlags;                      // �ի髰
	long m_lSelected;                   // ��??��
	long m_lLyricNoteCnt;
	struct tagMusicalScoreNoteInfo* m_pPrevNoteInfo; // ����ݬ���êتΫݫ���(�ʪ����NULL)
	struct tagMusicalScoreNoteInfo* m_pNextNoteInfo; // �����ݬ���êتΫݫ���(�ʪ����NULL)
	void* m_pNoteGroupInfo;             // ������?�����êتΫݫ���(�ʪ����NULL)
	void* m_pTripletGroupInfo;          // 3֧ݬ����?�����êتΫݫ���(�ʪ����NULL)
} MusicalScoreNoteInfo;

typedef struct tagMusicalScoreNoteGroupInfo {
	MusicalScoreNoteInfo* m_pFirstNoteInfo; //���� ��ǥ ����
	MusicalScoreNoteInfo* m_pLastNoteInfo;  //���� ��ǥ ����
	long m_lBeginTime;                  //���۽ð�
	long m_lEndTime;                    //����ð�
	long m_lMinKey;                     //���� ����(0-127)
	long m_lMaxKey;                     //�ְ� ����(0-127)
	long m_lMinDur;                     //�ִ� ��ǥ ����
	long m_lMaxDur;                     //���� ��ǥ ����
	long m_lNumNoteInfo;                //�׷쳻�� ��ǥ��
	long m_arrKeys[64];
	//WCArray<long, long> mw_arrKeys;		//�׷쳻�� Ű
	//	long m_arrKeysCnt;
} MusicalScoreNoteGroupInfo;


// 3֧ݬ����?������(��?����ݬ��-3-�ǪĪʪ����֪ͪ�����)
typedef struct tagMusicalScoreTripletGroupInfo {
	MusicalScoreNoteInfo* m_pFirstNoteInfo; // ��������ݬ���êتΫݫ���
	MusicalScoreNoteInfo* m_pLastNoteInfo;  // ��������ݬ���êتΫݫ���
	long m_lBeginTime;                  // �����ʾ
	long m_lEndTime;                    // ������ʾ
	long m_lMinKey;                     // �����ͭ(0-127)
	long m_lMaxKey;                     // ������ͭ(0-127)
	long m_lMinDur;                     // ��ӭ������
	long m_lMaxDur;                     // ����������
	long m_lNumNoteInfo;                // ����?��?����ݬ?
} MusicalScoreTripletGroupInfo;
//////////////////////////////
typedef struct tagLyrics
{
	long ltime;
	string str;
} LYRICS;

typedef struct _CHORD
{
	long lTime;
	long FlatSharpTableNo[2];//Major or Minor
	long Code[2];
	string subCodeStr[2];
} CHORD;


typedef struct tagFermata
{
	long ltime;
	long fermatakey;
	long Cref;
	long m_measureNo;
} FERMATA;

typedef struct tagScoreBar
{
	long lTime;
	long offTime;
	long key;
	long lKeySignature;
//	long znumber;
//	long duration;
} SCORE_BAR;

typedef	struct tagMidiEventList
{
	MIDIEvent* m_pEvent; //��ġ�� ���� �޸𸮰� �̻��ϰ� �ǳ�.....�ֱ׷���?
	long ltime;
}MIDI_EVENT_LIST;

typedef	struct tagScore
{
	MusicalScoreNoteInfo* pmsni;
	MusicalScoreNoteGroupInfo *pmsngi;
	MusicalScoreTripletGroupInfo *pmstgi;
	MusicalScoreTrackInfo* pmsti;
	int lxScoreCnt;
	int PutNoteGroupCnt;
	int PutTripletGroupCnt;
}SCORE_INFORMATION;

/*
	typedef	struct tagClefSig
	{
		long lsf;
		long lnn;
		long ldd;
		long lclefXpos;
		long lclefYpos;
	}CLEF_SIG;
*/
////////////////////////////////
enum MusicRest
{
	rest_of_start = -1,
	rest_sixtyfourth,		//0
	rest_thirtysecond,		//1
	rest_dotted_thirtysecond, //2
	rest_sixteenth,			//3
	rest_dotted_sixteenth,	//4
	rest_eighth,			//5
	rest_dotted_eighth,		//6
	rest_quarter,			//7
	rest_dotted_quarter,	//8
	rest_half,				//9
	rest_dotted_half,		//10
	rest_whole,				//11
	rest_dotted_whole,		//12
	rest_of_end				//13
};

enum MusicNote
{
	note_1 = 0, //0-��� ����
	note_02,    //1-���� �۴��
	note_12,    //2-�Ʒ��� �۴��
	note_04,		//3-4����ǥ�ϼ� ���� �۴��
	note_14,		//4-4����ǥ�ϼ� �Ʒ��� �۴��
	note_40,	  //5-4����ǥ�̿ϼ�(���� �۴�� ���߿� �������ʿ��Ѱ�)
	note_41,	  //6-4����ǥ�̿ϼ�(�Ʒ��� �۴�� ���߿� �������ʿ��Ѱ�)
	note_08,		//7-��8����ǥ
	note_18,		//8-��8����ǥ
	note_016,   //9-2
	note_116,   //10-
	note_032,		//11-
	note_132,		//12-
};

class music_score
{

public:
	char test;
	int lxMeasureCnt;
	int lxMeventCnt;
	int pmidi_bufSz;

	long putNoteCnt;
	long NoteCnt;
	long RestCnt;

	int lxLyricCnt;
	int lxCordCnt;
	int lxFermataCnt;
	//int lxScoreBarCnt;

	long m_TZoom;
	int tr_num;


	MIDITrack *ScoreTrack;
	MIDITrack *ObbliTrack;
	MIDITrack *ChordTrack;
	MIDITrack *LyricTrack;
	MIDITrack *FirstTrack;

	vector <LYRICS> vLyric;
	vector <CHORD> vChord;
	vector <FERMATA> vFermata;
	vector <SCORE_BAR> vScoreBar;

	//CHORD *pChord;

	//SCORE_BAR *pScoreBar;
	MusicalScoreMeasureInfo* pmsmi;
	SCORE_INFORMATION Score[2];
public:
	music_score();
	~music_score();
	MusicalScoreMeasureInfo* lxGetMeasureInfo(long lMeasureIndex);

	/////////////////////////
	int LoadMusicScore(MIDIData* pMIDIData);
	void unLoadMusicScore(void);
	int UpdateMeasureInfo(MIDIData* pMIDIData);

	BOOL UpdateTrackInfo(MIDIData* pMIDIData, int index);
	BOOL UpdateNoteInfo (MIDIData* pMIDIData, int index);
	BOOL UpdateScoreBarInfo (MIDIData* pMIDIData, int index);
	BOOL UpdateTripletGroupInfo(MIDIData* pMIDIData, int index);
	BOOL UpdateNoteGroupInfo (MIDIData* pMIDIData, int index);

	BOOL UpdateChordInfo(MIDITrack* pMIDITrack);
	BOOL UpdateFermataInfo(MIDITrack* pMIDITrack);
	BOOL UpdateMeventLstInfo(MIDIData* pMIDIData);
	BOOL UpdateLyricInfo();

	void PutNote
			(
					int Cnt, MIDIEvent* pNoteEvent, long lNoteOnTime, long lNoteOnMeasure, long lNoteOnBeat, long lNoteOnTick,
					long lNoteOffTime, long lNoteOffMeasure, long lNoteOffBeat, long lNoteOffTick, long lFlags, long lLyricNoteCnt, int index
			);
	MusicalScoreNoteGroupInfo* PutNoteGroupInfo(int Cnt, MusicalScoreNoteInfo* pNoteInfo, int index);
	MusicalScoreTripletGroupInfo* PutTripletGroupInfo(int Cnt, MusicalScoreNoteInfo* pNoteInfo, int rest, int index);

	BOOL CreateAddRestsInfo(MIDIData* pMIDIData, MusicalScoreTrackInfo* pTrackInfo, long lNoteOffPrev, long lNoteOnCur, int index);

	enum MusicRest GetKindOfRestBelowBeat(long lTimeDur, long lnn, long ldd, long lTimePerBeat);
	enum MusicRest GetKindOfRestByBeats(long lNumBeats, long lnn, long ldd, long *lRemained_8);

	void PutRest(MIDIData* pMIDIData, int Cnt, long lNoteOnTime, enum MusicRest lKindOfRest, long lRestDura, int index);
	int GetTrackListAndCnt(MIDIData *pMIDIData);

	int FindKeyValue(string chord);

	int GetMeventCnt(MIDIData* pMIDIData);
	int calcu_delay(unsigned char* mbuf, int mem_pos, long ltime);
	long lxGetMeasureTime(long lMeasureIndex);
	int GetTrackEventCnt(MIDIData *pMIDIData, int track);
	static int  compare(const void* a, const void* b);

};

//#ifdef __cplusplus
//}
//#endif

#endif