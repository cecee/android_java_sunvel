//#pragma once
#ifndef __DRAW_SCORE_H__
#define __DRAW_SCORE_H__

//#include "SDL_ttf.h"
#include "music_score.h"
#include <string>
using namespace std;
/*
#define MUSICSCORE_YOFFSET 80
#define MUSICSCORE_HIGHT 150
#define MUSICSCORE_XSTART 60
#define MUSICSCORE_BEAT0_POS 60+140
#define MUSICSCORE_VIEW 1020
#define MUSICSCORE_LENGTH 1160
*/
#define SCREEN_WIDTH 1920
#define SCREEN_HEIGHT 1080

#define MUSICSCORE_YOFFSET 130//100
#define MUSICSCORE_HIGHT 280//220
#define MUSICSCORE_XSTART 60
#define MUSICSCORE_BEAT0_POS 60+140
#define MUSICSCORE_VIEW 1660
#define MUSICSCORE_LENGTH 1800


#define MUSICSCORE_START_MEASURE 1
////
//|0___|60____|200_____________________|1220___|1280
//       (140)|  MUSICSCORE_VIEW(1020) |
//     |____MUSICSCORE_LENGTH(1160)____|
//  |30(30)____________1220__________________|-30(1250)
         
#define MUSICSCORE_LINE_WITH 4
#define MUSICSCORE_YGAP 6//6  //���� ���ΰ�

#define TEXT_FONT_12 12 //�ڵ� ��Ʈ���� �÷� ��Ʈũ��
#define TEXT_FONT_20 20 //Lyric font size

#define TEXT_FONT_24 24 //Lyric font size
#define TEXT_FONT_18 18 //�ڵ� ��Ʈũ��
#define TEXT_FONT_32 32
#define TEXT_FONT_48 48
#define MUSICSCORE_FONT_30  30 //����ȿ� �÷� ��Ʈũ��
#define MUSICSCORE_FONT_40  40 //�������ڸ�  ��Ʈũ��
#define MUSICSCORE_FONT_42  42 //size 42
//#define MUSICSCORE_FONT_REST 42


#define MUSICSCORE_LINE_LYRIC_NO  24 //Lyric �׸�����ġ�� ���� �ѹ�
#define MUSICSCORE_LINE_ZIFFER_LYRIC_NO  25
#define LINE_COLOR   0x010101ff    //RRGGBBAA

#define COLOR_ZIFNO   0xF457A6ff    //RRGGBBAA
#define COLOR_CHORD  0xF457A6ff    //RRGGBBAA 244,87,166
//#define COLOR_ZFNUM  0xFF8800ff    //RRGGBBAA 244,87,166

#define COLOR_ZFH  0xFF0000FF    //RRGGBBAA 244,87,166
#define COLOR_ZFL  0x0000FFFF    //RRGGBBAA 244,87,166
#define COLOR_ZFN  0x000000FF    //RRGGBBAA 244,87,166
//3eadeb
#define SCORE_BAR_COLOR  0xF00000FF    //RRGGBBAA //FF44FDFF
#define SCORE_BAR_LENGTH1  150   //Short Cursor
#define SCORE_BAR_LENGTH2  300   //Long Cursor MUSICSCORE_HIGHT+(MUSICSCORE_YGAP*9);

//#define SCORE_BAR_COLOR  0x3eadebFF    //RRGGBBAA
#define SCORE_BAR_WIDTH  2    //RRGGBBAA

#define COLOR_Lyric 0xF000A0FF    //RRGGBBAA
#define UNDER_LINE_LENGTH 1096
#define UNDER_LINE_START_X 90

#define TEXT_FONT_PATH  "/system/data_l/GOG/disk1/fnt/m.ttc"
#define MUS_FONT_PATH   "/system/data_l/GOG/disk1/fnt/maestro.ttf"

typedef struct tagNatural
{
	long linePos;//Major or Minor
	long m_time;//Major or Minor
	long m_measureNo;
	long sf_flag;
} NATUARAL;

typedef	struct tagClefSig
{
	long lsf;
	long lnn;
	long ldd;
	long lclefXpos;
	long lclefYpos;
	long mi;
}CLEF_SIG;

typedef struct tagTieHalf
	{
		int a;
		int b;
		int c;
		int y;
		int flag;
		int Znum;
} TIE_HALF;

typedef struct tagMeasureLyric
{
	int LyricNumOfMeasure;
	int MeasureLineCnt;
	char strU8[64][64];
	string str[64];
//	Uint16  strUni[64][64];//Unicode
	int x[64];
//	int len[64];
	int y;
//	int LyricDisplayLength;
	long ltime[64];
}MEASURE_LYRIC;

typedef struct tagZiffer_info
{
	int kind;//0: num 1:underline(start, end) 2: xA:center_point xB:������ 3: dot 4:desh 5:tie 6:�ӽ�ǥ
	int num;
	int level;
	int xA;
	int xB;
}ZIFFER_IFO;

typedef struct tagZifferTitle_info
{
	int mi;
	int jangjo;
	char mother[4];// xA;
	char baby[4];
}ZIFFER_TITLE;

typedef struct tagXAXB_info
{
	long xA[4];// xA;
	long xB[4];
}XAB;


class draw_score
{
public:

	int m_lTrackKeyPlus;
	int m_lTrackchordPlus;
	long newSF;
	int TieCnt;
	int  cntMeasurePerPage;
	int  lCurPage;//16
	int lbeatCnt[16];
//	int loldCursor;
	int ObliEnable;
	int Ziffersystem;
	int cntMeasurePerLine;
	int cntLinePerPage;
	int ZinfoCnt;
	int s_NaturalCnt;
	long lZifferLineNo;

	long lbeatTime[16][32];// [ypos][beat_time]
	long XA[4];
	long XB[4];
	int XPos;
	int YPos;

	int  lCursorLen;
	long DrawingFag;
	int ScoreCursorEnable;//Draw Cursor Flag
	int curPageLastLtime;
	int curPageBeginLtime;
	long curMeasureLtime[8];
	int curMeasureX[8];
	int lyricPos[128];
	int curLineLyricCnt;

	int score_AB_id0[2];//점수계산용 Display구간
	int score_AB[2];//점수계산용 Display구간
	char buf[128];

	long screenMeasure[4];
	long screenLtimeA[4];
	long screenLtimeB[4];
	
	TIE_HALF TieBuf[20];
	NATUARAL s_Natural[64];
	ZIFFER_IFO Zinfo[128];
	ZIFFER_TITLE Ztitle;
	XAB xAB[2];

	draw_score();
	~draw_score();
	void run(void);
//	static int SCOREthread(void *ptr);
//	void PushDrawScorePage(int page);
	int GetCurrentPage(long ltime);
	int GetLastPage(void)	;
	int Get_CursorPos(long ltime);
	int Score_xTOX(int xA, int xB, int x);
	int TimeToScorelineX(long ltime);
	void MusicScoreDraw_View(int page, int index);//16���� �׸���
	void MusicScoreDraw_Line(int page);//16���� �׸���
	void Draw_ScoreTitle(void);
	int Draw_HLine(int x, int y, int xlength, int dw, Uint32 color );
	int Draw_VLine(int x, int y, int ylength, int dw, Uint32 color );
	int _Draw_UTF_Text(char *kind, int x, int y, const char *s, Uint32 color);
	int _Draw_UTF_Lyric(char *kind, int x, int y, const char *s, Uint32 color);
//	int _Draw_UNICODE_Text(char *kind, int x, int y,  Uint16 *s, Uint32 color);
	void Draw_ZifferTitle(ZIFFER_TITLE ztitle);

	void DrawLyric (MEASURE_LYRIC MsLyric, int msXA, int msXB, int ypos);
	void DrawFiveLine(int y, int ypos);
	int DrawMeasureLine(int measureNum, int x, int y0, int y1, int ypos, int index);
	int DrawMeasureLineXPos( int x, int xA, int xB);
	void DrawTimeAndKeySignature (MIDIData* pMIDIData, long lTime, int flag, int ypos, int index);
	void DrawMusicalText(char* str, int x , int y, int flag, int ypos);
	void DrawSharp(int x , int y, int flag, int ypos);
	void DrawFlat(int x , int y, int flag, int ypos);
	void DrawGClef(CLEF_SIG clefSig, int flag ,int ypos, int index);
	long GetNewSf_Mi(long KeySig, int mPlus);
	void DrawLegLine (int x, int y, int ypos);//������ �׸���
	void DrawDoubleFlat(int x , int y, int flag, int ypos);
	void DrawDoubleSharp(int x , int y, int flag, int ypos);
	void DrawPole (int x0, int y0, int y1, int lup, int ypos);
	void DrawTadpole (int x, int y, long lFlags, int ypos);
	void DrawDot (int x, int y, int ypos);
	void DrawNatural (int x, int y, int ypos);
	void DrawZiffer (int ypos);

	int DrawNoteWtail (int x, int y, int flag2, int lbIsUp, int ypos ,int lflag );

//void DrawSingleFlag (int x, int y, int y1, int flag, int ypos);
	void DrawChainedFlag (int x1, int x2, int y1, int y2, int rx, int ry, int lup, int ypos);
	void DrawTieFull(int x1, int x2, int y, int bUp, int fr, int ypos) ;
	void DrawZiffTieFull(int x1, int x2, int fr, int ypos, int znum) ;
	long Get_ZifferColor(ZIFFER_IFO lZinfo);
	int GetZifferNumber (int lineNunber);
	int GetZiffeHighLow (int lineNunber);
	
	void DrawTieHalf_table(long x1, long x2, long y, long lFlags);
	void DrawLine(int x1, int y1, int x2, int y2, int ypos);
	void DrawTripletSign(int x1, int x2, int y1, int y2, long lFlags, int ypos, int index);
	void DrawStaff(MIDIData* pMIDIData, long ltimeA, long ltimeB, int ypos, int index) ;
//	void DrawScoreLine(MIDIData* pMIDIData, long ltimeA, long ltimeB, int ypos, int index) ;
//	void DrawScoreLineObj(MIDIData* pMIDIData, MusicalScoreNoteInfo* pNoteInfo, int ypos);

	void DrawRest(MIDIData* pMIDIData, MusicalScoreNoteInfo* pNoteInfo, MusicRest lFlags, int ypos, int index);
	void DrawNote(MIDIData* pMIDIData, MusicalScoreNoteInfo* pNoteInfo, int ypos, int index);
	void DrawChord (MIDIData* pMIDIData, long lTime, int mcode0, char* scode0, int mcode1, char* scode1, int lmajor0, int lmajor1, int ypos, int index) ;
	void DrawFermata (MIDIData* pMIDIData, FERMATA *plFermata, int ypos, int index) ;
	void DrawEllipse_half(int x0, int y0, int rx0, int ry0, int bUp, int fr, Uint32 color);
	void _DrawEllipse_half(int x0, int y0, int rx0, int ry0, int bUp, int fr, Uint32 color);
//	int Draw_Page_Cursor(SDL_Surface *Surface, long ltime)	;
	//void Draw_Page_Cursor(SDL_Surface *lSurface, long ltime, int height);
	long TrackIndexLineNotoY (long lTrackIndex, long lLineNo, int index) ;
	long MeasuretoX(long lMeasure);
	long TimetoX(MIDIData* pMIDIData, long lTime);
	long XtoTime(long x);
	long KeytoSF(long lKey, long lKeySignature);
	long KeytoLineNo(long lKey, long lKeySignature);

	void PutZiffer(int kind, int num, int level, int xA, int xB);
	//int Get_ZifferRefNum(void);

	void test(MIDIData* pMIDIData);
	
};
#endif

