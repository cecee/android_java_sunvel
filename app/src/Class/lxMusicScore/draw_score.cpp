#include <string>
#include "draw_score.h"
#include "../extern.h"

//#include "MidiManager.h"

#define min std::min
#define max std::max
	
extern long X00, X01;


draw_score::draw_score()
{
	lCurPage=-1;
	ObliEnable=0;
	lCursorLen= SCORE_BAR_LENGTH1;
	s_NaturalCnt=0;
	DrawingFag=0;
	memset(lbeatTime,0 ,sizeof(lbeatTime));
}

 draw_score::~draw_score()
{

}

void draw_score::run(void)
{
}

int draw_score::Get_CursorPos(long ltime)
{
		int Xpos, Ypos;
		int ypos;

        if(pDoc->PLY_STATUS.totalLyricLine==0 || ltime<480) return -1;
		if (screenLtimeA[0] <= ltime && screenLtimeB[0]>ltime) ypos=0;
		else 	ypos=1;
		long XX00 = XA[ypos];
		long XX01 = XB[ypos];
		int  x = TimetoX(pDoc->pMIDIData, ltime);
		 if(XX00>=XX01) return -1;//요거때문에 개고생함....... 악보그리기전에 지울려고 해서 그런거 같음....

		if(XX00>x) 	Xpos=MUSICSCORE_BEAT0_POS;
		else 	Xpos=( ((x-XX00) * MUSICSCORE_VIEW)/(XX01-XX00)) + MUSICSCORE_BEAT0_POS;
		if(ypos) Xpos=Xpos+10000;
		return Xpos;
}

int draw_score::TimeToScorelineX(long ltime)
{
		int x, xA,xB,xC,dAB;
		//long msA, msB, msX;
		long timeA=0,timeB=0;
		long dw_dt, dt, dx, posA, posX;
		int measure_inx;
		
		if(ltime<480) return -1;

		int page = pDoc->PLY_STATUS.lCurPage;
		int startMeasure= page*cntMeasurePerPage ;
		posA=page*1920;
		
		measure_inx = MUSICSCORE_START_MEASURE+startMeasure ;
		MIDIData_MakeTime (pDoc->pMIDIData, measure_inx, 0, 0, &timeA);
		measure_inx= measure_inx+cntMeasurePerLine;//
		MIDIData_MakeTime (pDoc->pMIDIData, measure_inx, 0, 0, &timeB);
		
		//msA=MIDIData_TimeToMillisec(pDoc->pMIDIData, timeA);
		//msB=MIDIData_TimeToMillisec(pDoc->pMIDIData, timeB);
		//msX= msA-msPos;
		
		dw_dt=(1920*100)/(timeB-timeA);
		dt=ltime-timeA;
		dx=(dw_dt*dt)/100;
		posX=posA+dx;
		/*
		 x = TimetoX (pDoc->pMIDIData, ltime);
		 xA = TimetoX (pDoc->pMIDIData, timeA);
		 xB = TimetoX (pDoc->pMIDIData, timeB);
		 posA=page*1920;
		 
		 dAB=xB-xA;
		 xC=x-xA;
		 posX=posA+((1920*xC)/dAB);
		 */
		//GetCurrentPage()
		//TRACE("#@#### ltime [%d] posA[%d] posX[%d] timeA[%d]  timeB[%d] xA[%d] xB[%d] x[%d]  page[%d]\n", ltime, posA, posX, timeA, timeB,xA, xB, x, page);
		return posX;
}

void draw_score::MusicScoreDraw_View(int page, int index)
{
		int i,imlx;//, drawCnt;
		int measure_inx;
		int startMeasure;
		int mLastPage;

		long x0,x1;
		long timeA=0,timeB=0;
		//pDoc->lflag.staffReady=0;
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS score_load_ok[%d] MusicScoreDrawON[%d] --page[%d] --index[%d]\n",pDoc->lflag.score_load_ok, pDoc->lflag.MusicScoreDrawON, page, index);
#endif
		startMeasure= page*cntMeasurePerPage ;

		memset(&screenMeasure, -1, sizeof(long)* 4);
		memset(&screenLtimeA, -1, sizeof(long)* 4);
		memset(&screenLtimeB, -1, sizeof(long)* 4);
		//memset(&SCR_YPos, -1, sizeof(long)* 4);
		//RefZifferNo=Get_ZifferRefNum();

//draw_title
		//TRACE("#@#D=============Draw_Title(); cntMeasurePerPage[%d]==============\n", cntMeasurePerPage);
		Draw_ScoreTitle();

		if(startMeasure<0) startMeasure=0;
		DrawingFag = 1;
		int m= (pDoc->PLY_STATUS.lmesureCnt%2)?  pDoc->PLY_STATUS.lmesureCnt: pDoc->PLY_STATUS.lmesureCnt+1;
		mLastPage=(m-3)/2;
		int D_Ypos=page%2;
		//TRACE("#@#D_Ypos[%d] page[%d/%d]==---lmesureCnt[%d]\n",D_Ypos, page, mLastPage,  pDoc->PLY_STATUS.lmesureCnt);
		if(page==mLastPage) D_Ypos=0;

		for(i=0;i<cntLinePerPage;i++)
		{
			D_Ypos=D_Ypos%2;
			measure_inx = MUSICSCORE_START_MEASURE+startMeasure + (i*cntMeasurePerLine);
			screenMeasure[i] = measure_inx;
			MIDIData_MakeTime (pDoc->pMIDIData, measure_inx, 0, 0, &timeA);

			if(i==0) curPageBeginLtime = timeA;
			measure_inx= measure_inx+cntMeasurePerLine;//
			if (measure_inx > pDoc->PLY_STATUS.lmesureDrawCnt)	measure_inx = pDoc->PLY_STATUS.lmesureCnt;
			MIDIData_MakeTime(pDoc->pMIDIData, measure_inx, 0, 0, &timeB);
			screenLtimeA[D_Ypos] = timeA;
			screenLtimeB[D_Ypos] = timeB;

			//TRACE("#@#D_Ypos[%d] DrawStaff()==timeA[%d] timeB[%d]----lmesureCnt[%d/%d]\n",D_Ypos, timeA, timeB, measure_inx, pDoc->PLY_STATUS.lmesureCnt);

			if(index)
			{
				//2 Track 악보
				imlx=i*2;
				DrawStaff(pDoc->pMIDIData, timeA, timeB - 1, imlx, 0);
				DrawStaff(pDoc->pMIDIData, timeA, timeB - 1, imlx+1, 1);
			}
			else
			{
				//1 Track 악보
				DrawStaff(pDoc->pMIDIData, timeA, timeB - 1, D_Ypos, 0);
			}
			XA[D_Ypos] = X00;
			XB[D_Ypos] = X01;
			D_Ypos++;
			if (measure_inx > pDoc->PLY_STATUS.lmesureDrawCnt) break;
	}

	DrawingFag = 0;
	curPageLastLtime = timeB;
}


int draw_score::GetCurrentPage( long ltime)
{
	int lMeasure, lpage;//, lline;//,lpageUnit=0;
	long mx, bx, tx;

	if(pDoc->PLY_STATUS.LastScoreNoteOff<=ltime || ltime<10) return -2;//ending or before start
	else if(ltime>10 && ltime<120) return -1;//Starting

//	if(ltime<10) return -1;//before playing
	MIDIData_BreakTime(pDoc->pMIDIData, ltime, &mx, &bx, &tx);
	lMeasure=mx;
	if(lMeasure)	lpage=(lMeasure-1)/cntMeasurePerPage;
	else lpage=0;
	pDoc->PLY_STATUS.lCurPage = lpage;
	return lpage;
}

int draw_score::GetLastPage(void)
{
	int LastPage;//, lpageUnit;
	long LastMeasure;

//	lpageUnit=pDoc->PLY_STATUS.lPageUnit;
	LastMeasure=	pDoc->PLY_STATUS.LastMeasure;
	LastPage=(LastMeasure-1)/cntMeasurePerPage;
	return LastPage;
}


void draw_score::Draw_ScoreTitle(void)
{
	int len1,len2;
	Uint16 title[256];
	char chorus[64]={0,};
	switch(pDoc->PLY_STATUS.ChorusPart)
	{
		case 0:
			chorus[0]=0;
			break;
		case 1:
			sprintf(chorus,"Alto");//
			break;
		case 2:
			sprintf(chorus,"Tenor");//
			break;

	}
	_Draw_UTF_Lyric("T42", SCREEN_WIDTH-200, 120, chorus, 2);
}

int draw_score::Draw_HLine(int x, int y, int xlength, int dw, Uint32 color )
{
	memset(buf,0,sizeof(buf));
	sprintf(buf, "Draw_HLine:|%d|%d|%d|%d|%d|", x, y, xlength, dw, color);
	pDoc->v_staff.push_back(buf);
	//memcpy(&pDoc->staff_char[pDoc->staff_cnt++],buf,64);
	//sprintf(pDoc->staff_char[pDoc->staff_cnt++], "Draw_HLine:|%d|%d|%d|%d|%d|", x, y, xlength, dw, color);
	return 1;
}

int draw_score::Draw_VLine(int x, int y, int ylength, int dw, Uint32 color )
{
	memset(buf,0,sizeof(buf));
	sprintf(buf, "Draw_VLine:|%d|%d|%d|%d|%d|", x, y, ylength, dw, color);
	pDoc->v_staff.push_back(buf);
	//memcpy(&pDoc->staff_char[pDoc->staff_cnt++],buf,64);
	//sprintf(pDoc->staff_char[pDoc->staff_cnt++], "Draw_VLine:|%d|%d|%d|%d|%d|", x, y, ylength, dw, color);
	return 1;
}

int draw_score::_Draw_UTF_Text(char *kind, int x, int y, const char *s, Uint32 color)
{
	memset(buf,0,sizeof(buf));
	sprintf(buf, "Draw_UTF:|%s|%d|%d|%s|%d|", kind, x, y, s,  color);
	pDoc->v_staff.push_back(buf);
//	TRACE("#@#======%s\n",buf);
//	memcpy(&pDoc->staff_char[pDoc->staff_cnt++],buf,64);
//	sprintf(pDoc->staff_char[pDoc->staff_cnt++], "Draw_UTF:|%s|%d|%d|%s|%d|", kind, x, y, s,  color);
	return 1;//
}

int draw_score::_Draw_UTF_Lyric(char *kind, int x, int y, const char *s, Uint32 color)
{
	memset(buf,0,sizeof(buf));
	sprintf(buf, "Draw_LYR:|%s|%d|%d|%s|%d|", kind, x, y, s,  color);
#ifdef DBG_STAFF
    //TRACE("#@#DBG_STAFF [%s]\n",buf);
#endif
	pDoc->v_staff.push_back(buf);
	return 1;//
}

void draw_score::DrawPole (int x0, int y0, int y1, int lup, int ypos)//��ì�� ���׸���
{
	int Xadj;//
	int Yadj= 0;
	int Yref;
	int Ylength;
	int Y;


	Yref=min(y0,y1);
	Ylength=abs(y1-y0);

	if(lup)	Xadj= 8;
	else 	Xadj= -2;//���ٷ� �ִ°� org -4
	int X=(((x0-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS + Xadj;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + Yref + Yadj;
	Draw_VLine(X, Y, Ylength, 1, LINE_COLOR);
}

int draw_score::DrawMeasureLine(int measureNum, int x, int y0, int y1, int ypos, int index)
{

	long Xstart;
	long Ystart;
	long Ylength=y1-y0;
	int UnitLineX, UnitLineY, UnitLineLen;
	char str[4];
	//---------------------------
	//--80--|--------1000--------
//		printf("��Ʈ��X00[%d]-X01[%d] x[%d]\n",X00, X01, x);
	Ystart= MUSICSCORE_YOFFSET + y0 + MUSICSCORE_HIGHT*ypos;
//	printf("###############------------measureNum[%d] PLY_STATUS.lmesureCnt[%d] \n", measureNum, PLY_STATUS.lmesureCnt);
		if(x==0)
		{
			Xstart=MUSICSCORE_XSTART;
			sprintf(str,"%d",measureNum+1);
			int y = TrackIndexLineNotoY (0, 48, index);
			int YMeasureNo = MUSICSCORE_YOFFSET + y + MUSICSCORE_HIGHT*ypos;
			if(index==0)	{
				_Draw_UTF_Text("T24", Xstart+0, YMeasureNo, str, 0x606060ff)	;
			}
			else //���پǺ� ������ �׸���
			{
				UnitLineX=MUSICSCORE_XSTART-6;
				UnitLineY=YMeasureNo-MUSICSCORE_HIGHT+18;
		   		UnitLineLen=MUSICSCORE_HIGHT+(MUSICSCORE_YGAP*8) -2;
				Draw_VLine(UnitLineX, UnitLineY, UnitLineLen, 2, LINE_COLOR);
			}
		}
		else
		{
			Xstart=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
			if( pDoc->PLY_STATUS.LastMeasure == (measureNum-1))//마지막 라인
			{
				Draw_VLine(Xstart-5,  Ystart, Ylength, 8, LINE_COLOR );
				Draw_VLine(Xstart-14, Ystart, Ylength, 1, LINE_COLOR );
			}
			else
				Draw_VLine(Xstart, Ystart, Ylength, 1, LINE_COLOR );
		}
	return Xstart;
}

int draw_score::DrawMeasureLineXPos( int x, int xA, int xB)
{

	long Xstart;
//	long Ystart;
//	Ystart= MUSICSCORE_YOFFSET + y0 + MUSICSCORE_HIGHT*ypos;
	Xstart= (x==0) ? MUSICSCORE_XSTART: ((abs(x-xA)*MUSICSCORE_VIEW)/(xB-xA))+MUSICSCORE_BEAT0_POS;
	return Xstart;
}


void draw_score::DrawFiveLine(int y, int ypos)
{
	int rtn;
	long Xstart= MUSICSCORE_XSTART;
	long Xlength=MUSICSCORE_LENGTH;
	long Ystart;
	Ystart= MUSICSCORE_YOFFSET + y + MUSICSCORE_HIGHT*ypos;
	rtn=Draw_HLine(Xstart, Ystart, Xlength, 1, LINE_COLOR);
	//TRACE("draw_score::DrawFiveLiney[%d] Xstart[%d] Ystart[%d]----rtn[%d]\n",y, Xstart, Ystart, rtn);

}

void draw_score::DrawLyric (MEASURE_LYRIC MsLyric, int msXA, int msXB, int ypos)
{
	int x,y,X,Y;
	char Ly_cnt=0;
	char Ly_sub_cnt=0;
	char Ly_strU8[64][64]={0,};
	int  xPosLy[64];
	int i,j;
	int xPosA[64];
	int xPosB[64];
	char xOVR[64];

	int Cnt=MsLyric.LyricNumOfMeasure;
	y=MsLyric.y;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y;
	for(i=0;i<Cnt;i++)
	{
		x=MsLyric.x[i];
		xPosA[i]=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
		X=xPosA[i];
		_Draw_UTF_Lyric("T40", X-20, Y+80, MsLyric.strU8[i], 0);
	}

}

void draw_score::DrawMusicalText(char* str, int x , int y, int flag, int ypos)
{
	int X,Y;
	if(flag) X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;// + Xadj;
	else {
		X = MUSICSCORE_XSTART + 100;
	}
 	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT * ypos) ;
	_Draw_UTF_Text("M42", X, Y-12, str, LINE_COLOR);
}

void draw_score::DrawSharp(int x , int y, int flag, int ypos)
{
	int Xadj;
	int X, Y;//, Xorg, Yorg;
	if(flag) //����ȿ�
	{
		Xadj=-12;
		X =((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS + Xadj;
		if(X<=MUSICSCORE_BEAT0_POS) 	X=MUSICSCORE_BEAT0_POS-12;//-24;
		Y = MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos) ;
		//Draw_UTF_Text(pFONT_MUSICAL_30, X, Y-44, "#", LINE_COLOR);
		_Draw_UTF_Text("M30", X, Y, "#", LINE_COLOR);
	}
	else //flag==0 �϶� ���� ��ǥ ��ġ�� �׸���.
	{
		X=x;
		Y = MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos) ;
		//Draw_UTF_Text(pFONT_MUSICAL_42, X, Y-60, "#", LINE_COLOR);
		_Draw_UTF_Text("M42", X, Y, "#", LINE_COLOR);
	}
}

void draw_score::DrawFlat(int x , int y, int flag, int ypos)
{
	int  Xadj;
	int X, Y;
	if(flag) //����ȿ�
	{
		Xadj=-12;
		X =((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS + Xadj;
		if(X<=MUSICSCORE_BEAT0_POS) 	X=MUSICSCORE_BEAT0_POS-12;//-24;
		Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos);
		_Draw_UTF_Text("M30", X, Y, "b", LINE_COLOR);
	}
	else //flag==0 �϶� ���� ��ǥ ��ġ�� �׸���.
	{
		X=x;
		Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos);
		_Draw_UTF_Text("M42", X, Y, "b", LINE_COLOR);
	}
}

//Ʈ�� �ε���(0~65535)(�ټ� �� ��ȣ�� Y��ǥ[pixel]�� ��ȯ
long draw_score::TrackIndexLineNotoY(long lTrackIndex, long lLineNo, int index)
{
//	m_lTrackZoom=1;
	MusicalScoreTrackInfo* pTrackInfo = NULL;
	pTrackInfo = pMusic_score->Score[index].pmsti;//+lTrackIndex;

	long lTrackTop = pTrackInfo->m_lTop;
	long lTrackHeight = pTrackInfo->m_lHeight;
	long lTrackFlags = pTrackInfo->m_lFlags;
	long lKey000Y = 0;
	long ry = 6; //8


	switch (lTrackFlags & 0x0000000F) {
	case 1: // ������?
		lKey000Y = (lTrackTop + lTrackHeight / 2)  + 41 * ry;
		break;
	case 2: // ������?
		lKey000Y = (lTrackTop + lTrackHeight / 2)  + 29 * ry;
		break;
	case 3: // ������
		lKey000Y = (lTrackTop + lTrackHeight / 2)  + 35 * ry;
		break;
	default:
		lKey000Y = (lTrackTop + lTrackHeight / 2)  + 35 * ry;
		break;
	}
	//TRACE("lKey000Y[%d]============[%d]\n",lKey000Y, lKey000Y - lLineNo * ry);
	return lKey000Y - lLineNo * ry;
}

void draw_score::DrawGClef(CLEF_SIG clefSig, int flag ,int ypos, int index)
{

	long lGCrefSharpLineNo[7] = {45, 42, 46, 43, 40, 44, 41};
	long lGCrefFlatLineNo[7] = {41, 44, 40, 43, 39, 42, 38};

	int x, y, y1, X, Y;
	int i, j, sigCnt;
	int rx=4;
	long ry = MUSICSCORE_YGAP;

	char strText1[4];
	char strText2[4];

	long sigXpos[8]={0,};
	long sigYpos[8]={0,};

	sprintf(strText1,"%ld",clefSig.lnn);//����
	sprintf(strText2,"%ld",(1 << clefSig.ldd));//�и�

	x=clefSig.lclefXpos;

	long x_txext1     =x + rx * 10 + rx * 2 * abs(clefSig.lsf);
	long x_flagtxext1 = rx * 10 + rx * 2 * abs(clefSig.lsf);
//	TRACE("#@#DrawGClef------------XXXXXXXXXXXXXX------newSF[%d] clefSig.lsf[%+d]\n", newSF, clefSig.lsf);
//	TRACE("#@#-strText1-lnn:%s-----XXXXXXXXXXXXXX------strText2-ldd:%s---------------\n", strText1, strText2);
//	TRACE("#@#DrawGClef------------x_txext1[%d] x_flagtxext1[%d]---------\n", x_txext1, x_flagtxext1);
	sigCnt=0;
	if (clefSig.lsf > 0) {
		for (j = 1; j <= clefSig.lsf; j++)
		{
			sigYpos[sigCnt]= TrackIndexLineNotoY (0, lGCrefSharpLineNo[j - 1], index);
			sigXpos[sigCnt]= x + rx * 8 + rx * 2 * j;
			sigCnt++;
		}
	}
	else if (clefSig.lsf < 0)
	{
		for (j = 1; j <= -clefSig.lsf; j++) {
			sigYpos[sigCnt]= TrackIndexLineNotoY (0, lGCrefFlatLineNo[j - 1], index);
			sigXpos[sigCnt] = x + rx * 8 + rx * 2 * j;
			sigCnt++;
		}
	}
	y =  TrackIndexLineNotoY (0, 41, index);//����--text position
	y1 = TrackIndexLineNotoY (0, 37, index);//�и�---text position

	long step=10;
	if(sigCnt>4) step=6;
	if(flag<=0)//Flag 0 or -1�϶�  -1:ù��°���� 0:�������� ù��°
	{
		if (clefSig.lsf > 0)
		{

		 	for(i=0;i<sigCnt;i++)
		 	{
		 		//flag==0�϶��� x�� ������ǥ�� �׸�.
		 		DrawSharp ((i*step)+MUSICSCORE_XSTART+50, sigYpos[i], 0, ypos);
			}
		}
		else if (clefSig.lsf < 0)
		{
		 	for(i=0;i<sigCnt;i++)
		 	{
		 		//flag==0�϶��� x�� ������ǥ�� �׸�.
		 		DrawFlat ((i*step)+MUSICSCORE_XSTART+50, sigYpos[i], 0, ypos);
			}
		}
		if(flag==-1)
		{
			DrawMusicalText (strText2, x_flagtxext1, y1, 0, ypos);//�и�
			DrawMusicalText (strText1, x_flagtxext1,  y, 0, ypos); //����
		}
	}
	else
	{
		//need something
		DrawMusicalText (strText2, x_txext1, y1, 1, ypos);
		DrawMusicalText (strText1, x_txext1, y , 1, ypos);

	}

	//TRACE("#@#1111------------YYYYYYYYYYYYYY----------\n");
	 Y= MUSICSCORE_YOFFSET + y + MUSICSCORE_HIGHT * ypos;

	if(flag>0) //normal
	{
		X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS - 6;
	}
	else //first
	{
		X= MUSICSCORE_XSTART+10;
	}
	//Draw_UTF_Text(pFONT_MUSICAL_42, X, Y-49, "&", LINE_COLOR);
	_Draw_UTF_Text("M42", X, Y+12, "&", LINE_COLOR);

	if(ypos==0 && Ziffersystem )
	{
		char codeStr[32];
		int mlsf;
		int jangjo=99;
		//TRACE("#@#-----------DrawGClef--------------clefSig.lsf [%d]  mi[%d] ------\n", clefSig.lsf, clefSig.mi);
		for(j=0;j<12;j++)
		{
			if(clefSig.mi==1) mlsf=clefKeyTbl_minor[j];
			else mlsf=clefKeyTbl[j];

			if(clefSig.lsf==mlsf)
			{
				jangjo=j;
				break;
			}
		}
		Ztitle.mi=clefSig.mi;
		Ztitle.jangjo=jangjo;
		memcpy(Ztitle.baby,strText1,4);
		memcpy(Ztitle.mother,strText2,4);
		Draw_ZifferTitle(Ztitle);
		//TRACE("#@#-----------DrawGClef--------------clefSig.lsf [%d]   jangjo[%s]---------\n", clefSig.lsf, codeStr );
	}
}
void draw_score::Draw_ZifferTitle(ZIFFER_TITLE ztitle)
{
		char codeStr[32];
		char mm[4];
		char bb[4];

		int mi=ztitle.mi;
		int jangjo=ztitle.jangjo;

		if(jangjo<12){
			if(mi==1)	sprintf(codeStr,"1=%s", JungKeyMinor[jangjo]);
			else 	sprintf(codeStr,"1=%s",jungKeyMajor[jangjo]);
		}
		memcpy(mm, ztitle.mother,4);
		memcpy(bb,ztitle.baby,4);
		int mmpos=120;
		_Draw_UTF_Lyric("T42", SCREEN_WIDTH-400, mmpos , codeStr, 1);
		_Draw_UTF_Lyric("M60", SCREEN_WIDTH-280, mmpos-34, bb, 1);
		_Draw_UTF_Lyric("M60", SCREEN_WIDTH-280, mmpos+14, mm, 1);
		_Draw_UTF_Lyric("M60", SCREEN_WIDTH-280, mmpos-10, "_", 1);
}

long draw_score::GetNewSf_Mi(long KeySig, int mPlus)
{
	int i,x, xx;
	long Sigature;
	long lsf = KeySig & 0x000000FF;
	long lmi = (KeySig >> 8) & 0x000000FF;
	lsf = (signed char)lsf;

//	TRACE("#@#GetNewSf_Mi 111XXXXXXXXXXXX lsf[%+d]\n",lsf);
	for(i=0;i<12;i++)
	{
		if (lsf == clefKeyTbl[i]) break;
	}
	x=(i + mPlus+12)%12;
	xx = clefKeyTbl[x];
	Sigature = ((xx & 0xFF) | ((lmi & 0xFF) << 8));
//	TRACE("#@# XXXXXXXXXXXX Sigature[%x] xxD[%+d] lmiX[%x]\n",Sigature, xx, lmi);
	return Sigature;
}

void draw_score::DrawTimeAndKeySignature (MIDIData* pMIDIData, long lTime, int flag, int ypos,int index)
{

	int i;
	MusicalScoreTrackInfo* pTrackInfo = NULL;
	pTrackInfo = pMusic_score->Score[index].pmsti;
	long lMeasure, lBeat, lTick;
	MIDIData_BreakTime (pMIDIData, lTime, &lMeasure, &lBeat, &lTick);
	long x = MeasuretoX (lMeasure);
	long lsf, lmi;
	long lnn, ldd, lcc, lbb;
	long lltime;
	//signed char charSF;

	//lltime = (flag==0)? 0:lTime;
	lltime=lTime;
	MIDIData_FindKeySignature (pMIDIData, lltime, &lsf, &lmi);
	MIDIData_FindTimeSignature (pMIDIData, lltime, &lnn, &ldd, &lcc, &lbb);

	//TRACE("#@#First  Sharf/Flat  MARK lsf--[%+d] lmi[%d]\n",lsf, lmi);
	//TRACE("#@#First Sharf/Flat  MARK lTime[%d] lnn[%d] ldd[%d]\n",lTime, lnn, ldd);

	long xSignature;
	long lsignature = ((lsf & 0xFF) | ((lmi & 0xFF) << 8));
	xSignature = GetNewSf_Mi(lsignature, m_lTrackKeyPlus);

//	TRACE("#@#DrawTimeAndKeySignature xSignature [%x]\n",xSignature);
	newSF = (signed char) (xSignature & 0x000000FF);
//	TRACE("#@#DrawTimeAndKeySignature newSF [%+d]\n",newSF);
	CLEF_SIG ClefSig;

	ClefSig.lsf=newSF;
	ClefSig.lnn=lnn;
	ClefSig.ldd=ldd;
	ClefSig.lclefXpos	=	 x ;//+ rx * 4;
	ClefSig.lclefYpos	=	 TrackIndexLineNotoY (0, 39, index);
	ClefSig.mi = lmi;
	DrawGClef(ClefSig, flag, ypos, index);
}

void draw_score::DrawLegLine (int x, int y, int ypos)//������ �׸���
{
//	int Xadj=0;//
	int Yadj= 0;
	int Xlength=26;// Xref;
	int X, Y;

	X = (((x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y + Yadj;
	Draw_HLine(X-(Xlength/2)+4, Y, Xlength, 1, LINE_COLOR);
}

void draw_score::DrawDoubleSharp(int x , int y, int flag, int ypos)
{
	int X, Y;
	char _str[3]={0xc3,0x9c,0,};//DrawDoubleSharp
	if(flag) {
		X =((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS -14;
		if(X<=MUSICSCORE_BEAT0_POS) 	X=MUSICSCORE_BEAT0_POS-16;//-24;
	}

	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos)+1 ;
	if(flag)
	{
		_Draw_UTF_Text("M30", X, Y, _str, LINE_COLOR);
	}
	else //flag==0 �϶� ���� ��ǥ ��ġ�� �׸���.
	{
		X=x;
		_Draw_UTF_Text("M40", X, Y, _str, LINE_COLOR);
	}

}

void draw_score::DrawDoubleFlat(int x , int y, int flag, int ypos)
{

	int Yadj= -10;
	int X, Y;
//	char str[2]={186,0,};//DrawDoubleFlat
	char _str[3]={0xc2,0xba,0,};//DrawDoubleFlat
	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos)+1 ;
	if(flag)
	{
		X =((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS -14;
		if(X<=MUSICSCORE_BEAT0_POS) 	X=MUSICSCORE_BEAT0_POS-16;//-24;
		_Draw_UTF_Text("M30", X, Y, _str, LINE_COLOR);

	}
	else //flag==0 �϶� ���� ��ǥ ��ġ�� �׸���.
	{
		X=x;
		_Draw_UTF_Text("M40", X, Y, _str, LINE_COLOR);
	}
	//pFont->DG_TextOut(MidiPlayBack::m_midSurface,	X-80, Y-80,	160,	160,	0xff202020,	0xff202020, str,	UTF8,	V_CENTER|H_CENTER);
}

void draw_score::DrawDot (int x, int y, int ypos)
{

	int Xadj= 12;
	int Yadj= 0;//-8
	int X, Y;

	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS + Xadj;
	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos) + Yadj;
	_Draw_UTF_Text("M42", X, Y, "k", LINE_COLOR);
}

void draw_score::DrawTadpole (int x, int y, long lFlags, int ypos)//��ì�� �׸���
{

	int Xadj=-4;//-10;
	int X,Y, Xorg;
//	Uint16 strb[2]={0xF0CF,0,};//header_b code
//	Uint16 strw[2]={0xF0FA,0,};//header_w code

	char _strb[3]={0xc3,0x8f,0,};//header_b code
	char _strw[3]={0xc3,0xba,0,};//header_w code

	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS + Xadj;
	Xorg=X-Xadj;

	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos) ;

	if (lFlags & 0x00010000) {
			//Draw_UNICODE_Text(pFONT_MUSICAL_42, X, Y-61, strw, LINE_COLOR);
			_Draw_UTF_Text("M42", X, Y, _strw, LINE_COLOR);
	}
	else {
			//Draw_UNICODE_Text(pFONT_MUSICAL_42, X, Y-61, strb, LINE_COLOR);
			_Draw_UTF_Text("M42", X, Y, _strb, LINE_COLOR);
	}
//	TRACE("��ì�� �׸���\n");

}


//lflag->������ lbIsUp-->0:Up
//lFlags2=0x00xy_0000; x:������ y:0->�� 1->��
int draw_score::DrawNoteWtail (int x, int y, int flag2, int lbIsUp, int ypos ,int lflag )
{
	int Xadj,Xadj3;//=-16;
	int Yadj,Yadj3;//= -6;//-28;//-34;//-42;
	int X, Y;
	int lpole=0;
//	int quarter=0;
	Xadj=-4;
	Yadj=6;
	char _str[3]={0,};
	char _str3Tail[3]={0,};

	Uint32 xcorlor=0x00ff;
//	printf("--------����������-------flag2[%lx] [%08lx][%d]\n",flag2, flag2 & 0x00010000, lflag);
	if(flag2 & 0x00100000 )//check ������������
	{
		switch(lflag)
		{
			case 0://check ��
					if((flag2 & 0x00010000)==0) lpole=0;
					else//white head with pole
					{
						lpole=1;
						//Yadj=-55;
						_str[0]= (lbIsUp) ? 'h':'H';_str[1]=0;
						PutZiffer(4, lZifferLineNo, 0, x, 1); //1-desh
					}
					break;
			case 1:
				if((flag2 & 0x00010000)==0)//1 tail
				{
				 		lpole=1;
				 		//Yadj=-55;
				 		_str[0]= (lbIsUp) ? 'e':'E';_str[1]=0;
				}
				break;
			case 2:
			case 3:
				if((flag2 & 0x00010000)==0)//�����ΰ�
				{
					lpole=1; //Yadj=-55;
					Yadj=Yadj-6;
					_str[0]= (lbIsUp) ? 'x':'X';_str[1]=0;

				}
				break;
		}
	}
	else
	{
		if(flag2 & 0x00010000)
		{
			lpole=1;
			//Yadj=-61;//whole Note check ��밡��
			_str[0]='w';//whole Note check ��밡��
			PutZiffer(4, lZifferLineNo, 0, x, 3); //3-desh 4����
		}
	}

	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS + Xadj;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y + Yadj;

	if(lflag==3 && flag2 & 0x00100000)
	{
		 lpole=1;
		 if (lbIsUp)
		 {
			Xadj3 = 13;
			Yadj3 = -8;
			_str3Tail[0]=0xc2; _str3Tail[1]=0x91;
		 }
		 else
		 {
			 Xadj3=0;	 Yadj3 = 0;
			 _str3Tail[0]=0xc2;   _str3Tail[1]=0x93; _str3Tail[2]=0x00;
		}
		//Draw_UNICODE_Text(pFONT_MUSICAL_42, X+Xadj3, Y+Yadj3, str3Tail, LINE_COLOR);//�������� �ִ°�
		_Draw_UTF_Text("M42", X+Xadj3, Y+Yadj3, _str3Tail, LINE_COLOR);//�������� �ִ°�
	}
	//Draw_UNICODE_Text(pFONT_MUSICAL_42, X, Y, str, xcorlor);
	_Draw_UTF_Text("M42", X, Y, _str, xcorlor);

	if(lflag>0)
	{
			PutZiffer(2, lZifferLineNo, 0, x, lflag);
	}
//  Draw_HLine(X, Y, 50, 10, 0xff);
	return lpole;
}

//x0:������x y0:������ rx:������ ry:������ bUp:��/�Ʒ� fr:��/��
void	draw_score::DrawEllipse_half(int x0, int y0, int rx0, int ry0, int bUp, int fr, Uint32 color)
{
    int i, len;
    int x, y;
    int rx2, ry2;
		int xpos[20]={0,};
		int ypos;
		int xc,rx,ry,yc;
		int dw=1;
		//if(fr) rx=rx12*2;
		//else
		if(fr==0){
    	xc=x0+rx0;
    	yc=y0;
    	rx=rx0;
    	ry=ry0;
  	}
  	else if(fr==1){
    	xc=x0;
    	yc=y0;
    	rx=rx0;
    	ry=ry0;
  	}
  	else{
    	xc=x0+rx0/2;
    	yc=y0;
    	rx=rx0/2;
    	ry=ry0;
  	}

    rx2 = rx*rx;
    ry2 = ry*ry;
    x = rx;

    for(y=0; y<ry; y++)
    {
        while((ry2*x*x) >= (rx2*(ry2-y*y)))
        {
            x--;
        }
      	xpos[y]=x;
    }

    for(i=0;i<ry;i++)
    {
    	if(bUp==0){ypos=yc+i;}
    	else{ypos=yc-i;}

    	if(i>=ry-1) {
    		if(fr==0)	Draw_HLine(xc-xpos[i], ypos, xpos[i]+1, dw, color );//DG_DrawRectangle(handle, xc-xpos[i], ypos, xpos[i]+1, 2, color);
    		else if(fr==1) Draw_HLine( xc, ypos, xpos[i]+1, dw, color );//DG_DrawRectangle(handle, xc, ypos, xpos[i]+1, 2, color);
    		else  Draw_HLine( xc-xpos[i], ypos, xpos[i]*2+1, dw, color );//DG_DrawRectangle(handle, xc-xpos[i], ypos, xpos[i]*2+1, 2, color);
    	}
    	else
    	{
    		len=xpos[i]-xpos[i+1];
  				if(fr==0)	Draw_HLine( xc-xpos[i], ypos, len+1, dw, color );//DG_DrawRectangle(handle, xc-xpos[i], ypos, len+1, 2, color);
    			else if(fr==1) Draw_HLine( xc+(xpos[i]-len), ypos, len+1, dw, color );//DG_DrawRectangle(handle, xc+(xpos[i]-len), ypos, len+1, 2, color);
    			else {
    				Draw_HLine( xc-xpos[i], ypos, len+1, dw, color );//DG_DrawRectangle(handle, xc-xpos[i], ypos, len+1, 2, color);
    			 	Draw_HLine( xc+(xpos[i]-len), ypos, len+1, dw, color );//DG_DrawRectangle(handle, xc+(xpos[i]-len), ypos, len+1, 2, color);
    		}
    	}
    }
}
void	draw_score::_DrawEllipse_half(int x0, int y0, int rx0, int ry0, int bUp, int fr, Uint32 color)
{
	int i;
	int mfr, kind;
	mfr=fr%3;
	if(bUp==0) kind=mfr;
	else kind=100+mfr;
	memset(buf,0,sizeof(buf));
	sprintf(buf, "Draw_Arc:|%d|%d|%d|%d|%d|", x0, y0, rx0, kind, color);
	pDoc->v_staff.push_back(buf);
	//memcpy(&pDoc->staff_char[pDoc->staff_cnt++],buf,64);
	//sprintf(pDoc->staff_char[pDoc->staff_cnt++], "Draw_Arc:|%d|%d|%d|%d|%d|", x0, y0, rx0, kind, color);
}
//DrawNatural
void draw_score::DrawNatural (int x, int y, int ypos)
{

	int X, Y;
	X =((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS -12;
	Y= MUSICSCORE_YOFFSET + y + (MUSICSCORE_HIGHT*ypos) ;
	if(X<=MUSICSCORE_BEAT0_POS) X=MUSICSCORE_BEAT0_POS-12;
//	Draw_UTF_Text(pFONT_MUSICAL_30, X, Y-44, "n", LINE_COLOR);
	_Draw_UTF_Text("M30", X, Y, "n", LINE_COLOR);
}

void draw_score::DrawChainedFlag (int x1, int x2, int y1, int y2, int rx, int ry, int lup, int ypos)
{
	int Xadj;//= 8;
	int Yadj= 0;
	int Xref, Yref;
	int Xlength, Ylength;
	int X,Y;
	int xv0,xv1,xvLen;
	Ylength=ry-1;
	Yref=y2;
	xv0= (((x1-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	xv1= (((x2-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	Xlength=abs(xv0-xv1);
	Xref =min(xv0,xv1);
	if(lup==0)
	{
		X=Xref -2;
		Yadj=4;
	}
	else
	{
		X=Xref + 8;//10
		Yadj=0;
		if(Xlength) Xlength+=1;
	}
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + Yref + Yadj;

	if(X<MUSICSCORE_BEAT0_POS) X=MUSICSCORE_BEAT0_POS-2;
	if(Xlength) 	Draw_HLine(X, Y, Xlength, Ylength, LINE_COLOR);
}

void draw_score::DrawTieHalf_table(long x1, long x2, long y, long lFlags)
{
	int i,find;
	int dir=0;
	if(x2>x1) dir=0;
	else dir=1;

	if(TieCnt==0)
	{
		if(dir==0)
		{
			TieBuf[0].a=x1;
			TieBuf[0].b=x2;
			TieBuf[0].c=-1;
			TieBuf[0].y=y;
			TieBuf[0].flag=lFlags;
			TieBuf[0].Znum=lZifferLineNo;
		}
		else
		{
			TieBuf[0].a=-1;
			TieBuf[0].b=x2;
			TieBuf[0].c=x1;
			TieBuf[0].y=y;
			TieBuf[0].flag=lFlags;
			TieBuf[0].Znum=lZifferLineNo;
		}
		TieCnt++;
	}
	else
	{
		if(dir==0)
		{
			find=0;
			for(i=0;i<TieCnt;i++)
			{
				if((TieBuf[i].b==x2)&&(TieBuf[i].y==y))
				{
					TieBuf[i].a=x1;
					find=1;
					break;
				}
			}
			if(find==0)//not find
			{
					TieBuf[TieCnt].a=x1;
					TieBuf[TieCnt].b=x2;
					TieBuf[TieCnt].c=-1;
					TieBuf[TieCnt].y=y;
					TieBuf[TieCnt].flag=lFlags;
					TieBuf[TieCnt].Znum=lZifferLineNo;
					TieCnt++;
			}

		}
		else //(dir==1)+
		{
			find=0;
			for(i=0;i<TieCnt;i++)
			{
				if((TieBuf[i].b==x2)&&(TieBuf[i].y==y))
				{
					TieBuf[i].c=x1;
					find=1;
					break;
				}
			}
			if(find==0)//not find
			{
					TieBuf[TieCnt].a=-1;
					TieBuf[TieCnt].b=x2;
					TieBuf[TieCnt].c=x1;
					TieBuf[TieCnt].y=y;
					TieBuf[TieCnt].flag=lFlags;
					TieBuf[TieCnt].Znum=lZifferLineNo;
					TieCnt++;
			}

		}//(dir==1)-
	}
	//printf("DrawTieHalf[%d][%d][%d]--[%x]\n",x1, x2, y, lFlags);//
	//printf("DrawTieHalf[%d][%d][%d]\n",TieBuf[0].a, TieBuf[0].b, TieBuf[0].c);//
}

void draw_score::DrawTieFull(int x1, int x2, int y, int bUp, int fr, int ypos)
{

	int Yadj;
	int Xlength, Ylength;
	int X,Y;
	int xv0,xv1,xvLen,len;
	int hight=10;

	//TRACE("#@# DrawTieFull[%d][%d]-[%d] bUp[%d] Fr[%d] ypos[%d]\n",x1, x2, y, bUp, fr, ypos);//
	xv0= (((x1-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	xv1= (((x2-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	xvLen=abs(xv1-xv0);

	if(bUp==0) Yadj= 6;
	else Yadj= -6;

	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y + Yadj;
 	X= xv0;
// 	if(bUp==0) X = xv0 ;
// 	else X = xv0-4 ;
	if(xvLen>8) xvLen = xvLen-8;
	if(xvLen<24) hight = 8;
	//DrawEllipse_half( X+6, Y, xvLen, hight, bUp, fr, LINE_COLOR);
	_DrawEllipse_half( X+6, Y, xvLen, hight, bUp, fr, LINE_COLOR);
	//PutZiffer(5, 0, fr, x1, x2);
}
void draw_score::DrawZiffTieFull(int x1, int x2, int fr, int ypos, int znum)
{

	int Yadj;
	int Xlength, Ylength;
	int X,Y;
	int xv0,xv1,xvLen,len;
	int hight=6;
	long lcolor;
//	TRACE("#@# DrawTieFull[%d][%d]-  Fr[%d] ypos[%d]\n", x1, x2, fr, ypos);//
	xv0= (((x1-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	xv1= (((x2-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	xvLen=abs(xv1-xv0);

	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) +160;
 	X= xv0;
	if(xvLen>8) xvLen = xvLen-8;
	if(xvLen<24) hight = 8;

	znum=znum - pDoc->PLY_STATUS.RefZifferNo;
	if(znum<0) lcolor=COLOR_ZFL;
	else if(znum>6) lcolor=COLOR_ZFH;
	else lcolor=COLOR_ZFN;
	_DrawEllipse_half( X+6, Y, xvLen, hight, 1, fr, lcolor);
}

void draw_score::DrawTripletSign(int x1, int x2, int y1, int y2, long lFlags, int ypos, int index)
{
	int Xadj= 0;
	int Yadj= 0;
	int xv0,xv1,xv0a, xv0b, xv0Len, xv1Len, xvcent;
	int yv0,yv1,yv0a, yv0b, yvLen, yvcent;
	char str[8];

	if((lFlags & 0x000000FF) < 2) return;

	y1=TrackIndexLineNotoY (0, 47, index);
	y2=TrackIndexLineNotoY (0, 45, index)-2;

		yv0= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y1 + Yadj;
		yv1= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y2 + Yadj;

	yv0a=min(yv0, yv1);
	yv0b=max(yv0, yv1);
	yvLen=yv0b-yv0a;
	yvcent=(yv0a+yv0b)/2;

	int xt0= (((x1-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
	int xt1= (((x2-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;

	xv0=min(xt0, xt1)+10-2;
	xv1=max(xt0, xt1)+4;

	xv0a= (xv0 + xv1) / 2 - 6;
	xv0b= (xv0 + xv1) / 2 + 6;
	xvcent= (xv0 + xv1) / 2 ;

	xv0Len=xv0a-xv0;
	xv1Len=xv1-xv0b;
// ____
// |
	Draw_HLine(xv0,  yv0a, xv0Len, 1, LINE_COLOR);
	Draw_VLine(xv0,  yv0a, yvLen, 1, LINE_COLOR);
	//DG_DrawRectangle(MidiPlayBack::m_midSurface, xv0,  yv0a, xv0Len, 1, 0xff000000);//------
	//DG_DrawRectangle(MidiPlayBack::m_midSurface, xv0,  yv0a, 1, yvLen, 0xff000000);//|(xv0)

//____
//       |
	Draw_HLine(xv0b, yv0a, xv1Len+4, 1, LINE_COLOR);//  ____
	Draw_VLine(xv1+4,  yv0a, yvLen,  1, LINE_COLOR);  //(xv1)|

//text
	sprintf(str,"%d",(lFlags & 0x000000FF));
	yvcent=yvcent-8;
	_Draw_UTF_Text("T20", xvcent-5, yvcent+4, str, LINE_COLOR);

	//if (pFont)
	//	pFont->DG_TextOut(MidiPlayBack::m_midSurface,	xvcent-24, yvcent-24,	48,	48,	MAKE_RGB(80,80,80), MAKE_RGB(80,80,80), str, UTF8, V_CENTER|H_CENTER);
}

void draw_score::PutZiffer(int kind, int num, int level, int xA, int xB)
{
	Zinfo[ZinfoCnt].kind=kind;
	Zinfo[ZinfoCnt].num=num;
	Zinfo[ZinfoCnt].level=level;
	Zinfo[ZinfoCnt].xA=xA;
	Zinfo[ZinfoCnt].xB=xB;
	ZinfoCnt++;
}

int draw_score::Score_xTOX(int xA, int xB, int x)
{
	int Xpos;
	if(xA>=xB) return -1;//요거때문에 개고생함....... 악보그리기전에 지울려고 해서 그런거 같음....
	Xpos=(xA>x)? 0: ( ((x-xA) * 1920)/(xB-xA)) + 0;
	return Xpos;
}

void draw_score::DrawNote(MIDIData* pMIDIData, MusicalScoreNoteInfo* pNoteInfo, int ypos, int index)
{
//	NOTE_STRUCT noteStruct;
//	memset(&noteStruct,0,sizeof(NOTE_STRUCT));
	ZinfoCnt=0;
	int score_ziffer;
	int score_ziffer_hilow;
	static int drawLyricCnt=0;
	long lTrackIndex=0;
	MIDIEvent* pNoteEvent = pNoteInfo->m_pNoteOnEvent;
	long lTimeMode, lTimeResolution;
	MIDIData_GetTimeBase (pMIDIData, &lTimeMode, &lTimeResolution);
	MusicalScoreTrackInfo* pTrackInfo = NULL;
	pTrackInfo = pMusic_score->Score[index].pmsti;

	long lTrackFlags = pTrackInfo->m_lFlags; //pMusicalScoreFrame->GetTrackFlags (lTrackIndex);
	long lKey=MIDIEvent_GetKey (pNoteEvent);
	long lZifferKey = lKey;
			lKey = lKey + m_lTrackKeyPlus;
	long velocity=MIDIEvent_GetVelocity (pNoteEvent);

	long lDuration = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
	long lNoteOnMeasure = pNoteInfo->m_lNoteOnMeasure;
	MusicalScoreMeasureInfo* pMeasureInfo = NULL;
	pMeasureInfo = pMusic_score->pmsmi + lNoteOnMeasure;
	long lKifferKeySignature= pMeasureInfo->m_lKeySignature; 	// �̼��� ����
	long lKeySignature = pMeasureInfo->m_lKeySignature; 	
			 lKeySignature= GetNewSf_Mi(pMeasureInfo->m_lKeySignature, m_lTrackKeyPlus);
	long lTimeSignature = pMeasureInfo->m_lTimeSignature; // �̼��� ����ǥ
	long lLineNo = KeytoLineNo (lKey, lKeySignature);
			lZifferLineNo = KeytoLineNo (lZifferKey, lKifferKeySignature);

	long lSF = KeytoSF (lKey, lKeySignature);
	long lnn = lTimeSignature & 0xFF;
	long ldd = (lTimeSignature & 0xFF00) >> 8;

	long x = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime);
	long y = 0;
	long rx = 4;
	long ry = MUSICSCORE_YGAP;
	long i = 0;

	long xx2=0;
	long drawHeaderOK=0;
	long poleLenGap=6;
	
//	char buf[128];
//score_ziffer=GetZifferNumber(lLineNo)%8;//1~7
//score_ziffer_hilow=GetZiffeHighLow(lLineNo);

//	score_ziffer=GetZifferNumber(lZifferLineNo)%8;//1~7
//	score_ziffer_hilow=GetZiffeHighLow(lZifferLineNo);	
//	if(score_ziffer_hilow<0) score_ziffer+=10;//11~17
//	if(score_ziffer_hilow>0) score_ziffer+=20;//21~27	
		
//	sprintf(buf, "_NOT:|%ld|%ld|%ld|%d|%d|", pNoteInfo->m_lNoteOnTime, pNoteInfo->m_lNoteOffTime, lLineNo, score_ziffer, ypos);
//TRACE("#@###DrawNote  (1)  lKey[%d] lLineNo[%d] lKeySignature[%x]===========\n", lKey,  lLineNo, lKeySignature);
//	v_score.push_back(buf);

//	sprintf(pDoc->score_char[pDoc->score_cnt++], "_NOT:|%ld|%ld|%ld|%d|%d|", pNoteInfo->m_lNoteOnTime, pNoteInfo->m_lNoteOffTime, lLineNo, score_ziffer, ypos);

// 수평보조선그림     _♪_
//                 --------
//                 --------
//printf("lLineNo :%d \n",lLineNo );
	if (lLineNo >= 47)
	{
		for (i = 47; i <= lLineNo; i += 2) {
				y = TrackIndexLineNotoY (0, i, index);
				DrawLegLine (x, y, ypos);
			}
	}
	else if (lLineNo <= 35) {
			for (i = 35; i >= lLineNo; i -= 2) {
				y = TrackIndexLineNotoY (0, i, index);
				DrawLegLine (x, y, ypos);
			}
	}

	y = TrackIndexLineNotoY (lTrackIndex, lLineNo, index);
	//---------------------- �ӽ�ǥ
	int drawSF_flag=0;
	for(i=0;i<s_NaturalCnt;i++)
	{
		if(s_Natural[i].m_time==pNoteInfo->m_lNoteOnTime)
		{
			drawSF_flag=1;
			break;
		}
	}
	if(drawSF_flag)
	{
		switch (s_Natural[i].sf_flag)
		{
			case 0:
				break;
			case 2:
			 		DrawDoubleFlat(x , y, 1, ypos);
			 		PutZiffer(6, lZifferLineNo, -2, x, 0);
				break;
			case 3:
					DrawFlat (x , y, 1, ypos);
					PutZiffer(6, lZifferLineNo, -1, x, 0);
				break;
			case 4:
					DrawNatural (x, y, ypos);
					PutZiffer(6, lZifferLineNo, 0, x, 0);
				break;
			case 5:
					DrawSharp (x , y, 1, ypos);
					PutZiffer(6, lZifferLineNo, 1, x, 0);
				break;
			case 6:
					DrawDoubleSharp(x , y, 1, ypos);
					PutZiffer(6, lZifferLineNo, 2, x, 0);
			break;
		}
	}
//-------------------------------------------
	long lFlags2 = 0;
	if (lDuration >= lTimeResolution * 2) {
		lFlags2 |= 0x00010000; // ��밡��
	}
//	if (lDuration < lTimeResolution * lnn * 4 / ldd) {
	if (lDuration < lTimeResolution * 4 ) {
		lFlags2 |= 0x00100000; // ����������
	}
//	lFlags2=0x00xy_0000; x:������ y:0->�� 1->��
//////////////////////////////////////////////////
	//noteStruct.head=lFlags2;

	// �����
	if (lDuration == lTimeResolution * 3 ||	lDuration == lTimeResolution * 3 / 2 ||	lDuration == lTimeResolution * 3 / 4 ||	lDuration == lTimeResolution * 3 / 8)
	{
		DrawDot (x, y, ypos);
		PutZiffer(3, lZifferLineNo, 0, x, 0);
	}

	// ��Ÿ��
	long x2 = 0;
	long kindOf_tie=pNoteInfo->m_lFlags & 0x0000000F;
	long Ov41 = lLineNo >= 41 ? 1 : 0;
//	long lPreWidth = 0;
//	long lPostWidth = 0;
//���ἱ�׸���
	switch (pNoteInfo->m_lFlags & 0x0000000F)
	{
	case 1: // ♪_
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 2: // _♪
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 3: // _♪_
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 4: // ♪_|
		x2 = MeasuretoX (pNoteInfo->m_lNoteOffMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 5: // |_♪
		x2 = MeasuretoX (pNoteInfo->m_lNoteOnMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 6: // _♪_|
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		x2 = MeasuretoX (pNoteInfo->m_lNoteOffMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 7: // |_♪_
		x2 = MeasuretoX (pNoteInfo->m_lNoteOnMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		x2 = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - 15);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	case 8: //|_전체_|
		x2 = MeasuretoX (pNoteInfo->m_lNoteOnMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		x2 = MeasuretoX (pNoteInfo->m_lNoteOffMeasure);
		DrawTieHalf_table(x, x2, y, Ov41);
		break;
	}


	xx2=x2;
//////////////////////////////////////////////////////
	//��� �׸���(20110905����)
	long lNoteCnt = 0;
	long lPoleKey = lKey ;
	long hasPole=0;
	MusicalScoreNoteGroupInfo* pNoteGroupInfo = (MusicalScoreNoteGroupInfo*)(pNoteInfo->m_pNoteGroupInfo);
	if (pNoteGroupInfo)  //������ �ִ� ���
	{
		lPoleKey = pNoteGroupInfo->m_lMaxKey+ m_lTrackKeyPlus;
		lNoteCnt = pNoteGroupInfo->m_lNumNoteInfo;
		//noteStruct.poleKey=lPoleKey;
		//noteStruct.lnoteCnt=lNoteCnt;
	}
	long lLineNo2=0;
	//hdkim+ //updown
	BOOL bIsUp = TRUE;
	if (lNoteCnt == 0)
	{
		lLineNo2 = KeytoLineNo(lPoleKey, lKeySignature);
		if (lLineNo2 >= 41)	bIsUp = FALSE;
		else 		bIsUp = TRUE;
		//noteStruct.isUp=bIsUp;
	}
	else
	{
		long lUpCnt = 0, lDownCnt = 0;
		for (long i = 0; i < lNoteCnt; i++)
		{
			if (KeytoLineNo(pNoteGroupInfo->m_arrKeys[i] + m_lTrackKeyPlus, lKeySignature) >= 41)

				lDownCnt++;
			else
				lUpCnt++;
		}

		if (lDownCnt >= lUpCnt)
		{
			bIsUp = FALSE;
			lPoleKey = pNoteGroupInfo->m_lMinKey + m_lTrackKeyPlus;
			lLineNo2 = KeytoLineNo(lPoleKey, lKeySignature);
		}
		else
		{
			bIsUp = TRUE;
			lLineNo2 = KeytoLineNo(lPoleKey, lKeySignature);
		}
		//noteStruct.isGUp=bIsUp;
		//printf("UpCnt: %d, DownCnt: %d\n", lUpCnt, lDownCnt);
	}
	//-

///////////	lLineNo2 = KeytoLineNo (lPoleKey, lKeySignature);
	long y2 = TrackIndexLineNotoY (lTrackIndex, lLineNo2, index);
/*
	if (lDuration < lTimeResolution * 4)
	{

		if (bIsUp == TRUE)
		{
		}
		else
		{
		}
	}
*/


/////////////////////////////////
	//���� �׸���
	int tailCnt=0;
	if (lDuration < lTimeResolution && lDuration != lTimeResolution * 2 / 3)
	{
		//�׷쳻 ��ǥ�� �ð��� ������ ���
		if (pNoteGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime == pNoteGroupInfo->m_pLastNoteInfo->m_lNoteOnTime)
		{
			//8����ǥ����8����ǥ�����մ�8����ǥ��16����ǥ����16����ǥ�����մ�16����ǥ��32����ǥ�����մ�32����ǥ
			if (lDuration == lTimeResolution / 2 ||
				lDuration == lTimeResolution * 3 / 4 ||
				lDuration == lTimeResolution / 3 ||
				lDuration == lTimeResolution / 4 ||
				lDuration == lTimeResolution * 3 / 8 ||
				lDuration == lTimeResolution / 6 ||
				lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12) //����
			{
				tailCnt++;

			}
			//16����ǥ����16����ǥ�����մ�16����ǥ��32����ǥ�����մ�32����ǥ
			if (lDuration == lTimeResolution / 4 ||
				lDuration == lTimeResolution * 3 / 8 ||
				lDuration == lTimeResolution / 6 ||
				lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12) {
				tailCnt++;
			}
			// 32����ݬ��3֧32����ݬ
			if (lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12) {
				tailCnt++;
			}
			//noteStruct.singleFlagCnt=tailCnt;
		}
		//�׷쳻 ��ǥ�� ������ �ð��� �����ִ� ���
		else
		{
			long x1[3]; //���� ���� ���� ��ǥ[1 ��, 2 ��, 3 ��]
			long x2[3]; //���� ���� ������ ��ǥ[1 ��, 2 ��, 3 ��]

			//�׷쳻�� ù ��° �ð��� ��ǥ �� ���
			if (pNoteGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime == pNoteInfo->m_lNoteOnTime) {
				x1[0] = x1[1] = x1[2] = x;
				//x2[0] = x2[1] = x2[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - lTimeResolution / 8);
				x2[0] = x2[1] = x2[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - 0);

			}
			//�׷��� ������ �ð��� ��ǥ �� ���
			else if (pNoteGroupInfo->m_pLastNoteInfo->m_lNoteOnTime == pNoteInfo->m_lNoteOnTime) {
				//x1[0] = x1[1] = x1[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - lTimeResolution / 8);
				x1[0] = x1[1] = x1[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - 0);

				x2[0] = x2[1] = x2[2] = x;
			}
			//�׷쳻�� �߰� �ð��� ��ǥ �� ���
			else {
				//x1[0] = x1[1] = x1[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - lTimeResolution / 8);
				//x2[0] = x2[1] = x2[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - lTimeResolution / 8);
				x1[0] = x1[1] = x1[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime - 0);
				x2[0] = x2[1] = x2[2] = TimetoX (pMIDIData, pNoteInfo->m_lNoteOffTime - 0);

				//���� �ð��� ��ǥ�� ���
				MusicalScoreNoteInfo* pPrevNoteInfo = pNoteInfo;
				long lPrevDuration = 0;
				while (pPrevNoteInfo) {
					if (pPrevNoteInfo->m_lNoteOnTime < pNoteInfo->m_lNoteOnTime) {
						break;
					}
					pPrevNoteInfo = pPrevNoteInfo->m_pPrevNoteInfo;
				}
				if (pPrevNoteInfo && pPrevNoteInfo->m_pNoteGroupInfo == pNoteGroupInfo) {
					lPrevDuration = pPrevNoteInfo->m_lNoteOffTime - pPrevNoteInfo->m_lNoteOnTime;
				}
				//���� �ð��� ��ǥ�� ���
				MusicalScoreNoteInfo* pNextNoteInfo = pNoteInfo;
				long lNextDuration = 0;
				while (pNextNoteInfo) {
					if (pNextNoteInfo->m_lNoteOnTime > pNoteInfo->m_lNoteOnTime) {
						break;
					}
					pNextNoteInfo = pNextNoteInfo->m_pNextNoteInfo;
				}
				if (pNextNoteInfo && pNextNoteInfo->m_pNoteGroupInfo == pNoteGroupInfo) {
					lNextDuration = pNextNoteInfo->m_lNoteOffTime - pNextNoteInfo->m_lNoteOnTime;
				}
				//1 ��°�� ���� ���� ���
				if (lDuration == lTimeResolution / 2 ||
					lDuration == lTimeResolution * 3 / 4 ||
					lDuration == lTimeResolution / 3 ||
					lDuration == lTimeResolution / 4 ||
					lDuration == lTimeResolution * 3 / 8 ||
					lDuration == lTimeResolution / 6 ||
					lDuration == lTimeResolution / 8 ||
					lDuration == lTimeResolution / 12) {
					if (lNextDuration == 0) {
						x2[0] = x;
					}
					else if (lPrevDuration == 0) {
						x1[0] = x;
					}
				}
				//2 ��°�� ���� ���� ���
				if (lDuration == lTimeResolution / 4 ||
					lDuration == lTimeResolution * 3 / 8 ||
					lDuration == lTimeResolution / 6 ||
					lDuration == lTimeResolution / 8 ||
					lDuration == lTimeResolution / 12) {
					if (lNextDuration >= lDuration * 2 || lNextDuration == 0) {
						x2[1] = x;
					}
					else if (lPrevDuration >= lDuration * 2 || lPrevDuration == 0) {
						x1[1] = x;
					}
				}
				//3 ��°�� ���� ���� ���
				if (lDuration == lTimeResolution / 8 ||
					lDuration == lTimeResolution / 12) {
					if (lNextDuration >= lDuration * 2 || lNextDuration == 0) {
						x2[2] = x;
					}
					else if (lPrevDuration >= lDuration * 2 || lPrevDuration == 0) {
						x1[2] = x;
					}
				}
			}
			//���� ���� 1��
			// 8����ǥ����8����ǥ�����մ�8����ǥ��16����ǥ����16����ǥ�����մ�16����ǥ��32����ǥ�����մ�32����ǥ
			if (lDuration == lTimeResolution / 2 ||
				lDuration == lTimeResolution * 3 / 4 ||
				lDuration == lTimeResolution / 3 ||
				lDuration == lTimeResolution / 4 ||
				lDuration == lTimeResolution * 3 / 8 ||
				lDuration == lTimeResolution / 6 ||
				lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12)
				{
					PutZiffer(1, lZifferLineNo, 0, x1[0], x2[0]);
					//Zinfo[ZinfoCnt].kind=1;
					//Zinfo[ZinfoCnt].level=0;
					//Zinfo[ZinfoCnt].xA=x1[0];
					//Zinfo[ZinfoCnt].xB=x2[0];
					//ZinfoCnt++;
					if (bIsUp){
						DrawChainedFlag (x1[0], x2[0], y, y2 - ry * poleLenGap, rx, ry, bIsUp, ypos);
					}
					else
					{
						DrawChainedFlag (x1[0], x2[0], y, y2 + ry * (poleLenGap-1), rx, ry, bIsUp, ypos);
					}

			}
			//���� ���� 2�� �׸���
			// 16����ǥ����16����ǥ�����մ�16����ǥ��32����ǥ�����մ�32����ǥ
			if (lDuration == lTimeResolution / 4 ||
				lDuration == lTimeResolution * 3 / 8 ||
				lDuration == lTimeResolution / 6 ||
				lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12)
			{
				PutZiffer(1, lZifferLineNo, 1, x1[1], x2[1]);
				//Zinfo[ZinfoCnt].kind=1;
				//Zinfo[ZinfoCnt].level=1;
				//Zinfo[ZinfoCnt].xA=x1[1];
				//Zinfo[ZinfoCnt].xB=x2[1];
				//ZinfoCnt++;

					if (bIsUp)
					{
						DrawChainedFlag (x1[1], x2[1], y, y2 - ry * (poleLenGap-2), rx, ry, bIsUp, ypos);
						//DrawChainedFlag(pDC, x1[1], x2[1], y, y2 - ry * 5, rx, ry, lFlags2);
					}
					else
					{
						//DrawChainedFlag (x1[1] - rx * 2, x2[1] - rx * 2, y, y2 + ry * (poleLenGap-3), rx, ry, bIsUp, ypos);
						DrawChainedFlag (x1[1], x2[1], y, y2 + ry * (poleLenGap-3), rx, ry, bIsUp, ypos);
						//DrawChainedFlag(pDC, x1[1] - rx * 2, x2[1] - rx * 2, y, y2 + ry * 4, rx, ry, lFlags2);
					}
			}
			//���� ���� 3�� �׸���
			//32����ǥ�����մ�32����ǥ
			if (lDuration == lTimeResolution / 8 ||
				lDuration == lTimeResolution / 12)
			{
				PutZiffer(1, lZifferLineNo, 2, x1[2], x2[2]);
				//Zinfo[ZinfoCnt].kind=1;
				//Zinfo[ZinfoCnt].level=2;
				//Zinfo[ZinfoCnt].xA=x1[2];
				//Zinfo[ZinfoCnt].xB=x2[2];
				//ZinfoCnt++;
				if (bIsUp)
				{
					DrawChainedFlag (x1[2], x2[2], y, y2 - ry * (poleLenGap-4), rx, ry, bIsUp, ypos);
				}
				else
				{
					DrawChainedFlag (x1[2] , x2[2] , y, y2 + ry * (poleLenGap-5), rx, ry, bIsUp, ypos);
				}
			}
		}

	}

	//���մ� ��ȣ �׸���(20110905�߰�)
	if (pNoteInfo->m_pTripletGroupInfo)
	{
		MusicalScoreTripletGroupInfo* pTripletGroupInfo = (MusicalScoreTripletGroupInfo*)(pNoteInfo->m_pTripletGroupInfo);
		if (pTripletGroupInfo->m_pFirstNoteInfo == pNoteInfo)
		{

			long x1 = TimetoX (pMIDIData, pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime);
			long x2 = TimetoX (pMIDIData, pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOnTime);
			long lMinLineNo = KeytoLineNo (pTripletGroupInfo->m_lMinKey, lKeySignature);
			long y = TrackIndexLineNotoY (lTrackIndex, lMinLineNo, index);
			long n12 = 12 * lTimeResolution / pTripletGroupInfo->m_lMinDur; // 12 * 3 or 12 * 6 or 12 * 12
			long lSpan = pTripletGroupInfo->m_lEndTime - pTripletGroupInfo->m_lBeginTime;
			if (lSpan >= 1)
			{
				if (12 * lTimeResolution / lSpan >= 1) {
					n12 /= (12 * lTimeResolution / lSpan);
				}
				else {
					n12 /= 12;
				}
			}
			else {
				n12 /= 12;
			}
			if (bIsUp)
			{
					DrawTripletSign(x1, x2, y + 4 * ry, y + 3 * ry, n12, ypos, index);
			}
			else
			{
					DrawTripletSign(x1, x2, y - 4 * ry, y - 3 * ry, n12, ypos, index);
			}

		}
	}

///////////////////
	int IsPole=0;
	int yy2=0;
	IsPole = DrawNoteWtail(x, y, lFlags2, bIsUp, ypos, tailCnt);//�۴�� �ִ� ��밡�� �Ǵ� ��밡��
	if(IsPole==0)//�۴�� ���� ��밡�� �Ǵ� ��밡��
	{
		//need draw pole;
		if (bIsUp == TRUE) yy2=y2 - ry * (poleLenGap-0);
		else 		yy2=y2 + ry * (poleLenGap-0);
		if(lFlags2 & 0x00100000 )	{
			DrawPole (x, y, yy2, bIsUp, ypos);
			DrawTadpole (x, y, lFlags2, ypos);
		}
	}
//	TRACE("#@# lKey[%d] lLineNo[%d]----x[%d] y[%d]\n",lKey,lLineNo,x,y);
	PutZiffer(0, lZifferLineNo, 0, x, 0);
///////////////////
	if(Ziffersystem)
	{
		DrawZiffer( ypos );
	}

return;
}

long draw_score::Get_ZifferColor(ZIFFER_IFO lZinfo)
{
	long lcolor;
	int znum;
	znum=lZinfo.num - pDoc->PLY_STATUS.RefZifferNo;
	if(znum<0) lcolor=COLOR_ZFL;
	else if(znum>6) lcolor=COLOR_ZFH;
	else lcolor=COLOR_ZFN;
	return lcolor;
}

int draw_score::GetZifferNumber (int lineNunber)
{
	int zn;
	int refZIF = pDoc->PLY_STATUS.RefZifferNo;
	zn= lineNunber - refZIF;
	if(zn<0) {
		zn=abs(zn)%7;
		zn=7-zn;
	}
	else if(zn>6) {
		zn=zn%7;
	}
	zn=(abs(zn)%7)+1;
	return zn;
}

 int draw_score::GetZiffeHighLow (int lineNunber)
{
	int zhilow;
	int zn;
	int refZIF = pDoc->PLY_STATUS.RefZifferNo;

	zn= lineNunber - refZIF;
	if(zn<0) //low
	{
		zhilow=-1;
	}
	else if(zn>6) //high
	{
		zhilow=1;
	}
	else 	zhilow=0;
	//TRACE("#@### GetZiffeHighLow lineNunber[%d] refZIF[%d] zhilow[%d]\n",lineNunber, refZIF,zhilow);
	return zhilow;
}

void draw_score::DrawZiffer (int ypos)
{
		int i,ix;
		int ZifNum;
		int Xo, X, Y, Yz, y, yzif, Yzif, hight;
		int zhilow=0;
		char str[8];
		char _str[8];

		int x1,x2, xv0,xv1,Xlength,Xref;
		long lcolor;
		int refZIF = pDoc->PLY_STATUS.RefZifferNo;
		y=TrackIndexLineNotoY(0, 36, 0);
		yzif=TrackIndexLineNotoY(0, MUSICSCORE_LINE_LYRIC_NO, 0);
		int Yref=MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) ;
		Y= Yref + y ;
		Yzif=Yref+yzif;
		for(i=0;i<ZinfoCnt;i++){
//			TRACE("#@# ZunderLine.level[%d] xA[%d] xB[%d] ZinfoCnt[%d]\n",Zinfo[i].level , Zinfo[i].xA, Zinfo[i].xB, ZinfoCnt);
			x1=Zinfo[i].xA;
			Xo=((abs(x1-X00)*MUSICSCORE_VIEW)/(X01-X00)) + MUSICSCORE_BEAT0_POS;

			switch(Zinfo[i].kind)
			{
				case 0:
					ZifNum=GetZifferNumber(Zinfo[i].num);
					zhilow=  GetZiffeHighLow(Zinfo[i].num);
					//sprintf(str,"%d",ZifNum+1);
					sprintf(str,"%d",ZifNum);
					X =Xo -4;
					lcolor=Get_ZifferColor(Zinfo[i]);
					
					
					switch(zhilow){
						case -1://bottom
							_Draw_UTF_Text("M48", X+6, Yzif+24, "k", lcolor);
							break;
						case 1://up
							_Draw_UTF_Text("M48", X+6, Yzif -10 , "k", lcolor);
							break;
					}
					_Draw_UTF_Text("M48", X, Yzif, str, lcolor);
					break;
				case 1:
					x2=Zinfo[i].xB;
					xv0= Xo;
					xv1= (((x2-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;
					Xlength=abs(xv0-xv1)+6;
					Xref =min(xv0,xv1);
					X=Xref -2;
					Yz= Yzif +(Zinfo[i].level *4);
					lcolor=Get_ZifferColor(Zinfo[i]);
					//TRACE("#@# 000000000Get_ZifferColor [0x%08x]\n", lcolor);
					if(X<MUSICSCORE_BEAT0_POS) X=MUSICSCORE_BEAT0_POS-2;
					if(Xlength)	Draw_HLine(X-4, Yz+22, Xlength+4+8, 2, lcolor);
					break;
				case 2:
					xv0=Xo;
					X=xv0-10;
					Xlength=20;
					lcolor=Get_ZifferColor(Zinfo[i]);
					for(ix=0;ix<Zinfo[i].xB;ix++){
						Yz= Yzif+(ix*4);
						Draw_HLine(X-4, Yz+22, Xlength+4+8, 2, lcolor);
					}
					break;
				case 3://�ݹ�����
					X=Xo +10;
					lcolor=Get_ZifferColor(Zinfo[i]);
					_Draw_UTF_Text("M42", X+5, Yzif+15, "k", lcolor);
					break;
				case 4:
					X=Xo +30;
					switch(Zinfo[i].xB)
					{
						case 1:
							sprintf(str,"_");
							break;
						case 3:
							sprintf(str,"_ _ _");
							break;
						default:
							sprintf(str,"");
							break;
					}
					lcolor=Get_ZifferColor(Zinfo[i]);
					//Draw_UTF_Text(pFONT_MUSICAL_42, X, Y-18, str, lcolor);
					_Draw_UTF_Text("M42", X, Yzif+6, str, lcolor);
					break;
				case 6:
				//PutZiffer(6, lZifferLineNo, 2, x, 0);
					lcolor=Get_ZifferColor(Zinfo[i]);
					//TRACE("#@# Ziffer Nature [%d]---[%d][%d][%d]\n", Zinfo[i].kind,  Zinfo[i].level, Zinfo[i].xA, Zinfo[i].xB);
					X= Xo;
					switch(Zinfo[i].level)
					{
						case -2:
							str[0]=186; str[1]=0;//DrawDoubleFlat
							_str[0]=0xc2; _str[1]=0xba; _str[2]=0;//DrawDoubleFlat
							//Draw_UTF_Text(pFONT_MUSICAL_40, X-6, Y, str, lcolor);
							_Draw_UTF_Text("M42", X-14, Yzif+10, _str, lcolor);
							break;
						case -1:
							//Draw_UTF_Text(pFONT_MUSICAL_30, X-12, Y, "b", lcolor);
							_Draw_UTF_Text("M42", X-14, Yzif+10, "b", lcolor);
							break;
						case 0://nature
							//Draw_UTF_Text(pFONT_MUSICAL_30, X-12, Y, "n", lcolor);
							_Draw_UTF_Text("M42", X-14, Yzif+10, "n", lcolor);
							break;
						case 1:
							//Draw_UTF_Text(pFONT_MUSICAL_30, X-12, Y, "#", lcolor);
							_Draw_UTF_Text("M30", X-14, Yzif+10, "#", lcolor);
							break;
						case 2:
							str[0]=220; str[1]=0;//DrawDoubleSharp
							_str[0]=0xc3; _str[1]=0x9c;_str[2]=0;
							//Draw_UTF_Text(pFONT_MUSICAL_40, X-6, Y, str, lcolor);
							_Draw_UTF_Text("M40", X-14, Yzif+10, _str, lcolor);
							break;

					}
					break;
			}
		}
		ZinfoCnt=0;
}

void draw_score::DrawStaff(MIDIData* pMIDIData, long ltimeA, long ltimeB, int ypos, int index)
{
	//TRACE("#@### DrawStaff(1)=================[%d]==========================\n",ypos);
	MIDITrack* pMIDITrack = NULL;
	MIDIEvent* pMIDIEvent = NULL;

	long x, y, lTime;

	MusicalScoreMeasureInfo* pMeasureInfo = pMusic_score->pmsmi;
	MusicalScoreTrackInfo* pTrackInfo = pMusic_score->Score[index].pmsti;
	MusicalScoreNoteInfo* pNoteInfo;

	long lVisibleLeftTime = ltimeA;
	long lVisibleRightTime = ltimeB;
	long lLeftMeasure, lLeftBeat, lLeftTick;
	long lRightMeasure, lRightBeat, lRightTick;
	long ltimeMeasure, ltimeBeat, ltimeTick;
	long lnn, ldd, lcc, lbb;
	long lUnitTick;

	long lTrackZoom = 1;// pMusicalScoreFrame->GetTrackZoom ();
	long rx = 4;
	long ry = MUSICSCORE_YGAP;
	long i, j;
//	int putNoteCnt=0;
	long lTrackTop = pTrackInfo->m_lTop;
	long lTrackHeight = pTrackInfo->m_lHeight;
	long lTrackFlags = pTrackInfo->m_lFlags;
	long yc = (lTrackTop + lTrackHeight / 2) * lTrackZoom;
	long ii;
	long measureLine[20]={0,};
	long measureNum[20]={0,};
	long OLine[20]={0,};
	int OLineCnt=0;
    int measureCnt;
    char pData[256];
    string str;
//KeyAdj-----------------------------
	m_lTrackKeyPlus = (signed char)pDoc->PLY_STATUS.lCurKeyPlus;
	m_lTrackchordPlus=m_lTrackKeyPlus;
#if 0	
	if(pDoc->PLY_STATUS.ChorusPart==1) m_lTrackKeyPlus=m_lTrackKeyPlus-3;
	else if(pDoc->PLY_STATUS.ChorusPart==2) m_lTrackKeyPlus=m_lTrackKeyPlus+2;
#endif
    memset(pData,0,sizeof(pData));
    sprintf(pData, "_TIF:|%ld|%ld|%d|%d|%d|", ltimeA, ltimeB+1, 0, 0, ypos);
    str=(char*)pData;
    pDoc->vStrScore.push_back(str);

	//TRACE("#@# ++++++++++++m_lTrackchordPlus[%d]\n",m_lTrackchordPlus);
	//sprintf(pDoc->score_char[pDoc->score_cnt++], "_TIF:|%ld|%ld|%d|%d|%d|", ltimeA, ltimeB+1, 0, 0, ypos);

/////////////////////////////////////////////////////////////////////////////////
//���ν��� ����/���� ���ڸ��׸���
///////////////////////////////////////////////////////////////////////////////////
//�����߱�
	for (ii = -2; ii <= 2; ii++)
	{
		y = yc + ii * 2 * ry;
		OLine[OLineCnt++]=y;
	}

	for(i=0;i<OLineCnt;i++)
	{
//		TRACE("++++++++++++DrawFiveLine[%d][%d]\n",i,ypos);
		DrawFiveLine(OLine[i], ypos);
	}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Measure���߱�
///////////////////////////////////////////////////////////////////////////////////////////////////

	long lTimeMode, lTimeResolution;
	MIDIData_GetTimeBase (pMIDIData, &lTimeMode, &lTimeResolution);
	if (lTimeMode != MIDIDATA_TPQNBASE) return;

	MIDIData_BreakTime (pMIDIData, lVisibleLeftTime, &lLeftMeasure, &lLeftBeat, &lLeftTick);
	MIDIData_BreakTime (pMIDIData, lVisibleRightTime, &lRightMeasure, &lRightBeat, &lRightTick);
//last position
	pMeasureInfo = pMusic_score->pmsmi + lRightMeasure;
	X01 = (pMeasureInfo->m_lLeft +  pMeasureInfo->m_lSignatureWidth +  pMeasureInfo->m_lPreWidth +  pMeasureInfo->m_lWidth )*pMusic_score->m_TZoom;
#ifdef DBG_MUSICSCORE
///////////////////////////////
//	TRACE("#@# lVisibleLeftTime[%d] lVisibleRightTime[%d]\n",lVisibleLeftTime, lVisibleRightTime);
//	TRACE("#@# lLeftMeasure[%d] lLeftBeat[%d] lLeftTick[%d] \n",lLeftMeasure, lLeftBeat, lLeftTick);
//	TRACE("#@# lRightMeasure[%d] lRightBeat[%d] lRightTick[%d] X01[%d]\n",lRightMeasure, lRightBeat, lRightTick, X01);
//--------------------
#endif
	int ix=0;
	int bitCnt=0;
	for (i = lLeftMeasure; i <= lRightMeasure; i++)
	{
		// ����
		MIDIData_MakeTimeEx (pMIDIData, i, 0, 0, &lTime, &lnn, &ldd, &lcc, &lbb);
		x = MeasuretoX (i);
		//TRACE("#@# Measure line [%d]/[%d] -- x[%d] lTime[%d]\n", i, lRightMeasure, x, lTime);
		measureNum[ix]=i;
		curMeasureLtime[ix] = lTime;
		measureLine[ix++]=x;
		/////////////////////////////////////////
		// ����(Beat)��
		////////////////////////////////////////
		lUnitTick = lTimeResolution * 4 / (1 << ldd);
		long llbeatTime;
		for (long j = 0; j < lnn; j++)
		{
			llbeatTime= lTime + j * lUnitTick;
			lbeatTime[ypos][bitCnt++]=llbeatTime;//10tick ������ ǥ��
			if(i==lLeftMeasure && j==0){
				X00 = TimetoX (pMIDIData, llbeatTime);
			}
			//TRACE("#@# --------Draw MADI--ypos[%d] madi[%d] [%d] ---beatTime[%d]------------\n",ypos, i, j , llbeatTime);
		}
			//------------------------------------
	}
	lbeatCnt[ypos]=bitCnt;

	measureCnt=ix;
	//TRACE("#@# measureCnt:%d\n",measureCnt);
	measureNum[ix]=i;
	curMeasureLtime[ix] = lVisibleRightTime;//last measure ltime
	measureLine[ix++]=X01;//last measure position
	for(i=0;i<ix;i++)
	{
		if(i==0) measureLine[0]=0;
		//TRACE("#@#[%d]------------ measureLine[%d] ypos[%d] index[%d]\n",i, measureLine[i], ypos, index );
		curMeasureX[i] = DrawMeasureLine(measureNum[i], measureLine[i], OLine[0], OLine[OLineCnt - 1], ypos, index);
	}
	curMeasureX[0] = MUSICSCORE_BEAT0_POS;
//------------------------------------------

//�Ǻ� �Ѷ����� �������� ����
	if(index==0)
	{
		LYRICS l_lyric;
		vector<int> vlyricPos;
		vector<MEASURE_LYRIC> vMeasureLyric;
		vlyricPos.clear();
		vMeasureLyric.clear();
		int w_str, h_str;
		int meas_length;

		for ( i = 0; i < pMusic_score->lxLyricCnt; i++)
		{
			lTime = pMusic_score->vLyric[i].ltime;
			if (lVisibleLeftTime <= lTime && lTime  < lVisibleRightTime)
			{
				vlyricPos.push_back(i);
			}
			if(lTime> lVisibleRightTime)break;//더이상 볼거없이 끝내자!!!
		}
		curLineLyricCnt = vlyricPos.size();
//------------------------------------------
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS(1)================DRAW curLineLyricCnt[%d] measureCnt[%d]==========\n", curLineLyricCnt, measureCnt);
#endif
			MEASURE_LYRIC measureLyric;
		int ik=0;
		for(i=0;i<measureCnt;i++)
		{
			measureLyric.y = (Ziffersystem) ?
							 TrackIndexLineNotoY(0, MUSICSCORE_LINE_ZIFFER_LYRIC_NO, index):
							 TrackIndexLineNotoY(0, MUSICSCORE_LINE_LYRIC_NO, index);

			ik=0;
			for (int k = 0; k<curLineLyricCnt; k++)
			{
				l_lyric=pMusic_score->vLyric[vlyricPos[k]];
				lTime=l_lyric.ltime;

				if (curMeasureLtime[i] <= lTime && lTime  < curMeasureLtime[i + 1])
				{
					memset(pData,0,sizeof(pData));
					strcpy(pData,l_lyric.str.c_str());
					measureLyric.ltime[ik] = lTime;
					measureLyric.str[ik]=l_lyric.str;
					measureLyric.x[ik] = TimetoX (pMIDIData, lTime);	//llyric->lTime;
					memcpy(measureLyric.strU8[ik],pData, 64);
					ik++;
				}
			}
			measureLyric.LyricNumOfMeasure=ik;
			vMeasureLyric.push_back(measureLyric);
		}


//-----------------------------
//각마디의 DrawLyric
		for(i=0;i<measureCnt;i++)
		{
			DrawLyric(vMeasureLyric[i], curMeasureX[i], curMeasureX[i + 1], ypos);
		}
//-----------------------------------------
		vlyricPos.clear(); vector<int>().swap(vlyricPos);
		vMeasureLyric.clear(); vector<MEASURE_LYRIC>().swap(vMeasureLyric);

	}
#ifdef DBG_MUSICSCORE
    TRACE("#@#DBG_MUS(2)================DRAW curLineLyricCnt[%d] measureCnt[%d]==========\n", curLineLyricCnt, measureCnt);
#endif
	///beat0 lineǥ�� ������
	//printf("================��Ÿ���׸� X00[%d] X01[%d]\n",X00, X01);
////////////////////////////////////////////////////////////////////////////////
	///ppp+
	MIDIData_BreakTime (pMIDIData, ltimeA, &ltimeMeasure, &ltimeBeat, &ltimeTick);
	if(lLeftMeasure==1)	DrawTimeAndKeySignature (pMIDIData, ltimeA, -1, ypos, index);
	else DrawTimeAndKeySignature (pMIDIData, ltimeA, 0, ypos, index);
	///ppp-

	long jMax = pMusic_score->putNoteCnt;
#ifdef DBG_MUSICSCORE
	TRACE("#@#DBG_MUS================DRAW Note[%d]=================\n", jMax);
#endif
//////////////save �ӽ�ǥ+
	long tKey, tSF;
	long tMesureSF;
	long pPrevNoteOntime=0;
	int naturalCnt;
	int MeasureNoteCnt=0;

	MusicalScoreMeasureInfo* tMeasureInfo=NULL;
	memset(s_Natural,0,sizeof(s_Natural));
	naturalCnt=0;
	vector<NATUARAL> vNatrural;
	vector<NATUARAL> vDNatrural;

	NATUARAL Natural;

	vNatrural.clear();
	vDNatrural.clear();

	for (j = 0; j < jMax; j++)
	{
		//MusicalScoreNoteInfo*
		pNoteInfo = (MusicalScoreNoteInfo*)(pMusic_score->Score[index].pmsni + (j));
		if ((lVisibleLeftTime <= pNoteInfo->m_lNoteOnTime && pNoteInfo->m_lNoteOnTime <= lVisibleRightTime) ||
			//(lVisibleRightTime <= pNoteInfo->m_lNoteOffTime && pNoteInfo->m_lNoteOffTime < lVisibleRightTime) ||
			(lVisibleLeftTime <= pNoteInfo->m_lNoteOffTime && pNoteInfo->m_lNoteOffTime < lVisibleRightTime) ||
			(pNoteInfo->m_lNoteOnTime <= lVisibleLeftTime && lVisibleRightTime < pNoteInfo->m_lNoteOffTime)
				)
		{
			if (pNoteInfo->m_lNoteOrRest == 0)//��ǥ�̸�
			{
				tMeasureInfo = pMusic_score->pmsmi + pNoteInfo->m_lNoteOnMeasure;
				tKey=MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent) + m_lTrackKeyPlus;
				tMesureSF= GetNewSf_Mi(tMeasureInfo->m_lKeySignature, m_lTrackKeyPlus);
				tSF = KeytoSF(tKey, tMesureSF);//?
				//printf("0000#############tMesureSF[%ld] newSF[%ld] tSF[%ld]\n", tMesureSF, newSF, tSF);
				MeasureNoteCnt++;
				if(tSF)
				{
					Natural.m_time= pNoteInfo->m_lNoteOnTime;
					Natural.m_measureNo=pNoteInfo->m_lNoteOnMeasure;
					Natural.linePos = KeytoLineNo (tKey, tMesureSF);
					Natural.sf_flag=tSF;
					vNatrural.push_back(Natural);
					naturalCnt++;
				}//if tSF
			}//m_lNoteOrRest

		}
		else if(pNoteInfo->m_lNoteOffTime > lVisibleRightTime) break;

	}
#ifdef DBG_MUSICSCORE
	//TRACE("#@#DBG_MUS================NATUARAL naturalCnt[%d] vNatrural[%d]=================\n", naturalCnt, vNatrural.size());
#endif

//////////////save �ӽ�ǥ-
#if 1
	NATUARAL sx_Natural[128];
	memset(sx_Natural,0,sizeof(sx_Natural));
	int iz=0,iy=0;
	//uniq�Ѱ� ã��+
	naturalCnt=vNatrural.size();
	for(ix=0;ix<naturalCnt;ix++)
	{
		for( iy=0;iy<iz+1;iy++)
		{
			if((sx_Natural[iy].sf_flag==vNatrural[ix].sf_flag) &&
			   (sx_Natural[iy].linePos==vNatrural[ix].linePos) &&
			   (sx_Natural[iy].m_measureNo==vNatrural[ix].m_measureNo)
					)
			{
				break;
			}
		}
		if(iy>=iz)
		{
			//printf("������ ��ã��\n");
			sx_Natural[iz++]=vNatrural[ix];
		}
	}
	s_NaturalCnt=iz;

	//uniq�Ѱ� ã��-

	//���� �տ��� �O��+
	for(ix=0;ix<s_NaturalCnt;ix++){
		for( iy=0;iy<naturalCnt;iy++)
		{
			if((sx_Natural[ix].sf_flag==s_Natural[iy].sf_flag) && (sx_Natural[ix].linePos==s_Natural[iy].linePos) && (sx_Natural[ix].m_measureNo==s_Natural[iy].m_measureNo))
			{
				if(sx_Natural[ix].m_time >= s_Natural[iy].m_time) sx_Natural[ix].m_time = s_Natural[iy].m_time;
			}
		}
	}
	//���� �տ��� �O��-
	//printf("���� �տ���-UniqCnt[%d] naturalCnt[%d]\n",iz, naturalCnt);
	//for(ix=0;ix<iz;ix++)
	//{
	//	printf("�ӽø�ũ[%d]-����[%d]time[%d]linePos[%d]sf[%d]\n",ix, sx_Natural[ix].m_measureNo, sx_Natural[ix].m_time, sx_Natural[ix].linePos, sx_Natural[ix].sf_flag);
	//}
	memset(&s_Natural,0, sizeof(s_Natural));
//return;
	if(s_NaturalCnt>0)
		memcpy(&s_Natural, &sx_Natural, sizeof(s_Natural));
//////////////save �ӽ�ǥ-
#endif
	vNatrural.clear();
	vector<NATUARAL>().swap(vNatrural);
//TRACE("#@#================DRAW Note FINISH=================\n");
//=================��Ʈ  �׸��� ��====================		
	TieCnt=0; //��Ʈ �׸��� Tie���� �̸� �����.
	memset(&TieBuf,0,  sizeof(TieBuf));
	//MusicalScoreNoteInfo* pNoteInfo ;
	for (j = 0; j < jMax; j++) {
		pNoteInfo = (MusicalScoreNoteInfo*)(pMusic_score->Score[index].pmsni + (j));
		if ((lVisibleLeftTime <= pNoteInfo->m_lNoteOnTime && pNoteInfo->m_lNoteOnTime <= lVisibleRightTime) ||
			(lVisibleLeftTime <= pNoteInfo->m_lNoteOffTime && pNoteInfo->m_lNoteOffTime < lVisibleRightTime) ||
			//(lVisibleRightTime <= pNoteInfo->m_lNoteOffTime && pNoteInfo->m_lNoteOffTime < lVisibleRightTime) ||
			(pNoteInfo->m_lNoteOnTime <= lVisibleLeftTime && lVisibleRightTime < pNoteInfo->m_lNoteOffTime)
				)
		{
			if (pNoteInfo->m_lNoteOrRest == 0)
			{
				DrawNote(pMIDIData, pNoteInfo, ypos, index);///ppp note�׸��� �Լ�
			}
			if (pNoteInfo->m_lNoteOrRest == 1)
			{
				DrawRest(pMIDIData, pNoteInfo, (MusicRest)pNoteInfo->m_lFlags, ypos, index);///ppp note�׸��� �Լ�
			}
		}
		else if(pNoteInfo->m_lNoteOffTime > lVisibleRightTime) break;
	}



	//=================��Ÿ�� �׸���====================
	for(i=0;i<TieCnt;i++)
	{
//		printf("Tiedraw[%d][%d][%d]-[%d][%d]\n",TieBuf[i].a, TieBuf[i].b, TieBuf[i].c, TieBuf[i].y,TieBuf[i].flag);
		if(TieBuf[i].a==-1)// ___/
		{
			DrawTieFull(TieBuf[i].b , TieBuf[i].c, TieBuf[i].y, TieBuf[i].flag, 1, ypos) ;
			if(Ziffersystem)	DrawZiffTieFull(TieBuf[i].b , TieBuf[i].c, 1, ypos, TieBuf[i].Znum ) ;
		}
		else if(TieBuf[i].c==-1)//\___
		{
			DrawTieFull(TieBuf[i].a , TieBuf[i].b, TieBuf[i].y, TieBuf[i].flag, 0, ypos); 
			if(Ziffersystem)	DrawZiffTieFull(TieBuf[i].a , TieBuf[i].b, 0, ypos, TieBuf[i].Znum) ;
		}
		else//\___/
		{
			DrawTieFull(TieBuf[i].a , TieBuf[i].c, TieBuf[i].y, TieBuf[i].flag, 2, ypos) ;
			if(Ziffersystem)	DrawZiffTieFull(TieBuf[i].a , TieBuf[i].c, 2, ypos, TieBuf[i].Znum) ;
		}
	}

	if(index==0)
	{
		//char strCode[64];
		CHORD	lchord;
		int vlxCordCnt= pMusic_score->vChord.size();
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS-------------------vChord.size()[%d]\n",vlxCordCnt);
#endif
		for (i = 0; i < vlxCordCnt; i++)
		{
			lchord=pMusic_score->vChord[i];
			lTime = lchord.lTime;
			if (lVisibleLeftTime <= lTime && lTime  < lVisibleRightTime)
			{
#ifdef DBG_MUSICSCORE
				TRACE("#@#DBG_MUS-----subCodeStr[0][%s]\n",lchord.subCodeStr[0].c_str());
#endif
				//sprintf(strCode,"%d%s/%d%s",lchord->Code[0],lchord->subCodeStr[0],lchord->Code[1],lchord->subCodeStr[1]);
				//DrawChord( pMIDIData, lTime, lchord->Code[0],lchord->subCodeStr[0],lchord->Code[1],lchord->subCodeStr[1],
				//					lchord->FlatSharpTableNo[0], lchord->FlatSharpTableNo[1], ypos, index);
			}
		}
	}

//======================================
#if 1
	//==========  SCORE_BAR =============
	int sz;
    LYRICS l_lyric;
    sz=pMusic_score->vLyric.size();
    for ( i = 0; i < sz; i++)
    {
        l_lyric= pMusic_score->vLyric[i];
        lTime = l_lyric.ltime;
        if (ltimeA <= lTime && lTime  < ltimeB)
        {
            memset(pData,0,sizeof(pData));
            sprintf(pData, "_SLY:|T40|%s|%ld|X|0|", l_lyric.str.c_str(), lTime);
            //TRACE( "#@####lScore_bar %s--pos[%d]\n" , pData, ypos );
            str=(char*)pData;
            pDoc->vStrScore.push_back(str);
        }
    }


	if(index==0)
	{
		SCORE_BAR	lScore_bar;
		long lkey,lZifferKey;
		long lLineNo;
		//int ypos=0;
		int score_ziffer;
		int score_ziffer_hilow;
		string str;
		sz=pMusic_score->vScoreBar.size();
		for (i = 0; i < sz; i++)
		{
			lScore_bar=pMusic_score->vScoreBar[i];
			lTime = lScore_bar.lTime;
			if (lVisibleLeftTime <= lTime && lTime  < lVisibleRightTime)
			{
			   lZifferKey=lScore_bar.key;
			   lkey=lZifferKey+m_lTrackKeyPlus;
			   lZifferLineNo = KeytoLineNo (lZifferKey, lScore_bar.lKeySignature);
			   lLineNo = KeytoLineNo (lkey, lScore_bar.lKeySignature);

			   	score_ziffer=GetZifferNumber(lZifferLineNo)%8;//1~7
				score_ziffer_hilow=GetZiffeHighLow(lZifferLineNo);
				if(score_ziffer_hilow<0) score_ziffer+=10;//11~17
				if(score_ziffer_hilow>0) score_ziffer+=20;//21~27
				//sprintf(pDoc->score_char[pDoc->score_cnt++], "_NOT:|%ld|%ld|%ld|%d|%d|", lScore_bar->lTime, lScore_bar->offTime, lLineNo, score_ziffer, ypos);
				memset(pData,0,sizeof(pData));
				sprintf(pData, "_NOT:|%ld|%ld|%ld|%d|%d|", lScore_bar.lTime, lScore_bar.offTime, lLineNo, score_ziffer, ypos);
				//TRACE( "#@####lScore_bar %s--pos[%d]\n" , pData, ypos );
				str=(char*)pData;
            	pDoc->vStrScore.push_back(str);
			}
		}
	}
#endif

#if 0
//================�丣��Ÿ �׸���===========================
//pFermata
	FERMATA *plFermata;
	for (i = 0; i < pMusic_score->lxFermataCnt; i++)
	{
		plFermata = (FERMATA*)pMusic_score->pFermata + i;
		lTime = plFermata->ltime;
		if (lVisibleLeftTime <= lTime && lTime  < lVisibleRightTime) 
		{
			DrawFermata(pMIDIData, plFermata, ypos, index);
		}
	}
#endif
}

void draw_score::DrawRest(MIDIData* pMIDIData, MusicalScoreNoteInfo* pNoteInfo, MusicRest lFlags, int ypos, int index) 
{
	long x;
	long lLineNo = 41;
	long lTrackIndex=0;
	
	long lTimeMode, lTimeResolution;
	MIDIData_GetTimeBase (pMIDIData, &lTimeMode, &lTimeResolution);

	MusicalScoreTrackInfo* pTrackInfo = pMusic_score->Score[index].pmsti;

	long lMeasure = pNoteInfo->m_lNoteOnMeasure;
	MusicalScoreMeasureInfo* pMeasureInfo = (MusicalScoreMeasureInfo*)(pMusic_score->pmsmi + lMeasure);

	long lTrackFlags = pTrackInfo->m_lFlags; //GetTrackFlags (lTrackIndex);

#if 0
	switch (lTrackFlags)
	{
	case 1:		break;										//�������ڸ�
	case 2:		lLineNo -= 12;	break;    //�������ڸ�
	case 3:		break;										//�뺸ǥ
	}
#endif

//printf("@@@@---DrawRest[%d] [%03d:%02d:%03d] [%d]  lFlags[%d] Trip[%x] lTrackFlags[%d]\n", 
//	pNoteInfo->m_lNoteOnTime, pNoteInfo->m_lNoteOnMeasure+1, pNoteInfo->m_lNoteOnBeat+1,  pNoteInfo->m_lNoteOnTick, 
//	pNoteInfo->m_lRestDura, pNoteInfo->m_lFlags, pNoteInfo->m_pTripletGroupInfo, lTrackFlags);

	if(lFlags==rest_whole)
	{
	 	x = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime + (pMeasureInfo->m_lDuration/2));
	}
	else  x = TimetoX (pMIDIData, pNoteInfo->m_lNoteOnTime);

	long y = TrackIndexLineNotoY (lTrackIndex, lLineNo, index);

	long rx = 4;
	long ry = MUSICSCORE_YGAP;
	long i = 0;
	
	int Xadj,	Yadj;
	int X, Y;
	Xadj=0;
	Yadj=-51;
	Uint16 str[4]={0,};
	char _str[8]={0,};
	
	long xx1, xx2, yy1, yy2, n12, lSpan;
	int TripleRest_Flg=0;

	switch (lFlags)
	{
		case rest_sixtyfourth: 
			Yadj=-51;
			str[0]=0xF0F4;
			_str[0]=0xc3;_str[1]=0xb4;_str[2]=0x00;
			break;
		case rest_thirtysecond: //32
			Yadj=-51;
			//str[0]=0xF0A8;
			str[0]=0;
			_str[0]=0x00;//_str[1]=0xb4;_str[2]=0x00;
			break;
		case rest_dotted_thirtysecond: 
			Yadj=-51;
			//str[0]=0xF0A8;
			str[0]=0;	str[1]='.';
			_str[0]=0;	_str[1]='.';
			break;
		case rest_sixteenth: //16
			Yadj=-68;
			str[0]=0xF0C5;
			_str[0]=0xc3;_str[1]=0x85;_str[2]=0x00;
			break;

		case rest_dotted_sixteenth: 
			Yadj=-51;
			str[0]=0xF0C5;	str[1]='.';
			_str[0]=0xc3;_str[1]=0x85;_str[2]='.'; _str[3]=0;
			break;
		case rest_eighth: //8
			Yadj=-62;
			str[0]=0xF0E4;
			_str[0]=0xc3;_str[1]=0xa4;_str[2]=0;
			break;
		case rest_dotted_eighth:
			Yadj=-62;
			str[0]=0xF0E4;	str[1]='.';
			_str[0]=0xc3;_str[1]=0xa4;_str[2]='.';_str[3]=0;
			break;
		case rest_quarter://
			Yadj=-51;
			str[0]=0xF0CE;
			_str[0]=0xc3;_str[1]=0x8e;_str[2]=0;
			break;
		case rest_dotted_quarter:
			Yadj=-51;
			str[0]=0xF0CE;	str[1]='.';
			_str[0]=0xc3;_str[1]=0x8e;_str[2]='.';_str[3]=0;
			break;
		case rest_half:
			Yadj=-51;
			str[0]=0xF0b7;
			_str[0]=0xc2;_str[1]=0xb7;_str[2]=0;
			break;
		case rest_dotted_half:
			Yadj=-51;
			str[0]=0xF0b7;	str[1]='.';
			_str[0]=0xc2;_str[1]=0xb7;_str[2]='.';_str[3]=0;
			break;
		case rest_whole:
			Yadj=-51;
			str[0]=0xF0b7;
			_str[0]=0xc2;_str[1]=0xb7;_str[2]=0;
			break;
		case rest_dotted_whole:
			Yadj=-51;
			str[0]=0xF0b7;	str[1]='.';
			_str[0]=0xc2;_str[1]=0xb7;_str[2]='.';_str[3]=0;
			break;
		default: break;
	}
//	printf("default0000------------------------DrawRest() lFlags[%d]\n",lFlags);
		
	//��ǥ ���մ� ��ȣ �׸���(20160623�߰�)
	if (pNoteInfo->m_pTripletGroupInfo) 
	{
				//TRACE("default11111------------------------DrawRest() lFlags[%d]\n",lFlags);
				MusicalScoreTripletGroupInfo* pTripletGroupInfo = (MusicalScoreTripletGroupInfo*)(pNoteInfo->m_pTripletGroupInfo);
				if (pTripletGroupInfo->m_pFirstNoteInfo == pNoteInfo) 
				{
					xx1 = TimetoX (pMIDIData, pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime);
					xx2 = TimetoX (pMIDIData, pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOnTime);
					n12 = 12 * lTimeResolution / pTripletGroupInfo->m_lMinDur; // 12 * 3 or 12 * 6 or 12 * 12
					lSpan = pTripletGroupInfo->m_lEndTime - pTripletGroupInfo->m_lBeginTime;
					if (lSpan >= 1) {
						if (12 * lTimeResolution / lSpan >= 1) {
							n12 /= (12 * lTimeResolution / lSpan);
						}
						else {
							n12 /= 12;
						}
					}
					else {
						n12 /= 12;
					}
					//TripleRest_Flg=1;
					if(pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOrRest)
					{
						TripleRest_Flg=1;
						if(	pTripletGroupInfo->m_pFirstNoteInfo->m_lRestDura==80)
						{
							Yadj=-51;
							str[0]=0xF0CE;
							_str[0]=0xc3;_str[1]=0x8e;_str[2]=0;

						}
						else if(pTripletGroupInfo->m_pFirstNoteInfo->m_lRestDura==40)// && pTripletGroupInfo->m_pFirstNoteInfo->m_lRestDura<=40)
						{
							Yadj=-51;
							str[0]=0xF0E4;
							_str[0]=0xc3;_str[1]=0xa4;_str[2]=0;
						}
					}
					//DrawTripletSign(x1, x2,  y + 4 * ry, y + 3 * ry, n12, ypos)
				//TRACE("��ǥ-DrawTripletSign[%d][%d][%d][%d][%d][%d]--[%x]\n",xx1, xx2, y + 4 * ry, y + 3 * ry, rx, ry, n12);
			}
	}	//if 	
			
			
	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS ;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y ;

	//Draw_UNICODE_Text(pFONT_MUSICAL_42, X+Xadj, Y+Yadj, str, LINE_COLOR);	
	_Draw_UTF_Text("M42", X+Xadj, Y, _str, LINE_COLOR);	

	//Draw_HLine(X, Y, 50, 10, 0xff);
	//Draw_HLine(X-Xadj, Y-Yadj, 50, 10, 0xffff);
	if(TripleRest_Flg)
	{
		DrawTripletSign(xx1, xx2,  y + 4 * ry, y + 3 * ry, n12, ypos, index);
	}
			
}

void draw_score::DrawChord (MIDIData* pMIDIData, long lTime, int mcode0, char* scode0, int mcode1, char* scode1, int nn0, int nn1, int ypos, int index) 
{
	int X,Y;
//	int Xadj= 0;
//	int Yadj= +16;
	int yoffset=0;
	int x = TimetoX (pMIDIData, lTime);	
	int y = TrackIndexLineNotoY (0, 51, index);
	int i,j,z,k,kk,len;
	int strDivAB[8][2];
	int strDivLen[8];
	
	char str[64]={0,};
	Uint16  uniStr[64]={0,};
	
	char strDiv[8][64]={0,};
	char str_F[4]={0xe2, 0x99, 0xad, 0};
	char str_S[4]={0xe2, 0x99, 0xaf, 0};
	
	char temp[26]={0,};
	int ichord0, ichord1;
	
 	ichord0 = (mcode0 + m_lTrackchordPlus+12)%12;
 	ichord1 = (mcode1 + m_lTrackchordPlus+12)%12;
 
 	if(newSF>0){ //#�迭
 		if(nn0 == 0)  		  nn0=1;
 		else if(nn0 == 2 )	nn0=3;
 			
  	if(nn1 == 0 )	      nn1=1;
 		else if(nn1 == 2 )	nn1=3;

 	}
	else if(newSF<=0)//b�迭
	{ 
 		if( nn0 == 1)			nn0=0;
 		else if(nn0 == 3)	nn0=2;
 
  	if( nn1 == 1)			nn1=0;
 		else if(nn1 == 3)	nn1=2;
	}
 			
 	if(nn0>=0 && nn0<4) {
		sprintf(temp, "%s", codeTable[nn0][ichord0]);
		if(scode0[0]!=0) sprintf(temp,"%s%s",temp,scode0);
	}

 	if(nn1>=0 && nn1<4) {
		sprintf(temp, "%s/%s", temp, codeTable[nn1][ichord1]);
		if(scode1[0]!=0)sprintf(temp,"%s%s",temp,scode1);
	}
	
	k=0,kk=0;
	for(i=0;i<32;i++)
	{
//		printf("k[%d] kk[%d] temp[%d][%x] \n",k ,kk, i, temp[i]);
		if(temp[i]=='b' || temp[i]=='#')
		{ 
			k++; 
			strDiv[k++][0]=temp[i];
			kk=0;
		}
		else 
			strDiv[k][kk++]=temp[i];
			
		if(temp[i]==0) break;
	}
	k++;
		
//	printf("=================DrawChord k[%d]============= \n",k);
	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS;// + Xadj;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y;// + Yadj;

//	FontRenderer *pFont = NULL;
//	ResourceMgr *pRcMgr = ResourceMgr::GetInstance();
//	pRcMgr->GetFontRenderer(&pFont);
////	pFont->SetHeight(18);
	int w_str, h_str;
	for(i=0;i<k;i++)//������� ���̰��
	{
	 if(strDiv[i][0]==0) continue;
	 for(j=0;j<24;j++)	
	 {
	 		uniStr[j]=strDiv[i][j]; //utf8 ->Uni ���� �����̹Ƿ� �׳� test
	 		if(uniStr[j]==0) break;
	 }
	 if(uniStr[0]=='b' || uniStr[0]=='#')	
	 {
			//strDivLen[i]=GetStringWidth(uniStr, MUSICSCORE_FONT_CHORD1, MUSICSCORE_FONT_CHORD1, 1)-4;
			//TTF_SizeUNICODE(pFONT_TEXT_18, uniStr, &w_str, &h_str);
			for(z=0;z<16;z++){
				if(uniStr[z]==0) break;
			}
			if(z<15)w_str=z*18;
			strDivLen[i]=w_str;
		}
	 	else 
	 	{
	 		//TTF_SizeUNICODE(pFONT_TEXT_18,	uniStr, &w_str, &h_str);
 			for(z=0;z<16;z++){
				if(uniStr[z]==0) break;
			}
			if(z<15)w_str=z*18;
	 		strDivLen[i]= w_str;
	 		//strDivLen[i]= GetStringWidth(uniStr, MUSICSCORE_FONT_CHORD0, MUSICSCORE_FONT_CHORD0, 1)-(j*4);
//	 printf("[%d] DrawChord[%s] len[%d]\n",i,strDiv[i],strDivLen[i]);
		}
	}
	
	for(i=0;i<k;i++)//������� ���۹� ����
	{
		if(i==0) { strDivAB[0][0]=X; strDivAB[0][1]=X + strDivLen[0];}
		else
		{
			strDivAB[i][0]=strDivAB[i-1][1];//-4; //���� ���̱� 
			strDivAB[i][1]=strDivAB[i][0]+ strDivLen[i];
		}
//	 printf("[%d] DrawChord[%s] [%d]-[%d]len[%d]\n",i, strDiv[i], strDivAB[i][0], strDivAB[i][1], strDivLen[i]);
	}
	
//	
	for(i=0;i<k;i++)
	{
	
	 	X=strDivAB[i][0];
	 	memset(str,0,16);
	 	if(strDiv[i][0]==0) continue;
 		switch(strDiv[i][0])
	 	{
	 		case 'b':
				memcpy(str,str_F,4);
				yoffset=-4;
				//Draw_UTF_Text(pFONT_TEXT_18, X, (Y+yoffset), str, COLOR_CHORD);
				_Draw_UTF_Text("T18", X, (Y+yoffset), str, COLOR_CHORD);
 	 			break;
 	 		case '#':	
	 			memcpy(str,str_S,4);
	 			yoffset=-4;
	 			//Draw_UTF_Text(pFONT_TEXT_18, X, (Y+yoffset), str, COLOR_CHORD);
	 			_Draw_UTF_Text("T18", X, (Y+yoffset), str, COLOR_CHORD);
 	 			break;
 	 		default:
	 			memcpy(str,strDiv[i],16);
	 			yoffset=0;
	 			//Draw_UTF_Text(pFONT_TEXT_18, X, (Y+yoffset), str, COLOR_CHORD);
	 			_Draw_UTF_Text("T18", X, (Y+yoffset), str, COLOR_CHORD);
 	 			break;
	 	}
	}

}

void draw_score::DrawFermata (MIDIData* pMIDIData, FERMATA *plFermata, int ypos, int index) 
{
	int X,Y;
	int Xadj;//= -4;
	int Yadj= 0;
	char str[2]={0,};
	int len;
	
	MusicalScoreMeasureInfo* fMeasureInfo=NULL;
	fMeasureInfo = pMusic_score->pmsmi + plFermata->m_measureNo;
	long lLineNo = KeytoLineNo (plFermata->fermatakey+m_lTrackKeyPlus, fMeasureInfo->m_lKeySignature);
	int x = TimetoX (pMIDIData, plFermata->ltime);	
	int y = TrackIndexLineNotoY (0, lLineNo, index);

	//if((plFermata->Cref & 0x0000000F) == 1)
//	str[0]='U'; 
//	Yadj+=0;

	if(lLineNo>=41){ 
		str[0]='U'; 
		Xadj= -4;
		Yadj=-12;
	}
	else {//��
		str[0]='u'; 
		Xadj= -8;
		Yadj= +12;	
	}
		
	X=((abs(x-X00)*MUSICSCORE_VIEW)/(X01-X00))+MUSICSCORE_BEAT0_POS + Xadj;
	Y= MUSICSCORE_YOFFSET + (MUSICSCORE_HIGHT*ypos) + y + Yadj;
  	_Draw_UTF_Text("M30", X, Y, str, LINE_COLOR);
}


long draw_score::MeasuretoX (long lMeasure) 
{
	long lMeasureCount = pMusic_score->lxMeasureCnt;
	MusicalScoreMeasureInfo* pMeasureInfo = NULL;
	pMeasureInfo = (MusicalScoreMeasureInfo*)(pMusic_score->pmsmi + lMeasure);
	return pMeasureInfo->m_lLeft * pMusic_score->m_TZoom;
}

long draw_score::TimetoX (MIDIData* pMIDIData, long lTime) {
	long lMeasureCount = pMusic_score->lxMeasureCnt;
	long lMeasure, lBeat, lTick;
	MIDIData_BreakTime (pMIDIData, lTime, &lMeasure, &lBeat, &lTick);
//	ASSERT (0 <= lMeasure && lMeasure < lMeasureCount);
	long j;
	for (j = 0; j < lMeasureCount - 1; j++) {
		if (pMusic_score->lxGetMeasureTime(j + 1) > lTime) {
			break;
		}
	}
	//TRACE("#@#####TimetoX lMeasureCount[%d]\n",j);
	
	long lMeasureTime = pMusic_score->lxGetMeasureTime(j);
	long lDeltaTime = lTime - lMeasureTime;
//	ASSERT (lDeltaTime >= 0);
	long lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);
	MusicalScoreMeasureInfo* pMeasureInfo = NULL;
	pMeasureInfo = pMusic_score->lxGetMeasureInfo(j);
	return 
		pMeasureInfo->m_lLeft * pMusic_score->m_TZoom + 
		pMeasureInfo->m_lSignatureWidth * pMusic_score->m_TZoom +
		pMeasureInfo->m_lPreWidth * pMusic_score->m_TZoom +
		(lDeltaTime * 8 * pMusic_score->m_TZoom / lTimeResolution);
}


long draw_score::KeytoLineNo (long lKey, long lKeySignature) {
	long lsf = lKeySignature & 0x000000FF;
	long lmi = (lKeySignature >> 8) & 0x000000FF;
//	lsf = CLIP (-7, (char)lsf, 7);
	lsf = (signed char)lsf;
	if(lsf<-7)lsf=-7;	else if(lsf>7)lsf=7;
//TRACE("#@###Draw===lsf[%x] [%d]\n",lsf,lsf);	
	long lDeltaTable[15][12] = {
	//   C, -, D, -, E, F, -, G, -, A, -, B
		{0, 1, 2, 2, 3, 3, 4, 4, 5, 6, 6, 7}, // 7b (Cb-major / Ab-minor)
		{0, 1, 1, 2, 3, 3, 4, 4, 5, 6, 6, 7}, // 6b (Gb-major / Eb-minor)
		{0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 7}, // 5b (Db-major / Bb-minor)
		{0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 7}, // 4b (Ab-major / F-minor)
		{0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 6}, // 3b (Eb-major / C-minor)
		{0, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6}, // 2b (Bb-major / G-minor)
		{0, 0, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6}, // 1b (F-major / D-minor)
		{0, 0, 1, 2, 2, 3, 3, 4, 4, 5, 6, 6}, // 0 (C-major / A-minor)
		{0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 6, 6}, // 1# (G-major / E-minor)
		{0, 0, 1, 1, 2, 3, 3, 4, 4, 5, 5, 6}, // 2# (D-major / B-minor)
		{0, 0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6}, // 3# (A-major / F#-minor)
		{-1, 0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6}, // 4# (E-major / C#-minor)
		{-1, 0, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6}, // 5# (B-major / G#-minor)
		{-1, 0, 0, 1, 2, 2, 3, 3, 4, 5, 5, 6}, // 6# (F#-major / D#-minor)
		{-1, 0, 0, 1, 2, 2, 3, 3, 4, 4, 5, 6}  // 7# (C#-major / A#-minor)
	};
	return ((lKey / 12) * 7 + lDeltaTable[lsf + 7][lKey % 12]);
}

long draw_score::KeytoSF (long lKey, long lKeySignature) {
	long lsf = lKeySignature & 0x000000FF;
	long lmi = (lKeySignature >> 8) & 0x000000FF;
	
//	lsf = CLIP (-7, (char)lsf, 7);
	lsf = (signed char)lsf;
	if(lsf<-7)lsf=-7;	else if(lsf>7)lsf=7;
	
	long lSFTable[15][12] = {
	//   C, -, D, -, E, F, -, G, -, A, -, B
		{4, 0, 2, 0, 0, 4, 0, 4, 0, 2, 0, 0}, // 7b (Cb-major / Ab-minor)
		{4, 0, 4, 0, 3, 0, 0, 4, 0, 2, 0, 0}, // 6b (Gb-major / Eb-minor)
		{0, 0, 4, 0, 3, 0, 0, 4, 0, 4, 0, 3}, // 5b (Db-major / Bb-minor)
		{0, 0, 4, 0, 4, 0, 3, 0, 0, 4, 0, 3}, // 4b (Ab-major / F-minor)
		{0, 3, 0, 0, 4, 0, 3, 0, 0, 4, 0, 4}, // 3b (Eb-major / C-minor)
		{0, 3, 0, 0, 4, 0, 5, 0, 3, 0, 0, 4}, // 2b (Bb-major / G-minor)
		{0, 5, 0, 3, 0, 0, 5, 0, 3, 0, 0, 4}, // 1b (F-major / D-minor)
		{0, 5, 0, 3, 0, 0, 5, 0, 5, 0, 3, 0}, // 0 (C-major / A-minor)
		{0, 5, 0, 5, 0, 4, 0, 0, 5, 0, 3, 0}, // 1# (G-major / E-minor)
		{4, 0, 0, 5, 0, 4, 0, 0, 5, 0, 5, 0}, // 2# (D-major / B-minor) // 20110111 G,G#����
		{4, 0, 0, 5, 0, 5, 0, 4, 0, 0, 5, 0}, // 3# (A-major / F#-minor)
		{5, 0, 4, 0, 0, 5, 0, 4, 0, 0, 5, 0}, // 4# (E-major / C#-minor)
		{5, 0, 4, 0, 0, 5, 0, 6, 0, 4, 0, 0}, // 5# (B-major / G#-minor)
		{5, 0, 6, 0, 4, 0, 0, 6, 0, 4, 0, 0}, // 6# (F#-major / D#-minor)
		{0, 0, 6, 0, 4, 0, 0, 6, 0, 6, 0, 4}  // 7# (C#-major / A#-minor)
	};
	return (lSFTable[lsf + 7][lKey % 12]);
}

