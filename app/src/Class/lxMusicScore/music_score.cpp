#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include "../lxMIDIData/lxMIDIData.h"
//#include "../midi_document.h"
#include "music_score.h"
#include "draw_score.h"
#include "../extern.h"

#define LOG


extern  midi_doc* pDoc;

long X00, X01;
long lMx, lBx, lTx, lDx, lSx, lMix;//for test


char* codeTable[4][12] = {{ (char*)"C", (char*)"Db", (char*)"D", (char*)"Eb", (char*)"E", (char*)"F", (char*)"Gb", (char*)"G", (char*)"Ab", (char*)"A", (char*)"Bb", (char*)"B"},
						  { (char*)"C",(char*)"C#",(char*)"D",(char*)"D#",(char*)"E",(char*)"F",(char*)"F#",(char*)"G",(char*)"G#",(char*)"A", (char*)"A#", (char*)"B" },
						  { (char*)"Cm",(char*)"Dbm",(char*)"Dm",(char*)"Ebm",(char*)"Em",(char*)"Fm",(char*)"Gbm",(char*)"Gm",(char*)"Abm",(char*)"Am", (char*)"Bbm", (char*)"Bm"},
						  { (char*)"Cm",(char*)"C#m",(char*)"Dm",(char*)"D#m",(char*)"Em",(char*)"Fm",(char*)"F#m",(char*)"Gm",(char*)"G#m",(char*)"Am", (char*)"A#m", (char*)"Bm" }
};
string str_codeTable[4][12] = {{ "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B"},
							   { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" },
							   { "Cm","Dbm","Dm","Ebm","Em","Fm","Gbm","Gm","Abm","Am","Bbm","Bm"},
							   { "Cm","C#m","Dm","D#m","Em","Fm","F#m","Gm","G#m","Am","A#m","Bm" }
};

char* eomtKeyMajor[12] = { (char*)"C",(char*)"Db",(char*)"D",(char*)"Eb",(char*)"E",(char*)"F",(char*)"F#",(char*)"G",(char*)"Ab",(char*)"A",(char*)"Bb",(char*)"B"};
char* eomtKeyMinor[12] = { (char*)"Cm",(char*)"Dbm",(char*)"Dm",(char*)"Ebm",(char*)"Em",(char*)"Fm",(char*)"F#m",(char*)"Gm",(char*)"Abm",(char*)"Am", (char*)"Bbm", (char*)"Bm" };
char* jungKeyMajor[12] = { (char*)"C",(char*)"Db",(char*)"D",(char*)"Eb",(char*)"E",(char*)"F",(char*)"F#",(char*)"G",(char*)"Ab",(char*)"A",(char*)"Bb",(char*)"B"};
char* JungKeyMinor[12] =  { (char*)"Cm",(char*)"C#m",(char*)"Dm",(char*)"Ebm",(char*)"Em",(char*)"Fm",(char*)"F#m",(char*)"Gm",(char*)"Abm",(char*)"Am", (char*)"Bbm", (char*)"Bm" };

long clefKeyTbl[12]={0, -5, 2, -3, 4, -1, 6, 1, -4, 3, -2, 5};
long clefKeyTbl_minor[12]={-3,   4,    -1,    6,     1,   -4,   3,  -2,    5,    0,  -5,   2};

music_score::music_score()
{
	test=0;
	m_TZoom=8;
	putNoteCnt=0;
	RestCnt=0;
	NoteCnt=0;

	tr_num = 0;
	pmsmi=NULL;
	Score[0].pmsti = NULL;
	Score[0].pmsni = NULL;
	Score[0].pmsngi = NULL;
	Score[0].pmstgi = NULL;
	Score[1].pmsti = NULL;
	Score[1].pmsni = NULL;
	Score[1].pmsngi = NULL;
	Score[1].pmstgi = NULL;

	vLyric.clear();
	vChord.clear();
	vFermata.clear();
	vScoreBar.clear();
	ScoreTrack=NULL;
	ObbliTrack=NULL;
	ChordTrack=NULL;
	LyricTrack=NULL;
}

music_score::~music_score()
{

	if(pmsmi != NULL) free(pmsmi);
	if(Score[0].pmsti != NULL) free(Score[0].pmsti);
	if(Score[0].pmsni != NULL) free(Score[0].pmsni);
	if(Score[0].pmsngi != NULL) free(Score[0].pmsngi);
	if(Score[0].pmstgi != NULL) free(Score[0].pmstgi);
	if(Score[1].pmsti != NULL) free(Score[1].pmsti);
	if(Score[1].pmsni != NULL) free(Score[1].pmsni);
	if(Score[1].pmsngi != NULL) free(Score[1].pmsngi);
	if(Score[1].pmstgi != NULL) free(Score[1].pmstgi);

	//if(pLyric != NULL) free(pLyric);
	vLyric.clear(); vector<LYRICS> ().swap(vLyric);
	vChord.clear(); vector<CHORD> ().swap(vChord);
	vScoreBar.clear(); vector<SCORE_BAR> ().swap(vScoreBar);
	vFermata.clear(); vector<FERMATA> ().swap(vFermata);

}

char* trim(char* buf, int size);

char* trim(char* buf, int size)
{
	char *temp;// = new char[size];
	temp=(char*)malloc(size+1);
	memset(temp, 0, size+1);
	int j = 0;
	for(int i=0; i<size; i++)
	{
		if(buf[i] != ' ') temp[j++] = buf[i];
		if(buf[i]==0) break;
	}
	strcpy(buf, temp);
	free(temp);
	return buf;
}


int music_score::GetTrackListAndCnt(MIDIData *pMIDIData)
{
	MIDITrack* pMIDITrack;
	MIDIEvent* pMIDIEvent;
	char szBuf[32];
	char pData[128];
	string str;
	unsigned char pBuf[8];
	int ch;
	int i = 0;
	long lEndTime = MIDIData_GetEndTime (pMIDIData);
	long lEndMeasure, lEndBeat, lEndTick;

	MIDIData_BreakTime (pMIDIData, lEndTime, &lEndMeasure, &lEndBeat, &lEndTick);
	lxMeasureCnt=lEndMeasure;
	Score[0].lxScoreCnt=0;
	Score[1].lxScoreCnt=0;
	lxLyricCnt=0;
	lxCordCnt=0;
	lxFermataCnt=0;
	TRACE("#@#>>>>>lxMeasureCnt[%d]========\n",lxMeasureCnt);

	ScoreTrack=pDoc->TRACK_Info.pTRACK_Melody;
	LyricTrack=pDoc->TRACK_Info.pTRACK_Lyric;
	ChordTrack=pDoc->TRACK_Info.pTRACK_Chord1;
	ObbliTrack=pDoc->TRACK_Info.pTRACK_Chord2;

	lxLyricCnt = pDoc->PLY_STATUS.lNoteCnt;

	if(ScoreTrack !=NULL) Score[0].lxScoreCnt = MIDITrack_CountEvent(ScoreTrack);
	if(ObbliTrack !=NULL) Score[1].lxScoreCnt = MIDITrack_CountEvent(ObbliTrack);
	//if(LyricTrack !=NULL) forEachEvent(LyricTrack, pMIDIEvent) if (MIDIEvent_IsNoteOn(pMIDIEvent)) lxLyricCnt++;
	if(ChordTrack) forEachEvent(ChordTrack , pMIDIEvent) if (MIDIEvent_IsLyric(pMIDIEvent)) lxCordCnt++;
#ifdef DBG_MUSICSCORE
	TRACE("#@#>>>LyricTrack[%x] ChordTrack[%x] ScoreTrack[%x] ObbliTrack[%x]========\n",LyricTrack, ChordTrack, ScoreTrack, ObbliTrack);
#endif
	return lxMeasureCnt;
}

int music_score::LoadMusicScore(MIDIData* pMIDIData)
{
	int mem_cnt;
	putNoteCnt=0;
	RestCnt=0;
	NoteCnt=0;
	long lEndTime = MIDIData_GetEndTime (pMIDIData);
	long lEndMeasure, lEndBeat, lEndTick;
	long lscore_noteCnt;
	MIDIData_BreakTime (pMIDIData, lEndTime, &lEndMeasure, &lEndBeat, &lEndTick);
#ifdef DBG_MUSICSCORE
	TRACE("#@#DBG_MUS====>>>>music_score lEndTime[%d] lEndMeasure[%d]\n",lEndTime, lEndMeasure);
#endif
 	if( GetTrackListAndCnt(pMIDIData)==0 )  return 0;

	pDoc->PLY_STATUS.lNoteCnt = lxLyricCnt;
	pDoc->PLY_STATUS.lxLyricCnt=lxLyricCnt;

	//pDoc->PLY_STATUS.SongEnd_ltime = lEndTime;
	pDoc->PLY_STATUS.ScoreNoteCnt=Score[0].lxScoreCnt;

#ifdef DBG_MUSICSCORE
	TRACE("#@#DBG_MUS-->1)lEndMeasure[%d]\n",lEndMeasure);
	TRACE("#@#DBG_MUS-->2)lxCordCnt[%d]\n",lxCordCnt);
	TRACE("#@#DBG_MUS-->3)lxLyricCnt[%d]\n",lxLyricCnt);
	TRACE("#@#DBG_MUS-->4)Score[0].lxScoreCnt[%d]\n",Score[0].lxScoreCnt);
	TRACE("#@#DBG_MUS-->5)Score[1].lxScoreCnt[%d]\n",Score[1].lxScoreCnt);
	TRACE("#@#DBG_MUS-->6)lxFermataCnt[%d]\n",lxFermataCnt);
	TRACE("#@#DBG_MUS-->7)ChordTrack[%x]\n",ChordTrack);
	TRACE("#@#DBG_MUS-->8)LyricTrack[%x]\n",LyricTrack);
#endif
	if(lEndMeasure)
	{
		mem_cnt=(lEndMeasure+20 )*sizeof(MusicalScoreMeasureInfo);
		pmsmi=(MusicalScoreMeasureInfo*)calloc( lEndMeasure+40 , sizeof(MusicalScoreMeasureInfo));//madi memory 추가로 +40 메모리확보
		//pmsmi=(MusicalScoreMeasureInfo*)malloc( (lEndMeasure+40 )*sizeof(MusicalScoreMeasureInfo));
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS-->pmsmi[%x]  mem_cnt[%d]\n",pmsmi, mem_cnt);
#endif
		lxMeasureCnt=UpdateMeasureInfo(pMIDIData);//0��Ʈ������ ��������.
		TRACE("#@#-->^^^^Update UpdateMeasureInfo!!!lxMeasureCn[%d]\n",lxMeasureCnt);
	}
	if(Score[0].lxScoreCnt)
	{
		putNoteCnt=0;
		RestCnt=0;
		NoteCnt=0;
#ifdef DBG_MUSICSCORE
		TRACE("#@#~~~~~~~~DBG_MUS-->Score[0].lxScoreCnt[%d]\n",Score[0].lxScoreCnt);
#endif
		//if(Score[0].lxScoreCnt<200) lscore_noteCnt=300;
		//else lscore_noteCnt=Score[0].lxScoreCnt*2;
        lscore_noteCnt=(lEndMeasure*2) + (Score[0].lxScoreCnt*2);


		Score[0].pmsni=(MusicalScoreNoteInfo*)calloc( lscore_noteCnt, sizeof(MusicalScoreNoteInfo));
		UpdateNoteInfo (pMIDIData, 0);

		Score[0].pmsngi=(MusicalScoreNoteGroupInfo*)calloc( Score[0].lxScoreCnt+20, sizeof(MusicalScoreNoteGroupInfo));
		UpdateNoteGroupInfo (pMIDIData, 0);

		Score[0].pmstgi=(MusicalScoreTripletGroupInfo*)calloc(Score[0].lxScoreCnt+20 ,sizeof(MusicalScoreTripletGroupInfo));
		UpdateTripletGroupInfo (pMIDIData, 0);

		Score[0].pmsti=(MusicalScoreTrackInfo*)calloc(1,sizeof(MusicalScoreTrackInfo));//track information
		UpdateTrackInfo(pMIDIData, 0);
	}

	if(Score[1].lxScoreCnt)
	{
		putNoteCnt=0;
		RestCnt=0;
		NoteCnt=0;
		lscore_noteCnt=(lEndMeasure*2) + (Score[1].lxScoreCnt*2);
		Score[1].pmsni=(MusicalScoreNoteInfo*)calloc( lscore_noteCnt, sizeof(MusicalScoreNoteInfo));//Score[1].lxScoreCnt�޸�  ������ 2��
		UpdateNoteInfo (pMIDIData, 1);//
		Score[1].pmsngi=(MusicalScoreNoteGroupInfo*)calloc( Score[1].lxScoreCnt+20, sizeof(MusicalScoreNoteGroupInfo));//event memory ������ 20����
		UpdateNoteGroupInfo (pMIDIData, 1);
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS------------Score[1].pmsngi[%x]\n",Score[1].pmsngi);
#endif
		Score[1].pmstgi=(MusicalScoreTripletGroupInfo*)calloc(Score[1].lxScoreCnt+20 ,sizeof(MusicalScoreTripletGroupInfo));//event memory ������ 20����
		UpdateTripletGroupInfo (pMIDIData, 1);// 3֧ݬGroup �����迭 ����
		Score[1].pmsti=(MusicalScoreTrackInfo*)calloc(1, sizeof(MusicalScoreTrackInfo));//track information
		UpdateTrackInfo(pMIDIData, 1); //Ʈ������ ����
	}

	if(lxCordCnt)
	{
		UpdateChordInfo(ChordTrack); //�ڵ�Ʈ������ �ڵ�����
		//TRACE("#@#-->------------Update UpdateChordInfo!!![%x]\n",pChord);
	}
#ifdef DBG_MUSICSCORE
    TRACE("#@#DBG_MUS AAAA UpdateLyricInfo!!!\n");
#endif

	UpdateLyricInfo();

#ifdef DBG_MUSICSCORE
    TRACE("#@#DBG_MUS Complete UpdateLyricInfo!!!\n");
#endif
    UpdateScoreBarInfo(pMIDIData, 0);
#ifdef DBG_MUSICSCORE
    TRACE("#@#DBG_MUS Complete UpdateScoreBarInfo!!!\n");
#endif
//	if(lxFermataCnt)
//	{
//		UpdateFermataInfo(ScoreTrack); //�Ǻ�Ʈ������ �丣��Ÿ����
//		//TRACE("#@#-->------------Update UpdateFermataInfo!!![%x]  \n",pFermata);
//	}
	int i;
	long llMx, llBx, llTx, llSx, llMix;
	long lLastNoteOnTime, lLastNoteOffTime;
	MIDIEvent* pMIDIEvent = NULL;
	pDoc->lflag.score_load_ok=1;
//	pDoc->PLY_STATUS.DrawingFag = 0;
	MIDIData_FindKeySignature (pMIDIData, 480, &llSx, &llMix);
	pDoc->PLY_STATUS.SF = llSx;
	pDoc->PLY_STATUS.MI = llMix;
#ifdef DBG_MUSICSCORE
	TRACE("#@#DBG_MUS ScoreTrack  pMIDIData [%x]  ScoreTrack[%x] -- [%d][%d]!!!\n", pMIDIData, ScoreTrack, llSx, llMix);
#endif
	if(ScoreTrack!=NULL)
	{
		forEachEventInverse(ScoreTrack, pMIDIEvent)
		{
			if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
			{
				lLastNoteOnTime=MIDIEvent_GetTime(pMIDIEvent);
				lLastNoteOffTime = lLastNoteOnTime + MIDIEvent_GetDuration(pMIDIEvent);
				break;
			}
		}
		//pDoc->PLY_STATUS.LastScoreNoteOn = lLastNoteOnTime;
		pDoc->PLY_STATUS.LastScoreNoteOff =lLastNoteOffTime;
		MIDIData_BreakTime(pMIDIData, lLastNoteOffTime, &llMx, &llBx, &llTx);
#ifdef DBG_MUSICSCORE
		TRACE("#@#DBG_MUS-->000 lLastNoteOnTime[%d] lLastNoteOffTime[%d] lMx[%d]:[%d]:[%d]\n",lLastNoteOnTime, lLastNoteOffTime, llMx, llBx, llTx);
#endif
	}

	pDoc->PLY_STATUS.LastMeasure=llMx;
	pDoc->PLY_STATUS.lmesureCnt = llMx + 1;

	i = pDoc->PLY_STATUS.lmesureCnt - MUSICSCORE_START_MEASURE;
	pDoc->PLY_STATUS.lmesureDrawCnt = i;
    pDoc->PLY_STATUS.lLastPage = i / pDraw_score->cntMeasurePerPage;

#ifdef DBG_MUSICSCORE
	TRACE("#@#DBG_MUS(2)-->pDraw_score[%x]  \n", pDraw_score);
	TRACE("#@#DBG_MUS(2)-->pDraw_score[%x]  cntMeasurePerPage[%d]\n", pDraw_score, pDraw_score->cntMeasurePerPage);
	TRACE("#@#DBG_MUS(2)-->PLY_STATUS.lLastPage[%d]\n", pDoc->PLY_STATUS.lLastPage);
#endif

///////need check �򰥸�/////
	if ((i % 16) == 0 && i >= 16) pDoc->PLY_STATUS.lLastPage--;
	else if (i<16) pDoc->PLY_STATUS.lLastPage = 0;
///////////////////////////////

#ifdef DBG_MUSICSCORE
    TRACE("#@#DBG_MUS-->PLY_STATUS.lmesureCnt[%d]\n", pDoc->PLY_STATUS.lmesureCnt);
	TRACE("#@#DBG_MUS-->pMIDIData:%lx\n",(long)pDoc->pMIDIData);
	TRACE("#@#DBG_MUS----------Update Finish!!!-----\n");
#endif

	TRACE("#@## ^^^LoadMusicScore DrawCnt[%d] PLY_STATUS.lLastPage[%d] \n", pDoc->PLY_STATUS.lmesureDrawCnt, pDoc->PLY_STATUS.lLastPage);
	return 1;
}

void music_score::unLoadMusicScore(void)
{
//	if(log) fclose(log);
}
// ������������
int music_score::UpdateMeasureInfo(MIDIData* pMIDIData)
{
	MIDIEvent* pMIDIEvent;
	int TimeSigFlag=0;
	long iltime;

	long lTimeMode = MIDIData_GetTimeMode (pMIDIData);
	long lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);
	long lEndMeasure, lEndBeat, lEndTick;
	long lEndTime = MIDIData_GetEndTime (pMIDIData);
	long lEndTimeM = lEndTime;

	MIDIData_BreakTime (pMIDIData, lEndTime, &lEndMeasure, &lEndBeat, &lEndTick);
	if (lEndBeat != 0 || lEndTick != 0) 	MIDIData_MakeTime (pMIDIData, lEndMeasure + 1, 0, 0, &lEndTimeM);
	long lFeedTime = lTimeResolution * 4  * 4; //10--> 4/4박자로 4마디 여유있게하면 1/4->16마디확보되어야됨...

	lEndTime = lEndTimeM + lFeedTime;
	MIDIData_BreakTime (pMIDIData, lEndTime, &lEndMeasure, &lEndBeat, &lEndTick);

	long lMeasure;
	long lOldTimeSignature = 4 | (2 << 8);
	long lOldKeySignature = 0 | (0 << 8);
	long lNextMeasureLeft = 0;
	long lCurMeasureTime = 0;
	long lNextMeasureTime = 0;

//	TRACE("#@# ^^^lEndMeasure[%d]\n",lEndMeasure);
	long Cnt=0;
	for (lMeasure = 0; lMeasure < lEndMeasure; lMeasure++)
	{
		MusicalScoreMeasureInfo* pMeasureInfo = (MusicalScoreMeasureInfo*)(pmsmi+lMeasure);
		Cnt++;
		if (pMeasureInfo == NULL) break;
		TimeSigFlag = 0;
		long lMeasureLeft = lNextMeasureLeft;
		pMeasureInfo->m_lLeft = lNextMeasureLeft;
		pMeasureInfo->m_lTime = lCurMeasureTime;		// ���� [Tick][Subframe]
		MIDIData_MakeTime (pMIDIData, lMeasure + 1, 0, 0, &lNextMeasureTime);// ����[Tick][Subframe]
		pMeasureInfo->m_lDuration = lNextMeasureTime - lCurMeasureTime;

		long lnn, ldd, lcc, lbb;
		MIDIData_FindTimeSignature (pMIDIData, lCurMeasureTime, &lnn, &ldd, &lcc, &lbb);
		long lTimeSignature = ((lnn & 0xFF) | ((ldd & 0xFF) << 8));
		pMeasureInfo->m_lTimeSignature = lTimeSignature;

		long lsf, lmi;
		MIDIData_FindKeySignature (pMIDIData, lCurMeasureTime, &lsf, &lmi);
		long lKeySignature = ((lsf & 0xFF) | ((lmi & 0xFF) << 8));
		pMeasureInfo->m_lKeySignature = lKeySignature;
		//TRACE("#@# lMeasure[%d] -->Measure TimeA[%d] TimeB[%d]\n",lMeasure, lCurMeasureTime, lNextMeasureTime);

		if (lKeySignature != lOldKeySignature || lTimeSignature != lOldTimeSignature || lMeasure == 0)
		{
			////////check TimeSig
//			TRACE("#@#--MARK lKeySignature---lMeasure[%d] -->Measure TimeA[%d] TimeB[%d]\n", lMeasure, lCurMeasureTime, lNextMeasureTime);
			if(LyricTrack)
			{
				forEachEvent (LyricTrack, pMIDIEvent)
				{
					iltime=MIDIEvent_GetTime(pMIDIEvent);
					if(iltime>=lCurMeasureTime && iltime<lNextMeasureTime)
					{
						if (MIDIEvent_IsControlChange(pMIDIEvent)) {
							TimeSigFlag++;
							//TRACE("#@# ----------Have MIDIEvent_IsControlChange lMeasure[%d]  TimeSigFlag[%d]----------\n",lMeasure, TimeSigFlag);
						}
					}
				}
			}
			//else TimeSigFlag=0;

			if(TimeSigFlag && ( lMeasure==0 || lMeasure==1) )
			{
				//TRACE("#@#--MARK lKeySignature---lMeasure[%d] -->Measure TimeA[%d] TimeB[%d]\n", lMeasure, lCurMeasureTime, lNextMeasureTime);
				pMeasureInfo->m_lSignatureWidth = 16;
				pMeasureInfo->m_lFlags = 1;
			}
			else
			{
				pMeasureInfo->m_lSignatureWidth = 0;
				pMeasureInfo->m_lFlags = 0;
			}
			////////check TimeSig�׸����� ������-
		}
		else
		{
			pMeasureInfo->m_lSignatureWidth = 0;
		}

		pMeasureInfo->m_lPreWidth = 4;
		pMeasureInfo->m_lWidth = 8 * 4 * lnn / (1 << ldd);
		//TRACE("%d) lnn: %d, ldd: %d, m_lWidth: %d, lNextMeasureLeft: %d\n", lMeasure, lnn, ldd, pMeasureInfo->m_lWidth, lNextMeasureLeft);
		pMeasureInfo->m_lPostWidth = 0;

		lCurMeasureTime = lNextMeasureTime;
		lNextMeasureLeft =
				lMeasureLeft +
				pMeasureInfo->m_lSignatureWidth +
				pMeasureInfo->m_lPreWidth +
				pMeasureInfo->m_lWidth +
				pMeasureInfo->m_lPostWidth;

		lOldTimeSignature = lTimeSignature;
		lOldKeySignature = lKeySignature;
		//		TRACE("#@# lMeasure[%d] -->Measure TimeA[%d] TimeB[%d]\n",lMeasure, lCurMeasureTime, lNextMeasureTime);
		//		TRACE("#@#===================  lMeasure[%d]  lNextMeasureLeft[%d][%d]--\n", lMeasure, lNextMeasureLeft, pMeasureInfo->m_lSignatureWidth);
	}
	TRACE("#@#^^^^Update UpdateMeasureInfo!!!Cnt[%d]--\n",Cnt);
	return Cnt;
}

int music_score ::GetTrackEventCnt(MIDIData *pMIDIData, int track)
{

	MIDITrack* pMIDITrack;
	MIDIEvent* pMIDIEvent;
	int eventCnt=0;
	int i = 0;
	char szBuf[1024];

	forEachTrack (pMIDIData, pMIDITrack) {
		if(i==track)
		{
			ScoreTrack=pMIDITrack;
			eventCnt=	MIDITrack_CountEvent(pMIDITrack);
		}
		i++;
	}
	return eventCnt;
}

BOOL music_score::UpdateTrackInfo(MIDIData* pMIDIData, int index)
{
	MIDITrack* pMIDITrack;
	MIDIEvent* pMIDIEvent = NULL;
	if(index==0) pMIDITrack= ScoreTrack;
	else if(index==1) pMIDITrack = ObbliTrack;

	long lTimeMode = MIDIData_GetTimeMode (pMIDIData);
	long lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);
	long lNextTop = 0;
	// ��Track�� �ִ�key�� �ּ�key�� �������ڸ�/�������ڸ�/ū��ǥ����
//	forEachEvent (pMIDITrack, pMIDIEvent) {
//			if (pTrackInfo == NULL) break;
//			Cur_pMIDITrack=pMIDITrack;
	long lEndMeasure, lEndBeat, lEndTick;
	long lEndTime = MIDIData_GetEndTime (pMIDIData);

	MusicalScoreTrackInfo* pTrackInfo = Score[index].pmsti;
	MIDIData_BreakTime (pMIDIData, lEndTime, &lEndMeasure, &lEndBeat, &lEndTick);
	pTrackInfo->m_lMeasureCnt=lEndMeasure;
	pTrackInfo->m_lTop = lNextTop;
	pTrackInfo->m_lmajor = 1;
/*
				long lMaxKey = 0;
				long lMinKey = 127;
				long lCount = 0;
				forEachEvent (pMIDITrack, pMIDIEvent) {
					if (MIDIEvent_IsNoteOn (pMIDIEvent) || MIDIEvent_IsNoteOff (pMIDIEvent)) {
						long lKey = MIDIEvent_GetKey (pMIDIEvent);
						if (lKey < lMinKey)	lMinKey = lKey;
						if (lKey > lMaxKey) lMaxKey = lKey;
						lCount++;
					}
				}
				// ��ǥ�� �������� ����
				if (lMinKey == 127 && lMaxKey == 0) {
					pTrackInfo->m_lFlags = 1;
					pTrackInfo->m_lHeight = 160;
				}
				// �������ڸ�
				else if (lMinKey >= 60 && lCount > 0) {
					pTrackInfo->m_lFlags = 1;
					pTrackInfo->m_lHeight = 160;
				}
				// �������ڸ�
				else if (lMaxKey <= 60 && lCount > 0) {
					pTrackInfo->m_lFlags = 2;
					pTrackInfo->m_lHeight = 160;
				}
				// �뺸ǥ
				else {
					pTrackInfo->m_lFlags = 3;
					pTrackInfo->m_lHeight = 240;
				}
*/
	pTrackInfo->m_lFlags = 1;
	pTrackInfo->m_lHeight = 160;

	lNextTop = pTrackInfo->m_lTop + pTrackInfo->m_lHeight;
	return true;
}

int  music_score::compare(const void* a, const void* b)
{
	MusicalScoreNoteInfo* q0= (MusicalScoreNoteInfo*)a;
	MusicalScoreNoteInfo* q1= (MusicalScoreNoteInfo*)b;
	if ( q0->m_lNoteOnTime < q1->m_lNoteOnTime)     return -1;
	else if (q0->m_lNoteOnTime > q1->m_lNoteOnTime) return 1;
	else  return 0;
}


BOOL music_score::UpdateNoteInfo (MIDIData* pMIDIData, int index)
{
	MIDITrack* pMIDITrack;
	if(index==0) pMIDITrack = ScoreTrack;
	else  pMIDITrack = ObbliTrack;
	//TRACE("<================UpdateNoteInfo pMIDITrack[0x%x] index[%d]============>\n",pMIDITrack, index);

	long lyric_cnt=0;
	MIDIEvent* pMIDIEvent = NULL;
	long lTimeMode = MIDIData_GetTimeMode(pMIDIData);
	long lTimeResolution = MIDIData_GetTimeResolution(pMIDIData);
	//SMPTE베이스는 대응하지 않는다.
	if (lTimeMode != MIDIDATA_TPQNBASE)	return true;

	// 현재 표시 정확도 [틱]을 취득
	//0 = 4분음표, 1 = 8분음표, 2 = 셋잇단 8분음표, 3 = 16분음표,
	//4 = 셋잇단 16분음표, 5 = 32분음표, 6 = 셋잇단 32분음표

	//long lViewResolutionIndex = m_wndResolutionCombo.GetCurSel ();
	//long lViewResolutionIndex = 6;//4; //hdkim
	long lViewResolution = lTimeResolution; // ���� ��ǥ�� ǥ�� �ػ�
	long lViewResolution3 = lTimeResolution; // 3�մ� ��ǥ�� ǥ�� �ػ�

//	lViewResolution = lTimeResolution / 4;
//	lViewResolution3 = lTimeResolution / 6;

	lViewResolution = lTimeResolution / 8;
	lViewResolution3 = lTimeResolution / 12;

	if (lViewResolution <= 0)	lViewResolution = 1;
	if (lViewResolution3 <= 0)	lViewResolution = 1;

	//표현 가능한 음표의 길이
	long lDur960 = lTimeResolution * 8;
	long lDur840 = lTimeResolution * 7;
	long lDur720 = lTimeResolution * 6;
	long lDur600 = lTimeResolution * 5;
	long lDur480 = lTimeResolution * 4;		// 온음표
	long lDur360 = lTimeResolution * 3;		// 점2분음표
	long lDur240 = lTimeResolution * 2;		// 2분음표
	long lDur180 = lTimeResolution * 3 / 2; // 점4분음표
	long lDur120 = lTimeResolution;			// 4분음표
	long lDur90 = lTimeResolution * 3 / 4;	// 점8분음표
	long lDur80 = lTimeResolution * 2 / 3;	// 셋잇단 4분음표
	long lDur60 = lTimeResolution / 2;		// 8분음표
	long lDur45 = lTimeResolution * 3 / 8;	// 점16분음표
	long lDur40 = lTimeResolution / 3;		// 셋잇단 8분음표
	long lDur30 = lTimeResolution / 4;		// 16분음표
	long lDur20 = lTimeResolution / 6;		// 셋잇단 16분음표
	long lDur15 = lTimeResolution / 8;		// 32분음표
	long lDur10 = lTimeResolution / 12;		// 셋잇단 32분음표

	//보정 노트 온 타임을 계산하고 m_lUser1에 저장
	forEachEvent(pMIDITrack, pMIDIEvent)
	{
		MIDIEvent_Combine(pMIDIEvent);//NoteOn Combine
		if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
		{
			long lInTime = MIDIEvent_GetTime(pMIDIEvent);
			long lOutTime = lInTime + MIDIEvent_GetDuration(pMIDIEvent);
			//예 : 32분음표로 퀀타이즈
			if (lInTime % (lViewResolution3) == 0) // 셋잇단 x분음표(예: 셋잇단32분음표의 배수)
			{
				//TRACE("++-- %d ++\n", pMIDIEvent->m_lUser1);
				pMIDIEvent->m_lUser1 = lInTime;
			}
			else // 일반적으로 n분음표 또는 점n분 음표 (예: 32분음표의 배수)
			{
				pMIDIEvent->m_lUser1 = ((lInTime + lViewResolution / 2 - 1) / lViewResolution) * (lViewResolution);
			}
		}
	}
	long lTriDur;
	//보정 노트 오프 타임을 계산하고 m_lUser2에 저장
	forEachEvent (pMIDITrack, pMIDIEvent) {
		if (MIDIEvent_IsNoteOn (pMIDIEvent) && MIDIEvent_IsNote (pMIDIEvent)) {
			// ���Ϋ�?�ȫ��٫�Ȫ������������ʾ������
			long lNoteOnTime, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick;
			lNoteOnTime = pMIDIEvent->m_lUser1;
			MIDIData_BreakTime (pMIDIData, lNoteOnTime, &lNoteOnMeasure, &lNoteOnBeat, &lNoteOnTick);
			// ���Ϋ�?�ȫ��٫�Ȫ����Ҫ�������ʾ������
			long lNoteOffTime, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick;
			lNoteOffTime = MIDIEvent_GetTime (pMIDIEvent->m_pNextCombinedEvent);
			MIDIData_BreakTime (pMIDIData, lNoteOffTime, &lNoteOffMeasure, &lNoteOffBeat, &lNoteOffTick);
			// ����ʾ�Ϋ�?�ȫ��٫�Ȫ������������ʾ������
			long lNextOnTime, lNextOnMeasure, lNextOnBeat, lNextOnTick;
			MIDIEvent* pNextEvent = MIDIEvent_GetNextEvent (pMIDIEvent);
			while (pNextEvent) {
				if (MIDIEvent_IsNoteOn (pNextEvent) && MIDIEvent_IsNote (pNextEvent)) {
					if (pNextEvent->m_lUser1 > pMIDIEvent->m_lUser1) {
						break;
					}
				}
				pNextEvent = MIDIEvent_GetNextEvent (pNextEvent);
			}
			if (pNextEvent) {
				lNextOnTime = pNextEvent->m_lUser1;
				MIDIData_BreakTime (pMIDIData, lNextOnTime, &lNextOnMeasure, &lNextOnBeat, &lNextOnTick);
				if (lNextOnMeasure > lNoteOffMeasure) {
					//lNextOnTime = GetMeasureTime (lNoteOffMeasure + 1);
					lNextOnTime = lxGetMeasureTime (lNoteOffMeasure + 1);
				}
			}
			else {
				//lNextOnTime = GetMeasureTime (lNoteOnMeasure + 1);
				lNextOnTime = lxGetMeasureTime (lNoteOnMeasure + 1);
			}
			// 3֧n����ݬ������
			if (lNoteOnTime % lViewResolution3 == 0 &&
				(lNextOnTime - lNoteOnTime) % lViewResolution != 0) {
				pMIDIEvent->m_lUser2 = 	((lNoteOffTime + lViewResolution3 / 2 - 1) / lViewResolution3) * lViewResolution3;
				if (pMIDIEvent->m_lUser2 - pMIDIEvent->m_lUser1 < lViewResolution3) {
					pMIDIEvent->m_lUser2 = pMIDIEvent->m_lUser1 + lViewResolution3; // (��)�ի���3֧16����ݬ
				}

			}
				// ����n����ݬ�Ѫ�����n����ݬ������
			else {
				pMIDIEvent->m_lUser2 = 	((lNoteOffTime + lViewResolution / 2 - 1) / lViewResolution) * lViewResolution;
				if (pMIDIEvent->m_lUser2 - pMIDIEvent->m_lUser1 < lViewResolution) {
					pMIDIEvent->m_lUser2 = pMIDIEvent->m_lUser1 + lViewResolution; // (��)�ի���16����ݬ
				}
			}
		}
	}

#ifdef DBG_MUSICSCORE
	TRACE("#@#^^������index[%d] \n",index);
#endif
	//=============================================================================
	long lNoteOnPrev = 0, lNoteOffPrev = 0;
	long lNoteOnCur, lNoteOffCur, lNoteOnInterval = 0;//, lNoteOff2NoteOnGap = 0;
	MIDIEvent *pPrevEvent = NULL;
	MIDIEvent *pNextEvent = NULL;

/////////////////////////////////////////////////////////
//음표 정보의 생성과 배열에 등록
	long lLastNoteOnTime = 0, lLastNoteOffTime = 0, lTrackEndTime = 0;
	forEachEventInverse(pMIDITrack, pMIDIEvent)
	{
		if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
		{
			long lNoteOffMeasure, lNoteOffBeat, lNoteOffTick;
			long lTrackEndTimeTemp;

			lLastNoteOnTime = pMIDIEvent->m_lUser1;
			lLastNoteOffTime = pMIDIEvent->m_lUser2;

			MIDIData_BreakTime(pMIDIData, lLastNoteOffTime, &lNoteOffMeasure, &lNoteOffBeat, &lNoteOffTick);
			if (lNoteOffBeat != 0 || lNoteOffTick != 0)
			{
				MIDIData_MakeTime(pMIDIData, lNoteOffMeasure + 1, 0, 0, &lTrackEndTimeTemp);
				lTrackEndTime = lTrackEndTimeTemp;
			}
			else
			{
				lTrackEndTime = lLastNoteOffTime;
			}
			break;
		}
	}
	lNoteOnPrev = 0, lNoteOffPrev = 0;
	//-

	MusicalScoreTrackInfo* pTrackInfo = (MusicalScoreTrackInfo*)Score[index].pmsti;

	forEachEvent(pMIDITrack, pMIDIEvent)
	{
		if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
		{
			long lNoteOnMeasure, lNoteOnBeat, lNoteOnTick;
			long lNoteOffMeasure, lNoteOffBeat, lNoteOffTick;

			MIDIData_BreakTime(pMIDIData, pMIDIEvent->m_lUser1, &lNoteOnMeasure, &lNoteOnBeat, &lNoteOnTick);
			MIDIData_BreakTime(pMIDIData, pMIDIEvent->m_lUser2, &lNoteOffMeasure, &lNoteOffBeat, &lNoteOffTick);
			//TRACE("Note on: %d, off: %d\n", pMIDIEvent->m_lUser1, pMIDIEvent->m_lUser2);

			long lMeasure;
			//�� ��Ʈ �̺�Ʈ�� �� �Ҽ� ���� ���ؼ�
			for (lMeasure = lNoteOnMeasure; lMeasure <= lNoteOffMeasure; lMeasure++)
			{
				//MusicMeasureInfo* pMeasureInfo = GetMeasureInfo(lMeasure);
				MusicalScoreMeasureInfo* pMeasureInfo = (MusicalScoreMeasureInfo*)(pmsmi+lMeasure);
				long lMeasureTime = pMeasureInfo->m_lTime;					//이 소절의 시작 타임[틱]
				long lnn = (pMeasureInfo->m_lTimeSignature & 0xFF);			//분자 (이 마디의 박자 수) [비트]
				long ldd = (pMeasureInfo->m_lTimeSignature & 0xFF00) >> 8;	//분모
				long lBeatDur = 4 * lTimeResolution / (0x01 << ldd);		//1 박자의 길이 [틱]
				long lNextMeasureTime = lMeasureTime + lBeatDur * lnn;		//다음 마디의 시작 타임 [틱]
				if (pMIDIEvent->m_lUser2 - lMeasureTime <= 0)
				{
					break;
				}

				//패턴 A :이 마디에서 시작, 이 마디로 끝나는
				if (lMeasure == lNoteOnMeasure &&
					(lMeasure == lNoteOffMeasure || (lMeasure == lNoteOffMeasure - 1 && lNoteOffBeat == 0 && lNoteOffTick == 0)))
				{
					long lDur = pMIDIEvent->m_lUser2 - pMIDIEvent->m_lUser1;
					//일반적인 음표로 표현할 수없는 경우
					if ((lDur != lDur960 && lDur != lDur840 && lDur != lDur720 && lDur != lDur600 &&	lDur != lDur480 &&
						 lDur != lDur360 && lDur != lDur240 && lDur != lDur180 && lDur != lDur120 &&	lDur != lDur90 &&
						 lDur != lDur80 && lDur != lDur60 && lDur != lDur45 && lDur != lDur40 && lDur != lDur30 &&
						 lDur != lDur20 && lDur != lDur15 &&	lDur != lDur10) || pMIDIEvent->m_lUser3)
					{
						long lDivideTime1 = 0;
						long lDivideMeasure1 = 0;
						long lDivideBeat1 = 0;
						long lDivideTick1 = 0;
						long lDivideTime2 = 0;
						long lDivideMeasure2 = 0;
						long lDivideBeat2 = 0;
						long lDivideTick2 = 0;
						//a)첫 번째 박자에 우수리(半端)가 있고 다음 비트에 걸치는 경우
						if (lNoteOnTick > 0 && (lDur > lBeatDur || (lNoteOnBeat != lNoteOffBeat && lNoteOffTick > 0)))
						{
							lDivideTime1 = lMeasureTime + (lNoteOnBeat + 1) * lBeatDur;
							lDivideMeasure1 = lMeasure;
							lDivideBeat1 = lNoteOnBeat + 1;
							lDivideTick1 = 0;
						}
						//b)마지막 박자에 우수리(半端)가 있고 이전 비트에서 이어질 경우
						if (lNoteOnBeat != lNoteOffBeat && lNoteOffTick > 0)
						{
							lDivideTime2 = lMeasureTime + (lNoteOffBeat)* lBeatDur;
							lDivideMeasure2 = lMeasure;
							lDivideBeat2 = lNoteOffBeat;
							lDivideTick2 = 0;
						}
						//c)첫번째 박자 또는 마지막 박자에 연결해야하는 우수리(半端)가 없는 경우
						if (lDivideTime1 == lDivideTime2)
						{
							//4분음표보다 짧고, 기존 음표로 표현 할 수없는 길이의 경우, 8분음표 + 나머지로 표현
							if (lDur < lDur120)
							{
								lDivideTime2 = pMIDIEvent->m_lUser1 + lDur60;
								MIDIData_BreakTime(pMIDIData, lDivideTime2, &lDivideMeasure2, &lDivideBeat2, &lDivideTick2);
							}
								//2분음표보다 짧고, 기존 음표로 표현 할 수없는 길이의 경우, 4분음표 + 나머지로 표현
							else if (lDur < lDur240)
							{
								lDivideTime2 = pMIDIEvent->m_lUser1 + lDur120;
								MIDIData_BreakTime(pMIDIData, lDivideTime2, &lDivideMeasure2, &lDivideBeat2, &lDivideTick2);
							}
								//점2분음표보다 짧고, 기존 음표로 표현 할 수없는 길이의 경우, 2분음표 + 나머지로 표현
							else if (lDur < lDur360)
							{
								lDivideTime2 = pMIDIEvent->m_lUser1 + lDur240;
								MIDIData_BreakTime(pMIDIData, lDivideTime2, &lDivideMeasure2, &lDivideBeat2, &lDivideTick2);
							}
								//온음표보다 짧고, 기존 음표로 표현 할 수없는 길이의 경우, 점2분음표 + 나머지로 표현
							else if (lDur < lDur480)
							{
								lDivideTime2 = pMIDIEvent->m_lUser1 + lDur360;
								MIDIData_BreakTime(pMIDIData, lDivideTime2, &lDivideMeasure2, &lDivideBeat2, &lDivideTick2);
							}
								//기타의 경우 분할을 포기하고 가장 가까운 음표로 표현.
							else
							{
								lDivideTime2 = 0;
							}
						}
						//비트 경계 분할 없음 (음표 하나)
						if (lDivideTime1 == 0 && lDivideTime2 == 0)
						{

							//if(lNoteOnMeasure==0)
							//	TRACE("########PutNote ltime[%d] <%03d:%02d:%03d> dur[%d] \n",pMIDIEvent->m_lUser1,lNoteOnMeasure ,lNoteOnBeat, lNoteOnTick, pMIDIEvent->m_lUser2 - pMIDIEvent->m_lUser1);
							PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
										pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000000, lyric_cnt, index);
						}
							//첫 번째 비트 경계로 분할 (음표 2개)
						else if (lDivideTime1 != 0 && lDivideTime2 == 0)
						{
							PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
										lDivideTime1, lDivideMeasure1, lDivideBeat1, lDivideTick1, 0x00000001, lyric_cnt, index);

							PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime1, lDivideMeasure1, lDivideBeat1, lDivideTick1,
										pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000002, lyric_cnt, index);
						}
							//마지막 비트 경계로 분할 (음표 2개)
						else if (lDivideTime1 == 0 && lDivideTime2 != 0)
						{
							PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
										lDivideTime2, lDivideMeasure2, lDivideBeat2, lDivideTick2, 0x00000001, lyric_cnt, index);
							PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime2, lDivideMeasure2, lDivideBeat2, lDivideTick2,
										pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000002, lyric_cnt, index);
						}
							//첫 번째 비트 경계와 마지막 비트 경계로 분할 (음표 3개)
						else if (lDivideTime1 != 0 && lDivideTime2 != 0)
						{

							PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
										lDivideTime1, lDivideMeasure1, lDivideBeat1, lDivideTick1, 0x00000001, lyric_cnt, index);
							PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime1, lDivideMeasure1, lDivideBeat1, lDivideTick1,
										lDivideTime2, lDivideMeasure2, lDivideBeat2, lDivideTick2, 0x00000003, lyric_cnt, index);
							PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime2, lDivideMeasure2, lDivideBeat2, lDivideTick2,
										pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000002, lyric_cnt, index);
						}
					}
						//일반 음표로 표현할 수있는 경우
					else
					{
						PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
									pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000000, lyric_cnt, index);
					}
				}
					//패턴 B: 이 마디에서 시작, 다음 마디에 타이로 계속
				else if (lMeasure == lNoteOnMeasure && lMeasure < lNoteOffMeasure)
				{
					long lDur = lNextMeasureTime - pMIDIEvent->m_lUser1;
					//비트 경계를 넘어서서 일반 음표로 표현할 수없는 경우
					if ((lNoteOnBeat < lnn - 1 &&
						 lDur != lDur960 &&
						 lDur != lDur840 &&
						 lDur != lDur720 &&
						 lDur != lDur600 &&
						 lDur != lDur480 &&
						 lDur != lDur360 &&
						 lDur != lDur240 &&
						 lDur != lDur180 &&
						 lDur != lDur120 &&
						 lDur != lDur90 &&
						 lDur != lDur80 &&
						 lDur != lDur60 &&
						 lDur != lDur45 &&
						 lDur != lDur40 &&
						 lDur != lDur30 &&
						 lDur != lDur20 &&
						 lDur != lDur15 &&
						 lDur != lDur10) || pMIDIEvent->m_lUser3 )	{
						long lDivideTime = lMeasureTime + (lNoteOnBeat + 1) * lBeatDur;
						long lDivideMeasure = lMeasure;
						long lDivideBeat = lNoteOnBeat + 1;
						long lDivideTick = 0;
						PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
									lDivideTime, lDivideMeasure, lDivideBeat, lDivideTick, 0x00000001, lyric_cnt, index);
						PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime, lDivideMeasure, lDivideBeat, lDivideTick,
									lNextMeasureTime, lMeasure + 1, 0, 0, 0x00000006, lyric_cnt, index);
					}
						//음표 하나로 표현할 수있는 경우
					else
					{

						//TRACE("m_lflag4--->########PutNote ltime[%d] <%03d:%02d:%03d> dur[%d] \n",pMIDIEvent->m_lUser1, lNoteOnMeasure ,lNoteOnBeat, lNoteOnTick, pMIDIEvent->m_lUser2 - pMIDIEvent->m_lUser1);
						PutNote	(putNoteCnt++, pMIDIEvent, pMIDIEvent->m_lUser1, lNoteOnMeasure, lNoteOnBeat, lNoteOnTick,
									lNextMeasureTime, lNoteOnMeasure + 1, 0, 0, 0x00000004, lyric_cnt, index);
					}
				}
					//패턴 C: 이전 소절에서 타이로 계속, 이 소절에서 종료
				else if (lMeasure > lNoteOnMeasure &&
						 (lMeasure == lNoteOffMeasure || (lMeasure == lNoteOffMeasure - 1 && lNoteOffBeat == 0 && lNoteOffTick == 0)))
				{
					long lDur = pMIDIEvent->m_lUser2 - lMeasureTime;
					//비트 경계를 넘어서 있어 일반 음표로 표현할 수없는 경우
					if (lNoteOffBeat > 0 &&
						lDur != lDur960 &&
						lDur != lDur840 &&
						lDur != lDur720 &&
						lDur != lDur600 &&
						lDur != lDur480 &&
						lDur != lDur360 &&
						lDur != lDur240 &&
						lDur != lDur180 &&
						lDur != lDur120 &&
						lDur != lDur90 &&
						lDur != lDur80 &&
						lDur != lDur60 &&
						lDur != lDur45 &&
						lDur != lDur40 &&
						lDur != lDur30 &&
						lDur != lDur20 &&
						lDur != lDur15 &&
						lDur != lDur10)
					{
						long lDivideTime = lMeasureTime + (lNoteOffBeat)* lBeatDur;
						long lDivideMeasure = lMeasure;
						long lDivideBeat = lNoteOffBeat;
						long lDivideTick = 0;
						PutNote	(putNoteCnt++, pMIDIEvent, lMeasureTime, lMeasure, 0, 0,
									lDivideTime, lDivideMeasure, lDivideBeat, lDivideTick, 0x00000007, lyric_cnt, index);
						PutNote	(putNoteCnt++, pMIDIEvent, lDivideTime, lDivideMeasure, lDivideBeat, lDivideTick,
									pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000002, lyric_cnt, index);
					}
						//음표 하나로 표현할 수있는 경우
					else
					{
						PutNote	(putNoteCnt++, pMIDIEvent, lMeasureTime, lMeasure, 0, 0,
									pMIDIEvent->m_lUser2, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, 0x00000005, lyric_cnt, index);
					}
				}
					//패턴 D: 이전 소절에서 타이로 계속되고, 다음 소절에 타이로 계속되는 (온음표 만)
				else if (lMeasure > lNoteOnMeasure && lMeasure < lNoteOffMeasure)
				{
					PutNote	(putNoteCnt++, pMIDIEvent, lMeasureTime, lMeasure, 0, 0,
								lNextMeasureTime, lMeasure + 1, 0, 0, 0x00000008, lyric_cnt, index);
				}
			}
			//TRACE("PutNote	(putNoteCnt++[%d]\n",putNoteCnt);
			//hdkim+
			//*
			long lNoteOnCur = pMIDIEvent->m_lUser1;
			long lNoteOffCur = pMIDIEvent->m_lUser2;
			if (lNoteOnCur > lNoteOffPrev)
			{
				///TRACE("@@@@@@@@@lNoteOnCur[%d]---lNoteOffPrev[%d]\n",lNoteOnCur, lNoteOffPrev);
				CreateAddRestsInfo(pMIDIData, pTrackInfo, lNoteOffPrev, lNoteOnCur, index);
			}
			else if (lNoteOnCur >= lLastNoteOnTime)//last note�� ���Ͽ�
			{
				CreateAddRestsInfo(pMIDIData, pTrackInfo, lLastNoteOffTime, lTrackEndTime, index);
			}

			lNoteOnPrev = pMIDIEvent->m_lUser1;
			lNoteOffPrev = pMIDIEvent->m_lUser2;
			//*/
			//-
		}
	}

	long lNumNoteInfo = putNoteCnt;
	long j;

#if 1
	for (j = 0; j < lNumNoteInfo; j++)
	{
		MusicalScoreNoteInfo* pNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+j);
		if (pNoteInfo->m_lNoteOrRest == 1) continue;

		MusicalScoreMeasureInfo* pMeasureInfo = (MusicalScoreMeasureInfo*)(pmsmi + pNoteInfo->m_lNoteOnMeasure);
		long lnn = (pMeasureInfo->m_lTimeSignature & 0xFF);			//분자 (이 마디의 박자 수) [비트]
		long ldd = (pMeasureInfo->m_lTimeSignature & 0xFF00) >> 8;	//분모
		long lTimeTemp;
		//if (lnn == 4 )
		//��Ʈ�� �ɸ��°� ���ں��� �ؼ� note�߰��ϰ� �յ� ��Ʈ �ٿ��� ���� ����
		for (int k = 1; k <= lnn; k++)
		{
			MIDIData_MakeTime(pMIDIData,  pNoteInfo->m_lNoteOnMeasure, k, 0, &lTimeTemp);
			//  |__o__|____|
			//     \____/
			if ((pNoteInfo->m_lNoteOffTime > lTimeTemp) && (pNoteInfo->m_lNoteOnTime < lTimeTemp))
			{
				if ((pNoteInfo->m_lNoteOnTime % lDur120 == 0) && (pNoteInfo->m_lNoteOffTime % lDur120 == 0))
					continue;
				if (k == 1 || k == 3 || k == 2 || k == 4)
				{
					MusicalScoreNoteInfo* pPrevInfo = NULL;
					MusicalScoreNoteInfo* pNextInfo = NULL;
					long lPrevDur = 0;
					long lNextDur = 0;
					if (j != 0)
					{
						pPrevInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j-1));
						lPrevDur = pPrevInfo->m_lNoteOffTime - pPrevInfo->m_lNoteOnTime;
					}

					if (j < (lNumNoteInfo - 1))
					{
						pNextInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j+1));
						lNextDur = pNextInfo->m_lNoteOffTime - pNextInfo->m_lNoteOnTime;
					}

					long lNoteDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;

					//if (((lNoteDur == lDur180) && (lNextDur == lDur60)) || ((lNoteDur == lDur60) && (lNextDur == lDur180)))
					if ((lNoteDur =! lDur180 ) || (lNoteDur =! lDur90))
						continue;
				}
				long lNoteOnMeasure = pNoteInfo->m_lNoteOnMeasure;
				long lNoteOnBeat = pNoteInfo->m_lNoteOnBeat;
				long lNoteOnTick = pNoteInfo->m_lNoteOnTick;

				long lNoteOffTime = pNoteInfo->m_lNoteOffTime;
				long lNoteOffMeasure = pNoteInfo->m_lNoteOffMeasure;
				long lNoteOffBeat = pNoteInfo->m_lNoteOffBeat;
				long lNoteOffTick = pNoteInfo->m_lNoteOffTick;
				long lFlags = pNoteInfo->m_lFlags;
				long lFlagModi = 0;

				//����ǥ ����
				if ((lNoteOffMeasure == (lNoteOnMeasure + 1)) && (lNoteOnBeat == 0) && (lNoteOnTick == 0) && (lNoteOffBeat == 0) && (lNoteOffTick == 0))
					continue;

				switch (lFlags & 0x0f)
				{
					case 0: lFlagModi = 1; break;
					case 1: lFlagModi = 1; break;
					case 2: lFlagModi = 3; break;
					case 3: lFlagModi = 3; break;
					case 4: lFlagModi = 1; break;
					case 5: lFlagModi = 7; break;
					case 6: lFlagModi = 3; break;
					case 7: lFlagModi = 7; break;
					case 8: lFlagModi = 8; break;
				}

				pNoteInfo->m_lNoteOffTime = lTimeTemp;
				pNoteInfo->m_lNoteOffMeasure = pNoteInfo->m_lNoteOnMeasure;
				pNoteInfo->m_lNoteOffBeat = k;
				pNoteInfo->m_lNoteOffTick = 0;
				pNoteInfo->m_lNoteOrRest = 0; //��ǥ, hdkim
				pNoteInfo->m_lFlags = (lFlags & 0xf0)  | lFlagModi;
				pNoteInfo->m_lFlagNew = 0; //hdkim
				pNoteInfo->m_pNoteGroupInfo = NULL;
				pNoteInfo->m_pTripletGroupInfo = NULL;
				pNoteInfo->m_pNextNoteInfo = NULL;
				pNoteInfo->m_pPrevNoteInfo = NULL;

///////////////////////////

				switch (lFlags & 0x0f)
				{
					case 0: lFlagModi = 2; break;
					case 1: lFlagModi = 3; break;
					case 2: lFlagModi = 2; break;
					case 3: lFlagModi = 3; break;
					case 4: lFlagModi = 6; break;
					case 5: lFlagModi = 2; break;
					case 6: lFlagModi = 6; break;
					case 7: lFlagModi = 3; break;
					case 8: lFlagModi = 8; break;
				}

				PutNote(putNoteCnt++, pNoteInfo->m_pNoteOnEvent, lTimeTemp, pNoteInfo->m_lNoteOnMeasure, k, 0,
						lNoteOffTime, lNoteOffMeasure, lNoteOffBeat, lNoteOffTick, (lFlags & 0xf0) | lFlagModi, lyric_cnt, index);
			}
		}
	}

#endif
////check-


	qsort((MusicalScoreNoteInfo*)Score[index].pmsni,              // Beginning address of array
		  putNoteCnt,                            // Number of elements in array
		  sizeof(MusicalScoreNoteInfo),            // Size of each element
		  compare );                  // Pointer to compare function

//	TRACE("��ǥ ������ ������ ���� NoteCnt[%d] RestCnt[%d] putNoteCnt[%d]\n",NoteCnt ,RestCnt, putNoteCnt);

	lNumNoteInfo = putNoteCnt;
	for (j = 0; j < lNumNoteInfo; j++)
	{
		MusicalScoreNoteInfo* pNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+j);
		if (j > 0)pNoteInfo->m_pPrevNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j-1));
		else pNoteInfo->m_pPrevNoteInfo = NULL;
		if (j < lNumNoteInfo - 1) pNoteInfo->m_pNextNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j+1));
		else pNoteInfo->m_pNextNoteInfo = NULL;

//			TRACE("j=%d m_pNoteOnEvent[%08x] m_lNoteOrRest[%d] lNoteOnTime[%06d] [%03d:%02d:%03d] Dura[%d]\n",
//				j, pNoteInfo->m_pNoteOnEvent, pNoteInfo->m_lNoteOrRest, pNoteInfo->m_lNoteOnTime,
//				pNoteInfo->m_lNoteOnMeasure+1, pNoteInfo->m_lNoteOnBeat+1, 	pNoteInfo->m_lNoteOnTick, pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime);

	}

	//���� ��Ʈ �� Ÿ�Ӱ� ���� ��Ʈ ���� Ÿ���� 0���� ����
	forEachEvent(pMIDITrack, pMIDIEvent)
	{
		if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
		{
			pMIDIEvent->m_lUser1 = 0;
			pMIDIEvent->m_lUser2 = 0;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////
BOOL music_score::UpdateScoreBarInfo (MIDIData* pMIDIData, int index)
{

	SCORE_BAR lscore_bar;
	if(ScoreTrack==NULL) return false;

	TRACE("#@####===============UpdateScoreBarInfo pMIDITrack[0x%x] index[%d]============>\n",ScoreTrack, index);
	MIDIEvent* pMIDIEvent = NULL;
/*
	long lTime;
	long offTime;
	long key;
	long lKeySignature;
	long znumber;
	long duration;
*/
	long onTime;
	long lsf, lmi;
	long lKeySignature;

    vScoreBar.clear();
	forEachEvent(ScoreTrack, pMIDIEvent)
	{
		MIDIEvent_Combine(pMIDIEvent);//NoteOn Combine
		if (MIDIEvent_IsNoteOn(pMIDIEvent) && MIDIEvent_IsNote(pMIDIEvent))
		{
			//plscore_bar=(SCORE_BAR*)(pScoreBar+Cnt);
			onTime=MIDIEvent_GetTime(pMIDIEvent);
			MIDIData_FindKeySignature (pMIDIData, onTime, &lsf, &lmi);
			lKeySignature = ((lsf & 0xFF) | ((lmi & 0xFF) << 8));
			lscore_bar.lTime = onTime;
			lscore_bar.key  = MIDIEvent_GetKey(pMIDIEvent);
			lscore_bar.lKeySignature = lKeySignature;
			lscore_bar.offTime = onTime + MIDIEvent_GetDuration(pMIDIEvent);
			//lscore_bar.znumber = 0;
            //lscore_bar.duration = lscore_bar.offTime - lscore_bar.onTime;
			vScoreBar.push_back(lscore_bar);
			//Cnt++;
		}
	}
	//lxScoreBarCnt=vScoreBar.size();
/*
	SCORE_BAR* sbar;
	for(int i=0;i<Cnt;i++){
		sbar=(SCORE_BAR*)(pScoreBar+i);
		TRACE( "#@####pScoreBar ltime[%d] duration[%d] key[%d]\n" , sbar->lTime, sbar->duration, sbar->key);
	}
	TRACE("#@#####finish....music_score::UpdateChordInfo........\n");
*/
	return true;
}

////////////////////////////////////////////////////////////////////////////////////



BOOL music_score::UpdateNoteGroupInfo (MIDIData* pMIDIData, int index)
{
	long lTimeMode = MIDIData_GetTimeMode (pMIDIData);
	long lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);
	//표현 가능한 음표의 길이
	long lDur960 = lTimeResolution * 8;
	long lDur840 = lTimeResolution * 7;
	long lDur720 = lTimeResolution * 6;
	long lDur600 = lTimeResolution * 5;
	long lDur480 = lTimeResolution * 4;		// 온음표
	long lDur360 = lTimeResolution * 3;		// 점2분음표
	long lDur240 = lTimeResolution * 2;		// 2분음표
	long lDur180 = lTimeResolution * 3 / 2; // 점4분음표
	long lDur120 = lTimeResolution;			// 4분음표
	long lDur90 = lTimeResolution * 3 / 4;	// 점8분음표
	long lDur80 = lTimeResolution * 2 / 3;	// 셋잇단4분음표
	long lDur60 = lTimeResolution / 2;		// 8분음표
	long lDur45 = lTimeResolution * 3 / 8;	// 점16분음표
	long lDur40 = lTimeResolution / 3;		// 셋잇단8분음표
	long lDur30 = lTimeResolution / 4;		// 16분음표
	long lDur20 = lTimeResolution / 6;		// 셋잇단16분음표
	long lDur15 = lTimeResolution / 8;		// 32분음표
	long lDur10 = lTimeResolution / 12;		// 셋잇단32분음표

	//트랙정보 배열[lTrackIndex]의 음표 group 정보 배열을 일단 삭제
//	memset(Score[index].pmsngi,0,sizeof(MusicalScoreNoteGroupInfo) * (Score[index].lxScoreCnt+20));
	//��ǥGroup ������ ������ �迭�� ���� ���
	TRACE("#@# index[%d] UpdateNoteGroupInfo ------------\n",index);

	MusicalScoreTrackInfo* pTrackInfo = (MusicalScoreTrackInfo*)Score[index].pmsti;
	Score[index].PutNoteGroupCnt=0;

	long lNumNoteInfo = putNoteCnt;
	long j;
	long ix;
	MusicalScoreNoteGroupInfo* pNoteGroupInfo = NULL;
	MusicalScoreNoteInfo* pNextNoteInfo=NULL;
	BOOL bGroupContinueFlag = false;

//	TRACE("#@# UpdateNoteGroupInfo lNumNoteInfo[%d] --------\n",lNumNoteInfo);
	for (j = 0; j < lNumNoteInfo; j++)
	{
		MusicalScoreNoteInfo* pNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j));

		if(pNoteInfo->m_lNoteOrRest==1) continue;//ppp
		long lDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
        if(lDur==0) continue;//ppp

	///TRACE("#@# UpdateNoteGroupInfo j[%d]lDur[%d] --------\n",j, lDur);
		if (lDur < lDur120 && lDur != lDur80)
		{
			//printf("이 음표가 아직 어느 그룹에도 속하지 않은 경우 pNoteInfo->m_pNoteGroupInfo[%x] bGroupContinueFlag[%d] \n",pNoteInfo->m_pNoteGroupInfo, bGroupContinueFlag);
			if (pNoteInfo->m_pNoteGroupInfo == NULL)
			{
				//새로운 음표 그룹 정보를 생성할 필요가 있는 경우
				if (bGroupContinueFlag == false)
				{
//for test+
					MIDIData_BreakTime (pMIDIData, pNoteInfo->m_lNoteOnTime, &lMx, &lBx, &lTx);
					lDx=lDur;
					//if((lMx==0) && lDx)	TRACE("########PutNoteGroupInfo litme[%d] <%03d:%02d:%03d> dur[%d] \n",pNoteInfo->m_lNoteOnTime,lMx ,lBx, lTx, lDx);
//for test-
					pNoteGroupInfo = PutNoteGroupInfo(Score[index].PutNoteGroupCnt++, pNoteInfo, index);
				}
				pNoteInfo->m_pNoteGroupInfo = pNoteGroupInfo;
				pNoteGroupInfo->m_pLastNoteInfo = pNoteInfo;
				//다음의 음표가 있는 경우
				if (j + 1 < lNumNoteInfo)
				{
//					MusicalScoreNoteInfo* pNextNoteInfo = (MusicalScoreNoteInfo*)(pTrackInfo->m_theNoteInfoArray.GetAt (j + 1));
//						TRACE("UpdateNoteGroupInfo j=%d lNumNoteInfo[%d]\n", j, lNumNoteInfo);
					for(ix=0;ix<lNumNoteInfo;ix++)
					{
						//while(1){
						pNextNoteInfo = (MusicalScoreNoteInfo*)(MusicalScoreNoteInfo*)(Score[index].pmsni+(j+1));
						if(pNextNoteInfo->m_lNoteOrRest)j++;
						else {
							break;
						}
					}
					//다음 음표의 온타임이 현재 음표의 오프 타임과 떨어져있는 경우
					//다음 음표의 온타임이 현재 음표의 온타임과 동일하지 않은 경우
					if (pNoteInfo->m_lNoteOffTime != pNextNoteInfo->m_lNoteOnTime &&
						pNoteInfo->m_lNoteOnTime != pNextNoteInfo->m_lNoteOnTime) {
						bGroupContinueFlag = false; //현재 음표 그룹을 종료
					}
						//다음 음표의 소속 박자가 현재 음표의 소속 박자와 다를 경우
					else if (pNoteInfo->m_lNoteOnBeat != pNextNoteInfo->m_lNoteOnBeat ||
							 pNoteInfo->m_lNoteOnMeasure != pNextNoteInfo->m_lNoteOnMeasure) {
						bGroupContinueFlag = false; //���� ��ǥ �׷��� ����
					}
						//다음 음표가 8 분 음표를 넘는 길이의 경우
					else if (pNextNoteInfo->m_lNoteOffTime - pNextNoteInfo->m_lNoteOnTime > lTimeResolution / 2) {
						bGroupContinueFlag = false; //현재 음표 그룹을 종료
					}
					else {
						bGroupContinueFlag = true; //현재 음표 그룹을 지속
					}

				}
					//다음의 음표가 없는 경우
				else {
					bGroupContinueFlag = false;
				}
				//그룹내 최대 키와 최소 키를 갱신
				if(pNoteInfo->m_pNoteOnEvent)
				{
					long lKey = MIDIEvent_GetKey (pNoteInfo->m_pNoteOnEvent);
					if (pNoteGroupInfo->m_lMaxKey < lKey) {
						pNoteGroupInfo->m_lMaxKey = lKey;
					}
					if (pNoteGroupInfo->m_lMinKey > lKey) {
						pNoteGroupInfo->m_lMinKey = lKey;
					}
				}
				//TRACE("88####### index[%d] UpdateNoteGroupInfo j[%d]------------\n",index, j);
				//그룹의 최대 길이와 최소 길이를 갱신
				long lDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
				if (pNoteGroupInfo->m_lMaxDur < lDur) {
					pNoteGroupInfo->m_lMaxDur = lDur;
				}
				if (pNoteGroupInfo->m_lMinDur > lDur) {
					pNoteGroupInfo->m_lMinDur = lDur;
				}
				//음표 그룹의 음표 수를 증가
				if(pNoteInfo->m_pNoteOnEvent)
				{
					pNoteGroupInfo->m_arrKeys[pNoteGroupInfo->m_lNumNoteInfo]= MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent);
					pNoteGroupInfo->m_lNumNoteInfo++;
				}
				continue; //hdkim
			}

		}
		pNoteInfo->m_pNoteGroupInfo = NULL; //hdkim
	} //end for

	TRACE("#@#----index[%d] end j=%d lNumNoteInfo[%d]\n",index, j, lNumNoteInfo);

	return true;
}

BOOL music_score::UpdateTripletGroupInfo (MIDIData* pMIDIData, int index)
{

	long lTimeMode = MIDIData_GetTimeMode (pMIDIData);
	long lTimeResolution = MIDIData_GetTimeResolution (pMIDIData);

	// ����ʦ������ݬ������
	long lDur960 = lTimeResolution * 8;
	long lDur840 = lTimeResolution * 7;
	long lDur720 = lTimeResolution * 6;
	long lDur600 = lTimeResolution * 5;
	long lDur480 = lTimeResolution * 4;
	long lDur360 = lTimeResolution * 3;
	long lDur240 = lTimeResolution * 2;
	long lDur180 = lTimeResolution * 3 / 2;
	long lDur120 = lTimeResolution;
	long lDur90 = lTimeResolution * 3 / 4;
	long lDur80 = lTimeResolution * 2 / 3;
	long lDur60 = lTimeResolution / 2;
	long lDur45 = lTimeResolution * 3 / 8;
	long lDur40 = lTimeResolution / 3;
	long lDur30 = lTimeResolution / 4;
	long lDur20 = lTimeResolution / 6;
	long lDur15 = lTimeResolution / 8;
	long lDur10 = lTimeResolution / 12;

	//3연음부 Group 정보의 생성과 배열에 대한 등록?
	//	MusicalScoreTrackInfo* pTrackInfo = GetTrackInfo (lTrackIndex);
	Score[index].PutTripletGroupCnt=0;
	MusicalScoreTrackInfo* pTrackInfo = Score[index].pmsti;
	MusicalScoreNoteInfo* pNextNoteInfo=NULL;
	//long lNumNoteInfo = pTrackInfo->m_theNoteInfoArray.GetSize ();
	//long lNumNoteInfo=lxNoteCnt;
	long lNumNoteInfo=putNoteCnt;
	long j;

	MusicalScoreTripletGroupInfo* pTripletGroupInfo = NULL;
	BOOL bGroupContinueFlag = false;
	for (j = 0; j < lNumNoteInfo; j++)
	{
		MusicalScoreNoteInfo* pNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j));
		long lDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
/*
		if(pNoteInfo->m_lNoteOrRest==1)
		{
		 if(lDur==0) lDur=1;
			pNextNoteInfo = (MusicalScoreNoteInfo*)(Score[0].pmsni+(j + 1));
			TRACE("UpdateTripletGroupInfo--------------lDur[%d]-[%d] [%d]\n",lDur, pNextNoteInfo->m_lNoteOnTime, pNextNoteInfo->m_lNoteOnTime);
		}
		//if(pNoteInfo->m_lNoteOrRest==1 ) continue;//ppp ???
*/
		// 3연음인 경우
		if (lDur == lDur80 ||	lDur == lDur40 ||	lDur == lDur20 || lDur == lDur10)
		{
			//이 음표가 아직 어디의 3련 그룹에도 속하지 않은 경우
			if (pNoteInfo->m_pTripletGroupInfo == NULL)
			{
				// 새로운 3연음부 그룹정보를 생성할 필요가 있는 경우
				if (bGroupContinueFlag == false) {
					pTripletGroupInfo = PutTripletGroupInfo (Score[index].PutTripletGroupCnt++, pNoteInfo, pNoteInfo->m_lNoteOrRest, index);
				}
				//포인터의 설정
				pNoteInfo->m_pTripletGroupInfo = pTripletGroupInfo;
				pTripletGroupInfo->m_pLastNoteInfo = pNoteInfo;
				//다음의 음표가 있는 경우
				if (j + 1 < lNumNoteInfo) {
					pNextNoteInfo = (MusicalScoreNoteInfo*)(Score[index].pmsni+(j + 1));
					//다음의 음표가 이 음표보다 나중에이고 3연음부가 아닌 경우
					long lNextDur = pNextNoteInfo->m_lNoteOffTime - pNextNoteInfo->m_lNoteOnTime;
					if ((pNextNoteInfo->m_lNoteOnTime >= pNoteInfo->m_lNoteOffTime) &&
						(lNextDur != lDur80) &&	(lNextDur != lDur40) &&	(lNextDur != lDur20) &&	(lNextDur != lDur10) )
					{
						bGroupContinueFlag = false;
					}
						//다음의 음표가 있는소절이 현재의 음표 소절과 다른 경우
					else if (pNoteInfo->m_lNoteOnMeasure != pNextNoteInfo->m_lNoteOnMeasure)
					{
						bGroupContinueFlag = false;
					}
						//다음의 음표가 있는소절이 현재의 음표의 박자와 다른 경우(음표가 박자 경계에서 끊어지고 있는 경우에만)
					else if ((pNoteInfo->m_lNoteOnBeat != pNextNoteInfo->m_lNoteOnBeat) && 	(pNextNoteInfo->m_lNoteOffTick > 0))
					{
						bGroupContinueFlag = false;
					}
					else {
						bGroupContinueFlag = true;
					}
				}
					//다음의 음표가 없는 경우
				else {
					bGroupContinueFlag = false;
				}
				//그룹최대키 최소키 갱신
				if(pNoteInfo->m_lNoteOrRest==0)
				{
					long lKey = MIDIEvent_GetKey (pNoteInfo->m_pNoteOnEvent);
					if (pTripletGroupInfo->m_lMaxKey < lKey) {
						pTripletGroupInfo->m_lMaxKey = lKey;
					}
					if (pTripletGroupInfo->m_lMinKey > lKey) {
						pTripletGroupInfo->m_lMinKey = lKey;
					}
				}
				else
				{
					pTripletGroupInfo->m_lMaxKey=NULL;
					pTripletGroupInfo->m_lMinKey=NULL;
				}
				//그룹프의 최대 길이 최소길이의 갱신
				long llDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
				if (pTripletGroupInfo->m_lMaxDur < llDur) {
					pTripletGroupInfo->m_lMaxDur = llDur;
				}
				if (pTripletGroupInfo->m_lMinDur > llDur) {
					pTripletGroupInfo->m_lMinDur = llDur;
				}
				//3연음부 그룹의 음표을 증가
				pTripletGroupInfo->m_lNumNoteInfo++;
			}
			//3련 부호 개시 시각[Tick]과 종료 시각[Tick]의 보정
			if (pTripletGroupInfo->m_lMinDur == lDur80) {
				pTripletGroupInfo->m_lBeginTime =
						(pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime / lDur80) * lDur80;
				pTripletGroupInfo->m_lEndTime =
						((pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOffTime + lDur80 - 1) / lDur80) * lDur80;
			}
			else if (pTripletGroupInfo->m_lMinDur == lDur40) {
				pTripletGroupInfo->m_lBeginTime =
						(pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime / lDur40) * lDur40;
				pTripletGroupInfo->m_lEndTime =
						((pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOffTime + lDur40 - 1) / lDur40) * lDur40;
			}
			else if (pTripletGroupInfo->m_lMinDur == lDur20) {
				pTripletGroupInfo->m_lBeginTime =
						(pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime / lDur20) * lDur20;
				pTripletGroupInfo->m_lEndTime =
						((pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOffTime + lDur20 - 1) / lDur20) * lDur20;
			}
			else if (pTripletGroupInfo->m_lMinDur == lDur10) {
				pTripletGroupInfo->m_lBeginTime =
						(pTripletGroupInfo->m_pFirstNoteInfo->m_lNoteOnTime / lDur10) * lDur10;
				pTripletGroupInfo->m_lEndTime =
						((pTripletGroupInfo->m_pLastNoteInfo->m_lNoteOffTime + lDur10 - 1) / lDur10) * lDur10;
			}
		}
	}

	return true;
}

void music_score::PutNote
		(int Cnt, MIDIEvent* pNoteEvent, long lNoteOnTime, long lNoteOnMeasure, long lNoteOnBeat, long lNoteOnTick,
		 long lNoteOffTime, long lNoteOffMeasure, long lNoteOffBeat, long lNoteOffTick, long lFlags ,long lLyricNoteCnt, int index)
{
	MusicalScoreNoteInfo* pNoteInfo;
	pNoteInfo=(MusicalScoreNoteInfo*)(Score[index].pmsni+Cnt);
	pNoteInfo->m_pNoteOnEvent = pNoteEvent;
	pNoteInfo->m_pNoteOffEvent = pNoteEvent->m_pNextCombinedEvent;
	pNoteInfo->m_lNoteOnTime = lNoteOnTime;
	pNoteInfo->m_lNoteOnMeasure = lNoteOnMeasure;
	pNoteInfo->m_lNoteOnBeat = lNoteOnBeat;
	pNoteInfo->m_lNoteOnTick = lNoteOnTick;
	pNoteInfo->m_lNoteOffTime = lNoteOffTime;
	pNoteInfo->m_lNoteOffMeasure = lNoteOffMeasure;
	pNoteInfo->m_lNoteOffBeat = lNoteOffBeat;
	pNoteInfo->m_lNoteOffTick = lNoteOffTick;
	pNoteInfo->m_lFlags = lFlags;
	pNoteInfo->m_lSelected = (pNoteEvent->m_lUserFlag & MIDIEVENT_SELECTED) ? 1 : 0;
	pNoteInfo->m_pNoteGroupInfo = NULL;
	pNoteInfo->m_pTripletGroupInfo = NULL;
	pNoteInfo->m_pNextNoteInfo = NULL;
	pNoteInfo->m_pPrevNoteInfo = NULL;
	pNoteInfo->m_lLyricNoteCnt = lLyricNoteCnt;
	MIDIEvent_GetParent (pNoteEvent);
	//	TRACE("PutNote[%04d]:Note_OnTime[%06d] NoteOnMeas[%03d:%02d:%03d][%d]-pNoteInfo[0x%x] pNoteEvent[0x%x]\n",
	//	Cnt, lNoteOnTime, lNoteOnMeasure+1, lNoteOnBeat+1, lNoteOnTick, lNoteOffTime-lNoteOnTime, pNoteInfo, pNoteEvent);
//	return pNoteInfo;
}

MusicalScoreNoteGroupInfo* music_score::PutNoteGroupInfo(int Cnt, MusicalScoreNoteInfo* pNoteInfo, int index)
{
	MusicalScoreNoteGroupInfo* pNoteGroupInfo;
	pNoteGroupInfo=(MusicalScoreNoteGroupInfo*)(Score[index].pmsngi+Cnt);
	pNoteGroupInfo->m_pFirstNoteInfo = pNoteInfo;
	pNoteGroupInfo->m_pLastNoteInfo = pNoteInfo;

	if(pNoteInfo->m_pNoteOnEvent)
	{
		pNoteGroupInfo->m_lMinKey = MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent);
		pNoteGroupInfo->m_lMaxKey = MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent);
	}

	pNoteGroupInfo->m_lMinDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
	pNoteGroupInfo->m_lMaxDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
	pNoteGroupInfo->m_lNumNoteInfo = 0;
	memset(pNoteGroupInfo->m_arrKeys,0,sizeof(pNoteGroupInfo->m_arrKeys));
	return pNoteGroupInfo;
}

MusicalScoreTripletGroupInfo* music_score::PutTripletGroupInfo(int Cnt, MusicalScoreNoteInfo* pNoteInfo, int rest, int index)
{
	MusicalScoreTripletGroupInfo* pTripletGroupInfo =(MusicalScoreTripletGroupInfo*)(Score[index].pmstgi+Cnt);
	pTripletGroupInfo->m_pFirstNoteInfo = pNoteInfo;
	pTripletGroupInfo->m_pLastNoteInfo = pNoteInfo;
	if(rest){
		pTripletGroupInfo->m_lMinKey = NULL;
		pTripletGroupInfo->m_lMaxKey = NULL;
	}
	else
	{
		pTripletGroupInfo->m_lMinKey = MIDIEvent_GetKey (pNoteInfo->m_pNoteOnEvent);
		pTripletGroupInfo->m_lMaxKey = MIDIEvent_GetKey (pNoteInfo->m_pNoteOnEvent);
	}

	pTripletGroupInfo->m_lMinDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
	pTripletGroupInfo->m_lMaxDur = pNoteInfo->m_lNoteOffTime - pNoteInfo->m_lNoteOnTime;
	pTripletGroupInfo->m_lNumNoteInfo = 1;
	return pTripletGroupInfo;
}

MusicalScoreMeasureInfo* music_score::lxGetMeasureInfo (long lMeasureIndex)
{
	if(lMeasureIndex < lxMeasureCnt){
		return (MusicalScoreMeasureInfo*)(pmsmi+lMeasureIndex);
	}
	else return NULL;
//	return lMeasureIndex < Score[0].lxMeasureCn ? (MusicalScoreMeasureInfo*)(pmsmi+lMeasureIndex) : NULL;
}

BOOL music_score::CreateAddRestsInfo(MIDIData* pMIDIData, MusicalScoreTrackInfo* pTrackInfo, long lNoteOffPrev, long lNoteOnCur, int index)
{
	//long lDD;
	long lMeasure1, lBeat1, lTick1, nn1, dd1, cc1, bb1;
	long lMeasure2, lBeat2, lTick2;
	long lMeasureDiff, lTimeDiff;
	long lTickPerBeat, lTickPerMeasure;
	enum MusicRest lKindOfRest;
	long lRemained_8, lRestStart;

	lTimeDiff = lNoteOnCur - lNoteOffPrev;
	lRestStart = lNoteOffPrev;

	MIDIData_MakeTime(pMIDIData, 0, 1, 0, &lTickPerBeat);
	MIDIData_MakeTime(pMIDIData, 1, 0, 0, &lTickPerMeasure);

	MIDIData_BreakTimeEx(pMIDIData, lRestStart, &lMeasure1, &lBeat1, &lTick1, &nn1, &dd1, &cc1, &bb1);
	MIDIData_BreakTime(pMIDIData, lNoteOnCur, &lMeasure2, &lBeat2, &lTick2);

	lMeasureDiff = lMeasure2 - lMeasure1;


	//while ((lBeat1 == 0 && lTick1 <= 2) && (lTimeDiff >= lTickPerMeasure)) //��ӵǴ� �󸶵�
	//{
	//	lKindOfRest = MusicRest::rest_whole; //�½�ǥ
	//	pNoteInfo = CreateRest(lRestStart,  lKindOfRest);
	//	AddNoteInfo(pTrackInfo,  pNoteInfo);

	//	lMeasure1++;
	//	lTimeDiff -= lTickPerMeasure;
	//	lRestStart += lTickPerMeasure;
	//}

	if (lBeat1 > 0 || lTick1 > 2) //첫 마디가 빈 마디가 아닐 경우
	{
		long lNextMeasureStartTime;

		if (lMeasureDiff == 0) //같은 마디이면
		{
			//온쉼표일경우 추가후 리턴 고려
			long lBeatDiff = lBeat2 - lBeat1;

			if (lBeatDiff == 0) //동일 비트이면
			{
				//1
				long lTickDiff = lTick2 - lTick1; //lTickDiff 이 쉼표 길이가 됨

				lKindOfRest = GetKindOfRestBelowBeat(lTickDiff, nn1, dd1, lTickPerBeat);
				if(lTickDiff) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTickDiff, index);
			}
			else //다음 박자로 넘어가면
			{
				//1
				lKindOfRest = GetKindOfRestBelowBeat(lTickPerBeat - lTick1, nn1, dd1, lTickPerBeat);
				if(lTickPerBeat - lTick1) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTickPerBeat - lTick1, index);

				lTimeDiff -= (lTickPerBeat - lTick1);
				lRestStart += (lTickPerBeat - lTick1);
				lBeat1++;

				MIDIData_MakeTime(pMIDIData, lMeasure1, 0, 0, &lNextMeasureStartTime);
				lNextMeasureStartTime += lTickPerMeasure; //20141117
				if (abs(lNoteOnCur - lNextMeasureStartTime) <= 2)
				{
					//2
					lKindOfRest = GetKindOfRestByBeats(nn1 - lBeat1, nn1, dd1, &lRemained_8);
					if(nn1 - lBeat1) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, (nn1 - lBeat1)*lTickPerBeat, index);

				}
				else  //마디 끝이 아니면
				{
					//2
					if (lBeat2 - lBeat1 > 0)
					{
						lKindOfRest = GetKindOfRestByBeats(lBeat2 - lBeat1, nn1, dd1, &lRemained_8);
						if(lBeat2 - lBeat1) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest,(lBeat2 - lBeat1)*lTickPerBeat, index );

					}

					//3
					long lStartTime;
					MIDIData_MakeTime(pMIDIData, lMeasure2, lBeat2, 0, &lStartTime);
					lKindOfRest = GetKindOfRestBelowBeat(lTick2, nn1, dd1, lTickPerBeat);
					if(lTick2)	PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTick2, index);

				}
			}

			return true;
		}
		else //다음 마디로 넘어가면
		{
			//1
			long lTickRemain = lTickPerBeat - lTick1;
			if (lTickRemain % lTickPerBeat > 2)
			{
				lKindOfRest = GetKindOfRestBelowBeat(lTickRemain, nn1, dd1, lTickPerBeat);
				if(lTickRemain) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTickRemain, index);
			}

			//2
			long lBeatRemain;
			if (lTickRemain % lTickPerBeat > 2)
			{
				lBeatRemain = nn1 - lBeat1 - 1;
				lRestStart = lRestStart + (lTickPerBeat - lTickRemain % lTickPerBeat);
			}
			else
			{
				lBeatRemain = nn1 - lBeat1;
				lRestStart = lRestStart;
			}

			lKindOfRest = GetKindOfRestByBeats(lBeatRemain, nn1, dd1, &lRemained_8);
			if(lBeatRemain) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lBeatRemain*lTickPerBeat , index);

			lMeasure1++;
			MIDIData_MakeTime(pMIDIData, lMeasure1, 0, 0, &lNextMeasureStartTime);
			lTimeDiff = lTimeDiff - (lNextMeasureStartTime - lRestStart);
			lRestStart = lNextMeasureStartTime;
			lBeat1 = 0;
			lTick1 = 0;
		}
	}
	long lTickPerMeasureA,lTickPerMeasureB, DifflTickPerMeasure;
	while ((lBeat1 == 0 && lTick1 <= 2) && (lTimeDiff >= lTickPerMeasure)) //��ӵǴ� �󸶵�
	{
		lKindOfRest = rest_whole; //�½�ǥ
		//pNoteInfo = CreateRest(lRestStart,  lKindOfRest);
		//TRACE("@@@@@PutRest[rest_whole]======>mesure[%d] lTickPerMeasure[%d]\n", lMeasure1, lTickPerMeasure, index);
		PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTickPerMeasure, index);
		//ppp+
		MIDIData_MakeTime(pMIDIData, lMeasure1, 0, 0, &lTickPerMeasureA);
		MIDIData_MakeTime(pMIDIData, lMeasure1+1, 0, 0, &lTickPerMeasureB);
		DifflTickPerMeasure=lTickPerMeasureB-lTickPerMeasureA;
		lTickPerMeasure=DifflTickPerMeasure;
		//ppp-
		lMeasure1++;
		lTimeDiff -= lTickPerMeasure;
		lRestStart += lTickPerMeasure;
	}

	if (lMeasure2 == 0 && lTick2 >= 2)
	{
		//1
		lKindOfRest = GetKindOfRestBelowBeat(lTick2, nn1, dd1, lTickPerBeat);
		if(lTick2) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTick2, index);
	}
	else
	{
		long lMeasureStartTime;
		//1
		MIDIData_MakeTime(pMIDIData, lMeasure2, 0, 0, &lMeasureStartTime);
		lKindOfRest = GetKindOfRestByBeats(lBeat2, nn1, dd1, &lRemained_8);
		if(lBeat2) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lBeat2*lTickPerBeat, index);

		lKindOfRest = GetKindOfRestBelowBeat(lTick2, nn1, dd1, lTickPerBeat);
		if(lTick2) PutRest(pMIDIData, putNoteCnt++, lRestStart, lKindOfRest, lTick2, index);
	}

	return true;
}


enum MusicRest music_score::GetKindOfRestBelowBeat(long lTimeDur, long lnn, long ldd, long lTimePerBeat)
{
	//TRACE("TimePerBeat: %d\n", lTimePerBeat);
	//ASSERT(lTimeDur <= lTimePerBeat);
	long lDD = 0x02 << (ldd - 1);
	long lTolerance = 2;
	long lTime8, lTime16, lTime32, lTime64;
	long lTimeDotted8, lTimeDotted16, lTimeDotted32;

	switch (lDD)
	{
		case 2:
			lTime8 = lTimePerBeat / 4;
			lTime16 = lTimePerBeat / 8;
			lTime32 = lTimePerBeat / 16;
			lTime64 = lTimePerBeat / 32;
			lTimeDotted8 = lTime8 + lTime16;
			lTimeDotted16 = lTime16 + lTime32;
			lTimeDotted32 = lTime32 + lTime64;
			break;
		case 4:
			lTime8 = lTimePerBeat / 2;
			lTime16 = lTimePerBeat / 4;
			lTime32 = lTimePerBeat / 8;
			lTime64 = lTimePerBeat / 16;
			lTimeDotted8 = lTime8 + lTime16;
			lTimeDotted16 = lTime16 + lTime32;
			lTimeDotted32 = lTime32 + lTime64;
			break;
		case 8:
			lTime8 = lTimePerBeat;
			lTime16 = lTimePerBeat / 2;
			lTime32 = lTimePerBeat / 4;
			lTime64 = lTimePerBeat / 8;
			lTimeDotted8 = lTime8 + lTime16;
			lTimeDotted16 = lTime16 + lTime32;
			lTimeDotted32 = lTime32 + lTime64;
			break;
		default:
			break;
			//ASSERT(FALSE);
	}

	if (lTimeDur >= lTimeDotted8 - lTolerance &&  lTimeDur <= lTimeDotted8 + lTolerance)
	{
		//return MusicRest::rest_dotted_eighth;
		return rest_dotted_eighth;
	}
	else if (lTimeDur >= lTime8 - lTolerance &&  lTimeDur <= lTime8 + lTolerance)
	{
		//return MusicRest::rest_eighth;
		return rest_eighth;
	}
	else if (lTimeDur >= lTimeDotted16 - lTolerance &&  lTimeDur <= lTimeDotted16 + lTolerance)
	{
		//return MusicRest::rest_dotted_sixteenth;
		return rest_dotted_sixteenth;
	}
	else if (lTimeDur >= lTime16 - lTolerance &&  lTimeDur <= lTime16 + lTolerance)
	{
		//return MusicRest::rest_sixteenth;
		return rest_sixteenth;
	}
	else if (lTimeDur >= lTimeDotted32 - lTolerance &&  lTimeDur <= lTimeDotted32 + lTolerance)
	{
		//return MusicRest::rest_dotted_thirtysecond;
		return rest_dotted_thirtysecond;
	}
	else if (lTimeDur >= lTime32 - lTolerance &&  lTimeDur <= lTime32 + lTolerance)
	{
		//return MusicRest::rest_thirtysecond;
		return rest_thirtysecond;
	}
	else if (lTimeDur >= lTime64 - lTolerance &&  lTimeDur <= lTime64 + lTolerance)
	{
		//return MusicRest::rest_sixtyfourth;
		return rest_sixtyfourth;
	}
	else
	{

	}
	//return MusicRest::rest_of_end;
	return rest_of_end;
}

enum MusicRest music_score::GetKindOfRestByBeats(long lNumBeats, long lnn, long ldd, long *lRemained_8)
{
	//ASSERT(lNumBeats >= 0);

	if (lNumBeats >= lnn)
		//return MusicRest::rest_whole;;
		return rest_whole;;

	long lDD = 0x02 << (ldd - 1);
	long lTolerance = 2;
	long lB4Count = 0;
	long lNamoji = 0;
	long lValue = 0;

	switch (lDD)
	{
		case 2:
			lB4Count = lNumBeats * 2;
			break;

		case 4:
			lB4Count = lNumBeats;
			break;

		case 8:
			lB4Count = lNumBeats / 2;
			lNamoji = lNumBeats % 2;
			break;
		default:
			break;//ASSERT(FALSE);
	}

	switch (lB4Count)
	{
		case 0:
			if (lNamoji == 0)
				//return MusicRest::rest_of_end;
				return rest_of_end;
			else if (lNamoji == 1)
				return rest_eighth;
			break;
		case 1:
			if (lNamoji == 0)
				return rest_quarter;
			else if (lNamoji == 1)
				return rest_dotted_quarter;
			break;
		case 2:
			if (lNamoji == 1) *lRemained_8 = 8;
			return rest_half;
			break;
		case 3:
			if (lNamoji == 1) *lRemained_8 = 8;
			return rest_dotted_half;
			break;
		case 4:
			return rest_whole;
			break;
		default:
			break;//ASSERT(FALSE);
	}

	return rest_of_end;
}

void music_score::PutRest (MIDIData* pMIDIData, int Cnt, long lNoteOnTime, enum MusicRest lKindOfRest, long lRestDura, int index)
{

	long lNoteOnMeasure, lNoteOnBeat, lNoteOnTick;
	long lNoteOffMeasure, lNoteOffBeat, lNoteOffTick;
	long lNoteOffTime=lNoteOnTime + lRestDura;

	MIDIData_BreakTime(pMIDIData, lNoteOnTime, &lNoteOnMeasure, &lNoteOnBeat, &lNoteOnTick);
	MIDIData_BreakTime(pMIDIData, lNoteOffTime, &lNoteOffMeasure, &lNoteOffBeat, &lNoteOffTick);

	MusicalScoreNoteInfo* pNoteInfo;
	pNoteInfo=(MusicalScoreNoteInfo*)(Score[index].pmsni+Cnt);
	RestCnt++;
	pNoteInfo->m_lNoteOrRest = 1; //��ǥ
	pNoteInfo->m_lRestDura = lRestDura;
	pNoteInfo->m_lNoteOnTime = lNoteOnTime;
	pNoteInfo->m_lNoteOffTime = lNoteOffTime;

	pNoteInfo->m_lNoteOnMeasure = lNoteOnMeasure;
	pNoteInfo->m_lNoteOnBeat = lNoteOnBeat;
	pNoteInfo->m_lNoteOnTick = lNoteOnTick;
	pNoteInfo->m_lNoteOffMeasure = lNoteOffMeasure;
	pNoteInfo->m_lNoteOffBeat = lNoteOffBeat;
	pNoteInfo->m_lNoteOffTick = lNoteOffTick;

	//pNoteInfo->m_lFlags = static_cast<long>(lKindOfRest);
	pNoteInfo->m_lFlags =  (long)(lKindOfRest);
	pNoteInfo->m_pNextNoteInfo = NULL;
	pNoteInfo->m_pPrevNoteInfo = NULL;
	pNoteInfo->m_pTripletGroupInfo = NULL;
//	TRACE("PutRest[%06d]  lKindOfRest[%d] lRestDura[%d]\n",Cnt, lKindOfRest, lRestDura);

}

int music_score::FindKeyValue(string chord)
{
	int i;
	int code=-1;
	char rcode=-1;
	char rFlatSharpTableNo=-1;
	//lcode.Code[nn]=-1;
	//lcode.FlatSharpTableNo[nn] =-1;
	for(i=0;i<12;i++)
	{
		if(chord.compare(str_codeTable[0][i])==0)
			//if(strcmp( CodeText, str_codeTable[0][i])==0)
		{
			rcode=i;//keysig=0;
			rFlatSharpTableNo=0; //Major flat�迭
			break;
		}
			//else if(strcmp( CodeText, str_codeTable[1][i])==0)
		else if(chord.compare(str_codeTable[1][i])==0)
		{
			rcode=i;//keysig=0;
			rFlatSharpTableNo=1; //Major flat�迭
			break;
		}
	}
	for(i=0;i<12;i++)
	{
		//if(strcmp( CodeText, str_codeTable[2][i])==0)
		if(chord.compare(str_codeTable[2][i])==0)
		{
			//lcode.Code[nn]=i;//keysig=0;
			//lcode.FlatSharpTableNo[nn]=2; //Minor flat�迭
			rcode=i;//keysig=0;
			rFlatSharpTableNo=2; //Minor flat계열
			break;
		}
			//else if(strcmp( CodeText, str_codeTable[3][i])==0)
		else if(chord.compare(str_codeTable[3][i])==0)
		{
			//lcode.Code[nn]=i;//keysig=0;
			//lcode.FlatSharpTableNo[nn]=3;//Minor Sharf�迭
			rcode=i;//keysig=0;
			rFlatSharpTableNo=3; //Minor Sharf계열
			break;
		}
	}
	code=(rFlatSharpTableNo<<8)&0xff00 + rcode;
	return code;
}

BOOL music_score::UpdateChordInfo(MIDITrack* pMIDITrack)
{
	MIDIEvent* pMIDIEvent = NULL;
	//CHORD* plchord;
	CHORD lchord;
	int i;
	int keysig;
//	int code0;
//	int code1;

	char pBuf[32]={0,};
	long ltime;
//	char find=0;
//	char *ptr=NULL;

	int Cnt=0;
	string str;
	int int_code;
	char sftable,code;
	vector<string>vx;
/*
	typedef struct _CHORD
	{
		long lTime;
		long FlatSharpTableNo[2];//Major or Minor
		long Code[2];
		string subCodeStr[2];
	} CHORD;
*/
	vChord.clear();
	forEachEvent (pMIDITrack, pMIDIEvent)
	{
		if (MIDIEvent_IsLyric(pMIDIEvent))
		{
			//plchord=(CHORD*)(pChord+Cnt);
			//plchord->lTime = MIDIEvent_GetTime(pMIDIEvent);
			lchord.lTime = MIDIEvent_GetTime(pMIDIEvent);
			MIDIEvent_GetText(pMIDIEvent,pBuf,32);
			trim(pBuf, strlen(pBuf));
			str=pBuf;
			if (str.find("/") != string::npos) //"/"가 있으면
			{
				vx=pDoc->split(str,"/");
				lchord.subCodeStr[0]=vx[0];
				lchord.subCodeStr[1]=vx[1];

			}
			else{
				lchord.subCodeStr[0]=str;
				lchord.subCodeStr[1]="";
			}

			/////////////////////////////

			//		TRACE( "%s|%s  [%d]\n" , pCbuf0,pCbuf1,Cnt);
			if(!lchord.subCodeStr[0].empty()){
				int_code=FindKeyValue(lchord.subCodeStr[0]);
				sftable=(int_code>>8)&0xff;
				code=int_code&0xff;
				lchord.FlatSharpTableNo[0]=sftable;
				lchord.Code[0]=code;
			}
			if(!lchord.subCodeStr[1].empty()){
				int_code=FindKeyValue(lchord.subCodeStr[0]);
				sftable=(int_code>>8)&0xff;
				code=int_code&0xff;
				lchord.FlatSharpTableNo[1]=sftable;
				lchord.Code[1]=code;
			}
			vChord.push_back(lchord);

		}
	}
//TRACE("finish....music_score::UpdateChordInfo........\n");
	return true;
}

BOOL music_score::UpdateLyricInfo()
{
    LYRICS l_lyric;
	int i,ix,iy, iz,flg;
	string str;
//	char aData[128];
//	char bData[128];
	long sZ=pDoc->vLi.size();
	//TRACE("#@### UpdateLyricInfo pDoc->vLi.size[%d]\n",sZ);
	vLyric.clear();
	for(i=0;i<sZ;i++){
		//TRACE("#@### UpdateLyricInfo Blk_Cnt[%d]\n", pDoc->vLi[i].Blk_Cnt);
		for(ix=0;ix< pDoc->vLi[i].Blk_Cnt;ix++){
			str=pDoc->vLi[i].vLyNote[ix].str;
			pDoc->ReplaceAll(str,"[","");
			pDoc->ReplaceAll(str,"]","");
			// TRACE("#@### UpdateLyricInfo[%d][%d] str[%s]\n",i, ix, str.c_str());
			l_lyric.str=str;
			l_lyric.ltime= pDoc->vLi[i].vLyNote[ix].ltimeA;

#ifdef DBG_MUSICSCORE
			TRACE("#@#vv UpdateLyricInfo[%d][%d] str[%s][%d]\n",i, ix, str.c_str(), l_lyric.ltime); usleep(10);
#endif
			vLyric.push_back(l_lyric);
		}
	}
	return true;
}

BOOL music_score::UpdateFermataInfo(MIDITrack* pMIDITrack)
{
/*
	typedef struct tagFermata
	{
		long ltime;
		long fermatakey;
		long Cref;
		long m_measureNo;
	} FERMATA;
*/
	MIDIEvent* pMIDIEvent = NULL;
	MusicalScoreNoteInfo* pNoteInfo=NULL;
	FERMATA l_fermata;
//	FERMATA* pfermata;
	unsigned char pBuf[8]={0,};
	int i;
	int find=0;
	int Cnt=0;
	long ltime;
//	long fermatakey;

	forEachEvent (pMIDITrack, pMIDIEvent) {
		if (MIDIEvent_IsControlChange(pMIDIEvent))
		{
			MIDIEvent_GetData(pMIDIEvent,pBuf,8);
			if(pBuf[1]==3 && pBuf[2]==0)
			{
				//pfermata=(FERMATA*)(pFermata+Cnt);
				//pfermata->ltime = MIDIEvent_GetTime(pMIDIEvent);
				l_fermata.ltime = MIDIEvent_GetTime(pMIDIEvent);

				find=0;
				for(i=0;i<Score[0].lxScoreCnt;i++)
				{
					pNoteInfo = (MusicalScoreNoteInfo*)(Score[0].pmsni+i);
					if( pNoteInfo->m_lNoteOnTime == l_fermata.ltime)
					{
						//pfermata->fermatakey = MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent);
						//pfermata->m_measureNo = pNoteInfo->m_lNoteOnMeasure;
						//pfermata->Cref = Score[0].pmsti->m_lFlags;//�������ڸ� �Ǵ� �������ڸ�
//					 	TRACE("FERMATA---KEY[%d] NEED\n", pfermata->fermatakey);
						l_fermata.fermatakey = MIDIEvent_GetKey(pNoteInfo->m_pNoteOnEvent);
						l_fermata.m_measureNo = pNoteInfo->m_lNoteOnMeasure;
						l_fermata.Cref = Score[0].pmsti->m_lFlags;//�������ڸ� �Ǵ� �������ڸ�
						vFermata.push_back(l_fermata);
						find=1;
						break;
					}
				}
				if(find==0) TRACE("Not Found FERMATA---KEY NEED\n");
			}
			Cnt++;
		}
	}
//	FERMATA* ltest;
//	for(i=0;i<Cnt;i++){
//		ltest=(FERMATA*)(pFermata+i);
//		TRACE( "[%d] ltime[%d][%]\n" , i,ltest->lTime, ltest->fermatakey);//[0], ltest->LyricChunk[1], ltest->LyricChunk[2], ltest->LyricChunk[3]);
//	}
	return true;
}

long music_score::lxGetMeasureTime (long lMeasureIndex)
{

	if(lxMeasureCnt<lMeasureIndex) return 0;
	MusicalScoreMeasureInfo* pMeasureInfo = pmsmi+lMeasureIndex;
	return pMeasureInfo->m_lTime;
	//return lMeasureIndex < m_theMeasureInfoArray.GetSize () ?
	//	((MusicalScoreMeasureInfo*)m_theMeasureInfoArray.GetAt (lMeasureIndex))->m_lTime : 0;
}

