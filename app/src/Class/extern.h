#ifndef __EXTERN_H__
#define __EXTERN_H__

#define xDBG_MDK
#define xDBG_COCO
#define xDBG_JULL
#define xDBG_MUSICSCORE //SCORE loading하는 과정 디버깅
#define XDBG_STAFF //악보디버깅메세지
#define XDBG_SCORE //SCORE 디버깅
#define DBG_RHY
#define XDBG_NFC
#define xDBG_JULL
#define xDBG_FRONT
#define DBG_LYRIC
#define xRHY_FUNCTION

#include <stdio.h>
#include <string.h>

#include <jni.h>

#include "midi_main.h"
#include "midi_timer.h"
#include "midi_document.h"
#include "lxMIDIData/lxMIDIData.h"
#include "lxMusicScore/music_score.h"
#include "lxMusicScore/draw_score.h"
#include "coco.h"
#include "front.h"

#include <wchar.h>
#include <android/log.h>
#define  LOG_TAG "cecee"
#define  TRACE(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)

#define true 1
#define false 0

typedef unsigned char BOOL;

#ifndef UINT16
	typedef unsigned short        UINT16;
#endif
	typedef unsigned short        Uint16;


extern  midi_doc* pDoc;
extern  midi_main *pMIDI_MAIN;
extern  music_score* pMusic_score;
extern draw_score* pDraw_score;
extern midi_timer* pMidi_timer;
extern coco *pCoco;
extern front *pFRONT;

extern	int TTY_MIDI_Port0,TTY_MIDI_Port1, TTY_COCO;
extern int MIDI_Master_Volume;
extern long CurrentScoreKey;

extern JNIEnv *_env;
extern JNIEnv* jniENV;

extern int  QuerySendToJava(int kind);
extern char query_buf[1024];
extern char* codeTable[4][12];

extern vector<string>v_staff;
extern long timer_sigfrom_mic(void);
extern void  MIDIIO_Put_n(long port, unsigned char* data, long length);
extern void  SetMIDI_Volume(int vol);


#endif
