package com.danalenter.karaoke.dalkommpartykaraoke.base;

import android.view.KeyEvent;

/**
 * Created by kumo on 2018. 5. 16..
 */

public abstract class BaseKaraoke extends Object{
    public abstract void release();

    public boolean dispatchKeyEvent(KeyEvent event) {
        return false;
    }
}
