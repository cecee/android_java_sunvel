package com.danalenter.karaoke.dalkommpartykaraoke.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;

import com.cecee.imove.R;

/**
 * Created by youngchil.cho on 2019-05-01.
 * chc07ktm@gmail.com
 */
public class DKVerticalSeekBar extends View {
    int progress = 0;
    int max = 0;
    float heightY = 0;
    Paint paint;
    Paint bgPaint;
    Paint txtPaint;

    public DKVerticalSeekBar(Context context) {
        super(context);
        init();
    }

    public DKVerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttr(context, attrs);
        init();
    }

    public DKVerticalSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttr(context, attrs);
        init();
    }

    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(h, w, oldh, oldw);
    }

    void initAttr(Context context, AttributeSet attrs){
        if (attrs != null) {
            max = attrs.getAttributeIntValue(null, "max", 0);
            progress = attrs.getAttributeIntValue(null, "progress", 0);
        }
    }

    void init(){
        this.setClickable(true);
        paint = new Paint();
        paint.setColor(0xffff0000);
        bgPaint = new Paint();
        bgPaint.setColor(0xffafafaf);
        txtPaint = new Paint();
        txtPaint.setColor(0xff0000ff);
        txtPaint.setTextSize(20);
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    protected void onDraw(Canvas c) {

        c.drawColor(0xffffffff);
        final float seekWidth = 30;
        final float left = (getWidth() - seekWidth)/2;
        final float gapY = seekWidth/2;
        int height = getHeight() - (int)seekWidth;
        if(height <1)
            height = 1;

        float y = 0;
        Rect bodyRc = new Rect((int)left, (int)gapY, (int)(left+seekWidth), height);
        c.drawRect(bodyRc, bgPaint);
        if(progress >= 0) {
            if(max < 1)
                max = 1;
            float rate = (float)progress/(float)max;
            y = rate*(float)height;
            Rect rc = new Rect((int)left, (int)(y+gapY), (int)(left+seekWidth), height);
            c.drawRect(rc, paint);
        }
        c.drawText(String.format("%d", max-progress), left, y+gapY, txtPaint);
        super.onDraw(c);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
            {
                float y = event.getY();
                heightY = y;
                final float seekWidth = 30;
                int height = getHeight() - (int)seekWidth;
                if(height <1)
                    height = 1;
                float rate = y/(float)height;
                int current = (int)(rate*(float)max);
                if(current < 0)
                    current = 0;
                else if(current > max)
                    current = max;
                if((float)current+0.5f < rate*(float)max){
                    current+=1;
                    if(current > max)
                        current = max;
                }
                if(current != progress){
                    if(listener != null){
                        listener.OnProgress(max - current);
                    }
                }
                progress = current;
                invalidate();
            }
                break;

            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }

    public int getProgress() {
        return max - progress;
    }

    public void setProgress(int progress) {
        this.progress = max - progress;
        invalidate();
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    OnDKListener listener;

    public void setListener(OnDKListener listener){
        this.listener = listener;
    }

    public interface OnDKListener{
        void OnProgress(int progress);
    }
}
