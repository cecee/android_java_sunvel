package com.danalenter.karaoke.dalkommpartykaraoke.record;

import android.content.Context;
import android.media.MediaRecorder;
import android.widget.Toast;

import com.danalenter.karaoke.dalkommpartykaraoke.base.BaseKaraoke;

import java.io.File;

public abstract class BaseRecorder extends BaseKaraoke {

    protected final int AUDIO_SAMPLING_RATE = 96000;
    protected final int AUDIO_ENCODING_BITRATE = 256000;
    protected final int RECORDING_WIDTH = 1920;
    protected final int RECORDING_HEIGHT = 1080;

    protected MediaRecorder mediaRecorder;
    private String rootPath ="/system/data_l/GOG/disk1/dalkomm/tmp";
    protected String filePath;
    protected Thread thread = null;
    private volatile boolean recording = false;
    protected Context context;

    public BaseRecorder(Context context){
        try {
            this.context = context;
            File root = new File(rootPath);
            if (!root.exists())
                root.mkdirs();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public abstract void start();

    public synchronized boolean isRecording() {
        return recording;
    }

    public synchronized void setRecording(boolean recording) {
        this.recording = recording;
    }

    public void setFilePath(String filePath){
        this.filePath = filePath;
    }

    public void stop() {
        setRecording(false);
        if (thread != null && thread.isAlive()) {
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException iex) {
            }finally {
                thread = null;
            }

        }

        if(mediaRecorder != null) {
            try {
                mediaRecorder.stop();
                mediaRecorder.release();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                mediaRecorder = null;
            }
        }
    }

    void showToast(String message){
        //Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void release() {
        stop();
    }
}
