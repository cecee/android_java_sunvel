package com.danalenter.karaoke.dalkommpartykaraoke.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

/**
 * Created by youngchil.cho on 2019-05-01.
 * chc07ktm@gmail.com
 */
public class DKSettingData {
    public static final String KEY_PREFERENCE = DKSettingData.class.getSimpleName();
    public static final String KEY_DATA = "DK_SETTING_DATA";
    protected SparseArray<String> values;

    public static DKSettingData from(Context context){
        try{
            SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCE, Context.MODE_PRIVATE);
            String data = pref.getString(KEY_DATA, null);
            if(data != null){
                Gson gson = new Gson();
                JsonParser parser = new JsonParser();
                JsonObject jobj = parser.parse(data).getAsJsonObject();
                Object obj = gson.fromJson(jobj, DKSettingData.class);
                if(obj != null)
                {
                    return (DKSettingData)obj;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void to(Context context, DKSettingData data){
        try{
            SharedPreferences pref = context.getSharedPreferences(KEY_PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();
            if(data != null)
                editor.putString(KEY_DATA, new Gson().toJson(data));
            else
                editor.remove(KEY_DATA);
            editor.commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public DKSettingData(){
        values = new SparseArray<>();
    }

    public void add(int address, int data){
        values.put(address, String.format("%d", data));
    }

    public boolean hasAddress(int address){
        return values.indexOfKey(address) >=0;
    }

    public int getData(int address){
        String strData = values.get(address);
        return Integer.parseInt(strData);
    }
    public int getKeySize(){
        return values.size();
    }

    public int getKey(int index){
        return values.keyAt(index);
    }
}
