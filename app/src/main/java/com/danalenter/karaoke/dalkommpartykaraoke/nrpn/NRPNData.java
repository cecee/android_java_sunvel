package com.danalenter.karaoke.dalkommpartykaraoke.nrpn;

public class NRPNData {

    protected int address;
    protected int data;
    protected int max;
    protected int defaultData;
    protected int addValue = 1;
    protected int pencentage = 0;
    public NRPNData(){}

    public NRPNData(int address){
        this.address = address;
    }

    public NRPNData(int address, int data){
        this.address = address;
        this.data = data;

    }
    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        if(data <0) {
            data = 0;
            pencentage = 0;
        }
        else if(data > max) {
            data = max;
            pencentage = 100;
        }else{
            pencentage = (int)(((float)data/(float)max)*100.0f);
        }

        this.data = data;
    }

    public void setPencentage(int percentage){

        this.pencentage = percentage;
        if(max <=0) return;
        float tick = max/100.0f;
        this.data = (int)((float)percentage*tick);
    }

    public int getMax() {
        return max;
    }

    public int getPercentage(){
        if(max <=0) return 0;
        float rate = (float)data/(float)max;
        return (int)(rate*100.0f);
    }

    public int percentageToValue(int percent){
        if(max <=0) return 0;
        float tick = (float)max/100.0f;
        return (int)(percent*tick);
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getDefaultData() {
        return defaultData;
    }

    public void setDefaultData(int defaultData) {
        this.defaultData = defaultData;
    }

    public void increase(){
        data+=addValue;
        if(data > max)
            data = max;

    }

    public void decrease(){
        data-=addValue;
        if(data < 0)
            data = 0;
    }

    public int getAddValue() {
        return addValue;
    }

    public void setAddValue(int addValue) {
        this.addValue = addValue;
    }

    public void reset(){
        data = defaultData;
    }

    public String toString(){
        return null;
    }
}
