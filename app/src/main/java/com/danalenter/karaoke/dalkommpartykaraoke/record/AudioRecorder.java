package com.danalenter.karaoke.dalkommpartykaraoke.record;

import android.content.Context;
import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

/**
 * Created by shootdol-tw on 2017-02-13.
 * Update by danal entertainment s/w team yc.cho on 2019-02-19
 * 업데이트 내용 : static, global member를 제거, 외부에서 무조건적인 접속을 방지, 모듈화
 */

public class AudioRecorder extends BaseRecorder {

    public static final String TAG = AudioRecorder.class.getSimpleName();

    public AudioRecorder(Context context){
        super(context);
    }

    @Override
    public void start() {
        stop();

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String TAG = getClass().getSimpleName();
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
                try {
                    File file = new File(filePath);
                    if(file.exists()) file.delete();
                    int sr = 96000, br = 256000;
                    //int sr = 44100, br = 128000;//192000;
                    mediaRecorder = new MediaRecorder();
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mediaRecorder.setAudioChannels(2);
                    mediaRecorder.setAudioSamplingRate(sr);
                    mediaRecorder.setAudioEncodingBitRate(br);

                    //mediaRecorder.setOutputFormat(AudioFormat.ENCODING_PCM_16BIT);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
                    mediaRecorder.setOutputFile(filePath);
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                    showToast("녹음 시작");
                } catch (final Exception ex) {
                    ex.printStackTrace();
                    mediaRecorder.release();
                    mediaRecorder = null;
                    setRecording(false);
                    Log.d (TAG, "recording" + ex.getMessage());
                    showToast("녹음 에러 : " + ex.toString());
                    return;
                }
            }
        });
        thread.start();

    }

}
