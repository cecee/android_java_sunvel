package com.danalenter.karaoke.dalkommpartykaraoke.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.List;

public class USBCameraView extends SurfaceView implements SurfaceHolder.Callback {
    protected final String TAG = getClass().getSimpleName();

    SurfaceView mSurfaceView;
    SurfaceHolder mHolder;
    Camera.Size mPreviewSize;
    List<Camera.Size> mSupportedPreviewSizes;
    Camera mCamera;
    MediaRecorder mMediaRecorder;

    public int cameraid = -1, camrotate = -1, rotationdgree = 0;
    public boolean isFrontCam = true, isOnLED = false;
    private int capWidth = 1920, capHeight = 1080, realCapWidth = 0, realCapHeight = 0;
    private byte[] videoBuffer;// = new byte[1920*1080*2];
    private int bufferSize;
    private volatile boolean isPreview = false;
    private boolean available = false;

    public USBCameraView(Context context) {
        super(context);
        init();
    }

    public USBCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public boolean isAvailable(){
        return available;
    }

    private void init() {
        mSurfaceView = this;
//        addView(mSurfaceView);

        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public SurfaceHolder getSurfaceHolder() {
    return mHolder;
}
    public Camera getCamera() {
        return mCamera;
    }
    public int getrealCapWidth() {
        return realCapWidth;
    }
    public int getrealCapHeight() {
        return realCapHeight;
    }

    public void setRecordingSize() {
        int rw, rh;
        int w = 0, h = 0;
        rw = 1280;
        rh = 720;

        Camera.Parameters camParams = mCamera.getParameters();
        List<Camera.Size> camSizes1 = camParams.getSupportedPreviewSizes();
        for (Camera.Size vsize: camSizes1) {
            if (vsize.width <= rw && vsize.height <= rh && w < vsize.width && h < vsize.height){
                w = vsize.width; h = vsize.height;
            }
            else;
        }
        realCapWidth = w;
        realCapHeight = h;
        Log.d(TAG, "#####Recording Size_" + w + " x " + h);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        Log.d(TAG, "#####onLayout");
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "#####surfaceCreated");
        available = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "#####surfaceDestroyed");
        //available = false;
        if (holder == mHolder) {
            stopCapture();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.d(TAG, "#####surfaceChanged");
        available = true;
        if (holder == mHolder) {
            refreshCamear();
        }
    }

    public void showpreviewSize() {
        if(mCamera!=null) {
            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters != null) {
                List<Camera.Size> pictureSizeList = parameters.getSupportedPictureSizes();
                for (Camera.Size size : pictureSizeList) {
                    Log.d(TAG, "#####PictureSize " + size.width + " x " + size.height);
                }

                List<Camera.Size> previewSizeList = parameters.getSupportedPreviewSizes();
                for (Camera.Size size : previewSizeList) {
                    Log.d(TAG, "#####previewSize " + size.width + " x " + size.height);
                }
            }
        }
    }

    private void refreshCamear(){
        try {
            if (mCamera == null || camrotate == -1)
                startCapture();
            else if (camrotate != rotationdgree) {
                camrotate = rotationdgree;
                mCamera.stopPreview();
                mCamera.setDisplayOrientation(rotationdgree);
                mCamera.addCallbackBuffer(videoBuffer);
                mCamera.setPreviewCallbackWithBuffer(myPreviewCallback);
                mCamera.startPreview();
            }
        }catch (Exception e){
            e.printStackTrace();
            isPreview = false;
        }
    }

    public void stopCapture() {
        if(!available) return;
        isPreview = false;
        camrotate=-1;

        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            cameraid = -1;
        }
        //ssStopAudioCapture();
    }

    public void setCameraId(int id) {
        if(id > Camera.getNumberOfCameras()-1) cameraid = 0;
        else cameraid = id;

        Log.d(TAG, "setCameraId : " + cameraid);
    }

    public synchronized boolean getIsPreview () {
        return isPreview;
    }

    public void startCapture() {
        if(!available) return;
        stopCapture();
        Camera.Parameters newCamParams;
        if (mHolder != null) {
            //if(cameraid<0)
                //cameraid = Camera.getNumberOfCameras()-1;
            //else;

            try {
                Log.d(TAG, "cameraid !!!! :"+ cameraid);
                mCamera = Camera.open(cameraid);
                if(mCamera == null ){
                    available = false;
                    return;
                }
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(cameraid, cameraInfo);
                isFrontCam = cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT;

                Camera.Parameters camParams = mCamera.getParameters();
                List<Camera.Size> camSizes = camParams.getSupportedPreviewSizes();
                int w = 0, h = 0;
                for (Camera.Size vsize: camSizes) {
                    Log.d(TAG, "Supported Preview Size " + vsize.width + " x " + vsize.height);
                    if (vsize.width <= capWidth && vsize.height <= capHeight && w<vsize.width && h < vsize.height){
                        w = vsize.width; h = vsize.height;
                    }
                    else;
                }

                if (w == 0 || h == 0) {
                    w = 320; h = 180;
                }
                //프리뷰는 1920x1080 노래 안느려짐
                //w = 1280; h = 720; // 레코딩 452441M  빠른 노래에 살짝 느려짐 알 수 있음
                //w = 1024; h = 576; // 레코딩 452441M  정상
                //w = 240; h = 135; // 레코딩 452441M  정상

                realCapWidth = w; realCapHeight = h;
                Log.d(TAG, "##### CAMERA Id : " + cameraid + " New Size: " + realCapWidth + "x" + realCapHeight);
                //camParams.setPreviewSize(640, 480);
                camParams.setPreviewSize(realCapWidth, realCapHeight);
                mCamera.setParameters(camParams);
                newCamParams = mCamera.getParameters();
                Camera.Size newsize = newCamParams.getPreviewSize();
                Log.d(TAG, "##### CAMERA Id : " + cameraid + " New Size: " + Integer.toString(newsize.width) + "x" + Integer.toString(newsize.height));

                mCamera.setDisplayOrientation(rotationdgree);
                mCamera.setPreviewDisplay(mHolder);
                mCamera.setPreviewCallbackWithBuffer(myPreviewCallback);

                if (videoBuffer == null) {
                    newCamParams = mCamera.getParameters();
                    bufferSize  = realCapWidth * realCapHeight * ImageFormat.getBitsPerPixel(newCamParams.getPreviewFormat()) / 8;
                    videoBuffer = new byte[bufferSize];
                    //videoBuffer = new byte[1920 * 1080 * 2];
                }
                mCamera.addCallbackBuffer(videoBuffer);
                //cam.setPreviewCallback(myPreviewCallback);
                mCamera.startPreview();

                //ssStartAudioCapture();
                camrotate=rotationdgree;

            } catch (Exception e) {
                e.printStackTrace();
                isPreview = false;
                try{
                    if(mCamera != null)
                        mCamera.stopPreview();
                }catch (Exception e2){
                    e2.printStackTrace();
                }

            }
        }
    }

    private final Camera.PreviewCallback myPreviewCallback = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            if (mCamera != null) {
                //IBUtil.Log(TAG, "onPreviewFrame");
                isPreview = true;
                mCamera.addCallbackBuffer(videoBuffer);
            }
        }
    };

}
