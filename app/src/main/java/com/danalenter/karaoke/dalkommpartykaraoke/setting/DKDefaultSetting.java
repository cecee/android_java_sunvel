package com.danalenter.karaoke.dalkommpartykaraoke.setting;

import com.danalenter.karaoke.dalkommpartykaraoke.view.DKSettingData;

/**
 * Created by youngchil.cho on 2019-05-02.
 * chc07ktm@gmail.com
 */
public class DKDefaultSetting {
    protected DKSettingData settingData;

    public DKDefaultSetting(){
        init();
    }

    void init(){
        makeDefaultSetting();
    }

    void makeDefaultSetting(){
        settingData = new DKSettingData();
        /*
         * @date 2019.5.9
         * @version : 0.5
         * @다날에서 튜닝
         * */

        settingData.add(0x3707, 122); // synth master volume
        //midi synth 2-bands Equalizer
        settingData.add(0x3755, 1); // equalizer on
        settingData.add(0x3708, 75);
        settingData.add(0x370b, 80);
        settingData.add(0x370c, 75);
        settingData.add(0x370f, 75);

        //music input controls
        settingData.add(0x0600, 118);
        settingData.add(0x0601, 64);

        //music-4Bands Parametric Equalizer
        //기본값으로 설정

        //Microphone input Controls
        settingData.add(0x0800, 110);
        settingData.add(0x0801, 0);
        settingData.add(0x0802, 0);
        settingData.add(0x0803, 20);
        settingData.add(0x0804, 10);
        settingData.add(0x0810, 127);
        settingData.add(0x0811, 110);
        settingData.add(0x0812, 120);
        settingData.add(0x0813, 110);

        //Microphone Compressor Controls
        settingData.add(0x0500, 1);
        settingData.add(0x0501, 10);
        settingData.add(0x0502, 60);
        settingData.add(0x0503, 100);
        settingData.add(0x0504, 20);
        settingData.add(0x0505, 80);
        settingData.add(0x0507, 0);

        //Microphone 5-Bands Parametric Equalizer
        //기본값으로 설정
        //64, 64, 64, 68, 68
        settingData.add(0x0900, 58);
        settingData.add(0x0901, 58);
        settingData.add(0x0902, 58);
        settingData.add(0x0903, 58);
        settingData.add(0x0904, 62);

        settingData.add(0x0910, 250);
        settingData.add(0x0911, 500);
        settingData.add(0x0912, 1000);
        settingData.add(0x0913, 2000);
        settingData.add(0x0914, 4000);

        settingData.add(0x0921, 0);
        settingData.add(0x0922, 0);
        settingData.add(0x0923, 0);

        settingData.add(0x097f, 1);//on

        //Microphone Echo/Reverb Type Control
        settingData.add(0x0200, 12);

        //Microphone Echo Controls
        settingData.add(0x0201, 30);
        settingData.add(0x0202, 30);
        settingData.add(0x0203, 60);
        settingData.add(0x0204, 0);
        settingData.add(0x0205, 115);
        settingData.add(0x0206, 70);
        settingData.add(0x0207, 20);
        settingData.add(0x0208, 0);
        settingData.add(0x0209, 127);
        settingData.add(0x020a, 127);
        settingData.add(0x020b, 0);
        settingData.add(0x020c, 0);

        //Microphone Reverb Controls
        settingData.add(0x0300, 120);
        settingData.add(0x0301, 15);
        settingData.add(0x0302, 80);
        settingData.add(0x0303, 64);
        settingData.add(0x0304, 64);
        settingData.add(0x0305, 64);

        //Output Mix Controls
        settingData.add(0x0100, 70);
        settingData.add(0x0101, 127);
        settingData.add(0x0102, 127);
        settingData.add(0x0103, 100);
        settingData.add(0x0104, 0);//mic+mr (out)
        settingData.add(0x0105, 0);//mic+mr (rec)
        settingData.add(0x0106, 64);//Rec Volume (rec)

        //android system volume
        settingData.add(0x4000, 15);//music
        settingData.add(0x4001, 6);//system
        settingData.add(0x4002, 4);//ring
        settingData.add(0x4003, 4);//alarm
        settingData.add(0x4004, 6);//noti
    }

    public DKSettingData getSettingData() {
        return settingData;
    }

    public void setSettingData(DKSettingData settingData) {
        this.settingData = settingData;
    }
}
