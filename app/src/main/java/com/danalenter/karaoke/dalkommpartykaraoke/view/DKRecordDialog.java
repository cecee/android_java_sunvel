package com.danalenter.karaoke.dalkommpartykaraoke.view;

import android.app.Dialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cecee.imove.JniFunction;
import com.cecee.imove.R;
import com.danalenter.karaoke.dalkommpartykaraoke.record.AudioRecorder;
import com.danalenter.karaoke.dalkommpartykaraoke.record.VideoRecorder;

import java.io.File;

import static android.content.ContentValues.TAG;

/**
 * Created by youngchil.cho on 2019-05-03.
 * chc07ktm@gmail.com
 */
public class DKRecordDialog extends Dialog implements View.OnClickListener {

    String audioFilePath;
    AudioRecorder audioRecorder;
    VideoRecorder videoRecorder;
    JniFunction jni;
    OnDkReocrdListener listener;
    public DKRecordDialog(@NonNull Context context, JniFunction jni, OnDkReocrdListener listener) {
        super(context);
        this.jni = jni;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dlg_rec);
        findViewById(R.id.selectMic).setOnClickListener(this);
        findViewById(R.id.selectAux).setOnClickListener(this);
        findViewById(R.id.close).setOnClickListener(this);
        findViewById(R.id.startAudio).setOnClickListener(this);
        findViewById(R.id.playAudio).setOnClickListener(this);
       // String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        //String rootPath = "/system/data_l/GOG/disk1/dalkomm/tmp";
        String rootPath = "/sdcard/tmp";
        String recPath = String.format("%s/%s", rootPath, "rec");
        Log.i("#@#", "=============>recPath["+recPath+"]");

        File dirRec = new File(recPath);
        if(!dirRec.exists())
            dirRec.mkdirs();
        audioFilePath = String.format("%s/rec_audio.m4a", recPath);
        Log.i("#@#", "=============>audioFilePath[" + audioFilePath + "]");

        selectMic();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.selectMic:
                selectMic();
                break;
            case R.id.selectAux:
                selectAux();
                break;
            case R.id.close:
                closeDialog();
                break;
            case R.id.startAudio:
                startAudioRecord();
                break;
            case R.id.playAudio:
                playRecordedAudio();
                break;
        }
    }

    void closeDialog(){
        stopAudioRecord();
        stopVideoRecord();
        dismiss();
        if(listener != null)
            listener.OnClose();
    }

    void selectMic(){
        if(jni == null){
            return;
        }
        showToast("레코딩 입력 타입이 마이크로 변경되었습니다.");
        SendingNRPN(0x015, 0);
    }



    void selectAux(){
        showToast("레코딩 입력 타입이 Aux로 변경되었습니다.");
        SendingNRPN(0x015, 3);
    }

    void stopAudioRecord(){
        if(audioRecorder == null) return;
        audioRecorder.stop();
        audioRecorder = null;
        showToast("녹음이 중지 되었습니다.");
    }

    void startAudioRecord(){
        if(audioRecorder != null && audioRecorder.isRecording()){
            stopAudioRecord();
            return;
        }
        showToast("녹음이 시작 되었습니다.");
        File file = new File(audioFilePath);
        if(file.exists())
            file.delete();
    }

    MediaPlayer audioPlayer;
    void playRecordedAudio(){
        File file = new File(audioFilePath);
        if(!file.exists()){
            showToast("녹음된 파일이 없습니다.");
            return;
        }
        if(audioPlayer != null){
            if(audioPlayer.isPlaying())
                audioPlayer.stop();
            audioPlayer.release();
        }
        audioPlayer = MediaPlayer.create(getContext(), Uri.fromFile(file));
        if(audioPlayer == null){
            showToast("playRecordedAudio : MediaPlayer생성 오류");
            return;
        }
        audioPlayer.start();
    }

    void startVideoRecord(){

    }

    void stopVideoRecord(){

    }

    void playRecordedVideo(){

    }

    void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void SendingNRPN(int nrpnAddress, int nrpnData) {
        if (jni != null) {
            String nrpnstr = "NRPN ADDR : 0x" + String.format("%04x ", nrpnAddress & 0xffff) + " DATA : 0x" + Integer.toHexString(nrpnData);
            jni.MIDISetNRPN(nrpnAddress, nrpnData);
            Log.d(TAG, nrpnstr);
            showToast(nrpnstr);
        }

    }

    public interface OnDkReocrdListener{
        void OnClose();
    }
}
