package com.danalenter.karaoke.dalkommpartykaraoke.record;

import android.content.Context;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Process;
import android.util.Log;

import com.danalenter.karaoke.dalkommpartykaraoke.camera.USBCameraView;

import java.io.File;

/**
 * Created by shootdol-tw on 2017-02-13.
 * Update by danal entertainment s/w team yc.cho on 2019-02-19
 * 업데이트 내용 : static, global member를 제거, 외부에서 무조건적인 접속을 방지, 모듈화
 */

public class VideoRecorder extends BaseRecorder {

    private Camera mCamera;
    private USBCameraView cameraView;

    public VideoRecorder(Context context, USBCameraView cameraView){
        super(context);
        this.cameraView = cameraView;
    }

    @Override
    public void start() {
        stop();
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String TAG = getClass().getSimpleName();
                Process.setThreadPriority(Process.THREAD_PRIORITY_DEFAULT); //THREAD_PRIORITY_URGENT_AUDIO

                try {
                    File file = new File(filePath);
                    if(file.exists()) file.delete();

                    cameraView.setRecordingSize();
                    mediaRecorder = new MediaRecorder();
                    mCamera = cameraView.getCamera();
                    mCamera.unlock();
                    mediaRecorder.setCamera(mCamera);

                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
                    mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
                    mediaRecorder.setAudioChannels(2);
                    mediaRecorder.setAudioSamplingRate(AUDIO_SAMPLING_RATE);
                    mediaRecorder.setAudioEncodingBitRate(AUDIO_ENCODING_BITRATE);
                    mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                    mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);

                    mediaRecorder.setVideoSize(RECORDING_WIDTH, RECORDING_HEIGHT);
                    mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
                    mediaRecorder.setOutputFile(filePath);
                    mediaRecorder.prepare();
                    mediaRecorder.start();

                    Log.d(TAG, "start video recording_Width x Height" + RECORDING_WIDTH + " x " + RECORDING_HEIGHT);
                    setRecording(true);

                } catch (final Exception ex) {
                    ex.printStackTrace();
                    mediaRecorder.release();
                    mediaRecorder = null;
                    setRecording(false);
                    Log.d(TAG, "recording" + ex.getMessage());
                    return;
                }
            }
        });
        thread.start();
    }


}
