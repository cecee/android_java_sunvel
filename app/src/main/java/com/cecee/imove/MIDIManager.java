package com.cecee.imove;
//package com.cecee.ibkara.Midi;
import android.content.Context;

import com.cecee.imove.data.BaseData;

import java.util.ArrayList;
import java.util.List;

public class MIDIManager extends BaseData {
    private static final String TAG = "MMR";
    public Context context;
    private boolean loaded = false;

    public String song_path = "";

    // ----------------------------------------------------------
//  hLyricBox : 가사 그리는 Area 높이
//  hLineBox:  one line lyric   높이
//  yPosUpA: Lyric box에서 첫번째 line start ypos
//  yPosUpB: Lyric box에서 첫번째 line end ypos
//  yPosDnA: Lyric box에서 두번째 line start ypos
//  yPosDnB: Lyric box에서 두번째 line end ypos
//---------------------------------------------------------------
    //public final int MAX_LINE =256;

    public String Str_MIDI_Info;
    public ArrayList<String> Str_MIDI_Info_Medley;


    public int b;
    //public int ScoreMode = -1;

    public boolean m_flag_midithread = false;

    public int totalLyricLine;
    public int[] Line_BlkCnt;
    public int[] Line_timeA;
    public int[] Line_timeB;
    public int[] updn;  //0:up  1:dn 2: No draw //public  int width;
    public int[] sex;  //0:남  1:여 2: 합창
    public int[] JULL_Pos;
    public int[] CountDn_Pos;
    public int[] CountDn_Bakja;


    private int ltime_SongEnding;
    public int TitleOfftime = 0;// = -1;//현재 진행중인 Line Sex
    public int isMP3 = 0;
    public int isA_B = 0;
    ////////////control key, tempo////////
    public int MIDIKey_SF = 0;
    public int MIDIKey_MI = 0;
    public int MIDI_SEX = 0;
    public int MIDI_DATAKEY = 0;
    public int MIDI_MALEKEY = 0;
    public int MIDI_FEMALEKEY = 0;
    public int MIDI_CHORUSKEY = 0;


    public int MIDIPause_flg = 0;
    public int MIDIKeyAdj = 0;
    public int MIDITempoAdj = 0;
    public int MIDIKeySex = 0;
    public int MIDIMelodyAdj = 70; //초기값   // 미디 기본값이 70으로 들음. [0-127]
    /////===============================================
    public int LyricChangedLine = -2;//현재 진행중인 Line
    public int LyricCurrentLine = -2;//현재 진행중인 Line
    public int LyricCurrentEraseLine = -2;//현재 진행중인 Line
    public int JNICurrentEraseLine = -2;//현재 진행중인 Line

    public int exCurrentTick = -1;//현재 진행중인 Line Sex


    private boolean midiEnd = false;
    public JniFunction jniCall;


    public int ScorePosX = 0;
    public boolean staff_ready = false;
    //public boolean score_ready=false;
    public boolean score_ending = false;
    //public int score_wait=0;
    public int score_key = -1;
    public int score_velocity = -1;
    public int mic_key = -1;
    public int mic_velocity = -1;
    public int CurrCursor = -1;
    public int ScoreCurPage = -2;
    public int ScoreOldPage = -2;

    public int exDrawUpDnMode = 5;
    public boolean lineStatus[];

    public String fileURL1 = "/sdcard/audio1_2s.raw", fileURL2 = "/sdcard/audio1_4s.raw", fileURL3 = "/sdcard/audio1_8s.raw", fileURL4 = "/sdcard/audio1_16s.raw";

    private List<OneLine> lines;

    public MIDIManager() {
        init();
    }

    public MIDIManager(Context context) {
        init();
        setContext(context);
    }

    private void init() {
        if (jniCall == null)
            jniCall = new JniFunction();
        if (lineStatus == null)
            lineStatus = new boolean[3];
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setMidiEnd(boolean value) {
        midiEnd = value;
    }

    public boolean isMidiEnd() {
        return midiEnd;
    }

    //BEEP ENABLE
    public void buzzerEnable(boolean enable) {
        if (enable) jniCall.MIDISetNRPN(0x3983, 0x01);//Enable Beep
        else jniCall.MIDISetNRPN(0x3983, 0x00);//Enable Beep
    }

    //부저소리
    public void buzzerOn() {
        jniCall.MIDISetNRPN(0x3984, 0x01);
    }

    public int getTotalLyricLine() {
        return totalLyricLine;

    }

    public void MIDI_GetSFMI() {
        int sfmi = jniCall.MIDIGetSfMi();
        int sf = (sfmi >> 8) & 0xff;
        int mi = sfmi & 0xff;
        MIDIKey_SF = sf;
        MIDIKey_MI = mi;
        System.out.printf("#@# MIDI_GetSFMI sf[%d] mi[%d]\n", sf, mi);
    }

    public void MIDI_GetKEY() {
        int key_info = jniCall.MIDIGetKey();

        MIDI_SEX = (key_info >> 24) & 0xff;
        MIDI_DATAKEY = (key_info >> 16) & 0xff;
        MIDI_MALEKEY = (key_info >> 8) & 0xff;
        MIDI_FEMALEKEY = key_info & 0xff;
        //    pMidiManager.MIDI_CHORUSKEY=0;
        System.out.printf("#@# MIDI_GetKEY MIDI_SEX[%d] MIDI_DATAKEY[%d] MIDI_MALEKEY[%d] MIDI_FEMALEKEY[%d]\n", MIDI_SEX, MIDI_DATAKEY, MIDI_MALEKEY, MIDI_FEMALEKEY);
    }

    public int My_MIDIPause(int pause) {
        if (pause == 1) {
            MIDI_MasterVolume(0);
            jniCall.GetlongValue(0xf0);//put clock_stop;
        } else {
            jniCall.GetlongValue(0xf1);//put clock_start;
            MIDI_MasterVolume(0x7f);
        }
        return 1;
    }

    public int My_KeyAdj(int keyAdj) {
        if (keyAdj >= -12 && keyAdj <= 12) {
            MIDIKeyAdj = keyAdj;
            jniCall.MIDISetSysKey(MIDIKeyAdj);
            // jniCall.MIDISetNRPN(0x0601, 0x40 + GlobalValue.MIDIKeyAdj);//For MP3
            if (ScoreCurPage >= 0 && staff_ready == false) {
                // Common.m_scoreBarView.ClearScoreBar();//jypark
                jniCall.DrawScorePage(ScoreCurPage);//악보준비
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            return keyAdj;
        } else return -1;
    }

    public int My_MelodyAdj(int adj) {
        if (adj >= 0 && adj < 128) {
            jniCall.MIDISetMelodyChVolume(adj);
        } else {
            System.out.printf("#@# Out of Melody Value[0-127] [%d]\n", adj);
            return -1;
        }
        return adj;
    }

    public void OpenSerial() {
        jniCall.MIDIOpenSerial(1);//open serial
    }

    public void WriterNFC() {
        byte[] pData={ (byte)0xD1, 0x01, 25, 0x55,0x00, 0x68, 0x74, 0x74, 0x70, 0x3A, 0x2F, 0x2F, 0x62, 0x6C, 0x6F, 0x67, 0x2E, 0x73, 0x74, 0x61, 0x72, 0x74, 0x6E, 0x66, 0x63, 0x2E, 0x63, 0x6F, 0x6D};

        jniCall.writerNfcBuf(pData);
    }

    public void MIDI_MIC_INIT() {
        jniCall.MIDISetNRPN(0x0600, 0x74);//Music Input Level //65 (-15db) --> 74 (0 db ) (원곡 볼륨이 작게 설정되 있는것을 미디 볼륨과 맞춤)
        jniCall.MIDISetNRPN(0x0800, 0x74);//mic1 input level 0db
        jniCall.MIDISetNRPN(0x0801, 0x74);//mic2 input level 0db
        jniCall.MIDISetNRPN(0x0810, 0x55);//Direct L
        jniCall.MIDISetNRPN(0x0811, 0x55);//Direct R
        jniCall.MIDISetNRPN(0x0812, 0x20);//Echo Send 0x20 --- Using Echo Volume
        jniCall.MIDISetNRPN(0x0900, 0x46);//Echo EQ1-Low
        jniCall.MIDISetNRPN(0x0901, 0x43);//Echo EQ2-
        //NRPN_SND(0x09, 0x02, 0x43);//Echo EQ3-
        jniCall.MIDISetNRPN(0x0903, 0x44);//Echo EQ4-
        jniCall.MIDISetNRPN(0x0904, 0x5A);//Echo EQ5-High

        jniCall.MIDISetNRPN(0x0200, 0x0c);//Mono echo-Rerev
        jniCall.MIDISetNRPN(0x0205, 0x4d);//77d Echo Time
        jniCall.MIDISetNRPN(0x0206, 0x45);//69d feedback

        jniCall.MIDISetNRPN(0x0105, 0x03);//03 Aux Only ( HDMI 에서 하울링 제거)

        buzzerEnable(true);
        //startEngine();
    }

    public int MIDI_MasterVolume(int volume) {
        if (volume >= 0 && volume < 128) {
            jniCall.MIDISetNRPN(0x3707, volume);
        } else {
            System.out.printf("#@# Out of Range !!!.... MIDI_MasterVolume[0-127] [%d]\n", volume);
            return -1;
        }
        return volume;
    }

    public int StringToChord(String chord) {
        int i;
        int ichord = 0xffff;
        String chordTable_major[] = {"C", "C#", "Db", "D", "D#", "Eb", "E", "F", "F#", "Gb", "G", "G#", "Ab", "A", "A#", "Bb", "B"};
        String chordTable_minor[] = {"Cm", "C#m", "Dbm", "Dm", "D#m", "Ebm", "Em", "Fm", "F#m", "Gbm", "Gm", "G#m", "Abm", "Am", "A#m", "Bbm", "Bm"};
        int clefKeyMajorTbl[] = {0, -5, -5, 2, -3, -3, 4, -1, 6, 6, 1, -4, -4, 3, -2, -2, 5};
        int clefKeyMinorTbl[] = {-3, 4, 4, -1, 6, 6, 1, -4, 3, 3, -2, 5, 5, 0, -5, -5, 2};
        for (i = 0; i < 17; i++) {
            if (chord.equals(chordTable_major[i]) == true) {
                ichord = clefKeyMajorTbl[i] & 0xff;
                break;
            } else if (chord.equals(chordTable_minor[i]) == true) {
                ichord = (clefKeyMinorTbl[i] & 0xff) + 0x100;
                break;
            }
        }
        System.out.printf("#@# StringToChord[%s] [%x]\n", chord, ichord);
        return ichord;
    }

    public int MIDILoad(String path, int LyricCode, int language, int key, int speed, int MelodyVol, String chord) {
        int rtn;
        int ichord = StringToChord(chord);
        //  public static String path="";
        LyricCurrentLine = -2;
        LyricCurrentEraseLine = -3;
        setMidiEnd(false);
        isA_B = 0;//A-B clear
        setContentsLoad(false);
        staff_ready = false;
        score_ending = false;

        ScoreOldPage = -2;
        CurrCursor = -1;

        //ScoreMode = LyricCode;

      //  System.out.printf("#@#################### Draw_Score.ScoreMode[%d] ichord[%x]%n", ScoreMode, ichord);
        System.out.printf("#@## MIDILoad path[%s] LyricCode[%d]\n", path, LyricCode);

        rtn = jniCall.MIDIOpen(path, LyricCode, key, speed, MelodyVol);
        if (rtn == 0) return 0;
        setContentsLoad(true);
        MIDIGetLyric();
        MIDIGetTitle();
        MIDI_GetKEY();
        return 1;
    }

    public int MIDIUnLoad() {
        isMP3 = 0;
        if (isContentsLoaded()) {
            setContentsLoad(false);
            MIDI_MasterVolume(0);
            jniCall.MIDIUnload();
        }
        return 1;
    }

    public void MIDIGetTitle() {
        int i;
        String[] title;
        title = jniCall.MIDIGetTitle();
        System.out.printf("#@#========MIDIGetTitle갯수[%d]=====\n", title.length);

        Str_MIDI_Info_Medley = new ArrayList<>();
        for(i=0;i<title.length;i++){
            System.out.printf("#@#========MIDIGetTitle[%d]=[%s]=====\n",i,title[i]);
            Str_MIDI_Info_Medley.add(title[i]);
        }

        if(title.length==1) {
            Str_MIDI_Info = title[0]; //#T #A..#B.. 등 모든 정보 한번에
        }
        else if(title.length>1) {
            Str_MIDI_Info = String.format("%s 곡외 총 %d곡 메들리", title[0], title.length);
        }
        // debug
        for(i=0;i<Str_MIDI_Info_Medley.size();i++){
            System.out.printf("#@#========MIDIGetTitle: Str_MIDI_Info_Medley[%d]=[%s]=====\n",i,Str_MIDI_Info_Medley.get(i));
        }
    }



    public int MIDIGetLyric() {
        System.out.printf("#@#========MIDIGetLyric=====\n");
        ltime_SongEnding = jniCall.songEndTick();
        TitleOfftime     = jniCall.GetlongValue(0x04);
        totalLyricLine = jniCall.songTotalLine();
        System.out.printf("#@#ltime_SongEnding[%d] TitleOfftime[%d] totalLyricLine[%d]\n",ltime_SongEnding, TitleOfftime, totalLyricLine );
        if(lines == null) lines = new ArrayList<>();
        for(OneLine line : lines)  line.release();
        lines.clear();

        if(totalLyricLine>0)
        {
            sex = jniCall.MIDIGetLineSex();
            updn = jniCall.MIDIGetLineUpDn();
            Line_BlkCnt = jniCall.MIDIGetLineBlkCnt();
            Line_timeA = jniCall.MIDIGetLineTimeA();
            Line_timeB = jniCall.MIDIGetLineTimeB();
            JULL_Pos = jniCall.MIDIGetJullPosA();//필요할까?

            CountDn_Pos = jniCall.MIDIGetCountDnPos();
            CountDn_Bakja = jniCall.MIDIGetCountDnBakja();

            System.out.printf("#@#========sex.length[%d]=====\n", sex.length);
            System.out.printf("#@#======== totalLyricLine[%d]=====\n", totalLyricLine);
            for (int i = 0; i < totalLyricLine; i++) {
                OneLine loneline = PutCharPos(jniCall, i);//글자마다 포인트 계산
                lines.add(loneline);
            }
        }
      return 1;
    }

    public boolean isContentsLoaded(){
        return loaded;
    }

    public void setContentsLoad(boolean value){
        loaded = value;
    }

    public int getSongEndTick(){
        //return jniCall.songTotalLine();
        if(!isContentsLoaded()) return -1;
        return ltime_SongEnding;
    }

    public boolean isTickEnd(){
        return (getCurrentTick() >= getSongEndTick());
    }

    public boolean stopMIDI(){
        if(!isContentsLoaded()) return false;
        System.out.printf("#@####################xxx>>>>>> Stop %n");
        MIDIUnLoad();
        setContentsLoad(false);
        MIDITempoAdj = 0;
        m_flag_midithread = false;
        return true;
    }

    public void DrawStaff() {
        System.out.printf("#@#-------DrawStaff -------------------\n");
        //jypark
        //if(Common.m_midi_func_sheet) BmStaff = SDraw.staff_draw();
        //if(Common.m_midi_func_score) SCORE_Draw.SCOREBOX_DrawBar2(); //jypark
        staff_ready = true;
    }

    public void update_current_status()
    {
        int currentTick=getCurrentTick();
        JNICurrentEraseLine = jniCall.MIDIGetLyricLine(1);  //EraseLine

        if(LyricChangedLine ==-3) {
            exDrawUpDnMode=6;//jump case
            LyricChangedLine =0;
        }

       // if(Common.m_midi_func_score || Common.m_midi_func_sheet)
        {
            CurrCursor=jniCall.MIDIGetLyricLine(4);//cursor Xpos
            ScoreCurPage = jniCall.MIDIGetLyricLine(3);
            ScorePosX = jniCall.DrawScoreGetPosX(currentTick);
           // System.out.printf("#@## currentTick[%d] ScoreCurPage[%d] ScoreOldPage[%d] ScorePosX[%d]\n", currentTick,ScoreCurPage, ScoreOldPage, ScorePosX );
            if(ScoreOldPage != ScoreCurPage)
            {
                ScoreOldPage=ScoreCurPage;
                 if(ScoreCurPage < 0 ) {
                 /////   BmStaff =null;
                    CurrCursor=0;//
                }
                else
                {
                    System.out.printf("#@#DBG_SCORE 악보그리기 [SCORE_BOX_ENABLE]  Draw_Score.ScoreCurPage [%d] curTick[%d] \n", ScoreCurPage, currentTick);
                    jniCall.DrawScorePage(ScoreCurPage );//악보준비
                }
            }
            //saff_line=jniCall.MIDIGetLyricLine(5);
//            if(jniCall.MIDIGetLyricLine(5)==2){
//                System.out.printf("#@#밑에서 악보 준비 되어 있음  ScoreCurPage[%d]-------------------\n", ScoreCurPage);
//                jniCall.MIDIGetLyricLine(6);//StaffReady Flag_reset
//                DrawStaff();
//
//            }

            if(staff_ready){
                /////if(Common.m_midi_func_sheet) StaffView.staffview.postInvalidate(); //자막 바꿈
                staff_ready=false;
            }
        }

        //if(Common.m_midi_func_score) {
        if(true) {
            //example note 64; 0-127  KeytoLineNo(note, keysignature)
            int midi_status= jniCall.MIDIGetCurrentScoreKey();
            score_key=(midi_status>>24)&0xff; //mid key
            score_velocity=(midi_status>>16)&0xff; //Mic line number
            mic_key=(midi_status>>8)&0xff; //mic key
            mic_velocity=midi_status & 0xff;//Mic Velocity==> 원래는 밑에서 받으로 했는데 포기했음
            // System.out.printf("#@#마이크[%x] score_key[%x]  score_velocity[%x] mic_key[%x]  mic_velocity[%x]\n",midi_status, score_key, score_velocity,mic_key,mic_velocity);

           // int currentTick = getCurrentTick();
            if(ltime_SongEnding < currentTick)
            {
                // BmScore=null;
                if(score_ending) return;
                score_ending=true;
            }

            //jypark
//            boolean testmode = false;
//            if(testmode) {
//                int rantest = 0;
//                rantest = IBUtil.getRandom(0, 3);
//                mic_key = score_key + rantest - 12;
//                mic_velocity = 100;
//            }
        }

//        if(Common.m_midi_func_sheet) {
//            if (GlobalValue.ScoreMode != -1) {
//                if (CurrCursor > 200 && CurrCursor < 10000) {
//                    seekBar1.setVisibility(View.VISIBLE);
//                    seekBar2.setVisibility(View.INVISIBLE);
//                    seekBar1.setProgress(CurrCursor - 200);
//                } else if (CurrCursor > 10000 && CurrCursor < 11920) {
//                    seekBar1.setVisibility(View.INVISIBLE);
//                    seekBar2.setVisibility(View.VISIBLE);
//                    seekBar2.setProgress(CurrCursor - 10000 - 200);
//                } else {
//                    seekBar1.setVisibility(View.INVISIBLE);
//                    seekBar2.setVisibility(View.INVISIBLE);
//                }
//            }
//        }

    }

    public int getCurrentTick()
    {
       //// if(jniCall.MIDIIsPlay() == 0 || !isContentsLoaded()) return -1;
        return jniCall.MIDIGetCurrentTick();
    }

    public JniFunction getJniCall(){
        return jniCall;
    }


    public int getCurrentLineIndex(){
        if(!isContentsLoaded() || jniCall.MIDIIsPlay() == 0) return -1;
        return jniCall.MIDIGetLyricLine(0);     //DisplayLine;
    }

    public int MIDIIsPlay(){
        return jniCall.MIDIIsPlay();
    }

    public boolean process_tick()
    {
        if(jniCall.MIDIIsPlay() == 0 )  {
            setMidiEnd(true);
            return false;
        }

        int currentTick = getCurrentTick();
        if(currentTick == exCurrentTick) return false;
        exCurrentTick= currentTick;
        return true;
    }

    public int[] getKeyBuffer()
    {
        int[] keybuf;
        keybuf=jniCall.readFrontKeyBuffer();
        return keybuf;
    }


    public void MIDIPlay(){
        jniCall.GetlongValue(0xf0);//put clock_stop;
        jniCall.GetlongValue(0xf2);//put clock_Reset;
        MIDI_MasterVolume(0x7f);
        jniCall.GetlongValue(0xf1);//put clock_start;
        m_flag_midithread = true;   // 미디 소리가 나고 있다는 플래글 설정하자.
        //jniCall.JNIThread(1);
    }

    @Override
    public void release() {
        MIDIUnLoad();
        setAvilable(false);
        Line_BlkCnt = null;
        Line_timeA = null;
        Line_timeB = null;
        updn = null;
        sex = null;
        JULL_Pos = null;
        CountDn_Pos = null;
        //jniCall release
    }

    public OneLine getLine(int line){
        if(lines == null || lines.size() <= line) return null;
        return lines.get(line);
    }

    public OneLine PutCharPos(JniFunction jniCall, int line)
    {
        int[] Blk_timeA = jniCall.MIDIGetLineBaseBlkTimeA(line);
        int[] Blk_timeB = jniCall.MIDIGetLineBaseBlkTimeB(line);
        String[] strBaseBlk = jniCall.MIDIGetLineBaseBlk(line);
        ////////
        String[] strRubyBlk = jniCall.MIDIGetLineRubyBlk(line);
        OneLine oneLine = PutCharPos(line, Blk_timeA, Blk_timeB, strBaseBlk, strRubyBlk);
        if(oneLine != null){
            jniCall.MIDISetLineTextTime(oneLine.Blk_BaseTxtPosB, line, oneLine.Line_TextPosB );//각블럭 텟스트 끝점을 pDoc->ProcessLine[line].Blk_lBpoint[]에 넣음
        }
        return oneLine;
    }

    private OneLine PutCharPos(int line, int[] Blk_timeA, int[] Blk_timeB, String[] strBaseBlk, String[] strRubyBlk)
    {
        OneLine oneLine = new OneLine();
        int i;
        String imsi;
        String str;
        //char imsiChar[];
        oneLine.Line_timeA = Line_timeA[line];
        oneLine.Line_timeB = Line_timeB[line];
        oneLine.Line_BlkCnt = Line_BlkCnt[line];
        oneLine.Blk_timeA = Blk_timeA;
        oneLine.Blk_timeB = Blk_timeB;

        oneLine.strBaseBlk= strBaseBlk;
        oneLine.strRubyBlk= strRubyBlk;

        if(oneLine.Line_BlkCnt==0) return oneLine;
        str="";
        for(i=0;i<oneLine.Line_BlkCnt;i++)
        {
            imsi=oneLine.strBaseBlk[i];
            str=str+imsi;
        }
        oneLine.str = str;

        return oneLine;
    }


}

