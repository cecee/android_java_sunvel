package com.cecee.imove;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class JniFunction {

    public MIDIManager pMidiManager;
    public AudioTrack chorus_track;
    public boolean ChorusDisable = false;

    static {
        System.loadLibrary("Jnilib");
    }
    Toast toast;
    int MidiEndingValueFromJNI = -1;


    public static void QueryFromC(int arg){
        String query_msg;
        System.out.printf("#@# QueryFromC_0: %x\n",arg);
        if(arg==0) {
            JniFunction jj=new JniFunction();
            query_msg = jj.MessageFromC("");
            query_msg = query_msg.replace("\n","");
            System.out.printf("#@#QueryFromC ==>[%s]\n", query_msg);
        }
    }
    public  void FromJNIThread(int arg) {
        int nextltimeA, ltimeB, timeGap;
        //  if(pMidiManager.loaded==false) return;
        // JniFunction JniCLSS=new JniFunction();
        if(arg > 0xc0 && arg <0xdf) {
            System.out.printf("#@#chorus==FromJNIThread: XXXX---[%x]----ChorusDisable[%b]\n",arg, ChorusDisable );
            if(ChorusDisable==false) PCM_Effect(arg);
        }
        switch(arg){
            case 1:
                break;
            case 2:
                // pMidiManager.MidiEnding = 1;    // 조차장이 쓰레드 세이프 하지 않다고 지적한 부분. // 곡종료 로직은  [ 전체 틱 < 현재 틱 ] 으로도 가능.
                MidiEndingValueFromJNI = 1;
                break;
            case 0xfc:
                float chorusVolume = chorusGetVolume();
                if(chorusVolume<0){
                    ChorusDisable=true;
                    PCM_EffectStop();
                }
                else{
                    PCM_EffectVolume(chorusVolume);
                    ChorusDisable=false;
                }

                break;
        }
    }

    public void PCM_Effect(int idx){
        String file;
        file = String.format("/sdcard/%X", idx);
        System.out.printf("#@#chorus PCM_Effect file[%s]  ====== \n",file);
        ByteBuffer bbx = readFile(file);
        System.out.printf("#@#Chorus file[%s]  capacity[%d] \n",file, bbx.capacity());
        playSound(bbx);//test
    }


    public void PCM_EffectStop(){
        Log.i("#@#chorus", "chorus_track["+chorus_track+"]");
        if(chorus_track==null)return;
        System.out.printf("#@#chorus PCM_EffectStop  ====== \n");
        chorus_track.stop();
        chorus_track.release();
        chorus_track=null;
    }

    public void PCM_EffectVolume(float volume){
        Log.i("#@#chorus", "chorus_track["+chorus_track+"] volume["+volume+"]");
        if(chorus_track==null) return;
        chorus_track.setStereoVolume(volume, volume);
       // chorus_track.setVolume(volume);
        System.out.printf("#@# PCM_EffectVolume  ====== \n");
    }

    public ByteBuffer readFile(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            FileChannel fileChannel = fis.getChannel();
            int fileSize = (int) fileChannel.size();
            ByteBuffer bb = ByteBuffer.allocateDirect(fileSize);
            int bytesRead = fileChannel.read(bb);
            if (bytesRead != -1) {
                bb.flip();
                return bb;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void playSound(ByteBuffer bb) {
        final int SAMPLE_RATE = 44100;
        int sample_len = bb.capacity();
        if(chorus_track !=null){
            Log.i("#@#chorus", "NOT NULL!!!!!!!!!!!!!!");
            chorus_track.stop();
            chorus_track.release();
        }
        chorus_track = new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE_RATE, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, sample_len, AudioTrack.MODE_STATIC);
        chorus_track.write(bb.array(), bb.arrayOffset(), sample_len);
        chorus_track.play();
        //chorus_track.setStereoVolume(0.1f, 0.1f);
    }

    public native  void JNIThread (int start_flag);
    public native String MessageFromC(String str);
    public native int MIDIOpenSerial(int arg);//1 open 0: close
    public native int MIDICloseDevice();

    public native int MIDIOpen(String path, int LyricCode, int key, int speed, int MelodyVol);//ppp
    public native int[] MIDIGetLineBlkCnt();
    public native int[] MIDIGetLineUpDn();
    public native int[] MIDIGetLineSex();
    public native int[] MIDIGetLineTimeA();
    public native int[] MIDIGetLineTimeB();
    public native int[] MIDIGetJullPosA();//@F@ Track에 마킹된 pos
    public native int[] MIDIGetCountDnPos();
    public native int[] MIDIGetCountDnBakja();


    public native String[] MIDIGetTitle();
    public native String[] MIDIGetLineBaseBlk(int line);
    public native String[] MIDIGetLineRubyBlk(int line);
    public native int[] MIDIGetLineBaseBlkTimeA(int line);
    public native int[] MIDIGetLineBaseBlkTimeB(int line);


    public native int MIDIUnload();

    //public native String GetMidiTxtTitle();
    public native int MIDIGetCurrentTick();//current time tick
    public native int MIDIGetCurrentTime();//current time millisecond
    public native int MIDIGetCurrentPage();
    public native int MIDIGetCursorPos();
    public native int DrawScoreGetPosX(int tick);
    public native int DrawScorePage(int page);
    public native int DrawScoreIsReady();
    public native int DrawScoreClearReady();

    public native String[] GetStaffStringArray();
    public native String[] GetScoreStringArray();




    public native int MIDIGetCurrentScoreKey();
    public native int TimeToPosition(int line, int ltime);

    public native int MIDIGetLyricLine(int flg);//0: DisplayLine  1:EraseLine
    public native int MIDISetSpeed(int speed); //one unit +-5% Adjust
    public native int MIDISetJumpPostion(int pos);//-1: 앞줄  1: 뒷줄  0: 전주건너뛰고(가사이전 한마디부터) 2:A-B의 A pos  3:A-B의 Bpos 4: A-B Clear
    public native int MIDISetNRPN(int add, int data);
    public native int MIDISetSysKey(int key);
    public native int MIDIGetSfMi();
    public native int MIDIGetKey();

    //   public native int[] MIDIGetLyricLineSex();
    //   public native short[] MIDIGetLineStr(int line, int sz);
    // public native int   MIDIGetTitleOffTime();

    // public native int[] MIDIGetLineBaseBlkTimeA(int line);
    // public native int[] MIDIGetLineBaseBlkTimeB(int line);
    public native int songLyricCode();
    public native int songEndTick();
    public native int songTotalLine();
    public native int madiPrev();
    public native int madiNext();
    public native int ganjuJump();//간주점프
    public native int firstVerse(int enable);//1절연주 0:disable [취소] 1:enable
    public native int verseJump();//절점프
    public native int rhythmChange(int rhythm);//리듬변환

    public native int writerNfcBuf(byte[] buf);//writer nfc max 128byte
    public native int[] readNfcBuf();
    public native int[] readFrontKeyBuffer();

    public native String getFrontVersion();//get FrontVersion

    public native int GetlongValue(int idx); //idx 0:current tickCnt

    public native int MIDISetMelodyChVolume(int volume);
    public native int MIDISetLineTextTime(int[] TextTime, int line, int textLength);
    public native int MIDIIsPlay();
    public native int MIDISeek(int tick);

    public native void pcmEnable(int enable);
    public native void chorusSetVolume(float volume);
    public native float chorusGetVolume();

   // public native void stopEngine();
   // public native void pcmStop();
   // public native void pcmPause();
   // public native void pcmTrackMute(int track, boolean mute);
   // public native void testPlay(int kind);
   // public native ByteBuffer pcmGetBuffer(int idx);
  // public native String[] pcmGetUrl();

    public native int[] cocoIsMessage();
    public native void cocoRespCmd(int id, int cmd);
    public native void cocoRespData(int id, int cmd, int d0,int d1,int d2,int d3,int d4);

    public native int TESTGetBirthDay(int flg);//0: Month  1:Date  <== // j.kim -- test 빌드되나 + 작동하나 테스트
    public native String TESTGetName(); // return KimJinYong <== // j.kim -- test 빌드되나 + 작동하나 테스트

    // j.kim 추가.
    //public native int[] returnedLongArr(int kind, int idx, int sz); // 지정한 블럭의 시작점 정보 가져옴.

}
