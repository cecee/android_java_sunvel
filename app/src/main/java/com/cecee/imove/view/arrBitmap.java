package com.cecee.imove.view;

import android.graphics.Bitmap;

import com.cecee.imove.data.BaseData;

public class arrBitmap extends BaseData {
    public int updn;
    public int lineNum;
    public int lineSex;
    public int paintingLength = 0;
    public boolean align = false;
    public Bitmap dispBitmap;
    public Bitmap paintBitmap;

    @Override
    public void release(){
        setAvilable(false);
        if(dispBitmap != null && !dispBitmap.isRecycled()){
            dispBitmap.recycle();;
            dispBitmap = null;
        }

        if(paintBitmap != null && !paintBitmap.isRecycled()){
            paintBitmap.recycle();
            paintBitmap = null;
        }

        paintingLength = 0;
        align = false;

    }
}
