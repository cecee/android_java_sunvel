package com.cecee.imove.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by shootdol on 2017-01-18.
 */

public class KaraokeVideoView extends SurfaceView implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {
//public class KaraokeVideoView extends SurfaceView implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {
    protected final String TAG = getClass().getSimpleName();

    private String mUrl;
    private MediaPlayer mediaPlayer;
    private volatile boolean surfaceCreated;

    private int mSeekTo = 0;

    private PlaybackThread playbackThread;
    private final Object playbackLock = new Object();

    public KaraokeVideoView(Context context) {
        super(context);
        init();
    }

    public KaraokeVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        //setZOrderOnTop(true); //always on Top
        getHolder().addCallback(this);
        getHolder().setFormat(PixelFormat.TRANSPARENT);
        mediaPlayer = new MediaPlayer();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceCreated = false;
        //mediaPlayer.release();
        Log.d(TAG, "surfaceDestroyed!!!!");
        stop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceCreated = true;

        if (playbackThread != null) {
            synchronized (playbackLock) {
                playbackLock.notifyAll();
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged!!!!");
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.d(TAG, "getDuration!!!!!!!!!!!!!  - " + mediaPlayer.getDuration());
        if(mSeekTo == -1) { // random seek for bgv
            //int rseek = IBUtil.getRandom(0, mediaPlayer.getDuration());
            //Log.d(TAG, "random seek to !!!!!!!!!!!!!  - " + rseek);
           // mediaPlayer.seekTo(rseek);
        } else if(mSeekTo > 0) {
            mediaPlayer.seekTo(mSeekTo);
        }
        mediaPlayer.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion !!!!!!!!!");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG, "onDraw!!!!!!!!!");
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

    public void load(String url, int msec) {
        mUrl = url;
        mSeekTo = msec;
        playbackThread = new PlaybackThread();
        playbackThread.start();
    }

    public void setLooping(boolean loop){
        if(mediaPlayer == null) return;
        mediaPlayer.setLooping(true);
    }

    private void play() {
        mediaPlayer.setDisplay(getHolder());

        if (mUrl == null || mUrl.equals("")) {
            return;
        }

        try {
            mediaPlayer.setDataSource(mUrl);
            mediaPlayer.setOnPreparedListener(this);
            //mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.prepareAsync();
            Log.d(TAG, "mUrl - " + mUrl);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Log.d(TAG, "play()1 - " + e.getMessage());
        } catch (IllegalStateException e) {
            e.printStackTrace();
            Log.d(TAG, "play()2 - " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "play()3 - " + e.getMessage());
        }
    }

    private class PlaybackThread extends Thread {
        @Override
        public void run() {
            super.run();

            if (!surfaceCreated) {
                synchronized (playbackLock) {
                    try {
                        playbackLock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            play();
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void pause() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer.pause();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            } else {
                resume();
                Log.d(TAG, "Playback has already stopped. Pause ignored");
            }
        }
    }

    public boolean isPlaying(){
        if(mediaPlayer == null) return false;
        return mediaPlayer.isPlaying();
    }

    public void resume() {
        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                try {
                    mediaPlayer.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            } else {
                Log.d(TAG, "Playback already in progress. Resume ignored");
            }
        }
    }

    public void stop() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop();
                mediaPlayer.reset();
                //mediaPlayer.release();
                //mediaPlayer = null;
                //mediaPlayer = new MediaPlayer();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void  nextFile(String url, int msec){
        try {
            stop();
            load(url, msec);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void setVolume (float leftVolume, float rightVolume) {
        mediaPlayer.setVolume(leftVolume, rightVolume);
    }

//    public void setVocalKara(int vocal){
//        try{
//            if (mediaPlayer != null) {
//                if (mediaPlayer.isPlaying()) {
//                    if (getAudioTrackCount() > 2) { //Audio 2 Tracks (Default Track1 Track2)
//                        if (vocal == 0) mediaPlayer.selectTrack(1);
//                        else mediaPlayer.selectTrack(2);
//                    } else { // Left Right vocal kara
//                        if (vocal == 0) mediaPlayer.setVolume(0.f, 1f);
//                        else mediaPlayer.setVolume(1f, 0.f);
//                    }
//                    //Common.m_vocal = Common.m_vocal^1;
//                }
//            }
//
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }
//    }

//    public int  getAudioTrackCount(){
//        int numTracks = -1;
//        try {
//            MediaExtractor extractor = new MediaExtractor();
//            extractor.setDataSource(mUrl);
//            numTracks = extractor.getTrackCount();
//            Log.d(TAG, "Audio Tracks : " + numTracks);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return numTracks;
//    }

    public void prevSec(int sec) {
        int currentTime, totalTime;
        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    totalTime = mediaPlayer.getDuration() / 1000;
                    currentTime = mediaPlayer.getCurrentPosition() / 1000;
                    currentTime = currentTime - sec;
                    if (currentTime < 0) currentTime = 0;
                    mediaPlayer.seekTo(currentTime * 1000);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void nextSec(int sec) {
        int currentTime, totalTime;
        try {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    totalTime   = mediaPlayer.getDuration() / 1000;
                    currentTime = mediaPlayer.getCurrentPosition() / 1000;
                    currentTime = currentTime + sec;
                    if(currentTime > totalTime) currentTime = totalTime;
                    mediaPlayer.seekTo(currentTime * 1000);
                }
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPosition() {
        int msec = 0;
        try {
            if (mediaPlayer != null) {
                msec = mediaPlayer.getCurrentPosition();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return msec;
    }

    public int getDuration() {
        int msec = 0;
        try {
            if (mediaPlayer != null) {
                msec = mediaPlayer.getDuration();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        return msec;
    }

    public void seekTo(int msec) {
        int totalTime;
        try {
            if (mediaPlayer != null) {
                totalTime   = mediaPlayer.getDuration();
                if(msec > totalTime) msec = totalTime;
                mediaPlayer.seekTo(msec);
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
