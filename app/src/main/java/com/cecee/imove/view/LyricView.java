package com.cecee.imove.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.cecee.imove.MIDIManager;
import com.cecee.imove.lyric.CeceeLabLyricManager;

import java.util.ArrayList;
import java.util.List;

public class LyricView extends SurfaceView implements SurfaceHolder.Callback{
    protected final String TAG = getClass().getSimpleName();
    //private Display display;
    private Paint paint;

    private SurfaceHolder holder;
    private RenderThread thread;
    private int nullFlag=0;
    private Bitmap LyricBitmap;
    private Canvas canvas;
    private Rect src;
    private Rect dsc;

    private int paintingUpdn = -1;

    private volatile boolean stop = false;
    private MIDIManager pMidiManager;
    CeceeLabLyricManager lyricManager;
    List<LyricLineBitmapInfo> lines;
    final int UP = 0;
    final int DOWN = 1;

    public LyricView(Context context, MIDIManager pMidiManager, CeceeLabLyricManager lyricManager){
        super(context);
        this.pMidiManager = pMidiManager;
        this.lyricManager = lyricManager;
        init();
    }

    private void init() {
        lines = new ArrayList<>();
        lines.add(new LyricLineBitmapInfo());
        lines.add(new LyricLineBitmapInfo());

        LyricBitmap = Bitmap.createBitmap(1920, 400, Bitmap.Config.ARGB_4444);
        canvas = new Canvas(LyricBitmap);
        src=new Rect();
        dsc=new Rect();
        paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        holder = getHolder();
        holder.addCallback(this);
    }

    public void release(){
        stop = true;
        if(thread != null && thread.isAlive()) {
            thread.release();
            thread = null;
        }

        if(LyricBitmap != null && !LyricBitmap.isRecycled()){
            LyricBitmap.recycle();
            LyricBitmap = null;
            canvas = null;
        }

        for(LyricLineBitmapInfo bitmap : lines)
            bitmap.release();
        lines.clear();

    }

    public void surfaceCreated(SurfaceHolder holder){
        if(thread != null && thread.isAlive()) {
            thread.release();
            thread = null;
        }

        thread = new RenderThread();
        thread.start();
    }

    public void surfaceChanged(SurfaceHolder holder, int a, int b, int c){
    }

    public void surfaceDestroyed(SurfaceHolder holder){
        release();
    }

    class RenderThread extends Thread{
        @Override
        public void run() {
            super.run();
            try {
                while ( !isInterrupted() && !stop) {
                    Canvas c = null;
                    try {
                        Thread.sleep(20);
                        c = holder.lockCanvas(null);
                        synchronized (holder) {
                            doDraw(c);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        if (c != null) {
                            holder.unlockCanvasAndPost(c);
                        }
                    }

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        public void release(){
            if(isAlive()) interrupt();
        }
    }

    volatile int wLyricBox = 0;
    volatile int hLyricBox = 0;
    volatile int currentTick = 0;
    volatile int ltime_SongEnding = 0;

    void update(int ltime_SongEnding, int currentTick, int wLyricBox, int hLyricBox){
        this.currentTick = currentTick;
        this.ltime_SongEnding = ltime_SongEnding;
        this.wLyricBox = wLyricBox;
        this.hLyricBox = hLyricBox;
    }

    volatile int yPosUpA, yPosUpB, yPosDnA, yPosDnB;
    void update2(int yPosUpA, int yPosUpB, int yPosDnA, int yPosDnB){
        this.yPosUpA = yPosUpA;
        this.yPosUpB = yPosUpB;
        this.yPosDnA = yPosDnA;
        this.yPosDnB = yPosDnB;
    }

    public void doDraw(Canvas c){
        if(stop) return;
        try {
            int endTick = pMidiManager.getSongEndTick();
            update(endTick, pMidiManager.getCurrentTick(), lyricManager.wLyricBox, lyricManager.hLyricBox);
            update2(lyricManager.yPosUpA, lyricManager.yPosUpB, lyricManager.yPosDnA, lyricManager.yPosDnB);

            nullFlag = 0;
            if (c != null) {
                c.drawRect(0, 0, wLyricBox, hLyricBox, paint);//두줄다 Clear
                if (nullFlag == 1) return;
                if(currentTick <0) return;
                if (ltime_SongEnding < currentTick) {
                    nullFlag = 1;
                    return;
                }
                mkLyricBitmap();

                if (LyricBitmap != null && !LyricBitmap.isRecycled())
                    c.drawBitmap(LyricBitmap, 0, 0, null);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void mkLyricBitmap()
    {
        int xpos = lyricManager.CurrErXpos;
        if(xpos<=0) xpos=1;

        if(lines.size() < 2) return;

        lines.get(UP).setDisplayBitmap(lyricManager.DispBitmapArray[0]);
        lines.get(DOWN).setDisplayBitmap(lyricManager.DispBitmapArray[1]);

        final boolean notAvailable = lines.get(UP).IsNotAvaileDisplayBitmap() && lines.get(DOWN).IsNotAvaileDisplayBitmap();

        if(notAvailable){
            canvas.drawRect(0, 0, wLyricBox, hLyricBox, paint);//전부
            lyricManager.Event_BringFront = -1;
            return;
        }

         if(lyricManager.Event_30P) {
             lyricManager.Event_30P = false;
            // IBUtil.Log(TAG, "#@#(xx)Event_30P----------------------paintingUpdn[" + paintingUpdn + "] ulist.ganju[" + Common.pMidiManager.ulist.ganju + "]  Event_BringFront["+Common.pMidiManager.Event_BringFront+"]");
            if (!lyricManager.ulist.align) {//간주중이 아닐때만 30%에서 담 문장 그림
                if (paintingUpdn == 0) {
                    canvas.drawRect(0, yPosDnA, wLyricBox, hLyricBox, paint);//두번째줄 시작에서 박스끝까지 BoxClear
                    lines.get(DOWN).drawLine(canvas, 0, yPosDnA);
                }else if(paintingUpdn == 1)
                {
                    canvas.drawRect(0, yPosUpA, wLyricBox, yPosDnA, paint);////윗줄 시작에서 두번째줄 시작 까지
                    lines.get(UP).drawLine(canvas, 0, yPosUpA);
                }
            }
        }

        if(lyricManager.Event_BringFront >= 0)
        {

            Log.d(TAG, "#@#(1)Event_BringFront ["+lyricManager.Event_BringFront+"]   paintingUpdn["+paintingUpdn+"]");
            if(lyricManager.Event_BringFront == 2 )
            {
                paintingUpdn = 0;
                canvas.drawRect(0, 0, wLyricBox, hLyricBox, paint);//전부
                lines.get(UP).drawLine(canvas, 0, yPosUpA);
                lines.get(DOWN).drawLine(canvas, 0, yPosDnA);
                xpos = 1;
            }
            else if(lyricManager.Event_BringFront == 3 )//현재 윗줄에서 앞으로가는 조건
            {
                paintingUpdn = 1;
                canvas.drawRect(0, 0, wLyricBox, hLyricBox, paint);//전부
                lines.get(UP).drawLine(canvas, 0, yPosUpA);
                lines.get(DOWN).drawLine(canvas, 0, yPosDnA);
                xpos = 1;
            }
            else
            {
                paintingUpdn = lyricManager.Event_BringFront;
                if (paintingUpdn == 0) {
                    if(lyricManager.DispBitmapArray[1]==null){
                        canvas.drawRect(0, 0, wLyricBox, hLyricBox, paint);//전부
                    }
                    else
                        canvas.drawRect(0, yPosUpA, wLyricBox, yPosDnA, paint);////윗줄 시작에서 두번째줄 시작 까지
                    lines.get(UP).drawLine(canvas, 0, yPosUpA);
                } else {
                    canvas.drawRect(0, yPosDnA, wLyricBox, hLyricBox, paint);//두번째줄 시작에서 박스끝까지 BoxClear
                    lines.get(DOWN).drawLine(canvas, 0, yPosDnA);
                }

            }
            lyricManager.Event_BringFront = -1;
            Log.d(TAG, "#@#(2)Event_BringFront ["+lyricManager.Event_BringFront+"]   paintingUpdn["+paintingUpdn+"]");

        }

        boolean DrawUp = (paintingUpdn == 0);
        //   Log.d(TAG, "#@#(3x)Event_BringFront xpos["+xpos+"]   paintingUpdn["+paintingUpdn+"]");
        src.set(0, 0, xpos, lyricManager.hOneLine);

        final int TargetLine = DrawUp ? UP : DOWN;
        if(lines.size() >1) {
            LyricLineBitmapInfo target = lines.get(TargetLine);
            target.setEraseBitmap(lyricManager.EraseBitmapArray[TargetLine]);
            target.setRect(0, TargetLine == UP ? yPosUpA : yPosDnA, xpos, TargetLine == UP ? yPosUpB : yPosDnB);
            target.drawEraseLine(canvas, src);
        }
    }

    class LyricLineBitmapInfo{
        Bitmap eraseBitmap;
        Bitmap displayBitmap;
        Rect rect;

        public LyricLineBitmapInfo(){
            init();
        }

        void init(){
            rect = new Rect();
        }

        public boolean IsNotAvaileDisplayBitmap(){
            return (displayBitmap == null || displayBitmap.isRecycled());
        }

        public void drawLine(Canvas canvas, int x, int y){
            try{
                if (canvas == null || displayBitmap == null || displayBitmap.isRecycled()) return;
                canvas.drawBitmap(displayBitmap, x, y, null);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        public void drawEraseLine(Canvas canvas, Rect src){
            try {
                if (canvas == null || eraseBitmap == null || eraseBitmap.isRecycled()) return;
                canvas.drawBitmap(eraseBitmap, src, rect, null);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        public Bitmap getEraseBitmap() {
            return eraseBitmap;
        }

        public void setEraseBitmap(Bitmap eraseBitmap) {
            this.eraseBitmap = eraseBitmap;
        }

        public Bitmap getDisplayBitmap() {
            return displayBitmap;
        }

        public void setDisplayBitmap(Bitmap displayBitmap) {
            this.displayBitmap = displayBitmap;
        }

        public Rect getRect() {
            return rect;
        }

        public void setRect(int left, int top, int right, int bottom){
            rect.set(left, top, right, bottom);
        }

        public void setRect(Rect rect) {
            this.rect = rect;
        }

        public void release(){
            if(eraseBitmap != null && !eraseBitmap.isRecycled()) {
                eraseBitmap.recycle();
                eraseBitmap = null;
            }

            if(displayBitmap != null && !displayBitmap.isRecycled()) {
                displayBitmap.recycle();
                displayBitmap = null;
            }
        }
    }

}
