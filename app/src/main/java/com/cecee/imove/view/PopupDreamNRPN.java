package com.cecee.imove.view;

import android.app.Dialog;
import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.cecee.imove.JniFunction;
import com.cecee.imove.R;
import com.danalenter.karaoke.dalkommpartykaraoke.setting.DKDefaultSetting;
import com.danalenter.karaoke.dalkommpartykaraoke.view.DKSettingData;
import com.danalenter.karaoke.dalkommpartykaraoke.view.DKVerticalSeekBar;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class PopupDreamNRPN extends Dialog {
    protected final String TAG = getClass().getSimpleName();

    private Context m_context;
    private JniFunction m_jni;

    private View dv;

    int nrpn_addr;
    int nrpn_data;
    DKSettingData dkSettingData;

    TextView mTvNrpn;

    // Midi Eq Contorols
    SeekBar mSbMidiEqLow, mSbMidiEqHigh;
    TextView mTvMidiEqLow, mTvMidiEqHigh;

    // Music Input Contorols
    SeekBar mSbMusciLv, mSbMusciShift;
    TextView mTvMusicLv, mTvMusicShift;

    // Echo Type
    SeekBar mSbEchoType;
    TextView mTvEchoType;

    // Echo Controls
    Switch mSwEchoCtrl1;
    SeekBar mSbEchoCtrl2, mSbEchoCtrl3;
    TextView mTvEchoCtrl1, mTvEchoCtrl2, mTvEchoCtrl3;

    // Output Mix Controls
    SeekBar mSbMicVol, mSbMusicVol, mSbAuxVol, mSbMasterVol;
    TextView mTvMicVol, mTvMusicVol, mTvAuxVol, mTvMasterVol,mTvMix1, mTvMix2;
    RadioGroup mRg1, mRg2;

    // System Controls
    Switch mSwSystemCtrl1, mSwSystemCtrl2, mSwSystemCtrl4;
    Button mBtSystemCtrl3, mBtSystemCtrl5, mBtSystemCtrl6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_dreamnrpn);
        dv = getWindow().getDecorView();

        init();
    }

    public PopupDreamNRPN(Context context, JniFunction jni) {
        super(context , android.R.style.Theme_Translucent_NoTitleBar);
        m_context = context;
        m_jni = jni;
    }

    private void init() {
        setLayout();
        setListener();

        Log.d(TAG, "#@#PopupDreamNRPN init()...");
    }

    private void setListener(){
        mSbMidiEqLow.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMidiEqLow.setText("" + progress);
                nrpn_addr = 0x3708;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMidiEqHigh.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMidiEqHigh.setText("" + progress);
                nrpn_addr = 0x370B;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMusciLv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMusicLv.setText(""+progress);
                progress = progress+116;
                nrpn_addr = 0x0600;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMusciShift.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int v = 0;
                if(progress < 6) {
                    v = progress - 6;
                    progress = progress+58;
                } else if(progress >= 6) {
                    v = progress - 7;
                    progress = progress+57;
                }
                mTvMusicShift.setText("" + v);
                nrpn_addr = 0x0601;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbEchoType.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvEchoType.setText("" + progress);
                nrpn_addr = 0x0200;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSwEchoCtrl1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int v = 0;
                if(isChecked) v = 1;
                else v = 0;
                mTvEchoCtrl1.setText("" + v);
                nrpn_addr = 0x0204;
                nrpn_data = v;
                SendingNRPN();
            }
        });

        mSbEchoCtrl2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvEchoCtrl2.setText("" + progress);
                nrpn_addr = 0x0205;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbEchoCtrl3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvEchoCtrl3.setText("" + progress);
                nrpn_addr = 0x0206;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMicVol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMicVol.setText("" + progress);
                nrpn_addr = 0x0100;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMusicVol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMusicVol.setText("" + progress);
                nrpn_addr = 0x0101;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbAuxVol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvAuxVol.setText("" + progress);
                nrpn_addr = 0x0102;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mSbMasterVol.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {}
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTvMasterVol.setText("" + progress);
                nrpn_addr = 0x0103;
                nrpn_data = progress;
                SendingNRPN();
            }
        });

        mRg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                nrpn_addr = 0x0104;
                switch (checkedId){
                    case R.id.RGOutputMix51:
                        nrpn_data = 0;
                        mTvMix1.setText("Mic+MR");
                        break;
                    case R.id.RGOutputMix52:
                        nrpn_data = 1;
                        mTvMix1.setText("Mic only");
                        break;
                    case R.id.RGOutputMix53:
                        nrpn_data = 2;
                        mTvMix1.setText("MR only");
                        break;
                    case R.id.RGOutputMix54:
                        nrpn_data = 3;
                        mTvMix1.setText("AUX only");
                        break;
                }
                SendingNRPN();
            }
        });

        mRg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                nrpn_addr = 0x0105;
                switch (checkedId){
                    case R.id.RGOutputMix61:
                        nrpn_data = 0;
                        mTvMix2.setText("Mic+MR");
                        break;
                    case R.id.RGOutputMix62:
                        nrpn_data = 1;
                        mTvMix2.setText("Mic only");
                        break;
                    case R.id.RGOutputMix63:
                        nrpn_data = 2;
                        mTvMix2.setText("MR only");
                        break;
                    case R.id.RGOutputMix64:
                        nrpn_data = 3;
                        mTvMix2.setText("AUX only");
                        break;
                }
                SendingNRPN();
            }
        });

        mSwSystemCtrl1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int v = 0;
                if(isChecked) v = 1;
                else v = 0;
                nrpn_addr = 0x3980;
                nrpn_data = v;
                SendingNRPN();
            }
        });

        mSwSystemCtrl2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int v = 0;
                if(isChecked) v = 1;
                else v = 0;
                nrpn_addr = 0x3981;
                nrpn_data = v;
                SendingNRPN();
            }
        });

        mSwSystemCtrl4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int v = 0;
                if(isChecked) v = 1;
                else v = 0;
                nrpn_addr = 0x3983;
                nrpn_data = v;
                SendingNRPN();
            }
        });

        mBtSystemCtrl3.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                nrpn_addr = 0x3982;
                nrpn_data = 0;
                SendingNRPN();
            }
        });

        mBtSystemCtrl5.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                nrpn_addr = 0x3984;
                nrpn_data = 0;
                SendingNRPN();
            }
        });

        mBtSystemCtrl6.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                dialogClose();
            }
        });
        findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });
    }

    void reset(){
        if(dkSettingData != null) {
            DKDefaultSetting dkDefaultSetting = new DKDefaultSetting();
            dkSettingData = dkDefaultSetting.getSettingData();
            for(int i=0; i< dkSettingData.getKeySize(); i++){
                int address = dkSettingData.getKey(i);
                int data = dkSettingData.getData(address);
                SendingNRPN(address, data);
            }
        }
        dialogClose();
    }

    private void dialogClose(){
        Log.d(TAG, "#@#dialogClose()...");
        if(dkSettingData != null){
            DKSettingData.to(m_context, dkSettingData);
        }
        dismiss();
    }
    AudioManager audioManager = null;
    LayoutInflater inflater;
    private void setLayout(){
        mTvNrpn = (TextView) findViewById(R.id.nrpnTxt);

        //Midi Eq Controls
        mSbMidiEqLow = (SeekBar) findViewById(R.id.SBMidiEq1);
        mSbMidiEqHigh = (SeekBar) findViewById(R.id.SBMidiEq2);
        mTvMidiEqLow = (TextView) findViewById(R.id.TVMidiEq1);
        mTvMidiEqHigh = (TextView) findViewById(R.id.TVMidiEq2);

        // Music Input Controls
        mSbMusciLv = (SeekBar) findViewById(R.id.SBMusicIn1);
        mSbMusciShift = (SeekBar) findViewById(R.id.SBMusicIn2);
        mTvMusicLv = (TextView) findViewById(R.id.TVMusicIn1);
        mTvMusicShift = (TextView) findViewById(R.id.TVMusicIn2);

        // Echo Type
        mSbEchoType = (SeekBar) findViewById(R.id.SBEchoType1);
        mTvEchoType = (TextView) findViewById(R.id.TVEchoType1);

        // Echo Controls
        mSwEchoCtrl1 = (Switch) findViewById(R.id.SWEchoCtrl1);
        mSbEchoCtrl2 = (SeekBar) findViewById(R.id.SBEchoCtrl2);
        mSbEchoCtrl3 = (SeekBar) findViewById(R.id.SBEchoCtrl3);
        mTvEchoCtrl1 = (TextView) findViewById(R.id.TVEchoCtrl1);
        mTvEchoCtrl2 = (TextView) findViewById(R.id.TVEchoCtrl2);
        mTvEchoCtrl3 = (TextView) findViewById(R.id.TVEchoCtrl3);

        // Output Mix Controls
        mSbMicVol = (SeekBar) findViewById(R.id.SBOutputMix1);
        mSbMusicVol = (SeekBar) findViewById(R.id.SBOutputMix2);
        mSbAuxVol = (SeekBar) findViewById(R.id.SBOutputMix3);
        mSbMasterVol = (SeekBar) findViewById(R.id.SBOutputMix4);
        mTvMicVol = (TextView) findViewById(R.id.TVOutputMix1);
        mTvMusicVol = (TextView) findViewById(R.id.TVOutputMix2);
        mTvAuxVol = (TextView) findViewById(R.id.TVOutputMix3);
        mTvMasterVol = (TextView) findViewById(R.id.TVOutputMix4);
        mTvMix1 = (TextView) findViewById(R.id.TVOutputMix5);
        mTvMix2 = (TextView) findViewById(R.id.TVOutputMix6);
        mRg1=(RadioGroup)findViewById(R.id.RGOutputMix5);
        mRg2=(RadioGroup)findViewById(R.id.RGOutputMix6);

        // System Controls
        mSwSystemCtrl1 = (Switch) findViewById(R.id.SWSystemCtrl1);
        mSwSystemCtrl2 = (Switch) findViewById(R.id.SWSystemCtrl2);
        mSwSystemCtrl4 = (Switch) findViewById(R.id.SWSystemCtrl4);
        mBtSystemCtrl3 = (Button) findViewById(R.id.BTSystemCtrl3);
        mBtSystemCtrl5 = (Button) findViewById(R.id.BTSystemCtrl5);
        mBtSystemCtrl6 = (Button) findViewById(R.id.BTSystemCtrl6);

        LinearLayout llOptions = findViewById(R.id.llOptions);

        inflater = (LayoutInflater) m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        areaDlg = findViewById(R.id.areaDlg);
        audioManager = (AudioManager)m_context.getSystemService(Context.AUDIO_SERVICE);

        dkSettingData = DKSettingData.from(m_context);
        if(dkSettingData == null) {
            DKDefaultSetting dkDefaultSetting = new DKDefaultSetting();
            dkSettingData = dkDefaultSetting.getSettingData();
        }

        for(int i=0; i< dkSettingData.getKeySize(); i++){
            int address = dkSettingData.getKey(i);
            int data = dkSettingData.getData(address);
            SendingNRPN(address, data);
        }

        int androidMusicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        final int androidMusicMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        if(dkSettingData != null){
            if(dkSettingData.hasAddress(0x4000)){
                androidMusicVolume = dkSettingData.getData(0x4000);
            }
        }
        if(androidMusicVolume > androidMusicMax)
            androidMusicVolume = androidMusicMax;

        int androidSystemVolume = audioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
        final int androidSystemMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM);
        if(dkSettingData != null){
            if(dkSettingData.hasAddress(0x4001)){
                androidSystemVolume = dkSettingData.getData(0x4001);
            }
        }
        if(androidSystemVolume > androidSystemMax)
            androidSystemVolume = androidSystemMax;

        int androidRingVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        final int androidRingMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        if(dkSettingData != null){
            if(dkSettingData.hasAddress(0x4002)){
                androidRingVolume = dkSettingData.getData(0x4002);
            }
        }
        if(androidRingVolume > androidRingMax)
            androidRingVolume = androidRingMax;

        int androidAlarmVolume = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);
        final int androidAlarmMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM);
        if(dkSettingData != null){
            if(dkSettingData.hasAddress(0x4003)){
                androidAlarmVolume = dkSettingData.getData(0x4003);
            }
        }
        if(androidAlarmVolume > androidAlarmMax)
            androidAlarmVolume = androidAlarmMax;

        int androidNotiVolume = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
        final int androidNotiMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION);
        if(dkSettingData != null){
            if(dkSettingData.hasAddress(0x4004)){
                androidNotiVolume = dkSettingData.getData(0x4004);
            }
        }
        if(androidNotiVolume > androidNotiMax)
            androidNotiVolume = androidNotiMax;

        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, androidMusicVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, androidSystemVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        audioManager.setStreamVolume(AudioManager.STREAM_RING, androidRingVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        audioManager.setStreamVolume(AudioManager.STREAM_ALARM, androidAlarmVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
        audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, androidNotiVolume, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

        NRPNTitleView group;
        List<DKView> views;

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] 안드로드 시스템 볼륨");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "STREAM_MUSIC",0x4000, androidMusicVolume, androidMusicMax));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "STREAM_SYSTEM",0x4001, androidSystemVolume, androidSystemMax));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "STREAM_RING",0x4002, androidRingVolume, androidRingMax));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "STREAM_ALARM",0x4003, androidAlarmVolume, androidAlarmMax));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "STREAM_NOTIFICATION",0x4004, androidNotiVolume, androidNotiMax));
        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] General");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "3707h EMIDI Synth Master volume 0 (mute) to 7Fh (max)",0x3707, 127, 127));
        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] MIDI Synth 2-bands Equalizer");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "3755h Equalizer ON/OFF",0x3755, 1, 1));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "3708h Equalizer Low Band Gain 0=-12dB, 40h=0dB, 7Fh=+12dB",0x3708, 96, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "370bh Equalizer High Band Gain 0=-12dB, 40h=0dB, 7Fh=+12dB",0x370b, 96, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "370ch Equalizer Low Band Freq 0=0Hz, 64=400Hz, 127=800Hz",0x370c, 16, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "370fh Equalizer High Band Freq 0=1kHz, 64=3.4KHz, 127=5.8kHz",0x370f, 127, 127));
        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Music Input Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0600h Music Level (pre-EQ): 0=-Inf ... 74h = 0dB ... 7Dh = +9dB, 7Eh = +10dB, 7Fh = +11dB",0x0600, 116, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0601h Music Shift Amount: ...58 = -6 semitones ... 64=no shift ...70=+6 semitones ",0x0601, 64, 127));
        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Music 4-Bands Parametric Equalizer");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        /*
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0700h EQ Band Gain: 31h = -15dB ... 40h = 0dB ... 4Fh = +15dB",0x0700, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0701h EQ Band Gain: 31h = -15dB ... 40h = 0dB ... 4Fh = +15dB",0x0701, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0702h EQ Band Gain: 31h = -15dB ... 40h = 0dB ... 4Fh = +15dB",0x0702, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0703h EQ Band Gain: 31h = -15dB ... 40h = 0dB ... 4Fh = +15dB",0x0703, 64, 127));

        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0710h EQ Band Frequency: 20..16000 (in Hz, using value send in 14bit, with LSB)",0x0710, 100, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0711h EQ Band Frequency: 20..16000 (in Hz, using value send in 14bit, with LSB)",0x0711, 400, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0712h EQ Band Frequency: 20..16000 (in Hz, using value send in 14bit, with LSB)",0x0712, 1600, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0713h EQ Band Frequency: 20..16000 (in Hz, using value send in 14bit, with LSB)",0x0713, 6400, 20,16000));

        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0721h EQ Band Q (only for 2 mid bands): 0= 0.707, 1...127",0x0721, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0722h EQ Band Q (only for 2 mid bands): 0= 0.707, 1...127",0x0722, 0, 127));
        */
        views.add(new NRPNButtonView("[수정] Music 4-Bands Parametric Equalizer", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show4Bands(true);
            }
        }));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone Input Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0800h Microphone Input 1 Level: 0=-Inf ... 74h = 0dB ... 7Dh = +9dB, 7Eh = +10dB, 7Fh = +11dB",0x0800, 116, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0801h Microphone Input 2 Level: 0=-Inf ... 74h = 0dB ... 7Dh = +9dB, 7Eh = +10dB, 7Fh = +11dB",0x0801, 116, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0802h Low-Cut Filter ON/OFF: 0 = off, else HPF at 50Hz",0x0802, 1, 1));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0803h Noise Gate Threshold level: 0 = off, 1..29h = -90dB, 2Ah=-85dB, 2Bh=84dB... 7Fh = 0dB",0x0803, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0804h Noise Gate Release: 0 = ~0.01s... 127 = ~10s",0x0804, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0810h Direct L send: 0...127 = -oo...0dBN",0x0810, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0811h Direct R send: 0...127 = -oo...0dB",0x0811, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0812h Echo send: 0...127 = -oo...0dB",0x0812, 32, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0813h Reverb send: 0...127 = -oo...0dB",0x0813, 0, 127));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone Compressor Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0500h Compressor ON/OFF: 0=OFF, else ON",0x0500, 0, 1));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0501h Attack time: 0=fast attack (0.1ms), ... 60=1ms, ...100=10ms, till 127=slow attack (100ms), exp. Curve",0x0501, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0502h Release time: 0=fast release (10ms), ... 60=100ms, ... 100=1s, till 127=slow release (~5s), exp. Curve",0x0502, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0503h Threshold: 127=0dB, 64=-6dB, 32=-12dB, 16=-18dB, 8=-24dB, 4=-30dB, 2=-36dB 0=-Inf",0x0503, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0504h Ratio: 127=1/128, 126=2/128 (1/64), 125=3/128, ... 64=64/128 (1/2), ... 0=1/1",0x0504, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0505h Boost: 0 = 0dB, 0x20=+4.5dB,... 0x40=+9dB, ... 0x7F=+18dB",0x0505, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0507h Compressor Preset (see Compressor Presets table)",0x0507, 0, 1));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone 5-Bands Parametric Equalizer");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        /*
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0900h EQ Band Gain : 32h = -15dB.. 40h = 0dB, 4Fh = +15dB",0x0900, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0901h EQ Band Gain : 32h = -15dB.. 40h = 0dB, 4Fh = +15dB",0x0901, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0902h EQ Band Gain : 32h = -15dB.. 40h = 0dB, 4Fh = +15dB",0x0902, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0903h EQ Band Gain : 32h = -15dB.. 40h = 0dB, 4Fh = +15dB",0x0903, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0904h EQ Band Gain : 32h = -15dB.. 40h = 0dB, 4Fh = +15dB",0x0904, 64, 127));

        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0910h EQ Band Frequency:20 ~ 16000(in Hz, using value send in 14bit, with LSB)",0x0910, 250, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0911h EQ Band Frequency:20 ~ 16000(in Hz, using value send in 14bit, with LSB)",0x0911, 500, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0912h EQ Band Frequency:20 ~ 16000(in Hz, using value send in 14bit, with LSB)",0x0912, 1000, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0913h EQ Band Frequency:20 ~ 16000(in Hz, using value send in 14bit, with LSB)",0x0913, 2000, 20,16000));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0914h EQ Band Frequency:20 ~ 16000(in Hz, using value send in 14bit, with LSB)",0x0914, 4000, 20,16000));

        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0921h EQ Band Q (only for 3 mid bands) : 0=0.707, 1 ~ 127",0x0921, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0922h EQ Band Q (only for 3 mid bands) : 0=0.707, 1 ~ 127",0x0922, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0923h EQ Band Q (only for 3 mid bands) : 0=0.707, 1 ~ 127",0x0923, 0, 127));

        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "097fh EQ ON/OFF 0=OFF, else ON ",0x097f, 1, 1));
        */
        views.add(new NRPNButtonView("[수정] Microphone 5-Bands Parametric Equalizer", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show5Bands(true);
            }
        }));
        views.add(new NRPNButtonView("097fh EQ ON/OFF 0=OFF, else ON ",0x097f, 1, true));


        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone Echo/Reverb Type Control");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0200h 0: Off 1: Short Room 2: Room A 3: Room \n" +
                "4: Small Hall A 5: Small Hall B 6: Large Hall A 7: Large Hall \n" +
                "8: Short Plate 9: Vocal Plate 10: Mono Echo 11: Stereo Echo\n" +
                "12: MonoEcho+Reverb 13: StereoEcho+Reverb",0x0200, 0, 13));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }


        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone Echo Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0201h Pre-High-Pass Filter Frequency",0x0201, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0202h Pre-High-Shelf Filter Gain",0x0202, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0203h Pre-High-Shelf Filter Frequency",0x0203, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0204h Long Echo Mode",0x0204, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0205h Echo Time",0x0205, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0206h Feedback",0x0206, 32, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0207h H-Damp Filter",0x0207, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0208h L-Damp Filter",0x0208, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0209h Echo Output Level Left",0x0209, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "020ah Echo Output Level Right",0x020a, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "020bh Echo Output Phase Left",0x020b, 0, 1));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "020ch Echo Output Phase Right",0x020c, 0, 1));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Microphone Reverb Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0300h Reverb Level",0x0300, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0301h Reverb Pre-HP",0x0301, 0, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0302h Reverb Tone Gain",0x0302, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0303h Reverb Tone Freq",0x0303, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0304h Reverb Time",0x0304, 64, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0305h Reverb H-Damp",0x0305, 0, 127));

        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

        group = new NRPNTitleView(inflater.inflate(R.layout.nrpn_slice_group, null), "[다날] Output Mix Controls");
        llOptions.addView(group.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        views = new ArrayList<>();
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0100h Mic Volume: 0=muted…32=-12dB…64=-6dB…127=0dB",0x0100, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0101h MR (Music) Volume: 0=muted…32=-12dB…64=-6dB…127=0dB",0x0101, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0102h AUX Volume: 0=muted…32=-12dB…64=-6dB…127=0dB",0x0102, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0103h Main output Master Volume: 0=muted…32=-12dB…64=-6dB…127=0dB",0x0103, 127, 127));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0104h Main output mix configuration: 0=Mic+MR, 1=Mic only, 2=MR only, 3=AUX only",0x0104, 0, 3));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0105h REC output mix configuration: 0=Mic+MR, 1=Mic only, 2=MR only, 3=AUX only",0x0105, 0, 3));
        views.add(new NRPNView(inflater.inflate(R.layout.nrpn_slice_item, null), "0106h REC output mix 볼륨: 0=muted…32=0dB …64=+6dB…127=+12dB",0x0106, 32, 127));


        for(int i=0; i< views.size(); i++) {
            DKView view = views.get(i);
            llOptions.addView(view.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            view.changeBackgroundColor(i%2==0 ? 0xffffffff : 0xffdddddc);
        }

    }

    private void SendingNRPN() {
        if (m_jni != null) {
            String nrpnstr = "NRPN ADDR : 0x" + String.format("%04x ", nrpn_addr&0xffff) + " DATA : 0x" + Integer.toHexString(nrpn_data);
            m_jni.MIDISetNRPN(nrpn_addr, nrpn_data);
            Log.d(TAG, nrpnstr);
            mTvNrpn.setText(nrpnstr);
            if(dkSettingData != null)
                dkSettingData.add(nrpn_addr, nrpn_data);

        }
    }

    private void SendingNRPN(int nrpnAddress, int nrpnData) {
        if (m_jni != null) {
            if(nrpnAddress<0x4000) {
                String nrpnstr = "NRPN ADDR : 0x" + String.format("%04x ", nrpnAddress&0xffff) + " DATA : 0x" + Integer.toHexString(nrpnData);
                m_jni.MIDISetNRPN(nrpnAddress, nrpnData);
                Log.d(TAG, nrpnstr);
                mTvNrpn.setText(nrpnstr);
            }else{
                StringBuilder sb = new StringBuilder();
                if(nrpnAddress == 0x4000){
                    sb.append(String.format("%s : %d", "STREAM_MUSIC", nrpnData));
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, nrpnData, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                }else if(nrpnAddress == 0x4001){
                    sb.append(String.format("%s : %d", "STREAM_SYSTEM", nrpnData));
                    audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, nrpnData, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                }
                else if(nrpnAddress == 0x4002){
                    sb.append(String.format("%s : %d", "STREAM_RING", nrpnData));
                    audioManager.setStreamVolume(AudioManager.STREAM_RING, nrpnData, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                }else if(nrpnAddress == 0x4003){
                    sb.append(String.format("%s : %d", "STREAM_ALARM", nrpnData));
                    audioManager.setStreamVolume(AudioManager.STREAM_ALARM, nrpnData, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);

                }else if(nrpnAddress == 0x4004){
                    sb.append(String.format("%s : %d", "STREAM_NOTIFICATION", nrpnData));
                    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, nrpnData, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
                }

                mTvNrpn.setText(sb.toString());

            }

            if(dkSettingData != null)
                dkSettingData.add(nrpnAddress, nrpnData);
        }
    }

    List<DKView> nrpnViews;

    class NRPNButtonView extends DKView{
        TextView title;
        Button button;
        boolean switchMode = false;
        public NRPNButtonView(String strTitle, View.OnClickListener listener){
            super(inflater.inflate(R.layout.nrpn_button_item, null));
            title = view.findViewById(R.id.title);
            title.setText(strTitle);
            button = view.findViewById(R.id.button);
            button.setOnClickListener(listener);
        }

        public NRPNButtonView(String strTitle, int _nrpnAddress, int defaultValue, boolean _switchMode){
            super(inflater.inflate(R.layout.nrpn_button_item, null));
            title = view.findViewById(R.id.title);
            title.setText(strTitle);
            button = view.findViewById(R.id.button);
            nrpnAddress = _nrpnAddress;
            if(dkSettingData != null){
                if(dkSettingData.hasAddress(nrpnAddress)){
                    defaultValue = dkSettingData.getData(nrpnAddress);
                }
            }
            nrpnData = defaultValue;
            this.switchMode = _switchMode;
            if(switchMode)
                button.setText(nrpnData ==0 ? "OFF" : "ON");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(switchMode) {
                        nrpnData = (nrpnData == 0) ? 1 : 0;
                        button.setText(nrpnData == 0 ? "OFF" : "ON");
                        SendingNRPN(nrpnAddress, nrpnData);
                    }
                }
            });
        }
    }

    class NRPNView extends DKView{
        TextView title;
        TextView value;
        SeekBar seek;
        int max;
        int _min = 0;

        public NRPNView(View view, String strTitle, int _nrpnAddress, int defaultValue, int min, int max){
            super(view);
            this.nrpnAddress = _nrpnAddress;
            if(dkSettingData != null){
                if(dkSettingData.hasAddress(nrpnAddress)){
                    defaultValue = dkSettingData.getData(nrpnAddress);
                }
            }
            this._min = min;
            title = view.findViewById(R.id.title);
            value = view.findViewById(R.id.value);
            seek = view.findViewById(R.id.seek);
            this.nrpnData = defaultValue;
            this.max = max;
            title.setText(strTitle);

            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onStopTrackingTouch(SeekBar seekBar) {}
                public void onStartTrackingTouch(SeekBar seekBar) {}
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(progress < _min) {
                        progress = _min;
                        seekBar.setProgress(progress);
                    }
                    value.setText("" + progress);
                    nrpnData = progress;
                    SendingNRPN(nrpnAddress, nrpnData);
                }
            });
            seek.setMax(max);
            seek.setProgress(defaultValue);
        }

        public NRPNView(View view, String strTitle, int _nrpnAddress, int defaultValue, int max){
            super(view);
            this.nrpnAddress = _nrpnAddress;
            if(dkSettingData != null){
                if(dkSettingData.hasAddress(nrpnAddress)){
                    defaultValue = dkSettingData.getData(nrpnAddress);
                }
            }
            title = view.findViewById(R.id.title);
            value = view.findViewById(R.id.value);
            seek = view.findViewById(R.id.seek);

            this.nrpnData = defaultValue;
            this.max = max;
            title.setText(strTitle);



            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onStopTrackingTouch(SeekBar seekBar) {}
                public void onStartTrackingTouch(SeekBar seekBar) {}
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(progress < _min) {
                        progress = _min;
                        seekBar.setProgress(progress);
                    }
                    value.setText("" + progress);
                    nrpnData = progress;
                    SendingNRPN(nrpnAddress, nrpnData);
                }
            });
            seek.setMax(max);
            seek.setProgress(defaultValue);
        }


    }

    class NRPNTitleView{
        View view;
        TextView title;
        public NRPNTitleView(View view, String strTitle){
            this.view = view;
            title = view.findViewById(R.id.title);
            title.setText(strTitle);
        }
        public View getView(){
            return view;
        }
    }

    class DKView{
        protected View view;
        protected int nrpnAddress;
        protected int nrpnData;

        public DKView(View view){
            this.view = view;
        }

        protected final <T extends View>View findViewById(int id){
            return view.findViewById(id);
        }

        public void changeBackgroundColor(int color){
            view.setBackgroundColor(color);
        }

        public View getView(){
            return view;
        }

        public int getNrpnAddress() {
            return nrpnAddress;
        }

        public void setNrpnAddress(int nrpnAddress) {
            this.nrpnAddress = nrpnAddress;
        }

        public int getNrpnData() {
            return nrpnData;
        }

        public void setNrpnData(int nrpnData) {
            this.nrpnData = nrpnData;
        }
    }

    class DKVerticalEQView extends DKView{
        public DKVerticalEQView(View view){
            super(view);
        }
    }

    class DKEQView extends DKView{
        DKVerticalSeekBar seek;
        TextView hz;
        int gainNrpnAddress;
        int freqNrpnAddress;
        int hzValue;
        int gainValue;
        public DKEQView(View view, int _freqNrpnAddress, int _hzValue, int _gainNrpnAddress, int gain){
            super(view);
            hzValue = _hzValue;
            gainValue = gain;
            freqNrpnAddress = _freqNrpnAddress;
            gainNrpnAddress = _gainNrpnAddress;

            seek = view.findViewById(R.id.seek);
            seek.setListener(new DKVerticalSeekBar.OnDKListener() {
                @Override
                public void OnProgress(int progress) {
                    gainValue = progress;
                    SendingNRPN(gainNrpnAddress, gainValue);
                }
            });

            seek.setMax(127);
            seek.setProgress(gain);
            hz = view.findViewById(R.id.hz);
            if(hzValue >= 1000){
                hz.setText(String.format("%.1fkHz", (float)hzValue/1000.0f));
            }else {
                hz.setText(String.format("%dHz", _hzValue));
            }

            SendingNRPN(freqNrpnAddress, hzValue);
            SendingNRPN(gainNrpnAddress, gainValue);
        }
    }

    class DKEQBandQView extends DKView{
        SeekBar seek;
        TextView hz;
        TextView value;
        public DKEQBandQView(View view, int _nrpnAddress, int defaultData, int hzValue){
            super(view);
            nrpnAddress = _nrpnAddress;
            nrpnData = defaultData;
            SendingNRPN(nrpnAddress, nrpnData);
            value = view.findViewById(R.id.value);
            seek = view.findViewById(R.id.seek);
            seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    nrpnData = i;
                    SendingNRPN(nrpnAddress, nrpnData);
                    if(nrpnData == 0)
                        value.setText(String.format("%f", 0.707f));
                    else
                        value.setText(String.format("%d", nrpnData));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            seek.setMax(127);
            seek.setProgress(defaultData);
            hz = view.findViewById(R.id.hz);
            if(hzValue >= 1000){
                hz.setText(String.format("%.1fkHz", (float)hzValue/1000.0f));
            }else {
                hz.setText(String.format("%dHz", hzValue));
            }

            if(nrpnData == 0)
                value.setText(String.format("%f", 0.707f));
            else
                value.setText(String.format("%d", nrpnData));

        }
    }

    class DKEQ5BandsView extends DKView{
        List<DKView> eqViews;
        public DKEQ5BandsView(View view){
            super(view);
            findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    show5Bands(false);
                }
            });
            int eq1Gain = 0x40;
            int eq2Gain = 0x40;
            int eq3Gain = 0x40;
            int eq4Gain = 0x40;
            int eq5Gain = 0x40;
            int q1Value = 0;
            int q2Value = 0;
            int q3Value = 0;

            if(dkSettingData != null){
                if(dkSettingData.hasAddress(0x0900))
                    eq1Gain = dkSettingData.getData(0x0900);
                if(dkSettingData.hasAddress(0x0901))
                    eq2Gain = dkSettingData.getData(0x0901);
                if(dkSettingData.hasAddress(0x0902))
                    eq3Gain = dkSettingData.getData(0x0902);
                if(dkSettingData.hasAddress(0x0903))
                    eq4Gain = dkSettingData.getData(0x0903);
                if(dkSettingData.hasAddress(0x0904))
                    eq5Gain = dkSettingData.getData(0x0904);

                if(dkSettingData.hasAddress(0x0921))
                    q1Value = dkSettingData.getData(0x0921);
                if(dkSettingData.hasAddress(0x0922))
                    q2Value = dkSettingData.getData(0x0922);
                if(dkSettingData.hasAddress(0x0923))
                    q3Value = dkSettingData.getData(0x0923);
            }
            eqViews = new ArrayList<>();
            eqViews.add(new DKEQView(view.findViewById(R.id.eq1), 0x0910, 250,
                    0x0900, eq1Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq2), 0x0911, 500,
                    0x0901, eq2Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq3), 0x0912, 1000,
                    0x0902, eq3Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq4), 0x0913, 2000,
                    0x0903, eq4Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq5), 0x0914, 4000,
                    0x0904, eq5Gain));

            eqViews.add(new DKEQBandQView(view.findViewById(R.id.q1), 0x0921, q1Value,
                    500));
            eqViews.add(new DKEQBandQView(view.findViewById(R.id.q2), 0x0922, q2Value,
                    1000));
            eqViews.add(new DKEQBandQView(view.findViewById(R.id.q3), 0x0923, q3Value,
                    2000));
        }
    }

    class DKMusic4BandsView extends DKView{
        List<DKView> eqViews;
        public DKMusic4BandsView(View view){
            super(view);
            findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    show4Bands(false);
                }
            });

            int eq1Gain = 0x40;
            int eq2Gain = 0x40;
            int eq3Gain = 0x40;
            int eq4Gain = 0x40;
            int q1Value = 0;
            int q2Value = 0;

            if(dkSettingData != null){
                if(dkSettingData.hasAddress(0x0700))
                    eq1Gain = dkSettingData.getData(0x0700);
                if(dkSettingData.hasAddress(0x0701))
                    eq2Gain = dkSettingData.getData(0x0701);
                if(dkSettingData.hasAddress(0x0702))
                    eq3Gain = dkSettingData.getData(0x0702);
                if(dkSettingData.hasAddress(0x0703))
                    eq4Gain = dkSettingData.getData(0x0703);

                if(dkSettingData.hasAddress(0x0721))
                    q1Value = dkSettingData.getData(0x0721);
                if(dkSettingData.hasAddress(0x0722))
                    q2Value = dkSettingData.getData(0x0722);
            }

            eqViews = new ArrayList<>();
            eqViews.add(new DKEQView(view.findViewById(R.id.eq1), 0x0710, 100,
                    0x0700, eq1Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq2), 0x0711, 400,
                    0x0701, eq2Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq3), 0x0712, 1600,
                    0x0702, eq3Gain));
            eqViews.add(new DKEQView(view.findViewById(R.id.eq4), 0x0713, 6400,
                    0x0703, eq4Gain));

            eqViews.add(new DKEQBandQView(view.findViewById(R.id.q1), 0x0721, q1Value,
                    400));
            eqViews.add(new DKEQBandQView(view.findViewById(R.id.q2), 0x0722, q2Value,
                    1600));
        }
    }

    LinearLayout areaDlg;
    DKEQ5BandsView dkeq5BandsView;
    DKMusic4BandsView dkMusic4BandsView;
    void show5Bands(boolean show){
        if(areaDlg == null) return;
        areaDlg.removeAllViews();
        if(dkeq5BandsView == null){
            dkeq5BandsView = new DKEQ5BandsView(inflater.inflate(R.layout.nrpn_mic_5bands, null));
        }
        areaDlg.addView(dkeq5BandsView.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dkeq5BandsView.getView().setVisibility(show ? View.VISIBLE : View.GONE);
    }

    void show4Bands(boolean show){
        if(areaDlg == null) return;
        areaDlg.removeAllViews();
        if(dkMusic4BandsView == null){
            dkMusic4BandsView = new DKMusic4BandsView(inflater.inflate(R.layout.nrpn_music_4bands, null));
        }
        areaDlg.addView(dkMusic4BandsView.getView(), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        dkMusic4BandsView.getView().setVisibility(show ? View.VISIBLE : View.GONE);
    }
}