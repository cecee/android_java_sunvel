package com.cecee.imove.remote;

import android.util.SparseArray;

/**
 * Created by kumo on 2018. 3. 22..
 */

public class RemoteController {

    public static final int RAW_FRONT_KEY_TEMPO_UP        = 0x00000001;
    public static final int RAW_FRONT_KEY_TEMPO_DOWN      = 0x00000002;
    public static final int RAW_FRONT_KEY_PITCH_UP        = 0x00000010;
    public static final int RAW_FRONT_KEY_PITCH_DOWN      = 0x00000020;
    public static final int RAW_FRONT_KEY_PITCH_MALE      = 0x00000400;
    public static final int RAW_FRONT_KEY_PITCH_FEMALE    = 0x00100000;
    public static final int RAW_FRONT_KEY_CHANGE_RHYTHM   = 0x00000004;
    public static final int RAW_FRONT_KEY_CHART_NEW       = 0x20000000;
    public static final int RAW_FRONT_KEY_MEDLEY          = 0x00000800;
    public static final int RAW_FRONT_KEY_GANJU_JUMP      = 0x00000008;
    public static final int RAW_FRONT_KEY_REMOVE_SCORE    = 0x00002000;
    public static final int RAW_FRONT_KEY_FAVORITE_CHART  = 0x00020000;

    public static final int RAW_FRONT_KEY_NUMBER_0        = 0x00004000;
    public static final int RAW_FRONT_KEY_NUMBER_1        = 0x00040000;
    public static final int RAW_FRONT_KEY_NUMBER_2        = 0x00400000;
    public static final int RAW_FRONT_KEY_NUMBER_3        = 0x00010000;
    public static final int RAW_FRONT_KEY_NUMBER_4        = 0x02000000;
    public static final int RAW_FRONT_KEY_NUMBER_5        = 0x40000000;
    public static final int RAW_FRONT_KEY_NUMBER_6        = 0x00000100;
    public static final int RAW_FRONT_KEY_NUMBER_7        = 0x10000000;
    public static final int RAW_FRONT_KEY_NUMBER_8        = 0x04000000;
    public static final int RAW_FRONT_KEY_NUMBER_9        = 0x00000200;
    public static final int RAW_FRONT_KEY_CANCEL          = 0x00001000;
    public static final int RAW_FRONT_KEY_START           = 0x00000080;
    public static final int RAW_FRONT_KEY_CANCEL_RESERVATION = 0x00200000;
    public static final int RAW_FRONT_KEY_PRE_RESERVATION = 0x00000040;
    public static final int RAW_FRONT_KEY_RESERVATION     = 0x01000000;

    public static final int RAW_FRONT_KEY_MUSIC_DOWN     = 0x80000002;
    public static final int RAW_FRONT_KEY_MUSIC_UP       = 0x80000001;
    public static final int RAW_FRONT_KEY_ECHO_DOWN      = 0x80000008;
    public static final int RAW_FRONT_KEY_ECHO_UP        = 0x80000004;
    public static final int RAW_FRONT_KEY_MODE_DOWN      = 0x80000080;
    public static final int RAW_FRONT_KEY_MODE_UP        = 0x80000040;
    public static final int RAW_FRONT_KEY_FRONT_MIC_DOWN = 0x80000020;
    public static final int RAW_FRONT_KEY_FRONT_MIC_UP   = 0x80000010;
    
    public static final int KEYCODE_UNKNOWN = -1;
    public static final int KEYCODE_PITCH_UP = 600;
    public static final int KEYCODE_PITCH_DOWN = 601;
    public static final int KEYCODE_MELODY_UP = 602;
    public static final int KEYCODE_MELODY_DOWN = 603;
    public static final int KEYCODE_TEMPO_UP = 604;
    public static final int KEYCODE_TEMPO_DOWN = 605;
    public static final int KEYCODE_MUSIC_UP = 606;
    public static final int KEYCODE_MUSIC_DOWN = 607;
    public static final int KEYCODE_MIC_UP = 608;
    public static final int KEYCODE_MIC_DOWN = 609;
    public static final int KEYCODE_ECHO_UP = 610;
    public static final int KEYCODE_ECHO_DOWN = 611;
    public static final int KEYCODE_REVERB_UP = 612;
    public static final int KEYCODE_REVERB_DOWN = 613;
    public static final int KEYCODE_PITCH_SWITCHING = 614;
    public static final int KEYCODE_RHYTHM_CHANGE = 615;
    public static final int KEYCODE_CHORUS = 616;
    public static final int KEYCODE_REMOVE_SCORE = 617;
    public static final int KEYCODE_NOTE = 618;
    public static final int KEYCODE_INTERLUDE_JUMP = 619;
    public static final int KEYCODE_BAR_JUMP_PREV = 620;
    public static final int KEYCODE_BAR_JUMP_NEXT = 621;
    public static final int KEYCODE_VERSE_JUMP = 622;
    public static final int KEYCODE_FIRST_VERSE = 623;
    public static final int KEYCODE_PAUSE = 624;
    public static final int KEYCODE_RESTART = 625;
    public static final int KEYCODE_RANDOM_PLAY = 626;
    public static final int KEYCODE_SELECT_MOVIE = 627;
    public static final int KEYCODE_TOP_CHARTS = 628;
    public static final int KEYCODE_NEW_CHARTS = 629;
    public static final int KEYCODE_FAVORITE_CHARTS = 630;
    public static final int KEYCODE_THEME_CHARTS = 631;
    public static final int KEYCODE_MEDLEY = 632;
    public static final int KEYCODE_UP = 19;
    public static final int KEYCODE_DOWN = 20;
    public static final int KEYCODE_LEFT = 21;
    public static final int KEYCODE_RIGHT = 22;
    public static final int KEYCODE_OK = 23;
    public static final int KEYCODE_PREV_PAGE = 633;
    public static final int KEYCODE_NEXT_PAGE = 634;
    public static final int KEYCODE_VIDEO_RECORD = 635;
    public static final int KEYCODE_AUDIO_RECORD = 636;
    public static final int KEYCODE_ORIGINAL_SONG = 637;
    public static final int KEYCODE_VOCAL_TRAINING = 638;
    public static final int KEYCODE_USB = 639;
    public static final int KEYCODE_EVENT = 640;
    public static final int KEYCODE_BACK = 4;
    public static final int KEYCODE_MENU = 155;
    public static final int KEYCODE_NUMBER_1 = 8;
    public static final int KEYCODE_NUMBER_2 = 9;
    public static final int KEYCODE_NUMBER_3 = 10;
    public static final int KEYCODE_NUMBER_4 = 11;
    public static final int KEYCODE_NUMBER_5 = 12;
    public static final int KEYCODE_NUMBER_6 = 13;
    public static final int KEYCODE_NUMBER_7 = 14;
    public static final int KEYCODE_NUMBER_8 = 15;
    public static final int KEYCODE_NUMBER_9 = 16;
    public static final int KEYCODE_NUMBER_0 = 7;
    public static final int KEYCODE_CANCEL = 641;
    public static final int KEYCODE_START = 642;
    public static final int KEYCODE_RESERVATION_CANCEL = 643;
    public static final int KEYCODE_RESERVATION_LIST = 644;
    public static final int KEYCODE_PRE_RESERVATION = 645;
    public static final int KEYCODE_RESERVATION = 646;
    public static final int KEYCODE_SEARCH_TOTAL = 647;
    public static final int KEYCODE_SEARCH_TITLE = 648;
    public static final int KEYCODE_SEARCH_SINGER = 649;
    public static final int KEYCODE_SEARCH_COUNTRY = 650;
    public static final int KEYCODE_SEARCH_LYRICS = 651;

    public static final int KEYCODE_MALE = 700;
    public static final int KEYCODE_FEMAIL = 701;
    public static final int KEYCODE_SCORE = 702;

    public static final int KEYCODE_CHAR_QQ = 131;
    public static final int KEYCODE_CHAR_WW = 132;
    public static final int KEYCODE_CHAR_EE = 133;
    public static final int KEYCODE_CHAR_RR = 134;
    public static final int KEYCODE_CHAR_TT = 135;
    public static final int KEYCODE_CHAR_OO = 136;
    public static final int KEYCODE_CHAR_PP = 137;
    public static final int KEYCODE_CHAR_SPACE = 62;
    public static final int KEYCODE_CHAR_DELETE = 67;
    public static final int KEYCODE_CHAR_Q = 45;
    public static final int KEYCODE_CHAR_W = 51;
    public static final int KEYCODE_CHAR_E = 33;
    public static final int KEYCODE_CHAR_R = 46;
    public static final int KEYCODE_CHAR_T = 48;
    public static final int KEYCODE_CHAR_Y = 53;
    public static final int KEYCODE_CHAR_U = 49;
    public static final int KEYCODE_CHAR_I = 37;
    public static final int KEYCODE_CHAR_O = 43;
    public static final int KEYCODE_CHAR_P = 44;
    public static final int KEYCODE_CHAR_A = 29;
    public static final int KEYCODE_CHAR_S = 47;
    public static final int KEYCODE_CHAR_D = 32;
    public static final int KEYCODE_CHAR_F = 34;
    public static final int KEYCODE_CHAR_G = 35;
    public static final int KEYCODE_CHAR_H = 36;
    public static final int KEYCODE_CHAR_J = 38;
    public static final int KEYCODE_CHAR_K = 39;
    public static final int KEYCODE_CHAR_L = 40;
    public static final int KEYCODE_CHAR_Z = 54;
    public static final int KEYCODE_CHAR_X = 52;
    public static final int KEYCODE_CHAR_C = 31;
    public static final int KEYCODE_CHAR_V = 50;
    public static final int KEYCODE_CHAR_B = 30;
    public static final int KEYCODE_CHAR_N = 42;
    public static final int KEYCODE_CHAR_M = 41;
    public static final int KEYCODE_CHAR_SWITCH = 652;
    public static final int KEYCODE_CHAR_ENTER = 66;

    public static final int KEYCODE_KR_CHAR_Q = 12610;
    public static final int KEYCODE_KR_CHAR_W = 12616;
    public static final int KEYCODE_KR_CHAR_E = 12599;
    public static final int KEYCODE_KR_CHAR_R = 12593;
    public static final int KEYCODE_KR_CHAR_T = 12613;
    public static final int KEYCODE_KR_CHAR_Y = 12635;
    public static final int KEYCODE_KR_CHAR_U = 12629;
    public static final int KEYCODE_KR_CHAR_I = 12625;
    public static final int KEYCODE_KR_CHAR_O = 12624;
    public static final int KEYCODE_KR_CHAR_P = 12628;
    
    public static final int KEYCODE_KR_CHAR_A = 12609;
    public static final int KEYCODE_KR_CHAR_S = 12596;
    public static final int KEYCODE_KR_CHAR_D = 12615;
    public static final int KEYCODE_KR_CHAR_F = 12601;
    public static final int KEYCODE_KR_CHAR_G = 12622;
    public static final int KEYCODE_KR_CHAR_H = 12631;
    public static final int KEYCODE_KR_CHAR_J = 12627;
    public static final int KEYCODE_KR_CHAR_K = 12623;
    public static final int KEYCODE_KR_CHAR_L = 12643;
    
    public static final int KEYCODE_KR_CHAR_Z = 12619;
    public static final int KEYCODE_KR_CHAR_X = 12620;
    public static final int KEYCODE_KR_CHAR_C = 12618;
    public static final int KEYCODE_KR_CHAR_V = 12621;
    public static final int KEYCODE_KR_CHAR_B = 12640;
    public static final int KEYCODE_KR_CHAR_N = 12636;
    public static final int KEYCODE_KR_CHAR_M = 12641;

    public static final int KEYCODE_KR_CHAR_QQ = 12611;
    public static final int KEYCODE_KR_CHAR_WW = 12617;
    public static final int KEYCODE_KR_CHAR_EE = 12600;
    public static final int KEYCODE_KR_CHAR_RR = 12594;
    public static final int KEYCODE_KR_CHAR_TT = 12614;
    public static final int KEYCODE_KR_CHAR_OO = 12626;
    public static final int KEYCODE_KR_CHAR_PP = 12630;

    public static final int KEYCODE_SPECIAL_EMERGENCY = 597;
    public static final int KEYCODE_SPECIAL_COIN = 599;

    public static final int KEYCODE_MODE_DOWN = 653;
    public static final int KEYCODE_MODE_UP = 654;

    public static final int KEYCODE_FRONT_TEMPO_UP = 657;
    public static final int KEYCODE_FRONT_TEMPO_DOWN = 658;
    public static final int KEYCODE_FRONT_PITCH_UP = 659;
    public static final int KEYCODE_FRONT_PITCH_DOWN = 660;
    public static final int KEYCODE_FRONT_PITCH_MAN = 661;
    public static final int KEYCODE_FRONT_PITCH_FEMALE = 662;
    public static final int KEYCODE_FRONT_CHANGE_RHYTHM = 663;
    public static final int KEYCODE_FRONT_NEW_CHART = 664;
    public static final int KEYCODE_FRONT_MEDLEY = 665;
    public static final int KEYCODE_FRONT_GANJU_JUMP = 666;
    public static final int KEYCODE_FRONT_REMOVE_SCORE = 667;
    public static final int KEYCODE_FRONT_CHART_FAVORITE = 668;
    public static final int KEYCODE_FRONT_MUSIC_UP = 684;
    public static final int KEYCODE_FRONT_MUSIC_DOWN = 685;
    public static final int KEYCODE_FRONT_ECHO_UP = 686;
    public static final int KEYCODE_FRONT_ECHO_DOWN = 687;
    public static final int KEYCODE_FRONT_NUMBER_0 = 678;
    public static final int KEYCODE_FRONT_NUMBER_1 = 669;
    public static final int KEYCODE_FRONT_NUMBER_2 = 670;
    public static final int KEYCODE_FRONT_NUMBER_3 = 671;
    public static final int KEYCODE_FRONT_NUMBER_4 = 672;
    public static final int KEYCODE_FRONT_NUMBER_5 = 673;
    public static final int KEYCODE_FRONT_NUMBER_6 = 674;
    public static final int KEYCODE_FRONT_NUMBER_7 = 675;
    public static final int KEYCODE_FRONT_NUMBER_8 = 676;
    public static final int KEYCODE_FRONT_NUMBER_9 = 677;
    public static final int KEYCODE_FRONT_CANCEL = 679;
    public static final int KEYCODE_FRONT_START = 680;
    public static final int KEYCODE_FRONT_CANCEL_RESERVATION = 681;
    public static final int KEYCODE_FRONT_PRE_RESERVATION = 682;
    public static final int KEYCODE_FRONT_RESERVATION = 683;
    public static final int KEYCODE_FRONT_MIC_UP = 688;
    public static final int KEYCODE_FRONT_MIC_DOWN = 689;

    public static final int KEYCODE_SETTING = 2000;

    public enum RemoteKey{
        UNKNOWN,
        PITCH_UP,
        PITCH_DOWN,
        MELODY_UP,
        MELODY_DOWN,
        TEMPO_UP,
        TEMPO_DOWN,
        MUSIC_UP,
        MUSIC_DOWN,
        MIC_UP,
        MIC_DOWN,
        ECHO_UP,
        ECHO_DOWN,
        REVERB_UP,
        REVERB_DOWN,
        PITCH_SWITCHING,
        RHYTHM_CHANGE,
        CHORUS,
        REMOVE_SCORE,
        NOTE,
        INTERLUDE_JUMP,
        BAR_JUMP_PREV,
        BAR_JUMP_NEXT,
        VERSE_JUMP,
        FIRST_VERSE,
        PAUSE,
        RESTART,
        RANDOM_PLAY,
        SELECT_MOVIE,
        TOP_CHARTS,
        NEW_CHARTS,
        FAVORITE_CHARTS,
        THEME_CHARTS,
        MEDLEY,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        OK,
        PREV_PAGE,
        NEXT_PAGE,
        VIDEO_RECORD,
        AUDIO_RECORD,
        ORIGINAL_SONG,
        VOCAL_TRAINING,
        USB,
        EVENT,
        BACK,
        MENU,
        NUMBER_1,
        NUMBER_2,
        NUMBER_3,
        NUMBER_4,
        NUMBER_5,
        NUMBER_6,
        NUMBER_7,
        NUMBER_8,
        NUMBER_9,
        NUMBER_0,
        CANCEL,
        START,
        RESERVATION_CANCEL,
        RESERVATION_LIST,
        PRE_RESERVATION,
        RESERVATION,
        SEARCH_TOTAL,
        SEARCH_TITLE,
        SEARCH_SINGER,
        SEARCH_COUNTRY,
        SEARCH_WORD,
        CHAR_QQ,
        CHAR_WW,
        CHAR_EE,
        CHAR_TT,
        CHAR_YY,
        CHAR_UU,
        CHAR_SPACE,
        CHAR_DELETE,
        CHAR_Q,
        CHAR_W,
        CHAR_E,
        CHAR_R,
        CHAR_T,
        CHAR_Y,
        CHAR_U,
        CHAR_I,
        CHAR_O,
        CHAR_P,
        CHAR_A,
        CHAR_S,
        CHAR_D,
        CHAR_F,
        CHAR_G,
        CHAR_H,
        CHAR_J,
        CHAR_K,
        CHAR_L,
        CHAR_Z,
        CHAR_X,
        CHAR_C,
        CHAR_V,
        CHAR_B,
        CHAR_N,
        CHAR_M,
        CHAR_SWITCH,
        CHAR_ENTER;
    }

    public enum LanguageType{
        KOERA,
        ENGLISH;
    }

    protected SparseArray<String> enChars;
    protected SparseArray<String> krChars;

    public RemoteController(){
        init();
    }

    void init(){
        enChars = new SparseArray<>();
        enChars.put(KEYCODE_NUMBER_0, "0");
        enChars.put(KEYCODE_NUMBER_1, "1");
        enChars.put(KEYCODE_NUMBER_2, "2");
        enChars.put(KEYCODE_NUMBER_3, "3");
        enChars.put(KEYCODE_NUMBER_4, "4");
        enChars.put(KEYCODE_NUMBER_5, "5");
        enChars.put(KEYCODE_NUMBER_6, "6");
        enChars.put(KEYCODE_NUMBER_7, "7");
        enChars.put(KEYCODE_NUMBER_8, "8");
        enChars.put(KEYCODE_NUMBER_9, "9");
        enChars.put(KEYCODE_CHAR_A, "A");
        enChars.put(KEYCODE_CHAR_B, "B");
        enChars.put(KEYCODE_CHAR_C, "C");
        enChars.put(KEYCODE_CHAR_D, "D");
        enChars.put(KEYCODE_CHAR_E, "E");
        enChars.put(KEYCODE_CHAR_F, "F");
        enChars.put(KEYCODE_CHAR_G, "G");
        enChars.put(KEYCODE_CHAR_H, "H");
        enChars.put(KEYCODE_CHAR_I, "I");
        enChars.put(KEYCODE_CHAR_J, "J");
        enChars.put(KEYCODE_CHAR_K, "K");
        enChars.put(KEYCODE_CHAR_L, "L");
        enChars.put(KEYCODE_CHAR_M, "M");
        enChars.put(KEYCODE_CHAR_N, "N");
        enChars.put(KEYCODE_CHAR_O, "O");
        enChars.put(KEYCODE_CHAR_P, "P");
        enChars.put(KEYCODE_CHAR_Q, "Q");
        enChars.put(KEYCODE_CHAR_R, "R");
        enChars.put(KEYCODE_CHAR_S, "S");
        enChars.put(KEYCODE_CHAR_T, "T");
        enChars.put(KEYCODE_CHAR_U, "U");
        enChars.put(KEYCODE_CHAR_V, "V");
        enChars.put(KEYCODE_CHAR_W, "W");
        enChars.put(KEYCODE_CHAR_X, "X");
        enChars.put(KEYCODE_CHAR_Y, "Y");
        enChars.put(KEYCODE_CHAR_Z, "Z");
        enChars.put(KEYCODE_CHAR_QQ, "!");
        enChars.put(KEYCODE_CHAR_WW, "?");
        enChars.put(KEYCODE_CHAR_EE, "@");
        enChars.put(KEYCODE_CHAR_RR, "&");
        enChars.put(KEYCODE_CHAR_TT, ".");
        enChars.put(KEYCODE_CHAR_OO, "-");
        enChars.put(KEYCODE_CHAR_PP, "'");
        enChars.put(KEYCODE_CHAR_SPACE, " ");

        krChars = new SparseArray<>();
        krChars.put(KEYCODE_NUMBER_0, "0");
        krChars.put(KEYCODE_NUMBER_1, "1");
        krChars.put(KEYCODE_NUMBER_2, "2");
        krChars.put(KEYCODE_NUMBER_3, "3");
        krChars.put(KEYCODE_NUMBER_4, "4");
        krChars.put(KEYCODE_NUMBER_5, "5");
        krChars.put(KEYCODE_NUMBER_6, "6");
        krChars.put(KEYCODE_NUMBER_7, "7");
        krChars.put(KEYCODE_NUMBER_8, "8");
        krChars.put(KEYCODE_NUMBER_9, "9");
        krChars.put(KEYCODE_CHAR_A, "ㅁ");
        krChars.put(KEYCODE_CHAR_B, "ㅠ");
        krChars.put(KEYCODE_CHAR_C, "ㅊ");
        krChars.put(KEYCODE_CHAR_D, "ㅇ");
        krChars.put(KEYCODE_CHAR_E, "ㄷ");
        krChars.put(KEYCODE_CHAR_F, "ㄹ");
        krChars.put(KEYCODE_CHAR_G, "ㅎ");
        krChars.put(KEYCODE_CHAR_H, "ㅗ");
        krChars.put(KEYCODE_CHAR_I, "ㅑ");
        krChars.put(KEYCODE_CHAR_J, "ㅓ");
        krChars.put(KEYCODE_CHAR_K, "ㅏ");
        krChars.put(KEYCODE_CHAR_L, "ㅣ");
        krChars.put(KEYCODE_CHAR_M, "ㅡ");
        krChars.put(KEYCODE_CHAR_N, "ㅜ");
        krChars.put(KEYCODE_CHAR_O, "ㅐ");
        krChars.put(KEYCODE_CHAR_P, "ㅔ");
        krChars.put(KEYCODE_CHAR_Q, "ㅂ");
        krChars.put(KEYCODE_CHAR_R, "ㄱ");
        krChars.put(KEYCODE_CHAR_S, "ㄴ");
        krChars.put(KEYCODE_CHAR_T, "ㅅ");
        krChars.put(KEYCODE_CHAR_U, "ㅕ");
        krChars.put(KEYCODE_CHAR_V, "ㅍ");
        krChars.put(KEYCODE_CHAR_W, "ㅈ");
        krChars.put(KEYCODE_CHAR_X, "ㅌ");
        krChars.put(KEYCODE_CHAR_Y, "ㅛ");
        krChars.put(KEYCODE_CHAR_Z, "ㅋ");
        krChars.put(KEYCODE_CHAR_QQ, "ㅃ");
        krChars.put(KEYCODE_CHAR_WW, "ㅉ");
        krChars.put(KEYCODE_CHAR_EE, "ㄸ");
        krChars.put(KEYCODE_CHAR_RR, "ㄲ");
        krChars.put(KEYCODE_CHAR_TT, "ㅆ");
        krChars.put(KEYCODE_CHAR_OO, "ㅒ");
        krChars.put(KEYCODE_CHAR_PP, "ㅖ");
        krChars.put(KEYCODE_CHAR_SPACE, " ");

    }

    public static RemoteKey parse(int keyCode){
        switch(keyCode){
            case KEYCODE_UP:
                break;
            case KEYCODE_DOWN:
                break;
            case KEYCODE_LEFT:
                break;
            case KEYCODE_RIGHT:
                break;
            case KEYCODE_OK:
                break;
            case KEYCODE_PREV_PAGE:
                break;
            case KEYCODE_NEXT_PAGE:
                break;
            case KEYCODE_BACK:
            case KEYCODE_CANCEL:
                break;
            case KEYCODE_START:
                break;
            case KEYCODE_RESERVATION:
                break;
            case KEYCODE_PRE_RESERVATION:
                break;
            case KEYCODE_SEARCH_TOTAL:
                break;
            case KEYCODE_SEARCH_TITLE:
                break;
            case KEYCODE_SEARCH_SINGER:
                break;
            case KEYCODE_SEARCH_COUNTRY:
                break;
            case KEYCODE_SEARCH_LYRICS:
                break;
            case KEYCODE_NUMBER_1:
            case KEYCODE_NUMBER_2:
            case KEYCODE_NUMBER_3:
            case KEYCODE_NUMBER_4:
            case KEYCODE_NUMBER_5:
            case KEYCODE_NUMBER_6:
            case KEYCODE_NUMBER_7:
            case KEYCODE_NUMBER_8:
            case KEYCODE_NUMBER_9:
            case KEYCODE_NUMBER_0:
            case KEYCODE_CHAR_QQ:
            case KEYCODE_CHAR_WW:
            case KEYCODE_CHAR_EE:
            case KEYCODE_CHAR_RR:
            case KEYCODE_CHAR_TT:
            case KEYCODE_CHAR_OO:
            case KEYCODE_CHAR_PP:
            case KEYCODE_CHAR_Q:
            case KEYCODE_CHAR_W:
            case KEYCODE_CHAR_E:
            case KEYCODE_CHAR_R:
            case KEYCODE_CHAR_T:
            case KEYCODE_CHAR_Y:
            case KEYCODE_CHAR_U:
            case KEYCODE_CHAR_I:
            case KEYCODE_CHAR_O:
            case KEYCODE_CHAR_P:
            case KEYCODE_CHAR_A:
            case KEYCODE_CHAR_S:
            case KEYCODE_CHAR_D:
            case KEYCODE_CHAR_F:
            case KEYCODE_CHAR_G:
            case KEYCODE_CHAR_H:
            case KEYCODE_CHAR_J:
            case KEYCODE_CHAR_K:
            case KEYCODE_CHAR_L:
            case KEYCODE_CHAR_Z:
            case KEYCODE_CHAR_X:
            case KEYCODE_CHAR_C:
            case KEYCODE_CHAR_V:
            case KEYCODE_CHAR_B:
            case KEYCODE_CHAR_N:
            case KEYCODE_CHAR_M:
                break;

            case KEYCODE_CHAR_SPACE:
                break;
            case KEYCODE_CHAR_DELETE:
                break;
            case KEYCODE_CHAR_SWITCH:
                break;
            case KEYCODE_CHAR_ENTER:
                break;
        }
        return RemoteKey.UNKNOWN;
    }

    public static boolean isExCharacter(int keyCode){
        switch(keyCode){
            case KEYCODE_CHAR_QQ:
            case KEYCODE_CHAR_WW:
            case KEYCODE_CHAR_EE:
            case KEYCODE_CHAR_RR:
            case KEYCODE_CHAR_TT:
            case KEYCODE_CHAR_OO:
            case KEYCODE_CHAR_PP:
                return true;
        }
        return false;
    }

    public static boolean isNumber(int keyCode){
        switch(keyCode){
            case KEYCODE_NUMBER_1:
            case KEYCODE_NUMBER_2:
            case KEYCODE_NUMBER_3:
            case KEYCODE_NUMBER_4:
            case KEYCODE_NUMBER_5:
            case KEYCODE_NUMBER_6:
            case KEYCODE_NUMBER_7:
            case KEYCODE_NUMBER_8:
            case KEYCODE_NUMBER_9:
            case KEYCODE_NUMBER_0:
            case KEYCODE_FRONT_NUMBER_1:
            case KEYCODE_FRONT_NUMBER_2:
            case KEYCODE_FRONT_NUMBER_3:
            case KEYCODE_FRONT_NUMBER_4:
            case KEYCODE_FRONT_NUMBER_5:
            case KEYCODE_FRONT_NUMBER_6:
            case KEYCODE_FRONT_NUMBER_7:
            case KEYCODE_FRONT_NUMBER_8:
            case KEYCODE_FRONT_NUMBER_9:
            case KEYCODE_FRONT_NUMBER_0:
                return true;
        }
        return false;
    }

    public static String numberToString(int keyCode){
        switch(keyCode) {
            case RemoteController.KEYCODE_NUMBER_1:
            case RemoteController.KEYCODE_NUMBER_2:
            case RemoteController.KEYCODE_NUMBER_3:
            case RemoteController.KEYCODE_NUMBER_4:
            case RemoteController.KEYCODE_NUMBER_5:
            case RemoteController.KEYCODE_NUMBER_6:
            case RemoteController.KEYCODE_NUMBER_7:
            case RemoteController.KEYCODE_NUMBER_8:
            case RemoteController.KEYCODE_NUMBER_9:
            case RemoteController.KEYCODE_NUMBER_0:{
                int number = keyCode - RemoteController.KEYCODE_NUMBER_0;
                return (String.format("%d", number));
            }
            case KEYCODE_FRONT_NUMBER_1:
                return String.format("%d", 1);
            case KEYCODE_FRONT_NUMBER_2:
                return String.format("%d", 2);
            case KEYCODE_FRONT_NUMBER_3:
                return String.format("%d", 3);
            case KEYCODE_FRONT_NUMBER_4:
                return String.format("%d", 4);
            case KEYCODE_FRONT_NUMBER_5:
                return String.format("%d", 5);
            case KEYCODE_FRONT_NUMBER_6:
                return String.format("%d", 6);
            case KEYCODE_FRONT_NUMBER_7:
                return String.format("%d", 7);
            case KEYCODE_FRONT_NUMBER_8:
                return String.format("%d", 8);
            case KEYCODE_FRONT_NUMBER_9:
                return String.format("%d", 9);
            case KEYCODE_FRONT_NUMBER_0:{
                return String.format("%d", 0);
            }
        }
        return null;
    }

    public static boolean isCharacter(int keyCode){
        switch(keyCode){
            case KEYCODE_NUMBER_1:
            case KEYCODE_NUMBER_2:
            case KEYCODE_NUMBER_3:
            case KEYCODE_NUMBER_4:
            case KEYCODE_NUMBER_5:
            case KEYCODE_NUMBER_6:
            case KEYCODE_NUMBER_7:
            case KEYCODE_NUMBER_8:
            case KEYCODE_NUMBER_9:
            case KEYCODE_NUMBER_0:
            case KEYCODE_CHAR_QQ:
            case KEYCODE_CHAR_WW:
            case KEYCODE_CHAR_EE:
            case KEYCODE_CHAR_RR:
            case KEYCODE_CHAR_TT:
            case KEYCODE_CHAR_OO:
            case KEYCODE_CHAR_PP:
            case KEYCODE_CHAR_Q:
            case KEYCODE_CHAR_W:
            case KEYCODE_CHAR_E:
            case KEYCODE_CHAR_R:
            case KEYCODE_CHAR_T:
            case KEYCODE_CHAR_Y:
            case KEYCODE_CHAR_U:
            case KEYCODE_CHAR_I:
            case KEYCODE_CHAR_O:
            case KEYCODE_CHAR_P:
            case KEYCODE_CHAR_A:
            case KEYCODE_CHAR_S:
            case KEYCODE_CHAR_D:
            case KEYCODE_CHAR_F:
            case KEYCODE_CHAR_G:
            case KEYCODE_CHAR_H:
            case KEYCODE_CHAR_J:
            case KEYCODE_CHAR_K:
            case KEYCODE_CHAR_L:
            case KEYCODE_CHAR_Z:
            case KEYCODE_CHAR_X:
            case KEYCODE_CHAR_C:
            case KEYCODE_CHAR_V:
            case KEYCODE_CHAR_B:
            case KEYCODE_CHAR_N:
            case KEYCODE_CHAR_M:
            case KEYCODE_CHAR_SPACE:
            case KEYCODE_CHAR_DELETE:
                return true;
        }
        return false;
    }
    
    public static String parseToChar(int keyCode, LanguageType type ){
        if(type == LanguageType.KOERA){
            return keyCodeToKorea(keyCode);
        }else if(type == LanguageType.ENGLISH){
            return keyCodeToEnglish(keyCode);
        }
        return null;
    }
    
    public static int keyCodeToUsKey(int keyCode){
        switch(keyCode) {
            case KEYCODE_NUMBER_0: return keyCode;
            case KEYCODE_NUMBER_1: return keyCode;
            case KEYCODE_NUMBER_2: return keyCode;
            case KEYCODE_NUMBER_3: return keyCode;
            case KEYCODE_NUMBER_4: return keyCode;
            case KEYCODE_NUMBER_5: return keyCode;
            case KEYCODE_NUMBER_6: return keyCode;
            case KEYCODE_NUMBER_7: return keyCode;
            case KEYCODE_NUMBER_8: return keyCode;
            case KEYCODE_NUMBER_9: return keyCode;
            case KEYCODE_CHAR_A: return 'A';
            case KEYCODE_CHAR_B: return 'B';
            case KEYCODE_CHAR_C: return 'C';
            case KEYCODE_CHAR_D: return 'D';
            case KEYCODE_CHAR_E: return 'E';
            case KEYCODE_CHAR_F: return 'F';
            case KEYCODE_CHAR_G: return 'G';
            case KEYCODE_CHAR_H: return 'H';
            case KEYCODE_CHAR_I: return 'I';
            case KEYCODE_CHAR_J: return 'J';
            case KEYCODE_CHAR_K: return 'K';
            case KEYCODE_CHAR_L: return 'L';
            case KEYCODE_CHAR_M: return 'M';
            case KEYCODE_CHAR_N: return 'N';
            case KEYCODE_CHAR_O: return 'O';
            case KEYCODE_CHAR_P: return 'P';
            case KEYCODE_CHAR_Q: return 'Q';
            case KEYCODE_CHAR_R: return 'R';
            case KEYCODE_CHAR_S: return 'S';
            case KEYCODE_CHAR_T: return 'T';
            case KEYCODE_CHAR_U: return 'U';
            case KEYCODE_CHAR_V: return 'V';
            case KEYCODE_CHAR_W: return 'W';
            case KEYCODE_CHAR_X: return 'X';
            case KEYCODE_CHAR_Y: return 'Y';
            case KEYCODE_CHAR_Z: return 'Z';
            case KEYCODE_CHAR_QQ: return keyCode;
            case KEYCODE_CHAR_WW: return keyCode;
            case KEYCODE_CHAR_EE: return keyCode;
            case KEYCODE_CHAR_RR: return keyCode;
            case KEYCODE_CHAR_TT: return keyCode;
            case KEYCODE_CHAR_OO: return keyCode;
            case KEYCODE_CHAR_PP: return keyCode;
            case KEYCODE_CHAR_SPACE: return keyCode;
            case KEYCODE_CHAR_DELETE:return keyCode;
            case KEYCODE_CHAR_ENTER:return keyCode;
            case KEYCODE_CHAR_SWITCH:return keyCode;
            default:break;

        }
        return keyCode;
    }

    public static int keyCodeToKoreaKey(int keyCode){
        switch(keyCode) {
            case KEYCODE_NUMBER_0: return keyCode;
            case KEYCODE_NUMBER_1: return keyCode;
            case KEYCODE_NUMBER_2: return keyCode;
            case KEYCODE_NUMBER_3: return keyCode;
            case KEYCODE_NUMBER_4: return keyCode;
            case KEYCODE_NUMBER_5: return keyCode;
            case KEYCODE_NUMBER_6: return keyCode;
            case KEYCODE_NUMBER_7: return keyCode;
            case KEYCODE_NUMBER_8: return keyCode;
            case KEYCODE_NUMBER_9: return keyCode;
            case KEYCODE_CHAR_A: return KEYCODE_KR_CHAR_A;
            case KEYCODE_CHAR_B: return KEYCODE_KR_CHAR_B;
            case KEYCODE_CHAR_C: return KEYCODE_KR_CHAR_C;
            case KEYCODE_CHAR_D: return KEYCODE_KR_CHAR_D;
            case KEYCODE_CHAR_E: return KEYCODE_KR_CHAR_E;
            case KEYCODE_CHAR_F: return KEYCODE_KR_CHAR_F;
            case KEYCODE_CHAR_G: return KEYCODE_KR_CHAR_G;
            case KEYCODE_CHAR_H: return KEYCODE_KR_CHAR_H;
            case KEYCODE_CHAR_I: return KEYCODE_KR_CHAR_I;
            case KEYCODE_CHAR_J: return KEYCODE_KR_CHAR_J;
            case KEYCODE_CHAR_K: return KEYCODE_KR_CHAR_K;
            case KEYCODE_CHAR_L: return KEYCODE_KR_CHAR_L;
            case KEYCODE_CHAR_M: return KEYCODE_KR_CHAR_M;
            case KEYCODE_CHAR_N: return KEYCODE_KR_CHAR_N;
            case KEYCODE_CHAR_O: return KEYCODE_KR_CHAR_O;
            case KEYCODE_CHAR_P: return KEYCODE_KR_CHAR_P;
            case KEYCODE_CHAR_Q: return KEYCODE_KR_CHAR_Q;
            case KEYCODE_CHAR_R: return KEYCODE_KR_CHAR_R;
            case KEYCODE_CHAR_S: return KEYCODE_KR_CHAR_S;
            case KEYCODE_CHAR_T: return KEYCODE_KR_CHAR_T;
            case KEYCODE_CHAR_U: return KEYCODE_KR_CHAR_U;
            case KEYCODE_CHAR_V: return KEYCODE_KR_CHAR_V;
            case KEYCODE_CHAR_W: return KEYCODE_KR_CHAR_W;
            case KEYCODE_CHAR_X: return KEYCODE_KR_CHAR_X;
            case KEYCODE_CHAR_Y: return KEYCODE_KR_CHAR_Y;
            case KEYCODE_CHAR_Z: return KEYCODE_KR_CHAR_Z;
            case KEYCODE_CHAR_QQ: return KEYCODE_KR_CHAR_QQ;
            case KEYCODE_CHAR_WW: return KEYCODE_KR_CHAR_WW;
            case KEYCODE_CHAR_EE: return KEYCODE_KR_CHAR_EE;
            case KEYCODE_CHAR_RR: return KEYCODE_KR_CHAR_RR;
            case KEYCODE_CHAR_TT: return KEYCODE_KR_CHAR_TT;
            case KEYCODE_CHAR_OO: return KEYCODE_KR_CHAR_OO;
            case KEYCODE_CHAR_PP: return KEYCODE_KR_CHAR_PP;
            case KEYCODE_CHAR_SPACE: return keyCode;
            case KEYCODE_CHAR_DELETE:return keyCode;
            case KEYCODE_CHAR_ENTER:return keyCode;
            case KEYCODE_CHAR_SWITCH:return keyCode;
            default:break;

        }
        return keyCode;
    }

    public static String keyCodeToKorea(int keyCode){
        switch(keyCode) {
            case KEYCODE_NUMBER_0: return "0";
            case KEYCODE_NUMBER_1: return "1";
            case KEYCODE_NUMBER_2: return "2";
            case KEYCODE_NUMBER_3: return "3";
            case KEYCODE_NUMBER_4: return "4";
            case KEYCODE_NUMBER_5: return "5";
            case KEYCODE_NUMBER_6: return "6";
            case KEYCODE_NUMBER_7: return "7";
            case KEYCODE_NUMBER_8: return "8";
            case KEYCODE_NUMBER_9: return "9";
            case KEYCODE_CHAR_A: return "ㅁ";
            case KEYCODE_CHAR_B: return "ㅠ";
            case KEYCODE_CHAR_C: return "ㅊ";
            case KEYCODE_CHAR_D: return "ㅇ";
            case KEYCODE_CHAR_E: return "ㄷ";
            case KEYCODE_CHAR_F: return "ㄹ";
            case KEYCODE_CHAR_G: return "ㅎ";
            case KEYCODE_CHAR_H: return "ㅗ";
            case KEYCODE_CHAR_I: return "ㅑ";
            case KEYCODE_CHAR_J: return "ㅓ";
            case KEYCODE_CHAR_K: return "ㅏ";
            case KEYCODE_CHAR_L: return "ㅣ";
            case KEYCODE_CHAR_M: return "ㅡ";
            case KEYCODE_CHAR_N: return "ㅜ";
            case KEYCODE_CHAR_O: return "ㅐ";
            case KEYCODE_CHAR_P: return "ㅔ";
            case KEYCODE_CHAR_Q: return "ㅂ";
            case KEYCODE_CHAR_R: return "ㄱ";
            case KEYCODE_CHAR_S: return "ㄴ";
            case KEYCODE_CHAR_T: return "ㅅ";
            case KEYCODE_CHAR_U: return "ㅕ";
            case KEYCODE_CHAR_V: return "ㅍ";
            case KEYCODE_CHAR_W: return "ㅈ";
            case KEYCODE_CHAR_X: return "ㅌ";
            case KEYCODE_CHAR_Y: return "ㅛ";
            case KEYCODE_CHAR_Z: return "ㅋ";
            case KEYCODE_CHAR_QQ: return "ㅃ";
            case KEYCODE_CHAR_WW: return "ㅉ";
            case KEYCODE_CHAR_EE: return "ㄸ";
            case KEYCODE_CHAR_RR: return "ㄲ";
            case KEYCODE_CHAR_TT: return "ㅆ";
            case KEYCODE_CHAR_OO: return "ㅒ";
            case KEYCODE_CHAR_PP: return "ㅖ";
            case KEYCODE_CHAR_SPACE: return " ";
        }
        return null;
    }

    public static String keyCodeToEnglish(int keyCode){
        switch(keyCode) {
            case KEYCODE_NUMBER_0: return "0";
            case KEYCODE_NUMBER_1: return "1";
            case KEYCODE_NUMBER_2: return "2";
            case KEYCODE_NUMBER_3: return "3";
            case KEYCODE_NUMBER_4: return "4";
            case KEYCODE_NUMBER_5: return "5";
            case KEYCODE_NUMBER_6: return "6";
            case KEYCODE_NUMBER_7: return "7";
            case KEYCODE_NUMBER_8: return "8";
            case KEYCODE_NUMBER_9: return "9";
            case KEYCODE_CHAR_A: return "A";
            case KEYCODE_CHAR_B: return "B";
            case KEYCODE_CHAR_C: return "C";
            case KEYCODE_CHAR_D: return "D";
            case KEYCODE_CHAR_E: return "E";
            case KEYCODE_CHAR_F: return "F";
            case KEYCODE_CHAR_G: return "G";
            case KEYCODE_CHAR_H: return "H";
            case KEYCODE_CHAR_I: return "I";
            case KEYCODE_CHAR_J: return "J";
            case KEYCODE_CHAR_K: return "K";
            case KEYCODE_CHAR_L: return "L";
            case KEYCODE_CHAR_M: return "M";
            case KEYCODE_CHAR_N: return "N";
            case KEYCODE_CHAR_O: return "O";
            case KEYCODE_CHAR_P: return "P";
            case KEYCODE_CHAR_Q: return "Q";
            case KEYCODE_CHAR_R: return "R";
            case KEYCODE_CHAR_S: return "S";
            case KEYCODE_CHAR_T: return "T";
            case KEYCODE_CHAR_U: return "U";
            case KEYCODE_CHAR_V: return "V";
            case KEYCODE_CHAR_W: return "W";
            case KEYCODE_CHAR_X: return "X";
            case KEYCODE_CHAR_Y: return "Y";
            case KEYCODE_CHAR_Z: return "Z";
            case KEYCODE_CHAR_QQ: return "!";
            case KEYCODE_CHAR_WW: return "?";
            case KEYCODE_CHAR_EE: return "@";
            case KEYCODE_CHAR_RR: return "&";
            case KEYCODE_CHAR_TT: return ".";
            case KEYCODE_CHAR_OO: return "-";
            case KEYCODE_CHAR_PP: return "'";
            case KEYCODE_CHAR_SPACE: return " ";
        }
        return null;
    }


}
