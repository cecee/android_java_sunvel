package com.cecee.imove.activity;

import java.io.Serializable;

public class ScaleData implements Serializable {
    private int [] arryValue;

    public int[] getArryValue() {
        return arryValue;
    }

    public void setArryValue(int[] arryValue) {
        this.arryValue = arryValue;
    }
}
