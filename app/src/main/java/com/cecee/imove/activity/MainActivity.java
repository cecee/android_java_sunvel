package com.cecee.imove.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Instrumentation;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;
import android.media.ToneGenerator;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StatFs;
import android.os.storage.IMountService;
import android.os.storage.StorageManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.cecee.imove.JniFunction;
import com.cecee.imove.MIDIManager;
import com.cecee.imove.R;
import com.cecee.imove.data.FileConfig;
import com.cecee.imove.data.GlobalValue;
import com.cecee.imove.directoryselector.DirectorySelectorDialog;
import com.cecee.imove.directoryselector.DirectorySelectorListener;
import com.cecee.imove.frontkeytest.PopupFrontKeyTest;
import com.cecee.imove.lyric.CeceeLabLyricManager;
import com.cecee.imove.midi.CMidiManager;
import com.cecee.imove.remote.RemoteController;
import com.cecee.imove.scoreview.ScoreBarView;
import com.cecee.imove.staffview.StaffDraw;
import com.cecee.imove.view.KaraokeVideoView;
import com.cecee.imove.view.LyricView;
import com.cecee.imove.view.PopupDreamNRPN;
import com.danalenter.karaoke.dalkommpartykaraoke.record.AudioRecorder;
import com.danalenter.karaoke.dalkommpartykaraoke.setting.DKDefaultSetting;
import com.danalenter.karaoke.dalkommpartykaraoke.view.DKRecordDialog;
import com.danalenter.karaoke.dalkommpartykaraoke.view.DKSettingData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import CP3Player.app.CP3File;

import static java.lang.Thread.sleep;

public class MainActivity extends Activity {
    private static final String TAG = "#@#";
    Timer timer = new Timer();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ImageView imageView_calendar;
    private TextView textView_calendar;
    private int[] mDataset = {333, 222, 333, 444, 555, 355, 377, 256, 666, 101, 111, 222};
    Context mContext = this;
    private ScaleData scaleData = new ScaleData();
    DatePickerDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView_calendar = (ImageView) findViewById(R.id.id_imageview_calendar);
        textView_calendar = (TextView)findViewById(R.id.id_textview_calendar);
        mRecyclerView = (RecyclerView) findViewById(R.id.id_recyclerview_scale);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        scaleData.setArryValue(mDataset);

        mAdapter = new ScaleAdapter(scaleData);
        mRecyclerView.setAdapter(mAdapter);
        imageView_calendar.setOnClickListener(new MyListener());

        // DatePickerDialog
        dialog = new DatePickerDialog(this, listener, 2013, 9, 22);
        timer.schedule(timerTask, 0, 1000); //Timer 실행

    }
    class MyListener implements View.OnClickListener {
        int i = 0;
        @Override
        public void onClick(View v) {
            //iv.setImageResource(ImageId[i]);
            //tv.setText("이미지뷰: " + i);
            Log.d(TAG,"MyListener~~~");
            dialog.show();
        } // end onClick
    } // end MyListener()
    private DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Toast.makeText(getApplicationContext(), year + "년" + monthOfYear + "월" + dayOfMonth +"일", Toast.LENGTH_SHORT).show();
        }
    };

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    long now = System.currentTimeMillis();
                    Date date = new Date(now);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy년MM월dd일 hh:mm:ss");
                    String getTime = sdf.format(date);

                    textView_calendar.setText(getTime);

                    mDataset[0] = mDataset[0] + 1;
                    scaleData.setArryValue(mDataset);
                    // Log.d("#@#","runOnUiThread scale:->"+scale[0]);
                    mAdapter.notifyDataSetChanged();
                }
            });
        }

    };
}
