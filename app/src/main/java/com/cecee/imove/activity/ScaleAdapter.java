package com.cecee.imove.activity;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cecee.imove.R;

public class ScaleAdapter extends RecyclerView.Adapter<ScaleAdapter.ScaleViewHolder> {

    //private int[] mDataset;
    private Context context;
    private ScaleData scaleData;
    private int[] scale;
    private String str;
    public static class ScaleViewHolder extends RecyclerView.ViewHolder {
        //recycleView안에 단위블럭을 v로 받고 v에 구성되어 있는 각각의 view를 인스턴스화한다.
        //지금은 텍스트뷰 하나밖에 없다.
        private final Button mButton,mBtnCircle;
        //private final FrameLayout mLayout;
        private final LinearLayout mLinear0,mLinear1;


        public ScaleViewHolder(View v) {
            super(v);
            mButton=v.findViewById(R.id.button_unit_scale);
            mBtnCircle=v.findViewById(R.id.button_circle);
            mLinear0=v.findViewById(R.id.linearlayout_btn);
            mLinear1=v.findViewById(R.id.linearlayout_cir);
            //mLayout=v.findViewById(R.id.relativelayout_unitscale);
        }
    }

    public ScaleAdapter(ScaleData dataSet) {
        scaleData = dataSet;
        scale = scaleData.getArryValue();
      //  Log.d("#@#","ScaleAdapter scale:->"+scale[2]);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ScaleAdapter.ScaleViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        //똥가리 전체를 감싼뷰
        //scale = scaleData.getArryValue();
        //Log.d("#@#","scale:->"+scale.toString());

        LinearLayout v = (LinearLayout)LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.unit_scale, viewGroup, false);//xml file 이름
        ScaleViewHolder vh= new ScaleViewHolder(v);
        context = viewGroup.getContext();
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ScaleViewHolder viewHolder, final int position) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        int index[]={0,6,1,7,2,8,3,9,4,10,5,11};
        int pos=index[position];

        LinearLayout.LayoutParams params =(LinearLayout.LayoutParams)viewHolder.mButton.getLayoutParams();
        str=String.valueOf(scale[pos]);
        viewHolder.mButton.setText(str);
        str = String.valueOf(pos+1);
        viewHolder.mBtnCircle.setText(str);
        if(position%2==0) {

            viewHolder.mLinear0.setGravity(Gravity.LEFT);
            viewHolder.mLinear1.setGravity(Gravity.RIGHT);
            viewHolder.mButton.setBackground(ContextCompat.getDrawable(context, R.drawable.unit_left_button));
            viewHolder.mButton.getBackground().setColorFilter(0xFF00FFFF, PorterDuff.Mode.SRC_ATOP);
        }
        else {
            viewHolder.mLinear0.setGravity(Gravity.RIGHT);
            viewHolder.mLinear1.setGravity(Gravity.LEFT);
            viewHolder.mButton.setBackground(ContextCompat.getDrawable(context, R.drawable.unit_right_button));
            viewHolder.mButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.SRC_ATOP);
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return (scale==null) ? 0 : scale.length;
    }
}