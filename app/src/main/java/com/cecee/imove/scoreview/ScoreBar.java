package com.cecee.imove.scoreview;

public class ScoreBar {
    protected final String TAG = getClass().getSimpleName();

    public int distFromOrigin = 0;
    public int x;
    public int y;
    public int lineno;
    public int octave;
    public int width;
    public boolean checked_score;
    public int[] combo_cnt;

    private int initX;


    public ScoreBar(int x, int y, int lineno, int octave, int xlen) {
        init(x, y, lineno, octave, xlen);
        //Log.d (TAG, "this.initXY_" + this.initX + ", " + this.initY );
    }

    public void init(int x, int y, int lineno, int octave, int xlen) {
        this.initX = this.x = x;
        this.y = y;
        this.lineno = lineno;
        this.octave = octave;
        this.width = xlen;
        this.checked_score = false;
        this.combo_cnt = new int[]{0, 0, 0, 0, 0}; //perfect, great, normal, bad, no mic
        distFromOrigin = 0;
    }

    public synchronized void noDisplayLineno() {
        lineno = -1;
    }

    public synchronized void setXpos(int xpos) {
        x = xpos;
    }

    public synchronized void setComboCount(int type, int value) {
        combo_cnt[type] = value;
    }

    public synchronized void setCheckedScore(boolean checked) {
        checked_score = checked;
    }

    public synchronized void setdistFromOrigin(int dist) {
        distFromOrigin = dist;
    }

}