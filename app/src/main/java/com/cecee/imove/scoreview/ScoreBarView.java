package com.cecee.imove.scoreview;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Timer;

public class ScoreBarView extends SurfaceView implements SurfaceHolder.Callback {
    protected final String TAG = getClass().getSimpleName();

    private ScoreBarDrawingThread drawingThread;
    private Timer scorebar_timer;

    private ArrayList<ScoreBar> scoreBarList;

    private Context context;
    private int canvasWidth, canvasHeight;
    private int last_offset = 0;

    private int drawcnt = 0;
    private double score = 50.0;
    private int chkscore_cnt = 1;
    private int combo = 0;
    private int combo_type = 0;

    public ScoreBarView(Context context) {
        super(context);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.context = context;
    }

    public ScoreBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        this.context = context;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d (TAG, "surfaceChanged!!!!");
        canvasWidth = width;//-1568;
        canvasHeight = height;
        drawingThread.setSurfaceSize(width, height);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d (TAG, "ScoreBarView surfaceCreated!!!!");

        combo = 0;
        combo_type = 0;
        score = 0.0;
        chkscore_cnt = 1;

        if (scorebar_timer != null) scorebar_timer.cancel();

        drawingThread = new ScoreBarDrawingThread(holder, context);
        scoreBarList = drawingThread.getScoreBarList();
        
        scorebar_timer = new Timer();
        scorebar_timer.scheduleAtFixedRate(drawingThread, 0, 20);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (drawingThread != null) drawingThread.stopDrawing();
        if (scorebar_timer != null) scorebar_timer.cancel();
        Log.d (TAG, "ScoreBarView surfaceDestroyed!!!!");
    }

    public void AddScoreBar(int xpos, int ypos, int lineno, int octave, int xlen) {
        if (scoreBarList == null) return;

        synchronized (scoreBarList) {
            //Log.d (TAG, "ScoreBarView ADD__" + xpos + "_" + ypos + "_" + lineno + "_" + octave + "_" + xlen);
            scoreBarList.add(new ScoreBar(xpos, ypos, lineno, octave, xlen));
        }
    }
    
    public void ClearScoreBar() {
        if (scoreBarList == null) return;
        synchronized (scoreBarList) {
            scoreBarList.clear();
        }
    }

    public void stopScoreBar() {
        if (drawingThread != null) drawingThread.stopDrawing();
        if (scorebar_timer != null) scorebar_timer.cancel();
    }

    public void setMidiStatus(int ms, int posx) {
        drawingThread.setMidiStatus(ms, posx);
    }

    public int getVerticalbarPosition() {
        return drawingThread.getVerticalbarPosition();
    }
}
