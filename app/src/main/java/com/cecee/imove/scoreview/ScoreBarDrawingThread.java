package com.cecee.imove.scoreview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.view.SurfaceHolder;


import com.cecee.imove.R;

import java.util.ArrayList;
import java.util.TimerTask;

class ScoreBarDrawingThread extends TimerTask {
    protected final String TAG = getClass().getSimpleName();

    private boolean isrun = true;
    private SurfaceHolder surfaceHolder;

    private ArrayList<ScoreBar> scoreBarList = new ArrayList<ScoreBar>();
    private ArrayList<ScoreBar> passList = new ArrayList<ScoreBar>();
    private int canvasWidth;
    private int canvasHeight;
    private Paint[] combo_paint;
    private Paint clear_paint;
    private Paint text_paint;//, text_stroke_paint;
    private int checkpos = 560; //vertical bar
    private int midi_status, score_posx;
    private int verticalbar_yPos;
    private Bitmap sparkImage[] = new Bitmap[5];
    private int sparkidx = 0;

    public ScoreBarDrawingThread(SurfaceHolder surfaceHolder, Context mContext) {
        this.surfaceHolder = surfaceHolder;

        this.text_paint = new Paint();
        text_paint.setAntiAlias(true);
        text_paint.setTextSize(50);
        text_paint.setStyle(Paint.Style.FILL);
        text_paint.setColor(Color.WHITE);

        this.combo_paint = new Paint[6];
        for (int i = 0; i < 6; i++) {
            combo_paint[i] = new Paint();
            combo_paint[i].setAntiAlias(true);
            combo_paint[i].setStyle(Paint.Style.STROKE);
            combo_paint[i].setStrokeWidth(20);
        }
        combo_paint[0].setColor(0xFF176EFF); //perfect bar
        combo_paint[1].setColor(0xFF0CE33A); //great bar
        combo_paint[2].setColor(0xFFFFBF11); //normal bar
        combo_paint[3].setColor(0xFFE50505); //bad bar
        combo_paint[4].setColor(0xFF9E9E9E); //checking pass bar
        combo_paint[5].setColor(0xFFFFFFFF); //not yet check bar

        this.clear_paint = new Paint();
        clear_paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        sparkImage[0] = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.score_light_effect00)).getBitmap();
        sparkImage[1] = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.score_light_effect01)).getBitmap();
        sparkImage[2] = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.score_light_effect02)).getBitmap();
        sparkImage[3] = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.score_light_effect03)).getBitmap();
        sparkImage[4] = ((BitmapDrawable) mContext.getResources().getDrawable(R.drawable.score_light_effect04)).getBitmap();
    }

    @Override
    public void run() {
        //while (isrun) {
        Canvas c = null;
        try {
            c = surfaceHolder.lockCanvas(null);
            synchronized (surfaceHolder) {
                doDraw(c);
            }
        } finally {
            if (c != null) {
                surfaceHolder.unlockCanvasAndPost(c);
            }
        }
    }

    private void doDraw(Canvas c) {
        try {
            c.drawRect(0, 0, canvasWidth, canvasHeight, clear_paint); //clear
            //c.drawColor(0x7f000000, PorterDuff.Mode.OVERLAY); //clear
            int score_key = (midi_status >> 24) & 0xff; //mid key
            int score_velocity = (midi_status >> 16) & 0xff; //Mic line number    // velocity의 값 범위 = 127이다.
            int mic_key = (midi_status >> 8) & 0xff; //mic key
            int mic_velocity = midi_status & 0xff;//Mic Velocity==> 원래는 밑에서 받으로 했는데 포기했음

            int score_key_mod = score_key % 12; // C C# D D# E F F# G G# A A# B = 12개 임. (점수 체크 특성상 옥타브는 무시 = 낮은도(C1) 높은도(C2) 더 높은도(C3)는 같은것으로 본다. )
            int mic_key_mod = mic_key % 12;

            synchronized (scoreBarList) {
                for (int i = 0; i < scoreBarList.size(); i++) {
                    ScoreBar p = scoreBarList.get(i);
                    int x = p.x - (score_posx - checkpos);
                    int lmargin = 10;
                    int xwitdth = x + p.width;
                    boolean addPass = true;

                    if (xwitdth < 0 || x > canvasWidth) {
                        if (xwitdth < 0) {
                            scoreBarList.remove(i);
                            i--;
                        }
                    } else {
                        if (x < checkpos) {
                            synchronized (passList) {
                                for (int ii = 0; ii < passList.size(); ii++) {
                                    ScoreBar pp = passList.get(ii);
                                    if (pp.x == p.x) addPass = false;
                                }
                            }

                            if (addPass) {
                                passList.add(p);
                                addPass = true;
                            }
                            if (xwitdth > checkpos)
                                c.drawLine(checkpos + 8, p.y, xwitdth - lmargin, p.y, combo_paint[5]); //not yet check bar
                        } else {
                            c.drawLine(x, p.y, xwitdth - lmargin, p.y, combo_paint[5]); //not yet check bar
//                            if (p.lineno != -1) {
//                                switch (p.octave) {
//                                    case 1: //한옥타브아래
//                                        c.drawText(p.lineno + "", x + 4, p.y - 28, text_paint);
//                                        c.drawText(".", x + 11, p.y - 18, text_paint);
//                                        break;
//                                    case 2:
//                                        c.drawText(p.lineno + "", x + 4, p.y - 22, text_paint);
//                                        c.drawText(".", x + 11, p.y - 64, text_paint);
//                                        break;
//                                    default:
//                                        c.drawText(p.lineno + "", x + 4, p.y - 22, text_paint);
//                                        break;
//                                }
//                            }
                        }
                    }
                }
            }

            synchronized (passList) {
                for (int i = 0; i < passList.size(); i++) {
                    ScoreBar p = passList.get(i);
                    int x = p.x - (score_posx - checkpos);
                    int lmargin = 10;
                    int xwitdth = x + p.width;

                    if (xwitdth < 0 || x > canvasWidth) {
                        if ((xwitdth) < 0) {
                            passList.remove(i);
                            i--;
                        }
                    } else {
                        if (x < checkpos) {
                            if (xwitdth > checkpos) xwitdth = checkpos;
                            if ((p.x + p.width) > score_posx) {
                                if (mic_key > 0) {
                                    String skill_text = null;
                                    int key_diff = Math.abs(score_key_mod - mic_key_mod);
                                    if (score_velocity > 0) {    // 노트가 있는 = 부를게 있는 상황.
                                        if (mic_velocity > 7) {    // 목소리를 내고 있는 상황. 잡음이 크지 않은 상황에서 목소리 내면 8이상 갚은 나온다. (실험적 체크된 사항)
                                            if (key_diff == 0) {
                                                skill_text = "Perfect!!!";
                                                p.setComboCount(0, p.combo_cnt[0] + 1);
                                            } else if (key_diff == 1) {
                                                skill_text = "Great!!";
                                                p.setComboCount(1, p.combo_cnt[1] + 1);
                                            } else if (key_diff == 2) {
                                                skill_text = "Good!";
                                                p.setComboCount(2, p.combo_cnt[2] + 1);
                                            } else if (key_diff == 3) {
                                                skill_text = "Normal";
                                                p.setComboCount(3, p.combo_cnt[3] + 1);
                                            } else if (key_diff >= 4) {
                                                skill_text = "Bad";
                                                p.setComboCount(4, p.combo_cnt[4] + 1);
                                            }
                                        } else {  // 목소리가 없는 상황.
                                            skill_text = "Bad";
                                        }
                                    } else {  // 노트가 = 부를게 없으니 점수 체크 예외.
                                        skill_text = ".....";
                                    }
                                    System.out.printf("#@#마이크[%x] Score[%d][%d] == Mic[%d][%d] == [[%s]]\n", midi_status, score_key_mod, score_velocity, mic_key_mod, mic_velocity, skill_text);

                                    if (key_diff == 0) verticalbar_yPos = p.y;
                                    else if (key_diff > -12 && key_diff < 12) verticalbar_yPos = p.y + (key_diff*16);
                                } else {
                                    verticalbar_yPos = 0;
                                }

                                c.drawLine(x, p.y, xwitdth - lmargin, p.y, combo_paint[4]); //checking bar

                                c.drawBitmap(sparkImage[sparkidx % 5], checkpos + 4, p.y - 54, combo_paint[4]);
                                sparkidx++;
                            } else {
                                int max = p.combo_cnt[0];
                                int combo_type = 0;
                                for (int j = 1; j < p.combo_cnt.length; j++) {
                                    if (p.combo_cnt[j] > max) {
                                        combo_type = j;
                                        max = p.combo_cnt[j];
                                    }
                                }
                                if (p.combo_cnt[combo_type] == 0) combo_type = 4;
                                c.drawLine(x, p.y, xwitdth - lmargin, p.y, combo_paint[combo_type]); //checked bar
                                if (!p.checked_score) {
                                    //Log.d (TAG, xwitdth + "_[" + combo_type + "]_" + p.combo_cnt[0] + "_" + p.combo_cnt[1] + "_" + +p.combo_cnt[2] + "_" + p.combo_cnt[3] + "_" + p.combo_cnt[4]);
                                    p.setCheckedScore(true);
                                }
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException e) {
            Log.d(TAG, "doDraw NullPointerException_" + e.getMessage());
        }
    }

    public void stopDrawing() {
        this.isrun = false;
    }

    public ArrayList<ScoreBar> getScoreBarList() {
        return scoreBarList;
    }

    public ArrayList<ScoreBar> getPassScoreBarList() {
        return passList;
    }

    public void setSurfaceSize(int width, int height) {
        canvasWidth = width;
        canvasHeight = height;
    }

    public void setMidiStatus(int ms, int posx) {
        midi_status = ms;
        score_posx = posx;
    }

    public int getVerticalbarPosition() {
        return verticalbar_yPos;
    }
}
