package com.cecee.imove.data;

import java.io.File;
import java.util.ArrayList;

//        root@firefly:/system/data_l/GOG/disk1 # df
//        Filesystem               Size     Used     Free   Blksize
//        /dev                  1006.8M    44.0K  1006.7M   4096
//        /sys/fs/cgroup        1006.8M     0.0K  1006.8M   4096
//        /mnt/asec             1006.8M     0.0K  1006.8M   4096
//        /mnt/obb              1006.8M     0.0K  1006.8M   4096
//        /system                  1.5G   403.3M     1.1G   4096
//        /cache                 122.0M   132.0K   121.9M   4096
//        /metadata               11.7M    40.0K    11.7M   4096
//        /data                  991.9M   615.9M   376.0M   4096
//        /mnt/usb_storage      1006.8M     0.0K  1006.8M   4096
//        /mnt/shell/emulated    991.9M   615.9M   376.0M   4096
//        /system/data_l/GOG/disk1   916.8G   352.0M   916.4G   4096
//        root@firefly:/system/data_l/GOG/disk1 # ls -alR
//
//        .:
//
//        drwxrwxrwx system   system            2019-01-14 10:54 pcm_imove
//
//        ./pcm_imove:
//        drwxrwxrwx system   system            2019-01-11 10:00 KARAOKE_VIDEO
//        drwxrwxrwx system   system            2019-01-11 10:01 MIDI
//        drwxrwxrwx system   system            2019-01-11 10:06 fnt
//
//        ./pcm_imove/KARAOKE_VIDEO:
//        -rwxrwxrwx system   system   102086413 2019-01-11 10:00 BG_100000.mp4
//
//        ./pcm_imove/MIDI:
//        -rwxrwxrwx system   system   10685567 2019-01-11 10:01 11401724.mdk
//        -rwxrwxrwx system   system   23708837 2019-01-11 10:01 11410359.mdk
//        -rwxrwxrwx system   system   24093418 2019-01-11 10:01 11414398.mdk
//        -rwxrwxrwx system   system    7579832 2019-01-11 10:01 11416773.mdk
//        -rwxrwxrwx system   system   13829069 2019-01-11 10:01 11416871.mdk
//        -rwxrwxrwx system   system   11667669 2019-01-11 10:01 11416889.mdk
//        -rwxrwxrwx system   system      94634 2019-01-11 10:01 11440733.mdk
//
//        ./pcm_imove/fnt:
//        -rwxrwxrwx system   system   23275812 2019-01-11 10:06 ARIALUNI.TTF
//        -rwxrwxrwx system   system      48072 2019-01-11 10:06 Petrucci.ttf
//        -rwxrwxrwx system   system    9910468 2019-01-11 10:06 c1.ttc
//        -rwxrwxrwx system   system   21162756 2019-01-11 10:06 j3.ttc
//        -rwxrwxrwx system   system   13518660 2019-01-11 10:06 k1.ttc
//        -rwxrwxrwx system   system   22935320 2019-01-11 10:06 m.ttc
//        -rwxrwxrwx system   system    7403500 2019-01-11 10:06 t1.ttc
//        root@firefly:/system/data_l/GOG/disk1 #

public class FileConfig {

    // option1 : base path ( sdcard )
    private static final String basePath = "/mnt/sdcard";
    //private static final String basePath = "/system/data_l/GOG/disk1/pcm_imove";

    // option2 : base path ( hdd )
    // TODO : 다음 명령으로 소유지/퍼미션 맞춰주기 필요
    // root@firefly:/system/data_l/GOG/disk1 # chown -R system.system pcm_imove
    // root@firefly:/system/data_l/GOG/disk1 # chmod -R 777 pcm_imove

   // private static final String basePath = "/system/data_l/GOG/disk1/pcm_imove";

    public static final String videoFilePath = basePath + "/KARAOKE_VIDEO/BG_100000.mp4";  // bg video file path BG_100000.mp4
    public static final String midiDirPath = basePath + "/MIDI";   // default midi directory path
    public static final String fontDirPath = basePath + "/fnt";    // font directory path

    //public static final String DefaultFnt= fontDirPath + "/ARIALUNI.TTF"; // 현재 안쓰이는듯하다. m.tcc와 동일한것으로 추정.
    public static final String TaiwanFnt= fontDirPath + "/t1.ttc";
    public static final String ChinaFnt= fontDirPath + "/c1.ttc";
    public static final String KoreaFnt= fontDirPath + "/k1.ttc";
    public static final String JapanFnt= fontDirPath + "/j3.ttc";
    public static final String UniFnt= fontDirPath + "/m.ttc";
    public static final String MusFnt= fontDirPath + "/Petrucci.ttf";

    //필수 파일 체크 함수.
    public static String checkNotExistFilesNeed() {

        // 필요한 파일 다 있으면 null
        // 존재해지 않는 파일/디렉토리 찾으면 그 이름 리턴.
        String returnValue = null;

        ArrayList<String> mFiles = new ArrayList<String>();

     //   mFiles.add(videoFilePath);
        mFiles.add(midiDirPath);
        mFiles.add(fontDirPath);
        //mFiles.add(DefaultFnt);
        mFiles.add(TaiwanFnt);
        mFiles.add(ChinaFnt);
        mFiles.add(KoreaFnt);
        mFiles.add(JapanFnt);
        mFiles.add(UniFnt);
        mFiles.add(MusFnt);

        for (String s : mFiles) {
            File f = new File(s);
            if (!f.exists()) {
                // do something
                System.out.printf("#@#===cannot play without : %s\n", s);
                returnValue =  s;   // 종료.
                break;
            }
        }
        return returnValue;
    }
}
