package com.cecee.imove.data;

public abstract class BaseData {
    protected boolean avilable = true;

    public boolean isAvilable() {
        return avilable;
    }

    public void setAvilable(boolean avilable) {
        this.avilable = avilable;
    }

    public abstract void release();
}
