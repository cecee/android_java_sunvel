package com.cecee.imove.data;

import android.content.Context;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.Gravity.TOP;

/**
 * Created by Administrator on 2017-03-05.
 */
///mnt/usb_storage/USB_DISK1/udisk0/59/5934/593407M.M //VCD Song
public class GlobalValue {
    public static boolean debug = true;

    public static void CustomToast(Context context, String msg, int sz, int yoffset)
    {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        toast.setGravity(TOP, 0, yoffset);
        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, sz);
        messageTextView.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
        toast.show();
    }
}
