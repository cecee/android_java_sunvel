package com.cecee.imove;

import com.cecee.imove.data.BaseData;

public class OneLine extends BaseData {
    public String str="";
    public boolean align;
    public int Line_timeA;
    public int Line_timeB;
    //public int Line_TextPosA;
    public int Line_TextPosB;
    public int Line_BlkCnt;
    public int[] Blk_timeA;
    public int[] Blk_timeB;
    public int[] Blk_BaseTxtPosA;
    public int[] Blk_BaseTxtPosB;
    public int[] Blk_RubyTxtPosA;
    public int[] Blk_RubyTxtPosB;
    public String[] strBaseBlk;
    public String[] strRubyBlk;

    public OneLine(){
        Blk_BaseTxtPosA = new int[64];
        Blk_BaseTxtPosB = new int[64];
        Blk_RubyTxtPosA = new int[64];
        Blk_RubyTxtPosB = new int[64];
    }

    @Override
    public void release() {
        setAvilable(false);
        Blk_timeA = null;
        Blk_timeB = null;
        strBaseBlk = null;
        strRubyBlk = null;
        Blk_BaseTxtPosA = null;
        Blk_BaseTxtPosB = null;
        Blk_RubyTxtPosA = null;
        Blk_RubyTxtPosB = null;
    }
}