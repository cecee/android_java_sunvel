package com.cecee.imove.lyric;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;

import com.cecee.imove.JniFunction;
import com.cecee.imove.OneLine;
import com.cecee.imove.data.BaseData;
import com.cecee.imove.data.FileConfig;
import com.cecee.imove.view.arrBitmap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CeceeLabLyricManager extends BaseData {
    private static final String TAG = "CeceeLabLyricManager";

    private String FntPath="";
    private String song_path="";
    private String TaiwanFnt= FileConfig.TaiwanFnt;
    private String ChinaFnt= FileConfig.ChinaFnt;
    private String KoreaFnt= FileConfig.KoreaFnt;
    private String JapanFnt= FileConfig.JapanFnt;
    private String UniFnt=  FileConfig.UniFnt;
    private String MusFnt= FileConfig.MusFnt;   //maestro
    private String strGanju0="間奏請稍";//間奏請稍候     ///ppp 180913 끝글자를 다르게 쓴경우 있음 (15211)
    private String strGanju1="间奏请稍";
    private String strGanju2="謝謝演唱";
    private String strGanju3="谢谢演唱";
    // ----------------------------------------------------------
//  hLyricBox : 가사 그리는 Area 높이
//  hLineBox:  one line lyric   높이
//  yPosUpA: Lyric box에서 첫번째 line start ypos
//  yPosUpB: Lyric box에서 첫번째 line end ypos
//  yPosDnA: Lyric box에서 두번째 line start ypos
//  yPosDnB: Lyric box에서 두번째 line end ypos
//---------------------------------------------------------------
    //private final int MAX_LINE =256;
    public final int LyricWidth=1920;
    public final int wLyricBox=1920;
    public final int hLyricBox=400;
    public final int hOneLine=164;
    public final int yPosUpA=0;
    public final int yPosUpB = yPosUpA + hOneLine;//ppp 180630
    public final int yPosDnA=164;//180;//ppp 1920*400 박스안에서 값만큼 밑으로
    public final int yPosDnB = yPosDnA + hOneLine;//ppp 180630

    private  float LyricFontSz=104;//13*8
    private  float RubyRubyFontSz = 54;//80
    private  final int FONT_BASE_STOKE= 12;
    private  final int FONT_RUBY_STOKE= 8;
    private  int BaseHeight= 120;
    private  int RubyHeight= 50;//LyricFontSz+4
    private  int PaintHeight= 164;//LyricFontSz+4
    private int FONT_RUBY_BASELINE= 8;
    private int FONT_BASE_BASELINE= -12; //"-" -->위로 "+" 아래로
    public int Event_BringFront = -1;

    private  int mCurrentErX, mCurrentErY;

    private String  Str_MIDI_Info;

    private final int MAX_LINE =256;
    public int CurrErXpos;

    private int[] DColorIn= new int[]{Color.WHITE, Color.WHITE, Color.WHITE};
    private int[] DColorOut= new int[]{Color.BLACK, Color.BLACK, Color.BLACK};
    private int[] EColorIn= new int[]{0xff335cff, 0xffc0392b,0xff16a047}; //합창자막 녹색
    private int[] EColorOut= new int[]{Color.WHITE, Color.WHITE,Color.WHITE};

    private Paint textPaint, ruby_Paint;
    private Typeface LyricTypeface;
    private Rect text_bounds;

    private  int totalLyricLine;
    private  int[] Line_BlkCnt;
    private  int[] Line_timeA;
    private  int[] Line_timeB;
    private  int[] updn;  //0:up  1:dn 2: No draw //private  int width;
    private  int[] sex;  //0:남  1:여 2: 합창
    private  int[] Line_offset;
    private  int[] JULL_Pos;
    private  int[] CountDn_Pos;
    private  int[] CountDn_Bakja;
    private int Event_Dujul;
    ///------------------------------------------
    //   private final int LyricWidth=1920;
    //  private final int BaseHeight=180;//200;

    private int DispCurrentLine=-2;

    private boolean nextDujul=false;
    private Bitmap tx_normal_bm0, tx_erase_male_bm0, tx_erase_female_bm0, tx_erase_hap_bm0;//up line의 Bitmap
    private  Bitmap tx_normal_bm1, tx_erase_male_bm1, tx_erase_female_bm1, tx_erase_hap_bm1;//dn line의 Bitmap
    private  Bitmap newBmp;//, fade_normal_bm1;
    public arrBitmap ulist;
    private arrBitmap dlist;
    private arrBitmap nlist;
    List<OneLine> lineArry;
    private int paintListCnt;
    public  Bitmap DispBitmapArray[];
    public  Bitmap EraseBitmapArray[];
    private  Bitmap BmStaff;
    private ArrayList<arrBitmap> aListBitmap;

    public CeceeLabLyricManager(){
        init();
    }

    void init(){
        release();
        DispBitmapArray = new Bitmap [2];
        EraseBitmapArray = new Bitmap [2];
        lineArry = new ArrayList<>();
        aListBitmap = new ArrayList<>();
    }

    public void setLyricData(JniFunction jniCall){
        init();
        sex = jniCall.MIDIGetLineSex();
        updn = jniCall.MIDIGetLineUpDn();
        Line_BlkCnt = jniCall.MIDIGetLineBlkCnt();
        Line_timeA = jniCall.MIDIGetLineTimeA();
        Line_timeB = jniCall.MIDIGetLineTimeB();
        JULL_Pos  = jniCall.MIDIGetJullPosA();
        CountDn_Pos = jniCall.MIDIGetCountDnPos();
        CountDn_Bakja = jniCall.MIDIGetCountDnBakja();

        totalLyricLine = jniCall.songTotalLine();
        Line_offset = new int[totalLyricLine+1];

        lineArry.clear();
        for (int i = 0; i < totalLyricLine; i++) {
            OneLine loneline = PutCharPos(jniCall, i);//글자마다 포인트 계산
            lineArry.add(loneline);
        }
    }

    public OneLine PutCharPos(JniFunction jniCall, int line)
    {
        int hangul_ruby_test=0;//for test 한글변환 flag
        int[] Blk_timeA = jniCall.MIDIGetLineBaseBlkTimeA(line);
        int[] Blk_timeB = jniCall.MIDIGetLineBaseBlkTimeB(line);
        String[] strBaseBlk = jniCall.MIDIGetLineBaseBlk(line);
        String[] strRubyBlk;
        if(hangul_ruby_test==0)  {
            strRubyBlk = jniCall.MIDIGetLineRubyBlk(line);
        }
        else {
            //for test 한글변환
            strRubyBlk = new String[strBaseBlk.length];
            for (int i = 0; i < strBaseBlk.length; i++) {
                strRubyBlk[i] = Jaso.hangulToJaso(strBaseBlk[i]);
            }
        }

        OneLine oneLine = PutCharPos(line, Blk_timeA, Blk_timeB, strBaseBlk, strRubyBlk);
        if(oneLine != null){
            jniCall.MIDISetLineTextTime(oneLine.Blk_BaseTxtPosB, line, oneLine.Line_TextPosB );//각블럭 텟스트 끝점을 pDoc->ProcessLine[line].Blk_lBpoint[]에 넣음
        }
        return oneLine;
    }

    private OneLine PutCharPos(int line, int[] Blk_timeA, int[] Blk_timeB, String[] strBaseBlk, String[] strRubyBlk)
    {
        OneLine oneLine = new OneLine();
        int i;
        String imsi;
        String str;
        //char imsiChar[];

        oneLine.Line_timeA = Line_timeA[line];
        oneLine.Line_timeB = Line_timeB[line];
        oneLine.Line_BlkCnt = Line_BlkCnt[line];
        oneLine.Blk_timeA = Blk_timeA;
        oneLine.Blk_timeB = Blk_timeB;

        oneLine.strBaseBlk= strBaseBlk;
        oneLine.strRubyBlk= strRubyBlk;

        if(oneLine.Line_BlkCnt==0) return oneLine;
        str="";
        for(i=0;i<oneLine.Line_BlkCnt;i++)
        {
            imsi=oneLine.strBaseBlk[i];
            str=str+imsi;
        }
        oneLine.str=str;

        //Log.d("#@#!!!", "line["+line+"]  Line_str["+ oneLine.str + "]");

        oneLine.align=(str.contains(strGanju0) || str.contains(strGanju1)||
                str.contains(strGanju2)||str.contains(strGanju3))? true:false;

        OneLine lruby= RubyPos(oneLine);

        for(i=0;i<oneLine.Line_BlkCnt;i++)
        {
            oneLine.Blk_BaseTxtPosA[i] =  lruby.Blk_BaseTxtPosA[i];
            oneLine.Blk_BaseTxtPosB[i] =  lruby.Blk_BaseTxtPosB[i];
            oneLine.Blk_RubyTxtPosA[i] =  lruby.Blk_RubyTxtPosA[i];
            oneLine.strRubyBlk[i]=oneLine.strRubyBlk[i].replace("=","");
        }
        oneLine.Line_TextPosB = lruby.Line_TextPosB;

        return oneLine;
    }


    public String LyricCodeToFontPath(int lyricCode, int language){
        String fontPath = "";
        switch(lyricCode)
        {
            case 2://BIG5 Big5로 된곡에는  일본어를 쓴 경우가 있다...
                if(language==8) FntPath=JapanFnt;
                else fontPath=TaiwanFnt;
                break;
            case 3:// code는 BIG5이지만 디스플레이는 GB2로
                fontPath=ChinaFnt;///test for china font
                break;
            case 4://utf-8
                if(language<7) fontPath=TaiwanFnt;///test for china font
                else if(language==8) fontPath=JapanFnt;///test for china font
                else if(language==9) fontPath=KoreaFnt;//
                else fontPath=UniFnt;///test for china font
                break;
            case 1://Shift-Jis
                fontPath=JapanFnt;
                break;
            case 0://KS-5601
                fontPath=KoreaFnt;
                break;
            default:
                fontPath=UniFnt;
                break;

        }
        return fontPath;
    }

    public Bitmap makeBitmapWithText_outline(String _txt, int _in_color, int _out_color, int width,int ruby) {

        float base_line;
        int h=(ruby==0) ? BaseHeight : RubyHeight;
        Bitmap textBitmap = Bitmap.createBitmap(width, h, Bitmap.Config.ARGB_4444);
        textBitmap.eraseColor(0);
        // if(ruby==0) textBitmap.eraseColor(0x80804040);  else textBitmap.eraseColor(0x80408040);
        Canvas canvas = new Canvas(textBitmap);
        switch(ruby) {
            case 0:
                base_line = LyricFontSz + FONT_BASE_BASELINE;
                textPaint.setColor(_out_color);
                textPaint.setStyle(Paint.Style.STROKE);
                canvas.drawText(_txt, 0, base_line, textPaint);
                textPaint.setStyle(Paint.Style.FILL);
                textPaint.setColor(_in_color);
                canvas.drawText(_txt, 0, base_line, textPaint);
                break;
            case 1:
                base_line= RubyRubyFontSz - FONT_RUBY_BASELINE;
                ruby_Paint.setColor(_out_color);
                ruby_Paint.setStyle(Paint.Style.STROKE);
                canvas.drawText(_txt, 0, base_line, ruby_Paint);
                ruby_Paint.setStyle(Paint.Style.FILL);
                ruby_Paint.setColor(_in_color);
                canvas.drawText(_txt, 0, base_line, ruby_Paint);
                break;
        }
        return textBitmap;
    }

    public Bitmap mk_ruby_ov( int line, int baseCnt, int offset, int inC, int outC)//pp
    {
        int i, x, w;
        String imsiStr;
        Bitmap imsi;
        Bitmap bmRuby = Bitmap.createBitmap(LyricWidth, PaintHeight, Bitmap.Config.ARGB_4444);
        ///bmRuby.eraseColor(0x40404080);
        Canvas canvas = new Canvas(bmRuby);

        for (i = 0; i < baseCnt; i++)
        {
            imsiStr = lineArry.get(line).strBaseBlk[i];
            w=(int) textPaint.measureText(imsiStr);
            x = lineArry.get(line).Blk_BaseTxtPosA[i];
            //Log.d("#@#>> mk_ruby_ov", "#@# line["+line+"/"+i+ " ["+imsiStr+"] baseCnt["+baseCnt+"] Blk_TxtPosA["+lineArry.get(line).Blk_TxtPosA[i]+"]");
            if (w>0) {
                imsi = makeBitmapWithText_outline(imsiStr, inC, outC, w,0);
                canvas.drawBitmap(imsi, x + offset, 0+RubyHeight, null);
            }

            imsiStr = lineArry.get(line).strRubyBlk[i];
            w=(int) textPaint.measureText(imsiStr);
            x = lineArry.get(line).Blk_RubyTxtPosA[i];
            //Log.d("#@#>> mk_ruby_ov", "#@# line["+line+"/"+i+ " ["+imsiStr+"] baseCnt["+baseCnt+"] Blk_TxtPosA["+lineArry.get(line).Blk_TxtPosA[i]+"]");
            if (w>0) {
                imsi = makeBitmapWithText_outline(imsiStr, inC, outC, w,1);
                canvas.drawBitmap(imsi, x + offset, 0, null);
            }
        }
        return bmRuby;
    }

    public int[] getLineOffset(){
        return Line_offset;
    }

    public arrBitmap overlay_bmp_arry(int line, int sex)
    {
        arrBitmap mlist = new arrBitmap();
        Log.d(TAG, "#@#>>>overlay_bmp_arry!----line["+line+"] sex["+sex+"]");
        int offset = 0;
        int BaseCnt = lineArry.get(line).Line_BlkCnt;
        int Line_length = lineArry.get(line).Line_TextPosB;
        mlist.align = (lineArry.get(line).align)? true:false;

        if(mlist.align)  offset = (LyricWidth - Line_length) / 2;
        else {
            if (updn[line] == 0) offset = 150;
            else //(LineInfo.updn[line] == 1)
                offset = LyricWidth - (Line_length + 150);
        }
        Line_offset[line]=offset;
        if(sex>0)sex=sex%3; else sex=0;
        mlist.dispBitmap = mk_ruby_ov(line, BaseCnt, offset, DColorIn[sex], DColorOut[sex]);
        mlist.paintBitmap = mk_ruby_ov(line, BaseCnt, offset, EColorIn[sex], EColorOut[sex]);
        mlist.paintingLength = Line_length;
        // Log.d(TAG, "#@#>> overlay_bmp_arry mlist.dispBitmap["+mlist.dispBitmap+" / "+mlist.dispBitmap+ " ["+ mlist.paintingLength+"]");
        return mlist;
    }

    public void PutBitmapArray (int line){
        int mline, msex, mupdn;
        arrBitmap cc;
        if(line<0) return;
        Log.d(TAG, "#@#>>>-------PutBitmapArray!--------["+line+"]");
////////////////////////arry remove
        Iterator it = aListBitmap.iterator();
        while (it.hasNext()) {
            cc = (arrBitmap) it.next();
            if(line > cc.lineNum) {
                //cc.release();
                it.remove();
            }
        }
////////////////////////arry 확인
        int arrCnt = aListBitmap.size();
        int line_num=0;
        for (int i = 0; i < arrCnt; i++) {
            cc = aListBitmap.get(i);
            line_num=cc.lineNum;
        }
        //System.out.printf("#@#>>>(2)=Confirm arrlist !!!===ix[%d] lineNum[%d]  curline[%d] arrCnt[%d] totalLyricLine[%d]\n", arrCnt, line_num,line, arrCnt, totalLyricLine);

/////////////////////////
        for(int i = arrCnt; i < 4; i++)
        {
            mline=line + i;
            // System.out.printf("#@#>>>(2-1)=Confirm arrlist !!! arrCnt[%d] line[%d] line_num[%d]\n", arrCnt, line, line_num);

            if(totalLyricLine<= mline) continue;
            if (line_num > mline) continue;
            mupdn = updn[mline];
            msex = sex[mline];

            //System.out.printf("#@#>>>(2-2)=Confirm arrlist[%d]= mline[%d] mupdn[%d] msex[%d]\n",i, mline, mupdn, msex);

            arrBitmap cb = new arrBitmap();
            if(mupdn >= 0){
                //    PutCharPos(mline,i);
                cb = overlay_bmp_arry(mline,  msex);
            }
            else{
                cb.release();
            }

            cb.lineNum = mline;
            cb.lineSex = msex;
            cb.updn = mupdn;
            // Log.d(TAG, "#@#>> lineNum["+cb.lineNum+"]  dispBitmap["+cb.dispBitmap+"]  paintBitmap["+cb.paintBitmap+"]  ");
            aListBitmap.add(cb);
        }
    }

    public void MoveBitmapArray (int line){
        int i,ix,mNextUpdn;
        if(line<0) return;
        //       IBUtil.Log(TAG, "#@#-------MoveBitmapArray!--------["+line+"]");
        arrBitmap cc;
        Iterator it = aListBitmap.iterator();
        while (it.hasNext()) {
            cc = (arrBitmap) it.next();
            if(line >= cc.lineNum) {
                //cc.release();
                it.remove();
            }
        }
        //첫번째가 가사없으면 지움
        if(aListBitmap.size()>0){
            cc = aListBitmap.get(0);
            if(cc.updn==-1) {
                //cc.release();
                aListBitmap.remove(0);
            }
        }
        //////////////////////arry 확인
        for (ix = 0; ix < aListBitmap.size(); ix++) {
            cc = aListBitmap.get(ix);
            System.out.printf("#@#(2)MoveBitmapArray======= Confirm arrlist !!!===ix[%d] lineNum[%d]  curline[%d]\n",ix, cc.lineNum,line);
        }
///////////////////////
    }

    public void DrawLineClean(int updn)
    {
        if(updn==0)
        {
/*
            if(tx_normal_bm0 != null && !tx_normal_bm0.isRecycled())
                tx_normal_bm0.recycle();
            tx_normal_bm0=null;

            if(tx_erase_male_bm0 != null && !tx_erase_male_bm0.isRecycled())
                tx_erase_male_bm0.recycle();
            tx_erase_male_bm0=null;

            if(tx_erase_female_bm0 != null && !tx_erase_female_bm0.isRecycled())
                tx_erase_female_bm0.recycle();
            tx_erase_female_bm0=null;

            if(newBmp != null && !newBmp.isRecycled())
                newBmp.recycle();
            newBmp=null;
*/
            tx_normal_bm0=null;
            tx_erase_male_bm0=null;
            tx_erase_female_bm0=null;
            newBmp=null;

        }
        else
        {

            /*
            if(tx_normal_bm1 != null && !tx_normal_bm1.isRecycled())
                tx_normal_bm1.recycle();
            tx_normal_bm1=null;

            if(tx_erase_male_bm1 != null && !tx_erase_male_bm1.isRecycled())
                tx_erase_male_bm1.recycle();
            tx_erase_male_bm1=null;

            if(tx_erase_female_bm1 != null && !tx_erase_female_bm1.isRecycled())
                tx_erase_female_bm1.recycle();
            tx_erase_female_bm1=null;

            if(newBmp != null && !newBmp.isRecycled())
                newBmp.recycle();
            newBmp=null;
            */
            tx_normal_bm1=null;
            tx_erase_male_bm1=null;
            tx_erase_female_bm1=null;
            newBmp=null;

        }
    }

    public void JamakAllClean() {
        Log.e(TAG, "#@#@@ --------------------JamakAllClean-------------------------");
        try{
            for(arrBitmap bitmap : aListBitmap)
                bitmap.release();
            aListBitmap.clear();

            for(Bitmap bitmap : DispBitmapArray)
            {
                if(bitmap != null && !bitmap.isRecycled())
                    bitmap.recycle();
            }
            DispBitmapArray[0]=DispBitmapArray[1]=null;

            Event_Dujul=1;
        }
        catch(Exception e)
        {
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///가사라인그리는 조건
    /// 1) line number가 -1 일때 가사 line 0--up, line 1--dn에그린다.
    /// 2) line number가 1 이상 일때 line number +1의줄을 그린다.
    ///    1.  up dn위치는 기존에 그려진 up/dn를 비교하여 줄번호가 낮은쪽에 그린다.
    ///				1.1 up위치에 그릴때 up_line값이 -1이 있으면 line number값에서 부터 SCAN해서 가사내용이 있을때 까지
    ///						SCAN해서 내용이 있는 줄을 up에 그리고 dn에 다음줄 그린다.
    ///    2.  up 에 그릴때 그릴 내용이 없으면 그리지 않고 up_line값에 -1를 준다.
    /// 3) 곡이 끝나면 다 지운다.
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public int curDrawBitmap(int line)
    {
        if(line < -1) return 0;
        if(line<=-1) {
            line=0;
        }
        System.out.printf("#@#@@@curDrawBitmap line[%d] ---------\n",line);
        PutBitmapArray(line);
        MoveBitmapArray(line);
        return 1;
    }

    public int ProcessLyric(int line) {
        if(line>=0) {
            CurrErXpos=0;
            curDrawBitmap(line);
        }
        return line;
    }

    public OneLine RubyPos(OneLine lone_line)
    {
        String imsiBase,imsiRuby;
        String  _ruby="=";
        int i,ix;
        int bCnt=lone_line.Line_BlkCnt;
        String[] conbine_base_str; conbine_base_str=new String[bCnt+2];
        int[] conbine_base_cnt;  conbine_base_cnt=new int[bCnt+2];

        String[] mbase_str=new String[bCnt+2];
        String[] mruby_str=new String[bCnt+2];
        int[] mbase_len= new int[bCnt+2];
        int[] mruby_len= new int[bCnt+2];
        int[] mblk_len= new int[bCnt+2];
        int[] mblk_pos= new int[bCnt+2];
        int[] mbase_cnt= new int[bCnt+2];
        boolean[] m_islong= new boolean[bCnt+2];//true:base is long
        int mBlk_cnt =0;

        int[] conbine_base_len; conbine_base_len=new int[bCnt+2];
        int[] ruby_len; ruby_len=new int[bCnt+2];
        int[] base_len; base_len=new int[bCnt+2];
        ///////(1) base ruby block string 만든다
        for(i=0;i<bCnt;i++)
        {
            imsiRuby=lone_line.strRubyBlk[i]; imsiBase=lone_line.strBaseBlk[i];
            //         System.out.printf("#@#=bCnt[%d]==imsiBase[%s] imsiRuby[%s]\n",i, imsiBase,imsiRuby);
            if(imsiRuby.length()>0 && !imsiRuby.contains(_ruby)){
                conbine_base_cnt[i]=0;
                imsiBase= lone_line.strBaseBlk[i]; conbine_base_cnt[i]++;
                for(ix=(i+1);ix<bCnt;ix++){
                    if(lone_line.strRubyBlk[ix].contains(_ruby)) {imsiBase= imsiBase + lone_line.strBaseBlk[ix]; conbine_base_cnt[i]++;}
                    else break;
                }
                conbine_base_str[i] =imsiBase;
            }
            else if(imsiRuby.contains(_ruby))  conbine_base_str[i] =imsiBase;
            else  conbine_base_str[i]=imsiBase;
        }


        //(2) base ruby block length 만든다
        for(i=0;i<bCnt;i++)
        {
            imsiBase=lone_line.strBaseBlk[i]; imsiRuby=lone_line.strRubyBlk[i];
            base_len[i] =(imsiBase.isEmpty()) ? 0: (int) textPaint.measureText(imsiBase);
            conbine_base_len[i] =(conbine_base_str[i].isEmpty()) ? 0: (int) textPaint.measureText(conbine_base_str[i]);
            if(imsiRuby.contains(_ruby)) ruby_len[i]=ruby_len[i-1]; //i-1 이 반드시 존재한다.
            else if(imsiRuby.isEmpty()) ruby_len[i]=0;
            else ruby_len[i]=(int) ruby_Paint.measureText(imsiRuby);
        }
        mBlk_cnt=0;
        for(i=0;i<bCnt;i++)
        {
            int subSz=conbine_base_cnt[i];
            if(subSz==0 && (lone_line.strRubyBlk[i].contains(_ruby)))continue;
            mbase_str[mBlk_cnt]=conbine_base_str[i];
            mruby_str[mBlk_cnt]=lone_line.strRubyBlk[i];
            mbase_cnt[mBlk_cnt]=conbine_base_cnt[i];
            mbase_len[mBlk_cnt]=conbine_base_len[i];
            mruby_len[mBlk_cnt]=ruby_len[i];
            mBlk_cnt++;
        }

        ///block length
        int total_length=0;
        mblk_pos[0]=0;
        for(i=0;i<mBlk_cnt;i++)
        {
            mblk_len[i]= ( mbase_len[i] >= mruby_len[i]) ? mbase_len[i] : mruby_len[i];
            m_islong[i]= ( mbase_len[i] >= mruby_len[i]) ? true : false;
            total_length=total_length+mblk_len[i];
            mblk_pos[i+1]=total_length;
        }
//        for(i=0;i<mBlk_cnt;i++)
//        {
//            System.out.printf("#@#==mBlk_cnt[%d/%d/%d] mbase[%s/%d][%d] combR[%s/%d] mblk_len[%d] mblk_pos[%d] m_islong[%B]\n", i,mBlk_cnt, bCnt,  mbase_str[i],mbase_len[i], mbase_cnt[i], mruby_str[i],mruby_len[i], mblk_len[i], mblk_pos[i],m_islong[i]);
//        }
//        System.out.printf("#@#==total_length[%d]\n",total_length);

        int sPointCnt=0; int sub; int blk_id=0;
        int[] sPonit_base= new int[bCnt+2];
        int[] sPonit_ruby= new int[bCnt+2];
        for(i=0;i<mBlk_cnt;i++)
        {
            int offset_pos= mblk_pos[i];
            if(m_islong[i]){
                sub=(mbase_cnt[i]==0) ? 1: mbase_cnt[i];
                for(ix=0;ix<sub;ix++){
                    if(ix==0) sPonit_base[sPointCnt] = offset_pos;
                    else  sPonit_base[sPointCnt] = sPonit_base[sPointCnt-1] + base_len[blk_id+ix];
                    if(ix==0) sPonit_ruby[sPointCnt]= offset_pos+ (mblk_len[i]-mruby_len[i])/2;
                    else sPonit_ruby[sPointCnt]=0;
                    sPointCnt++;
                }
            }
            else{
                sub=(mbase_cnt[i]==0) ? 1: mbase_cnt[i];
                for(ix=0;ix<sub;ix++){
                    if(ix==0) sPonit_base[sPointCnt] = offset_pos+(mruby_len[i]-mblk_len[i])/2;
                    else  sPonit_base[sPointCnt] = sPonit_base[sPointCnt-1] + base_len[blk_id+ix];
                    if(ix==0) sPonit_ruby[sPointCnt]= offset_pos;
                    else sPonit_ruby[sPointCnt]=0;
                    sPointCnt++;
                }
            }
            blk_id=blk_id+mbase_cnt[i];
        }

//        for(i=0;i<sPointCnt;i++)
//        {
//            System.out.printf("#@#==sPointCnt[%d/%d] sPonit_base[%d] sPonit_ruby[%d]\n",i, sPointCnt, sPonit_base[i], sPonit_ruby[i]);
//        }

        for(i=0;i<bCnt;i++)
        {
            lone_line.Blk_BaseTxtPosA[i]=  sPonit_base[i];
            lone_line.Blk_RubyTxtPosA[i]=  sPonit_ruby[i];
        }

        for(i=0;i<bCnt-1;i++)
        {
            lone_line.Blk_BaseTxtPosB[i]= sPonit_base[i+1];
        }
        lone_line.Blk_BaseTxtPosB[bCnt-1]= total_length;
        lone_line.Line_TextPosB=total_length;

        return lone_line;
    }

    public int  bringToFront(int updn)
    {
        paintListCnt = aListBitmap.size();
        System.out.printf("#@#@@ 1) bringToFront++ paintListCnt[%d]--currentline[%d/%d]\n",paintListCnt, DispCurrentLine, totalLyricLine);

        ulist = dlist = nlist = null;
        if(nextDujul) updn=2;

        for (int i = 0; i < paintListCnt; i++) {
            arrBitmap tlist = aListBitmap.get(i);
            if(ulist==null && tlist.updn==0)
                ulist=tlist;
            if(dlist==null && tlist.updn==1)
                dlist=tlist;
        }
        if(ulist!=null){
            DispBitmapArray[0] = ulist.dispBitmap;
            EraseBitmapArray[0] = ulist.paintBitmap;
            //System.out.printf("#@#@@ 2-1) ulist[%d][%d][%d] \n",ulist.lineNum, ulist.updn, ulist.paintingLength);
        }
        else{
            DispBitmapArray[0] = null;
            EraseBitmapArray[0] = null;
        }
        if(dlist!=null){
            // nextDujul=false;
            DispBitmapArray[1] = dlist.dispBitmap;
            EraseBitmapArray[1] = dlist.paintBitmap;
            //System.out.printf("#@#@@ 2-2) dlist[%d][%d][%d] \n",dlist.lineNum, dlist.updn, dlist.paintingLength);
        }
        else{
            // nextDujul=true;
            DispBitmapArray[1] = null;
            EraseBitmapArray[1] = null;
        }
        //nextDujul= (ulist.ganju) ? true:false;
        Event_BringFront = updn;
        //  Log.d(TAG, "#@# 3) bringToFront["+Event_BringFront+"] DispBitmapArray[0]["+ DispBitmapArray[0]+"]  DispBitmapArray[1]["+DispBitmapArray[1]+"]");
        return 1;
    }

    int LineUpdn = 0;
    public boolean Event_30P = false;
    public boolean Event_NewLine = false;
    int latestDispLine = 0;
    public boolean  process_tick(JniFunction jniCall, int currentTick, int currentLine){

        if(latestDispLine != currentLine)
        {
            latestDispLine = currentLine;
            CurrErXpos = 0;
            Event_NewLine = true;
            Event_30P = false;

            if(currentLine < -1) {
                JamakAllClean();
                return true;
            }

            if(currentLine < 0){
                JamakAllClean();
                LineUpdn = 0;
                PutBitmapArray(0);
            }
            else {
                LineUpdn = updn[currentLine];
            }

            this.DispCurrentLine = currentLine;
            int sz = aListBitmap.size();
            //DispCurrentLine가 aListBitmap에 없으면 PutBitmapArray만듬 (라인이 바뀌었는데 아직 bit map이 준비 안된경우
            boolean is_num = false;
            for (int i = 0; i < sz; i++)
                if (aListBitmap.get(i).lineNum == currentLine) is_num = true;
            if (is_num == false) PutBitmapArray(currentLine);


            int flg = LineUpdn;
            if(Event_Dujul == 1)
            {
                flg=2;
                Event_Dujul = 0;
            }
            else if(Event_Dujul == 2){//앞으로갈때 현재가 up-->다음엔 아래
                flg = 3;
                Event_Dujul = 0;
            }
            bringToFront(flg);

            msgSendProcssLyric(currentLine);
        }

        int mPos = JamakGetEresePos(jniCall, currentTick, currentLine);
        if(mPos > 0)
            CurrErXpos = mPos;

        return true;

    }


    //=====================================================================
//자막 지우는 Sub routine
//  [필요한 값]
// 1)현재시간
// 2)현재 지울 줄번호
// 3)지울위치(up/Dn)
// 4)지울 라인의 전체 길이
// 5)지우는 시작 point
// 6)지우는 라인의 전체시간 폭
// 7)지우는 시작시간
// Male/Femail Flag
// 현재 시간에서 현재 픽셀 포인트 계산
//======================================================================
    public int JamakGetEresePos(JniFunction jniCall, int currentTick, int currentLine)
    {
        int earse_point=0;
        int move_dx=0;
        int move_ratio=0;
        int mPosX, mOffsetPos,mlengthX;
        int NextEventPercent=30; //30% 지워졌을때 다음가사표시 Event
        if(currentLine<0) return -1;
        if(totalLyricLine <= currentLine) return -2;
        if(LineUpdn < 0)
        {
            return 0;
        }
        earse_point= jniCall.TimeToPosition(currentLine, currentTick);

        if(earse_point < 1)  return 0;

        mOffsetPos=Line_offset[currentLine];
        mlengthX=lineArry.get(currentLine).Line_TextPosB;
        if(mlengthX<=0) return -1;//error
        if(earse_point >= mlengthX) earse_point = mlengthX;//+50;
        mPosX = earse_point+mOffsetPos;
        if(  mPosX <=0 ) mPosX= 0;
        else if(mPosX>1920) mPosX=1920;
        move_dx= mPosX-mOffsetPos;
        move_ratio=(move_dx*100)/mlengthX;
        if(Event_NewLine && (move_ratio>NextEventPercent)) {
            //           IBUtil.Log(TAG, "#@#(xx)Event_NewLine----------------------");
            Event_NewLine=false;
            Event_30P=true;
        }
        if(LineUpdn == 2) mPosX=0;
        return mPosX;
    }

    public void msgSendProcssLyric(int line)
    {
        ProcessLyricAsyncTask plsync = new ProcessLyricAsyncTask();
        plsync.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void SET_LyricFont(String font_path){
        FntPath = font_path;
        textPaint = new Paint();
        ruby_Paint = new Paint();
        text_bounds = new Rect();
        LyricTypeface = Typeface.createFromFile(font_path);
        textPaint.setTypeface(LyricTypeface);
        textPaint.setTextSize(LyricFontSz);
        textPaint.setAntiAlias(true);
        textPaint.setStrokeWidth(FONT_BASE_STOKE);
        ruby_Paint.setTypeface(LyricTypeface);
        ruby_Paint.setTextSize(RubyRubyFontSz);
        ruby_Paint.setAntiAlias(true);
        ruby_Paint.setStrokeWidth(FONT_RUBY_STOKE);
    }

    @Override
    public void release() {
        LyricTypeface = null;
        Line_BlkCnt = null;
        Line_timeA = null;
        Line_timeB = null;
        updn = null;
        sex = null;
        Line_offset = null;
        JULL_Pos = null;
        CountDn_Pos = null;
        CountDn_Bakja = null;

        Bitmap[] bitmaps = {tx_normal_bm0, tx_erase_male_bm0, tx_erase_female_bm0, tx_erase_hap_bm0,
                tx_normal_bm1, tx_erase_male_bm1, tx_erase_female_bm1, tx_erase_hap_bm1,
                newBmp};

        for(Bitmap bitmap : bitmaps){
            if(bitmap != null && !bitmap.isRecycled()){
                bitmap.recycle();
            }
        }
        bitmaps = null;

        if(lineArry != null) {
            for (OneLine line : lineArry) {

                line.release();
            }
            lineArry.clear();
        }

        if(ulist != null)
            ulist.release();

        if(dlist != null)
            dlist.release();

        if(nlist != null)
            nlist.release();

        if(BmStaff != null && !BmStaff.isRecycled())
            BmStaff.recycle();


        if(DispBitmapArray != null){
            for(Bitmap bitmap : DispBitmapArray)
                if(bitmap != null && !bitmap.isRecycled())
                    bitmap.recycle();
            DispBitmapArray = null;
        }

        if(EraseBitmapArray != null){
            for(Bitmap bitmap : EraseBitmapArray)
                if(bitmap != null && !bitmap.isRecycled())
                    bitmap.recycle();
            EraseBitmapArray = null;
        }

        if(aListBitmap != null){
            for(arrBitmap bitmap : aListBitmap)
                bitmap.release();
            aListBitmap.clear();
        }
    }

    class ProcessLyricAsyncTask extends AsyncTask<Integer,Void,Integer> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Integer... params) {
            //System.out.printf("#@####################xxx>>>>>> ProcessLyricAsyncTask START\n");
            ProcessLyric(DispCurrentLine);
            return 0;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Integer s) {
            super.onPostExecute(s);
            //System.out.printf("#@####################xxx>>>>>> ProcessLyricAsyncTask END\n");
        }
    }
}


//나중에 알아서 적당한곳 배치
class Jaso {
    // ㄱ      ㄲ      ㄴ      ㄷ      ㄸ      ㄹ      ㅁ      ㅂ      ㅃ      ㅅ      ㅆ       ㅇ      ㅈ      ㅉ      ㅊ      ㅋ      ㅌ      ㅍ      ㅎ
    //final static char[] ChoSung   = { 0x3131, 0x3132, 0x3134, 0x3137, 0x3138, 0x3139, 0x3141, 0x3142, 0x3143, 0x3145, 0x3146, 0x3147, 0x3148, 0x3149, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e };
    final static String[] ChoSung   = {"g",     "kk",  "n",    "d",     "tt",   "r",    "m",  "b",    "pp",   "s",    "ss",     "",   "j",    "jj",   "ch",   "k",   "t",     "p",   "h"};

    // ㅏ      ㅐ      ㅑ      ㅒ      ㅓ      ㅔ      ㅕ      ㅖ      ㅗ      ㅘ      ㅙ      ㅚ      ㅛ        ㅜ      ㅝ      ㅞ      ㅟ      ㅠ      ㅡ       ㅢ       ㅣ
    // final static char[] JwungSung = { 0x314f, 0x3150, 0x3151, 0x3152, 0x3153, 0x3154, 0x3155, 0x3156, 0x3157, 0x3158, 0x3159, 0x315a, 0x315b, 0x315c, 0x315d, 0x315e, 0x315f, 0x3160, 0x3161, 0x3162, 0x3163 };
    final static String[] JwungSung = { "a",  "ae",  "ya",  "yae",  "eo",    "e",   "yeo",   "ye",  "o",    "wa",   "wae",   "oe",  "yo",   "u",   "wo",   "we",   "wi" ,    "yu",   "eu",  "ui",   "i"  };

    //                  ㄱ        ㄲ      ㄳ      ㄴ      ㄵ      ㄶ      ㄷ       ㄹ      ㄺ      ㄻ      ㄼ      ㄽ      ㄾ       ㄿ      ㅀ       ㅁ      ㅂ      ㅄ      ㅅ      ㅆ       ㅇ      ㅈ      ㅊ       ㅋ       ㅌ      ㅍ      ㅎ
    //   final static char[] JongSung  = { 0,      0x3131, 0x3132, 0x3133, 0x3134, 0x3135, 0x3136, 0x3137, 0x3139, 0x313a, 0x313b, 0x313c, 0x313d, 0x313e, 0x313f, 0x3140, 0x3141, 0x3142, 0x3144, 0x3145, 0x3146, 0x3147, 0x3148, 0x314a, 0x314b, 0x314c, 0x314d, 0x314e };
    final static String[] JongSung  = { "",     "g",      "kk",   "ks", "n",    "nj",   "nh",   "d",     "r",   "lg",   "lm",    "lb",  "ls",   "lt",   "lp",   "lh",   "m",     "b",   "ps",   "s",   "ss",    "ng",     "j",     "c", "k",     "t",     "p",  "h" };

    public static String hangulToJaso(String s) { // 유니코드 한글 문자열을 입력 받음
        int a, b, c; // 자소 버퍼: 초성/중성/종성 순
        String result = "";

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            if (ch >= 0xAC00 && ch <= 0xD7A3) { // "AC00:가" ~ "D7A3:힣" 에 속한 글자면 분해

                c = ch - 0xAC00;
                a = c / (21 * 28);
                c = c % (21 * 28);
                b = c / 28;
                c = c % 28;

                result = result + ChoSung[a] + JwungSung[b];
                if (c != 0) result = result + JongSung[c] ; // c가 0이 아니면, 즉 받침이 있으면
            } else {
                result = result + "";
            }
        }
        return result;
    }

}