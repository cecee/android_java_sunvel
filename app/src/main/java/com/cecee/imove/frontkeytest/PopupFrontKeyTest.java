package com.cecee.imove.frontkeytest;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.cecee.imove.JniFunction;
import com.cecee.imove.MIDIManager;
import com.cecee.imove.R;
import com.cecee.imove.remote.RemoteController;

import java.util.ArrayList;

public class PopupFrontKeyTest extends Dialog {
    protected final String TAG = getClass().getSimpleName();

    private Context m_context;
    private JniFunction m_jni;

    private GridView grid_front1, grid_front2, grid_front3, grid_front4;
    private KeyboardAdapter kbd_adapter1, kbd_adapter2, kbd_adapter3, kbd_adapter4;
    private ArrayList<KeyGridObj> kbd_array1, kbd_array2, kbd_array3, kbd_array4;

    private Button buttonExit, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup_frontkeytest);
        init();
    }

    public PopupFrontKeyTest(Context context, JniFunction jni) {
        super(context , android.R.style.Theme_Translucent_NoTitleBar);
        m_context = context;
        m_jni = jni;
    }

    private void init() {
        buttonExit = (Button) findViewById(R.id.buttonExit);
        buttonReset = (Button) findViewById(R.id.buttonReset);

        kbd_array1 = new ArrayList<KeyGridObj>();
        kbd_array2 = new ArrayList<KeyGridObj>();
        kbd_array3 = new ArrayList<KeyGridObj>();
        kbd_array4 = new ArrayList<KeyGridObj>();

        stateReset();

        grid_front1 = (GridView) findViewById(R.id.grid_front1);
        grid_front2 = (GridView) findViewById(R.id.grid_front2);
        grid_front3 = (GridView) findViewById(R.id.grid_front3);
        grid_front4 = (GridView) findViewById(R.id.grid_front4);

        kbd_adapter1 = new KeyboardAdapter(m_context, R.layout.item_kbd, kbd_array1);
        kbd_adapter2 = new KeyboardAdapter(m_context, R.layout.item_kbd, kbd_array2);
        kbd_adapter3 = new KeyboardAdapter(m_context, R.layout.item_kbd, kbd_array3);
        kbd_adapter4 = new KeyboardAdapter(m_context, R.layout.item_kbd, kbd_array4);

        grid_front1.setAdapter(kbd_adapter1);
        grid_front2.setAdapter(kbd_adapter2);
        grid_front3.setAdapter(kbd_adapter3);
        grid_front4.setAdapter(kbd_adapter4);

        setListener();
    }

    private void setListener() {
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogClose();
            }
        });

        buttonReset.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                stateReset();
                kbd_adapter1.notifyDataSetChanged();
                kbd_adapter2.notifyDataSetChanged();
                kbd_adapter3.notifyDataSetChanged();
                kbd_adapter4.notifyDataSetChanged();
            }
        });

        grid_front1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kbd_array1.get(position).setState(!kbd_array1.get(position).getState());
                kbd_adapter1.notifyDataSetChanged();
            }
        });

        grid_front2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kbd_array2.get(position).setState(!kbd_array2.get(position).getState());
                kbd_adapter2.notifyDataSetChanged();
            }
        });

        grid_front3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kbd_array3.get(position).setState(!kbd_array3.get(position).getState());
                kbd_adapter3.notifyDataSetChanged();
            }
        });

        grid_front4.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                kbd_array4.get(position).setState(!kbd_array4.get(position).getState());
                kbd_adapter4.notifyDataSetChanged();
            }
        });
    }

    private void stateReset() {
        kbd_array1.clear();
        kbd_array2.clear();
        kbd_array3.clear();
        kbd_array4.clear();
        for (String s : m_context.getResources().getStringArray(R.array.kbd_front_left)) {
            kbd_array1.add(new KeyGridObj(s, false));
        }
        for (String s : m_context.getResources().getStringArray(R.array.kbd_front_right)) {
            kbd_array2.add(new KeyGridObj(s, false));
        }
        for (String s : m_context.getResources().getStringArray(R.array.kbd_front_bottom_left)) {
            kbd_array3.add(new KeyGridObj(s, false));
        }
        for (String s : m_context.getResources().getStringArray(R.array.kbd_front_bottom_right)) {
            kbd_array4.add(new KeyGridObj(s, false));
        }
    }

    private void setKeyCheck(ArrayList<KeyGridObj> obj, int pos, KeyboardAdapter adt) {
        try {
            obj.get(pos).setState(!obj.get(pos).getState());
            adt.notifyDataSetChanged();
        } catch (Exception e) { }
    }

    private void dialogClose(){
        Log.d(TAG, "#@#dialogClose()...");
        dismiss();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case RemoteController.KEYCODE_FRONT_TEMPO_UP:
                setKeyCheck(kbd_array1, 0, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_PITCH_UP:
                setKeyCheck(kbd_array1, 1, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_PITCH_MAN:
                setKeyCheck(kbd_array1, 2, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_TEMPO_DOWN:
                setKeyCheck(kbd_array1, 3, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_PITCH_DOWN:
                setKeyCheck(kbd_array1, 4, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_PITCH_FEMALE:
                setKeyCheck(kbd_array1, 5, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_CHANGE_RHYTHM:
                setKeyCheck(kbd_array1, 6, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_NEW_CHART:
                setKeyCheck(kbd_array1, 7, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_MEDLEY:
                setKeyCheck(kbd_array1, 8, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_GANJU_JUMP:
                setKeyCheck(kbd_array1, 9, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_REMOVE_SCORE:
                setKeyCheck(kbd_array1, 10, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_CHART_FAVORITE:
                setKeyCheck(kbd_array1, 11, kbd_adapter1);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_1:
                setKeyCheck(kbd_array2, 0, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_2:
                setKeyCheck(kbd_array2, 1, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_3:
                setKeyCheck(kbd_array2, 2, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_4:
                setKeyCheck(kbd_array2, 3, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_5:
                setKeyCheck(kbd_array2, 4, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_6:
                setKeyCheck(kbd_array2, 5, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_7:
                setKeyCheck(kbd_array2, 6, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_8:
                setKeyCheck(kbd_array2, 7, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_9:
                setKeyCheck(kbd_array2, 8, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_CANCEL:
                setKeyCheck(kbd_array2, 9, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_NUMBER_0:
                setKeyCheck(kbd_array2, 10, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_START:
                setKeyCheck(kbd_array2, 11, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_CANCEL_RESERVATION:
                setKeyCheck(kbd_array2, 12, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_PRE_RESERVATION:
                setKeyCheck(kbd_array2, 13, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_RESERVATION:
                setKeyCheck(kbd_array2, 14, kbd_adapter2);
                break;
            case RemoteController.KEYCODE_FRONT_MUSIC_DOWN:
                setKeyCheck(kbd_array3, 0, kbd_adapter3);
                break;
            case RemoteController.KEYCODE_FRONT_MUSIC_UP:
                setKeyCheck(kbd_array3, 1, kbd_adapter3);
                break;
            case RemoteController.KEYCODE_FRONT_ECHO_DOWN:
                setKeyCheck(kbd_array3, 2, kbd_adapter3);
                break;
            case RemoteController.KEYCODE_FRONT_ECHO_UP:
                setKeyCheck(kbd_array3, 3, kbd_adapter3);
                break;
            case RemoteController.KEYCODE_MODE_DOWN:
                setKeyCheck(kbd_array4, 0, kbd_adapter4);
                break;
            case RemoteController.KEYCODE_MODE_UP:
                setKeyCheck(kbd_array4, 1, kbd_adapter4);
                break;
            case RemoteController.KEYCODE_FRONT_MIC_DOWN:
                setKeyCheck(kbd_array4, 2, kbd_adapter4);
                break;
            case RemoteController.KEYCODE_FRONT_MIC_UP:
                setKeyCheck(kbd_array4, 3, kbd_adapter4);
                break;
        }

        Log.d(TAG, "#@# onKeyDown !!! keyCode:" + keyCode);
        return super.onKeyDown(keyCode, event);
    }
}