package com.cecee.imove.frontkeytest;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.cecee.imove.R;

import java.util.ArrayList;

public class KeyboardAdapter extends ArrayAdapter<KeyGridObj> {
    protected final String TAG = getClass().getSimpleName();
    private Context mContext = null;
    private ArrayList<KeyGridObj> items;
    private int viewId;

    public KeyboardAdapter(Context context, int viewResourceId, ArrayList<KeyGridObj> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.mContext = context;
        this.viewId = viewResourceId;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public KeyGridObj getItem(int position) {
        if (items.size() <= position) {
            return items.get(items.size());
        } else {
            return items.get(position);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        public TextView kbd_text;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(viewId, null);

            holder.kbd_text = (TextView) convertView.findViewById(R.id.kbd_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (items.size() <= position) {
            Log.d(TAG, "#@# KeyboardAdapter index err_" + items.size() + " - " + position);
            return convertView;
        }

        final String p = items.get(position).getName();
        final int pos = position;

        try {
            if (p != null) {
                if (p.equals("*")) {
                    holder.kbd_text.setText("");
                    //holder.kbd_btn.setVisibility(View.INVISIBLE);
                }
                else holder.kbd_text.setText(p);

                if (items.get(pos).getState()) {
                    holder.kbd_text.setTextColor(Color.parseColor("#000000"));
                    holder.kbd_text.setBackgroundColor(Color.parseColor("#05c913"));
                } else {
                    holder.kbd_text.setTextColor(Color.parseColor("#FFFFFF"));
                    holder.kbd_text.setBackgroundColor(Color.parseColor("#6c6c6c"));
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "KeyboardAdapter err_" + e.getMessage());
        }

        return convertView;
    }
}
