package com.cecee.imove.frontkeytest;

public class KeyGridObj {
    private String name;
    private boolean state;

    public KeyGridObj(String name, boolean state) {
        super();
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
