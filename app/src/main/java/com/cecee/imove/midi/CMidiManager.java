package com.cecee.imove.midi;

import android.content.Context;
import android.widget.Toast;

import com.cecee.imove.JniFunction;
import com.cecee.imove.MIDIManager;
import com.cecee.imove.OneLine;
import com.cecee.imove.data.GlobalValue;
import com.danalenter.karaoke.dalkommpartykaraoke.nrpn.NRPNData;

public class CMidiManager {
    protected MIDIManager pMidiManager;
    protected Context context;
    protected String filePath;
    protected int melodyVolume = 110;

    public CMidiManager(Context context){
        init(context);
    }

    void init(Context context){
        this.context = context;
        pMidiManager = new MIDIManager(context);
        pMidiManager.OpenSerial();
        pMidiManager.MIDI_MIC_INIT();
        pMidiManager.setContentsLoad(false);
        pMidiManager.jniCall.JNIThread(1);
    }

    public void resetMIDIStatus(){
        pMidiManager.MIDITempoAdj=0;//곡재생할때 원래 템포로...
    }

    public int getMelodyVolume() {
        return melodyVolume;
    }

    public void setMelodyVolume(int melodyVolume) {
        this.melodyVolume = melodyVolume;
    }

    public void playMIDILoaded(){
        if(pMidiManager.isContentsLoaded()) {
            pMidiManager.MIDIPlay();    // 현재 로드된 미디파일을 플레이한다.
        }
        else
            if(GlobalValue.debug) Toast.makeText(context,"MIDI NOT LOADED!",Toast.LENGTH_LONG).show();
    }

    public boolean loadMIDI(String path, int LyricCode){
        filePath = path;
        pMidiManager.MIDIUnLoad();
        int returnValue = pMidiManager.MIDILoad(filePath, LyricCode,0,0,0,melodyVolume,"C"); // TODO : LyricCode > 0한국  1   2   // MIDILoad의 리컨값이 1이면 성공. 아니면 play하지 말아야함.

        if(returnValue==1){ // success
            System.out.printf("###@@@ LOADED MIDI = [%s]\n", path);
            return true;
        }
        else {  // fail
            return false;
        }
    }

    public boolean playMIDI() {
        if(isContentsLoaded()){
            resetMIDIStatus();    // 플레이전 초기값으로 설정 : 곡템포를 디폴트로 한다든지 등 처리.
            playMIDILoaded();
            return true;
        }
        return false;
    }


    public boolean playMIDI(String path, int LyricCode) {
        if(loadMIDI(path, LyricCode)){ // 로드가 성공하면.
            resetMIDIStatus();    // 플레이전 초기값으로 설정 : 곡템포를 디폴트로 한다든지 등 처리.
            playMIDILoaded();
            return true;
        }
        return false;
    }


    public boolean stopMidi(){
        if(pMidiManager == null) return false;
        return pMidiManager.stopMIDI();
    }

    public boolean isContentsLoaded(){
        if(pMidiManager == null ) return false;
        return pMidiManager.isContentsLoaded();
    }

    public boolean IsMidiLoaded(){
        if(pMidiManager == null ) return false;
        return pMidiManager.isContentsLoaded();
    }

    public boolean IsPause(){
        if(!IsMidiLoaded()) return false;
        return (pMidiManager.MIDIPause_flg == 1);
    }

    public boolean pauseMIDI() {
        if(!IsMidiLoaded()) return false;
        if(IsPause()) return true;
        pMidiManager.MIDIPause_flg = 1;
        pMidiManager.My_MIDIPause(pMidiManager.MIDIPause_flg);
        return true;
    }

    public boolean resumeMIDI(){
        if(!IsMidiLoaded()) return false;
        if(!IsPause()) return true;
        pMidiManager.MIDIPause_flg = 0;
        pMidiManager.My_MIDIPause(pMidiManager.MIDIPause_flg);
        return true;
    }

    public OneLine getLine(int line){
        int totalLine = getTotalLine();
        if(line <0 || line >= totalLine) return null;
        return pMidiManager.getLine(line);
    }

    public int getCurrentTick(){
        return pMidiManager.getCurrentTick();
    }

    public int getTotalLine(){
        if(pMidiManager == null || !pMidiManager.isContentsLoaded()) return 0;
        return pMidiManager.getTotalLyricLine();
    }

    public int getSongEndTick(){
        if(pMidiManager == null || !pMidiManager.isContentsLoaded()) return 0;
        return pMidiManager.getSongEndTick();
    }

    public void madiPrev(){
        pMidiManager.jniCall.madiPrev();
    }

    public void madiNext(){
        pMidiManager.jniCall.madiNext();
    }

    public void MIDISeek(int tick){
        pMidiManager.jniCall.MIDISeek(tick);
    }

    public void changeMelody(int melody){
        pMidiManager.My_MelodyAdj(melody);
    }

    public void changeTempo(int tempo){
        pMidiManager.jniCall.MIDISetSpeed(tempo);
    }

    public void changePitch(int pitch){
        pMidiManager.My_KeyAdj(pitch);
    }

    public MIDIManager getRawMidiManager(){
        return pMidiManager;
    }

    public boolean isFemale(){
        if(pMidiManager == null) return false;
        return pMidiManager.MIDI_SEX == 0;
    }

    public String getMaleKey(){
        if(pMidiManager == null) return "";
        return getKey(pMidiManager.MIDI_MALEKEY);
    }

    public String getFemaleKey(){
        if(pMidiManager == null) return "";
        return getKey(pMidiManager.MIDI_FEMALEKEY);
    }

    public String getKey(){
        if(pMidiManager == null) return "";
        return getKey(pMidiManager.MIDI_DATAKEY);
    }

    public String getMidiInfoString(){
        if(pMidiManager == null) return "";
        return pMidiManager.Str_MIDI_Info;
    }

    public JniFunction getJniCall(){
        if(pMidiManager == null) return null;
        return pMidiManager.jniCall;
    }

    public String getKey(int index){
        String keys[] = {"C","Db","D","Eb","E","F","F#","G","Ab","A","Bb","B","Cm","C#m","Dm","Ebm","Em","Fm","F#m","Gm","Abm","Am","Bbm","Bm" };
        if(index >=0 && index < keys.length)
            return keys[index];
        return "";
    }

    public void callNRPN(NRPNData data) {
        if (pMidiManager.jniCall != null) {
            pMidiManager.jniCall.MIDISetNRPN(data.getAddress(), data.getData());
        }
    }


    public void release(){}
}
