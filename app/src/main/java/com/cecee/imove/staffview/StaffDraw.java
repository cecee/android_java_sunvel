package com.cecee.imove.staffview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.net.ParseException;
import android.util.Log;

import com.cecee.imove.data.FileConfig;

/**
 * Created by Administrator on 2017-07-05.
 */

public class StaffDraw {
    private static final String TAG = "STAFF";
    Canvas canvas;
    Paint mpaint;
    private float[] lines;
    private int lineCnt = 0;
    String FntPath = FileConfig.KoreaFnt;

    public StaffDraw() {
        mpaint = new Paint();
        //  lines=new float[];
    }

    public Bitmap staff_draw(String[] staff_data, String lyricfnt) {
        int i, j;
        String[] StringStaff0 = staff_data;
        Bitmap mStaffBM = null;
        lines = new float[4096];

        FntPath = lyricfnt;

        mStaffBM = Bitmap.createBitmap(1920, 680, Bitmap.Config.ARGB_4444);
        canvas = new Canvas(mStaffBM);
        //mStaffBM.eraseColor(0xC0ffffff);
        mStaffBM.eraseColor(0xffffffff); //악보 불투명 처리

        try {
            //StringStaff0 = Common.m_midi.jniCall.GetStaffStringArray();
            System.out.printf("#@#StringStaff0.length [%d]-------------------\n", StringStaff0.length);
            lineCnt = 0;
            for (i = 0; i < StringStaff0.length; i++) {
                if (DrawSubject(StringStaff0[i]) == 0) continue;
            }
            if (lineCnt > 0) Draw_Lines();
        } catch (Exception e) { }

        return mStaffBM;
    }

    public int DrawSubject(String lstr) {
        int x, y, lengh;
        float x0, x1, y0, y1;
        int color;
        String str;

        if (lstr.contains("End:")) return 0;
        String[] dunk = lstr.split("[|]");  //"|"로 구분
        //System.out.printf("#@#StringStaff0  dunk.length [%d]-----------------\n",dunk.length);
        if (dunk.length < 6) return 0;
        if (dunk[0].contains("Draw_HLine:")) {
            x = ATOI(dunk[1]);
            y = ATOI(dunk[2]);
            lengh = ATOI(dunk[3]);
            int dw = ATOI(dunk[4]);
            color = reverse(ATOI(dunk[5]));
            if (dw != 1) Draw_HLine(x, y, lengh, dw, color);
            else {
                x0 = x;
                y0 = y1 = y;
                x1 = x0 + lengh;
                lines[(lineCnt * 4) + 0] = x0;
                lines[(lineCnt * 4) + 1] = y0;
                lines[(lineCnt * 4) + 2] = x1;
                lines[(lineCnt * 4) + 3] = y1;
                lineCnt++;
            }
        } else if (dunk[0].contains("Draw_VLine:")) {
            x = ATOI(dunk[1]);
            y = ATOI(dunk[2]);
            lengh = ATOI(dunk[3]);
            int dw = ATOI(dunk[4]);
            color = reverse(ATOI(dunk[5]));
            if (dw != 1) Draw_VLine(x, y, lengh, dw, color);
            else {
                x0 = x1 = x;
                y0 = y;
                y1 = y + lengh;
                lines[(lineCnt * 4) + 0] = x0;
                lines[(lineCnt * 4) + 1] = y0;
                lines[(lineCnt * 4) + 2] = x1;
                lines[(lineCnt * 4) + 3] = y1;
                lineCnt++;
            }
        } else if (dunk[0].contains("Draw_UTF:")) {
            String kind = dunk[1];
            x = ATOI(dunk[2]);
            y = ATOI(dunk[3]);
            str = dunk[4];
            color = reverse(ATOI(dunk[5]));
            Draw_UTF(kind, x, y, str, color);
        } else if (dunk[0].contains("Draw_LYR:")) {
            String kind = dunk[1];
            x = ATOI(dunk[2]);
            y = ATOI(dunk[3]);
            str = dunk[4];
            color = reverse(ATOI(dunk[5]));
            Draw_LYR(kind, x, y, str, color);
        } else if (dunk[0].contains("Draw_Arc:")) {
            int updn, fr;
            x = ATOI(dunk[1]);
            y = ATOI(dunk[2]);
            lengh = ATOI(dunk[3]);
            int kind = ATOI(dunk[4]);
            color = reverse(ATOI(dunk[5]));
            if (kind >= 100) {
                updn = 1;
                fr = kind - 100;
            } else {
                updn = 0;
                fr = kind % 3;
            }
            Draw_Arc(x, y, lengh, updn, fr, color);
        }

        return 1;
    }

//    public static int ATOI(String sTmp) {
//        String tTmp = "0", cTmp = "";
//        sTmp = sTmp.trim();
//        for (int i = 0; i < sTmp.length(); i++) {
//            cTmp = sTmp.substring(i, i + 1);
//            if (cTmp.equals("0") || cTmp.equals("1") || cTmp.equals("2") ||
//                    cTmp.equals("3") || cTmp.equals("4") ||  cTmp.equals("5") ||
//                    cTmp.equals("6") || cTmp.equals("7") ||  cTmp.equals("8") ||
//                    cTmp.equals("9")) tTmp += cTmp;
//            else if (cTmp.equals("-") && i == 0)
//                tTmp = "-";
//            else
//                break;
//        }
//        return (Integer.parseInt(tTmp));
//    }

    public static boolean isParsable(String input) {
        boolean parsable = true;
        try {
            Integer.parseInt(input);
        } catch (ParseException e) {
            parsable = false;
        } catch (Exception e) {
            parsable = false;
        }
        return parsable;
    }

    public int ATOI(String sTmp) {
        int val = 0;
        if (isParsable(sTmp)) val = Integer.parseInt(sTmp);
        return val;
    }

    public void Draw_Lines() {
        float[] marray;
        marray = new float[lineCnt * 4];
        if (lineCnt > 0) {
            try {
                System.arraycopy(lines, 0, marray, 0, lineCnt * 4);
            } catch (ArrayIndexOutOfBoundsException e) {
                Log.d(TAG, "#@#Draw_Lines__ArrayIndexOutOfBoundsException_" + e.getMessage());
            } catch (IndexOutOfBoundsException e) {
                Log.d(TAG, "#@#Draw_Lines__IndexOutOfBoundsException_" + e.getMessage());
            } catch (Exception | Error e) {
                Log.d(TAG, "#@#Draw_Lines__Exception_" + e.getMessage());
            }
        }
        mpaint.setColor(Color.BLACK);
        mpaint.setStrokeWidth(1);
        canvas.drawLines(marray, mpaint);
    }

    public void Draw_Arc(int x0, int y0, int length, int updn, int fr, int color) {
        mpaint.setColor(color);
        mpaint.setStrokeWidth(1);
        mpaint.setStyle(Paint.Style.STROKE);
        //Log.d(TAG, "#@####--------Draw_Arc------x0-"+x0 +"  y0-"+y0+"  x1-"+length +"  updn="+updn +"   fr="+fr);
        int hight = 10;
        if (length > 8) length = length - 8;
        if (length < 24) hight = 8;
        //y0=y0-18;
        RectF rect = new RectF();
        rect.set(x0, y0, x0 + length, y0 + hight);
        switch (fr) {
            case 0:
                if (updn == 0) {
                    canvas.drawArc(rect, 90, 90, false, mpaint);
                } else {
                    canvas.drawArc(rect, 180, 90, false, mpaint);
                }
                break;
            case 1:
                if (updn == 0) {
                    canvas.drawArc(rect, 0, 90, false, mpaint);
                } else {
                    canvas.drawArc(rect, 270, 90, false, mpaint);
                }
                break;
            case 2:
                if (updn == 0) {
                    canvas.drawArc(rect, 0, 180, false, mpaint);
                } else {
                    canvas.drawArc(rect, 180, 180, false, mpaint);
                }
                break;
        }
    }

    public void Draw_HLine(int x, int y, int length, int dw, int color) {
        mpaint.setColor(color);
        mpaint.setStrokeWidth(dw);
        canvas.drawLine(x, y, x + length, y, mpaint);
    }

    public void Draw_VLine(int x, int y, int length, int dw, int color) {
        mpaint.setColor(color);
        mpaint.setStrokeWidth(dw);
        canvas.drawLine(x, y, x, y + length, mpaint);
    }

    public void Draw_UTF(String fkind, int x, int y, String str, int color) {
        Typeface mtypeface;
        char k = fkind.charAt(0);
        String ssz = fkind.substring(1);
        int sz = Integer.parseInt(ssz);
        if (k == 'M') {
            mtypeface = IBTypefaces.get(FileConfig.MusFnt);
        } else {
            mtypeface = IBTypefaces.get(FntPath);
        }
        // xpaint= new Paint();
        mpaint.setTypeface(mtypeface);
        mpaint.setTextSize(sz);
        mpaint.setAntiAlias(true);
        mpaint.setStrokeWidth(2);
        mpaint.setColor(color);
        mpaint.setStyle(Paint.Style.FILL);
        canvas.drawText(str, x, y, mpaint);

    }

    public void Draw_LYR(String fkind, int x, int y, String str, int color) {
        Typeface mtypeface;
        int incolor, outcolor;
        char k = fkind.charAt(0);
        String ssz = fkind.substring(1);
        int sz = Integer.parseInt(ssz);
        if (k == 'M') {
            //mtypeface = Typeface.createFromFile(Common.m_midi.MusFnt);
            mtypeface = IBTypefaces.get(FileConfig.MusFnt);
        } else {
            //mtypeface = Typeface.createFromFile(Common.m_midi.FntPath);
            mtypeface = IBTypefaces.get(FntPath);
        }
        switch (color) {
            case 1://    1=4/4 color
                incolor = Color.RED;
                outcolor = Color.WHITE;
                break;
            case 2://    alto/tenor color
                incolor = Color.RED;
                outcolor = Color.WHITE;
                break;

            default: //악보의 가사 칼라
                incolor = Color.WHITE;
                outcolor = Color.BLUE;
                break;

        }
        //xpaint= new Paint();
        mpaint.setTypeface(mtypeface);
        mpaint.setTextSize(sz);
        mpaint.setAntiAlias(true);
        mpaint.setStrokeWidth(4);
        mpaint.setColor(outcolor);
        mpaint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawText(str, x, y, mpaint);
        mpaint.setColor(incolor);
        mpaint.setStyle(Paint.Style.FILL);
        canvas.drawText(str, x, y, mpaint);
    }

    public int reverse(int a) {
        int alfa = 0x0FF & a;
        int value = (a >>= 8) & 0x00ffffff;
        int r = (alfa << 24) | value;
        return r;
    }

}
