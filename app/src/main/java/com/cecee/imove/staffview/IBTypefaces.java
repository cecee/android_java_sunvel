package com.cecee.imove.staffview;

import android.graphics.Typeface;
import android.util.Log;

import java.util.Hashtable;

/**
 * Created by shootdol-tw on 2018-03-29.
 */

public class IBTypefaces {
    private static final String TAG = "IBTypefaces";

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    public static Typeface get(String name) {
        synchronized (cache) {
            try {
                if (!cache.containsKey(name)) {
                    Typeface t = Typeface.createFromFile(name);
                    cache.put(name, t);
                    //IBUtil.Log(TAG, "Typeface FONT PUSH!!!!__" + name);
                }
            } catch (Exception e) {
                Log.d(TAG, "Could not get typeface '" + name + "' because " + e.getMessage());
                return null;
            }

            //IBUtil.Log(TAG, "Typeface FONT GET!!!!__" + name);
            return (Typeface) cache.get(name);
        }
    }
}