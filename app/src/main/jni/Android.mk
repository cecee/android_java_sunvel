LOCAL_PATH := $(call my-dir)

# tinyalsa lib
include $(CLEAR_VARS)

#LOCAL_SRC_FILES := ../../Class/libs/libtinyalsa.so
#include $(PREBUILT_SHARED_LIBRARY)

#include $(CLEAR_VARS)
LOCAL_MODULE := libJnilib

LOCAL_SRC_FILES := ../../Class/jniMain.cpp \
                    ../../Class/lxMIDIData/lxMIDIData.c \
                    ../../Class/lxMusicScore/draw_score.cpp\
                    ../../Class/lxMusicScore/music_score.cpp\
                    ../../Class/midi_main.cpp \
                    ../../Class/midi_document.cpp \
                    ../../Class/midi_timer.cpp \
                    ../../Class/Doc_cnw.cpp \
                    ../../Class/coco.cpp \
                    ../../Class/front.cpp

LCOAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_C_INCLUDES += ../../Class
LOCAL_C_INCLUDES += ../../Class/lxMIDIData
LOCAL_C_INCLUDES += ../../Class/lxMusicScore

LOCAL_ARM_NEON := true
LOCAL_ARM_MODE := arm

LOCAL_CPPFLAGS += -std=c++1z -fno-stack-protector -g
#LOCAL_CPPFLAGS += -std=c++17 -fno-stack-protector -g
LOCAL_CPP_FEATURES := rtti exceptions

LOCAL_LDLIBS := -ldl -lGLESv1_CM -lGLESv2 -llog -landroid
#LOCAL_SHARED_LIBRARIES := tinyalsa
include $(BUILD_SHARED_LIBRARY)

#LOCAL_MODULE := tinyalsa